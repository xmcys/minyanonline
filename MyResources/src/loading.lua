cc.FileUtils:getInstance():addSearchPath("MyResources/src")
cc.FileUtils:getInstance():addSearchPath("MyResources/res")
require "cocos.init"
require "src/testResource"
-- cclog
cclog = function(...)
    --print(string.format(...))
end

-- for CCLuaEngine traceback
function __G__TRACKBACK__(msg)
    cclog("----------------------------------------")
    cclog("LUA ERROR: " .. tostring(msg) .. "\n")
    cclog(debug.traceback())
    cclog("----------------------------------------")
end

ALERT_VIEW_TAG = 2829
-- global lua bridge for use
luabridge = nil
userID = "1"
userCode = "23456"
url = '27.123.232.22/ws-service'
termType = 2 -- 2@apple,3@android,4@winphone,5@blackberry,6@windows,7@linux,8@mac,9@kali
className = 'TransitController'
userType = 1
actId = "000000000"

local function main()
    
    -- varible use for local
    local activity_name=""
    local aid = 10000001
    local search_path_parent = "MyResources/"
    local cdn_http_prefix = "http://kaeltomato.sinaapp.com/"
    local activity_name = "二货传奇"
    
    collectgarbage("collect")
    -- avoid memory leak
    collectgarbage("setpause", 100)
    local valstep = collectgarbage("setstepmul", 1000)

    local targetPlatform = cc.Application:getInstance():getTargetPlatform()
    -- lua function for outter call
    local function sUid(u)
    	if (cc.PLATFORM_OS_IPHONE == targetPlatform) or (cc.PLATFORM_OS_IPAD == targetPlatform) then
        	userID = u.uid
        else 
        	if(cc.PLATFORM_OS_ANDROID == targetPlatform) then
        		userID = u
        	end
        end
    end
    
    local function sUtype(u)
    	if (cc.PLATFORM_OS_IPHONE == targetPlatform) or (cc.PLATFORM_OS_IPAD == targetPlatform) then
            -- because logic use compare as number
        	userType = tonumber(u.utype)
        else 
        	if(cc.PLATFORM_OS_ANDROID == targetPlatform) then
        		userType = tonumber(u)
        	end
        end
    end

    local function sAid(u)
        if (cc.PLATFORM_OS_IPHONE == targetPlatform) or (cc.PLATFORM_OS_IPAD == targetPlatform) then
        	actId = u.aid
			aid = u.aid
        else 
        	if(cc.PLATFORM_OS_ANDROID == targetPlatform) then
        		actId = u
				aid = u
        	end
        end
        print("current aid is:"..actId)
    end

    local function sUcode(u)
    	if (cc.PLATFORM_OS_IPHONE == targetPlatform) or (cc.PLATFORM_OS_IPAD == targetPlatform) then
        	userCode = u.ucode
        else 
        	if(cc.PLATFORM_OS_ANDROID == targetPlatform) then
        		userCode = u
        	end
        end
    end
    
    local function sTermType(u)
    	if (cc.PLATFORM_OS_IPHONE == targetPlatform) or (cc.PLATFORM_OS_IPAD == targetPlatform) then
        	termType = u.tt
        else 
        	if(cc.PLATFORM_OS_ANDROID == targetPlatform) then
        		termType = u
        	end
        end
    end
    
    local function sServerURL(u)
    	if (cc.PLATFORM_OS_IPHONE == targetPlatform) or (cc.PLATFORM_OS_IPAD == targetPlatform) then
            url = u.surl
        else 
        	if(cc.PLATFORM_OS_ANDROID == targetPlatform) then
        		url = u
        	end
        end
    end

    local function sCdnURL(u)
    	if (cc.PLATFORM_OS_IPHONE == targetPlatform) or (cc.PLATFORM_OS_IPAD == targetPlatform) then
        	cdn_http_prefix = u.curl
        else 
        	if(cc.PLATFORM_OS_ANDROID == targetPlatform) then
        		cdn_http_prefix = u
        	end
        end
    end
    
    local function sActivityName(u)
    	if (cc.PLATFORM_OS_IPHONE == targetPlatform) or (cc.PLATFORM_OS_IPAD == targetPlatform) then
        	activity_name = u.an
        else 
        	if(cc.PLATFORM_OS_ANDROID == targetPlatform) then
        		activity_name = u
        	end
        end
    end
    -- lua function for outter call end


    -- lua bridge staff
    -- if java,need luaj


    if (cc.PLATFORM_OS_IPHONE == targetPlatform) or (cc.PLATFORM_OS_IPAD == targetPlatform) then
        luabridge = require "cocos.cocos2d.luaoc"
        luabridge.callStaticMethod(className, "registeScriptHandler", {setUid = sUid,setUcode = sUcode,setAid = sAid,setTermType = sTermType,setServerURL = sServerURL,setCdnURL = sCdnURL,setActivityName = sActivityName,setUsertype = sUtype})
    else 
        if(cc.PLATFORM_OS_ANDROID == targetPlatform) then
        luabridge = require "cocos.cocos2d.luaj"
	    className = "org/cocos2dx/lua/"..className
	    luabridge.callStaticMethod(className, "registeScriptHandler", {sUid,sUcode,sAid,sTermType,sServerURL,sCdnURL,sActivityName,sUtype})  -- sCdnURL, sUtype, sServerURL, are the functionID ??
        end
    end

    -- luabridge staff end


    -- loading search path
    cc.FileUtils:getInstance():addSearchPath(search_path_parent.."src");
    cc.FileUtils:getInstance():addSearchPath(search_path_parent.."res");

    local visibleSize = cc.Director:getInstance():getVisibleSize()
    local origin = cc.Director:getInstance():getVisibleOrigin()

    local schedulerEntry = nil
    local amgr = nil
    local labelGet = nil
    local sceneLoading = nil
    -- start check version
    local function checkLatestVersion()
        cc.Director:getInstance():getScheduler():unscheduleScriptEntry(schedulerEntry)
        amgr:update()
    end

    local function backToAppLua()
        -- release resource
        amgr:release()
        cc.Director:getInstance():getScheduler():unscheduleScriptEntry(schedulerEntry)
        -- if used,should xcall clean httpclient,actionmanagerex and so on
        luabridge.callStaticMethod(className,"backToApp")
    end

    local function onGameErrorLua()
        labelGet:setVisible(true)
        sceneLoading:removeChildByTag(ALERT_VIEW_TAG)
        schedulerEntry = cc.Director:getInstance():getScheduler():scheduleScriptFunc(backToAppLua, 1.6, false)
    end

    -- yes and no function
    local function onYesChoose(sender)
        -- dissmiss alert view and start update
        schedulerEntry = cc.Director:getInstance():getScheduler():scheduleScriptFunc(checkLatestVersion, 0.6, false)
        labelGet:setVisible(true)
        sceneLoading:removeChildByTag(ALERT_VIEW_TAG)
    end
    -- yes and no function end
    
    local function onNoChoose(sender)
        --labelGet:setString("it should be back to app view")
        onGameErrorLua()
    end

    -- init base scene first
    local function createAlertSimple(info,buttons)
        labelGet:setVisible(false)
        local labelmsg = cc.LabelTTF:create(info, s_arialPath, 26,cc.size(visibleSize.width,60),
                                     cc.TEXT_ALIGNMENT_CENTER)
        labelmsg:setAnchorPoint(cc.p(0.5, 0.5))
        labelmsg:setColor(cc.c3b(0,0,0))

        local label1 = cc.LabelTTF:create("是", s_arialPath, 50)
        label1:setAnchorPoint(cc.p(0.5, 0.5))
        label1:setColor(cc.c3b(0,0,0))
        local label2 = cc.LabelTTF:create("否", s_arialPath, 50)
        label2:setAnchorPoint(cc.p(0.5, 0.5))
        label2:setColor(cc.c3b(0,0,0))

        local  item3 = cc.MenuItemLabel:create(label1)
        item3:registerScriptTapHandler(onYesChoose)
        item3:setDisabledColor( cc.c3b(32,32,64) )
        --item3:setColor( cc.c3b(0,0,0) )
        
        local  item4 = cc.MenuItemLabel:create(label2)
        item4:registerScriptTapHandler(onNoChoose)
        item4:setDisabledColor( cc.c3b(32,32,64) )
        --item4:setColor( cc.c3b(0,0,0) )

        local menu = cc.Menu:create()
        menu:addChild(item3)
        if buttons > 1 then
            menu:addChild(item4)
        end
        menu:alignItemsHorizontallyWithPadding(36)
        --menu:alignItemsHorizontally()
        menu:setPosition(0,-labelmsg:getContentSize().height)
        local lay = cc.Layer:create()
        lay:addChild(labelmsg)
        lay:addChild(menu)
        lay:setPosition(visibleSize.width/2+origin.x,visibleSize.height/2+origin.y)
        lay:setTag(ALERT_VIEW_TAG)
    	return lay
    end
    local function createLayer()
    	local layerFarm = cc.LayerColor:create(cc.c4b(238,238,238,255))
        --layerFarm:setAnchorPoint(0.5,0.5)
        labelGet = cc.LabelTTF:create("开始检测最新版本", s_arialPath, 26)
        labelGet:setColor(cc.c3b(0,0,0))
        labelGet:setPosition(visibleSize.width/2+origin.x,visibleSize.height/2+origin.y)
    	layerFarm:addChild(labelGet)
    	-- need add origin because scale policy will change visible size depend on policy
        layerFarm:setPosition(0,0)
    	--layerFarm:setPosition(visibleSize.width/2+origin.x,visibleSize.height/2+origin.y)
    	return layerFarm
    end
    -- scene function
    local function changeScene(nextScene)
        if cc.Director:getInstance():getRunningScene() then
            cc.Director:getInstance():replaceScene(nextScene)
        else
            cc.Director:getInstance():runWithScene(nextScene)
        end
    end

    local function reloadModule( moduleName )
        return require(moduleName)
    end

    sceneLoading = cc.Scene:create()
    sceneLoading:addChild(createLayer())
    changeScene(sceneLoading)
    -- scene staff end

    local network_status = 0
    local stt2 = ccextra.CCNetwork:getInternetConnectionStatus()

    -- git hub wrong,use local nginx server for test right now
    --local http_prefix = "http://127.0.0.1/"
    --local http_prefix = "http://kaeltomato.sinaapp.com/"
    local version = cdn_http_prefix .. "version" .. aid
    local zip = cdn_http_prefix .. "package"..aid..".zip"

    -- asset manager start
    local pathToSave = ""
    pathToSave = createDownloadDirEx(aid)
    
    -- sub activity search path
    --print("kael log:"..pathToSave)
    -- if you do not use file name directly,add this,such as call require game10000001.lua
    -- will go lua_path find,then cocos call cocos2dx_lua_loader find his own search path for lua buffer
    -- and running lua_begin_path will be app sandbox ,such as me have Resource/src,Resource/res,Resource
    -- is just a group,in sandbox you will find src & res folder,and in Document will have package/src & res
    -- asset manager will add search path 'pathToSave' after download,so if you want use this path first,
    -- you should add it first,if you just use it after download,do not need do it here,
    -- p.s add pathToSave.."/src" so that you can require(something) direct in your lua file,not src.something
    -- make sure you use the right file path in lua file,i have package/src/game.lua require(function) if you
    -- should have searchpath pathToSave .. "/src" before,if you require(src.function) you should have 
    -- searchpath pathToSave added before
    
    -- all resource will work in writeable document
    --cc.FileUtils:getInstance():addSearchPath(pathToSave);
    cc.FileUtils:getInstance():addSearchPath(pathToSave.."/src");
    cc.FileUtils:getInstance():addSearchPath(pathToSave.."/res");
    print(pathToSave)
    local function onNewVersionOk()
        print("1323")
		package.loaded["game10000001"] = nil
        local sceneGame = require("game10000001")--..aid)
        print("123")
        changeScene(sceneGame)
        print("333")
        -- when update over,set this varible to true for new state
        amgr.needCheck = true
    end

    local function onError(errorCode)
        if errorCode == cc.ASSETSMANAGER_NO_NEW_VERSION then
            local ver = amgr:getVersion()
            if ver ~= "" then
                labelGet:setString("no new version")
                onNewVersionOk()
            else
                labelGet:setString("version code error")
                onGameErrorLua()
            end
        elseif errorCode == cc.ASSETSMANAGER_NETWORK then
            labelGet:setString("网络错误")
            -- when error comes,call function back to app,if need arguments,add back,here not argument for now
            onGameErrorLua()
        end
    end

    local function onProgress( percent )
        local progress = string.format("下载中 %d%%",percent)
        labelGet:setString(progress)
    end

    local function onSuccess()
        labelGet:setString("下载成功")
        --deleteDownloadDir(pathToSave)
        onNewVersionOk()
    end

    -- function define
    local function getAssetsManager()
        if nil == assetsManager then
            assetsManager = cc.AssetsManager:new(zip,version,
                                           pathToSave)
            assetsManager:retain()
            assetsManager:setDelegate(onError, cc.ASSETSMANAGER_PROTOCOL_ERROR )
            assetsManager:setDelegate(onProgress, cc.ASSETSMANAGER_PROTOCOL_PROGRESS)
            assetsManager:setDelegate(onSuccess, cc.ASSETSMANAGER_PROTOCOL_SUCCESS )
            assetsManager:setConnectionTimeout(6)
        end
        return assetsManager
    end
    
    -- asset manager
    -- assetmanager staff initialize
    amgr = getAssetsManager()
    --local ret = cc.UserDefault:getInstance():getStringForKey("current-version-code")
    local ret = amgr:getVersion()
    if ret == "" then
        if stt2 == 0 then
            labelGet:setString("网络暂时不可用")
            onGameErrorLua()
        else
            if stt2 == 1 then
                sceneLoading:addChild(createAlertSimple("您还未安装"..activity_name..",点击确认开始下载",1))
            else
                if stt2 == 2 then
                    sceneLoading:addChild(createAlertSimple("您还未安装"..activity_name..",检测到您当前WLAN还未打开,是否继续下载?",2))
                end
            end
        end
    else
        -- view status 1@no new version,2@got new version
        -- userdefault.xml will be delete when app deleted,so every time reinstall app
        -- need recheck again
        -- check version first,if need update,check the network is wifi or wan
        local beok = amgr:checkUpdate()
        -- if be ok is false,is init curl failed or something else(no new version,network error),will change scene and not need do below
        if beok then
            -- test network first
            local network_status = 0
            --local stt2 = ccextra.CCNetwork:getInternetConnectionStatus()
            if stt2 == 0 then
                labelGet:setString("网络暂时不可用")
                onGameErrorLua()
            else
                if stt2 == 1 then
                    network_status = 1
                    --labelGet:setString("kael said network is wifi")
                    sceneLoading:addChild(createAlertSimple("有新版本的"..activity_name..",点击确认开始下载",1))
                else
                    if stt2 == 2 then
                        network_status = 2
                        --labelGet:setString("kael said network is wan")
                        sceneLoading:addChild(createAlertSimple("有新版本的"..activity_name..",检测到您当前WLAN还未打开,是否继续下载?",2))
                    end
                end
            end
        end

        -- asset manager end
    end

    -- test code switch directly
    --onNewVersionOk()
end

local status, msg = xpcall(main, __G__TRACKBACK__)
if not status then
    error(msg)
end
