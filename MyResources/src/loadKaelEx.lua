require "Cocos2d"
require "Cocos2dConstants"
require "src/testResource"
-- cclog
cclog = function(...)
    print(string.format(...))
end

-- for CCLuaEngine traceback
function __G__TRACKBACK__(msg)
    cclog("----------------------------------------")
    cclog("LUA ERROR: " .. tostring(msg) .. "\n")
    cclog(debug.traceback())
    cclog("----------------------------------------")
end

-- global varibles you want pass in
uid = 1
aid = 10000001
ucode = 2

-- change this for local appversion check first,if you post to appstore for a new
-- version,this will change to your latest version so user do not need download again
-- this is resource version code for this game
-- i change cocos2dx's code in static std::string keyWithHash( const char* prefix, const std::string& url )
-- do not need hashcode for version key right now
appversion = "deadbeef"
-- current client side 's latest server resource version for directory rule
latestversion = ""

local function main()
    collectgarbage("collect")
    -- avoid memory leak
    collectgarbage("setpause", 100)
    -- memory step need be setting
    local valstep = collectgarbage("setstepmul", 1000)

    -- setters for global var
    local function sUid(u)
        uid = u.uid
    end

    local function sAid(u)
        aid = u.aid
        print("current aid is:"..aid)
    end

    local function sUcode(u)
        ucode = u.uc
    end

     -- todo lua bridge,if win32/linux,need c++ call
    local targetPlatform = cc.Application:getInstance():getTargetPlatform()
    local luabridge
    if (cc.PLATFORM_OS_IPHONE == targetPlatform) or (cc.PLATFORM_OS_IPAD == targetPlatform) then
        luabridge = require "luaoc"
    else 
        if(cc.PLATFORM_OS_ANDROID == targetPlatform) then
            luabridge = require "luaj"
        end
    end
    if luabridge ~= nil then
        luabridge.callStaticMethod("RootViewController", "registeScriptHandler", {setUid = sUid,setUcode = sUcode,setAid = sAid})
    end

    -- loading search path for loading usage
    cc.FileUtils:getInstance():addSearchPath(appversion .. "/src");
    cc.FileUtils:getInstance():addSearchPath(appversion .. "/res");

    local visibleSize = cc.Director:getInstance():getVisibleSize()
    local origin = cc.Director:getInstance():getVisibleOrigin()
    
    -- situations
    --[[
        1.app have only loading.lua and no other resource,download by compare version code,
          and appversion should be empty string
        2.app have updated and in appstore are full resource pack,do not need update again,
          and appversion should be latest resource version
        3.app have old version resources,update,and appversion should be latest resource version
    ]]--

    -- if not have user default,and appversion not empty,use appversion for current version
    local current_client_version = cc.UserDefault:getInstance():getStringForKey("current-version-code")
    if appversion ~= "" then
        -- cocos2dx default version key code
        if current_client_version == "" then
            -- if do not have user default yet,use appversion for now
            cc.UserDefault:getInstance():setStringForKey("current-version-code",appversion)
            cc.UserDefault:getInstance():flush()
            latestversion = appversion
        else
            -- if have user default,do not change any thing and set latest version version to recored version
            latestversion = current_client_version
        end
    end


    -- assetmanager staff initialize
    
    -- git hub wrong,use local nginx server for test right now
    local http_prefix = "http://127.0.0.1/"
    local version = http_prefix .. "version"
    
    local zip = http_prefix .. "package"..aid..".zip"
    
    local labelGet = cc.Label:createWithTTF("start check latest version:", s_arialPath, 22)
    -- asset manager start
    local pathToSave = ""
    pathToSave = createDownloadDirEx(aid)

    -- sub activity search path

     -- scene function
    local function changeScene(nextScene)
        if cc.Director:getInstance():getRunningScene() then
            cc.Director:getInstance():replaceScene(nextScene)
        else
            cc.Director:getInstance():runWithScene(nextScene)
        end
    end

    local function onNewVersionOk()
        local ret = cc.UserDefault:getInstance():getStringForKey("current-version-code")
        print("current version:"..ret)

        local sceneGame = require("game"..aid)
        changeScene(sceneGame)
    end

    local function onError(errorCode)
        if errorCode == cc.ASSETSMANAGER_NO_NEW_VERSION then
            labelGet:setString("no new version")
            onNewVersionOk()
        elseif errorCode == cc.ASSETSMANAGER_NETWORK then
            labelGet:setString("network error")
            -- need do after some seconds later or show a button let it reload?
        end
    end

    local function onProgress( percent )
        local progress = string.format("downloading %d%%",percent)
        labelGet:setString(progress)
    end

    local function onSuccess()
        labelGet:setString("downloading ok")
        --deleteDownloadDir(pathToSave)
        onNewVersionOk()
    end

    local function getAssetsManager()
        if nil == assetsManager then
            assetsManager = cc.AssetsManager:new(zip,version,
                                           pathToSave)
            assetsManager:retain()
            assetsManager:setDelegate(onError, cc.ASSETSMANAGER_PROTOCOL_ERROR )
            assetsManager:setDelegate(onProgress, cc.ASSETSMANAGER_PROTOCOL_PROGRESS)
            assetsManager:setDelegate(onSuccess, cc.ASSETSMANAGER_PROTOCOL_SUCCESS )
            assetsManager:setConnectionTimeout(6)
        end
        return assetsManager
    end
    
    local function reloadModule( moduleName )
        return require(moduleName)
    end

    -- asset manager end

    local function createLayer()
        local layerFarm = cc.Layer:create()
        layerFarm:addChild(labelGet)
        -- need add origin because scale policy will change visible size depend on policy
        layerFarm:setPosition(visibleSize.width/2+origin.x,visibleSize.height/2+origin.y)
        return layerFarm
    end
    
    -- check version and make     

    local sceneLoading = cc.Scene:create()
    sceneLoading:addChild(createLayer())

    changeScene(sceneLoading)
    
    local schedulerEntry = nil
    -- start check version
    local function checkLatestVersion()
        cc.Director:getInstance():getScheduler():unscheduleScriptEntry(schedulerEntry)
        getAssetsManager():update()

    end
    
    -- delay run,let view show first
    schedulerEntry = cc.Director:getInstance():getScheduler():scheduleScriptFunc(checkLatestVersion, 1.0, false)

    -- test switch directly
    --onNewVersionOk()
end


xpcall(main, __G__TRACKBACK__)
