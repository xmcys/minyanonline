//
//  CustServiceDetailController.m
//  CmAppCustIos
//
//  Created by Arcesaka on 14/8/25.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustServiceDetailController.h"
#import "CustServiceListController.h"

#define BTN_COMPLETE 1
#define BTN_TRACK    2
#define BTN_ALL      3

@interface CustServiceDetailController ()

@property (nonatomic, strong) IBOutlet UIView   * header;
@property (nonatomic, strong) IBOutlet UILabel  * managerlab;
@property (nonatomic, strong) IBOutlet UILabel  * driverlab;
@property (nonatomic, strong) IBOutlet UILabel  * deliverlab;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btn1x;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btn2x;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btn3x;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btnWidth;

- (IBAction)completeClick:(id)sender;
- (IBAction)trackClick:(id)sender;
- (IBAction)allClick:(id)sender;

- (void)updateViewCons;
- (void)autoArrangeBoxWithConstraints:(NSArray *)constraintArray with:(CGFloat)swidth;

@end

@implementation CustServiceDetailController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"服务受理";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)updateViewCons
{
    [self autoArrangeBoxWithConstraints:@[self.btn1x,
                                          self.btn2x,
                                          self.btn3x] with:self.btnWidth.constant];
}

- (void)autoArrangeBoxWithConstraints:(NSArray *)constraintArray with:(CGFloat)swidth
{
    CGFloat step = (self.view.frame.size.width - (swidth *constraintArray.count)) / (constraintArray.count + 1);
    for (int i = 0; i < constraintArray.count; i ++) {
        NSLayoutConstraint *constraint = constraintArray[i];
        constraint.constant = step * (i + 1) + swidth * i;
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.header.frame = CGRectMake(0, 0, self.view.frame.size.width, self.header.frame.size.height);
    [self.view addSubview:self.header];
    [self updateViewCons];
}

- (IBAction)completeClick:(id)sender
{
    CustServiceListController *controller = [[CustServiceListController alloc] initWithNibName:@"CustServiceListController" bundle:nil];
    controller.tag = BTN_COMPLETE;
    [self.navigationController pushViewController:controller animated:YES];
}
- (IBAction)trackClick:(id)sender
{
    CustServiceListController *controller = [[CustServiceListController alloc] initWithNibName:@"CustServiceListController" bundle:nil];
    controller.tag = BTN_TRACK;
    [self.navigationController pushViewController:controller animated:YES];
}
- (IBAction)allClick:(id)sender
{
    CustServiceListController *controller = [[CustServiceListController alloc] initWithNibName:@"CustServiceListController" bundle:nil];
    controller.tag = BTN_ALL;
    [self.navigationController pushViewController:controller animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
