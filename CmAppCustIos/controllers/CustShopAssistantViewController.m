//
//  CustShopAssistantViewController.m
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/18.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustShopAssistantViewController.h"
#import "CTTableView.h"
#import "CustMyShopViewController.h"
#import "CustSaleProfViewController.h"
#import "CustNewIvtViewController.h"
#import "CustModule.h"
#import "CustWebService.h"
#import "CustSumer.h"
#import "CustBiView.h"

static NSString * const SaleSortCellIdentifier = @"SaleSortCell";

@interface CustShopAssistantViewController () <UITableViewDataSource, UITableViewDelegate, CTTableViewDelegate, UIScrollViewDelegate, CTURLConnectionDelegate>

@property (strong, nonatomic) CTTableView *tv;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UILabel *labSaleAmountTotal;  // 销售汇总
@property (strong, nonatomic) IBOutlet UILabel *labProfit;           // 经营毛利
@property (strong, nonatomic) IBOutlet UILabel *labStock;            // 最新库存
@property (strong, nonatomic) IBOutlet UIView *dotView;              // 虚线
@property (strong, nonatomic) NSMutableArray *array;                 // 销售排行数据

- (IBAction)controlMyShopOnClick:(id)sender;                         // 我的店铺点击
- (IBAction)dateSegChange:(UISegmentedControl *)sender;              // 日期选择
- (IBAction)saleTotalCLick:(id)sender;                               // 销售汇总
- (IBAction)profitTotalClick:(id)sender;                             // 毛利汇总
- (IBAction)newIvt:(id)sender;                                       // 最新库存
- (IBAction)typeChange:(UISegmentedControl *)sender;                 // 卷烟、非烟类选择

- (void)loadData;

@end

@implementation CustShopAssistantViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setTabBarItemWithTitle:@"店铺助手" titleColor:[UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1] titleSelectedColor:[UIColor colorWithRed:4.0/255.0 green:168.0/255.0 blue:117.0/255.0 alpha:1] image:[UIImage imageNamed:@"CustTabShopNormal"] selectedImage:[UIImage imageNamed:@"CustTabShopSelected"]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.dotView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Dot"]];
    self.array = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_DPZS, CUST_MODULE_DPZS, nil]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.title = @"店铺助手";
    
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.tv.pullUpViewEnabled = NO;
        self.tv.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        [self.tv setTableHeaderView:self.headerView];
        [self.tv registerNib:[UINib nibWithNibName:@"CustSalesSortCell" bundle:nil] forCellReuseIdentifier:SaleSortCellIdentifier];
        self.tv.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.tv];
        
        [self loadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

# pragma http requests
- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"加载中...";
    [self.indicator show];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getCustBiView:[CustWebService sharedUser].userCode dateType:@"d"] delegate:self];
    [conn start];
}

- (void)loadDataSuccess
{
    [self.array removeAllObjects];
    [self.array addObjectsFromArray:@[@[@"1", @"11 * 11", @"1111", @"11%"], @[@"2", @"22 * 22", @"2222", @"22%"], @[@"3", @"33 * 33", @"3333", @"33%"], @[@"4", @"44 * 44", @"4444", @"44%"]]];
    [self.indicator hide];
    [self.tv reloadData];
    return;
}

# pragma button click events
- (IBAction)controlMyShopOnClick:(id)sender
{
    CustMyShopViewController *myShop = [[CustMyShopViewController alloc] initWithNibName:@"CustMyShopViewController" bundle:nil];
    [self.navigationController pushViewController:myShop animated:YES];
    return;
}

- (IBAction)dateSegChange:(UISegmentedControl *)sender
{
//    NSInteger i = sender.selectedSegmentIndex;
    return;
}

- (IBAction)saleTotalCLick:(id)sender {
    CustSaleProfViewController *spf = [[CustSaleProfViewController alloc] initWithNibName:nil bundle:nil];
    [spf setSTatusType:StatusTypeSale];
    [self.navigationController pushViewController:spf animated:YES];
}
- (IBAction)profitTotalClick:(id)sender {
    CustSaleProfViewController *spf = [[CustSaleProfViewController alloc] initWithNibName:nil bundle:nil];
    [spf setSTatusType:StatusTypeProf];
    [self.navigationController pushViewController:spf animated:YES];
}
- (IBAction)newIvt:(id)sender {
    CustNewIvtViewController *ivt = [[CustNewIvtViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:ivt animated:YES];
}

- (IBAction)typeChange:(UISegmentedControl *)sender
{
    return;
}

# pragma mark - UIScrollView delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}
#pragma mark UITableView Delegate & DataSource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    if (self.indicator.showing) {
        return NO;
    }
    [self loadData];
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 41.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:SaleSortCellIdentifier forIndexPath:indexPath];
    
    // 背景色
    UIView *contentView = cell.contentView;
    if (indexPath.row % 2 == 0) {
        contentView.backgroundColor = XCOLOR(245, 245, 245, 1);
    } else {
        contentView.backgroundColor = XCOLOR(238, 236, 239, 1);
    }
    
    NSArray *dataArray = [self.array objectAtIndex:indexPath.row];
    // 排名
    UILabel *labRange = (UILabel *)[cell viewWithTag:1];
    labRange.text = dataArray[0];
    // 规格
    UILabel *labGuiGe = (UILabel *)[cell viewWithTag:2];
    labGuiGe.text = dataArray[1];
    // 销售金额
    UILabel *labAmount = (UILabel *)[cell viewWithTag:3];
    labAmount.text = dataArray[2];
    // 占比
    UILabel *labRate = (UILabel *)[cell viewWithTag:4];
    labRate.text = dataArray[3];;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.indicator.text = @"加载失败";
    [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    CustBiView *bi = [[CustBiView alloc] init];
    [bi parseData:data complete:^(CustBiView *biView) {
        [self.indicator hide];
        self.labSaleAmountTotal.text = biView.amtSale;
        self.labProfit.text = biView.amtProfit;
        self.labStock.text = @"";
    }];
}
@end
