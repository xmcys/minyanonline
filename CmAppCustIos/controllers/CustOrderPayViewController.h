//
//  CustOrderPayViewController.h
//  CmAppCustIos
//
//  Created by hsit on 14-7-31.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@interface CustOrderPayViewController : CTViewController

@property (nonatomic, strong) NSString *orderId;

@end
