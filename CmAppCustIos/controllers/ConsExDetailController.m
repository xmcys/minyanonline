//
//  ConsExDetailController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-10.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsExDetailController.h"
#import "ConsExOrderController.h"
#import "CTImageView.h"
#import "CTScrollView.h"
#import "ConsGoods.h"
#import "CustWebService.h"
#import "CustExchangeGift.h"
#import "CustSumPoints.h"

# define PROCESS_POINTSCHECK    1

@interface ConsExDetailController () <CTScrollViewDelegate>

@property (nonatomic, strong) IBOutlet CTScrollView * container;
@property (nonatomic, strong) IBOutlet CTImageView * ivIcon;
@property (nonatomic, strong) IBOutlet UILabel * name;
@property (nonatomic, strong) IBOutlet UILabel * price;
@property (nonatomic, strong) IBOutlet UILabel * point;
@property (nonatomic, strong) IBOutlet UILabel * labSupplierName;
@property (nonatomic, strong) IBOutlet UILabel * labDes;

- (void)submit;
- (void)showDetail;

@end

@implementation ConsExDetailController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"商品详情";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton * rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.backgroundColor = [UIColor clearColor];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"BtnSubmit"] forState:UIControlStateNormal];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"BtnSubmit"] forState:UIControlStateHighlighted];
    rightBtn.showsTouchWhenHighlighted = YES;
    [rightBtn addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn sizeToFit];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    
    self.container.backgroundColor = [UIColor clearColor];
    self.container.ctScrollViewDelegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self showDetail];
}

- (void)showDetail
{
    if (self.gift != nil) {
        self.ivIcon.url = [CustWebService fileFullUrl:self.gift.giftPic];
        [self.ivIcon loadImage];
        self.name.text = self.gift.giftName;
        self.price.text = [NSString stringWithFormat:@"%@元", self.gift.advisePrice];
        self.point.text = self.gift.changePoint;
        self.labSupplierName.text = self.gift.supplierName;
        self.labDes.text = self.gift.giftDesc;
    }
}

- (void)submit
{
    if (self.indicator.showing) {
        return;
    }
    
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    // 验证积分是否足够
    NSString * url = [CustWebService getTotalPointsUrl];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:url delegate:self];
    conn.tag = PROCESS_POINTSCHECK;
    [conn start];
}

#pragma mark -
#pragma mark CTScrollView Delegate
- (BOOL)ctScrollViewDidPullDownToRefresh:(CTScrollView *)ctScrollView
{
    return NO;
}

- (BOOL)ctScrollViewDidPullUpToLoad:(CTScrollView *)ctScrollView
{
    return NO;
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    CustSumPoints *sumPointsParser = [[CustSumPoints alloc] init];
    [sumPointsParser parseData:data complete:^(CustSumPoints *sumPoints) {
        // 积分不足
        if ([sumPoints.sumPoints integerValue] < [self.gift.changePoint integerValue]) {
            self.indicator.text = [NSString stringWithFormat:@"您当前可用积分余额为%@，不足以兑换此商品。", sumPoints.sumPoints];
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        } else {
            [self.indicator hide];
            ConsExOrderController * controller = [[ConsExOrderController alloc] initWithNibName:@"ConsExOrderController" bundle:nil];
            controller.gift = self.gift;
            controller.myPoints = sumPoints.sumPoints;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }];
    return;
}

@end
