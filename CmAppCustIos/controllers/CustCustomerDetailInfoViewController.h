//
//  RYKCustomerDetailInfoViewController.h
//  RYKForCustIos
//
//  Created by hsit on 14-6-23.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTViewController.h"

@interface CustCustomerDetailInfoViewController : CTViewController

- (id)initWithId:(NSString *)vipId;

@end
