//
//  RYKAddPublicViewController.h
//  RYKForCustIos
//
//  Created by hsit on 14-6-26.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

@protocol CustRefreshPubDelegate <NSObject>

@optional

- (void)refreshPubDelegate:(BOOL)Refresh;

@end

#import "CTViewController.h"
#import "CustPubMsg.h"

@interface CustAddPublicViewController : CTViewController

@property (nonatomic) NSInteger tag;
@property (nonatomic, strong) NSString *msgId;
@property (nonatomic, weak) id<CustRefreshPubDelegate>delegate;

- (id)initWithMsgId:(NSString *)msgId;

@end
