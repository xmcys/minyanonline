//
//  ConsBrandEvaListController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-25.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsBrandEvaListController.h"
#import "CTRatingView.h"
#import "CustWebService.h"
#import "ConsBrand.h"
#import <QuartzCore/QuartzCore.h>
#import "CTTableView.h"
#import "ConsBrandEvaController.h"

#define PROCESS_LIST 1
#define PROCESS_SUM 2
@interface ConsBrandEvaListController () <CTTableViewDelegate, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, ConsBrandEvaControllerDelegate>

@property (nonatomic, strong) IBOutlet UIView * header;  // 总分视图
@property (nonatomic, strong) IBOutlet UIView * sumBg;  // 总分背景
@property (nonatomic, strong) NSMutableArray * evaArray;
@property (nonatomic, strong) CTTableView * tv;


- (void)showSummary;
- (void)loadData;
- (void)showEvaController;

@end

@implementation ConsBrandEvaListController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"卷烟标题";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton * rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.backgroundColor = [UIColor clearColor];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"BtnEva"] forState:UIControlStateNormal];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"BtnEva"] forState:UIControlStateHighlighted];
    rightBtn.showsTouchWhenHighlighted = YES;
    [rightBtn addTarget:self action:@selector(showEvaController) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn sizeToFit];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    
    self.sumBg.layer.cornerRadius = 5.0;
    self.evaArray = [[NSMutableArray alloc] init];
    
    UILabel * tasteLabel = (UILabel *)[self.header viewWithTag:101];
    tasteLabel.text = @"口味：";
    UILabel * pkgLabel = (UILabel *)[self.header viewWithTag:102];
    pkgLabel.text =   @"外观：";
    UILabel * priceLabel = (UILabel *)[self.header viewWithTag:103];
    priceLabel.text = @"性价比：";
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CONS_MODULE_JYPJ, CONS_MODULE_JYPJ, nil]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tv.pullUpViewEnabled = NO;
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        self.tv.separatorColor = [UIColor colorWithRed:209.0/255.0 green:209.0/255.0  blue:209.0/255.0  alpha:1];
        self.header.frame = CGRectMake(0, 0, self.tv.frame.size.width, 96);
        [self.tv setTableHeaderView:self.header];
        [self.view addSubview:self.tv];
        
        [self loadData];
    }
    
    self.title = self.brand.brandName;
}

- (void)showSummary
{
    UILabel * times = (UILabel *)[self.header viewWithTag:3];
    times.text = [NSString stringWithFormat:@"一共%@条评论", self.brand.surverNum];
    
    // 总分
    UILabel * totalScoreLabel = (UILabel *)[self.header viewWithTag:1];
    totalScoreLabel.text = [NSString stringWithFormat:@"%@分", self.brand.tTotalScore];
    CTRatingView * totalScore = (CTRatingView *)[self.header viewWithTag:2];
    totalScore.rate = [self.brand.tTotalScore doubleValue];
    totalScore.ratingViewEnabled = NO;
    
    // 口味
    
    CTRatingView * tasteScore = (CTRatingView *)[self.header viewWithTag:4];
    tasteScore.rate = [self.brand.tTasteScore doubleValue];
    tasteScore.ratingViewEnabled = NO;
    
    // 外观
    
    CTRatingView * pkgScore = (CTRatingView *)[self.header viewWithTag:5];
    pkgScore.rate = [self.brand.tPackageScore doubleValue];
    pkgScore.ratingViewEnabled = NO;
    
    // 性价比
    
    CTRatingView * priceScore = (CTRatingView *)[self.header viewWithTag:6];
    priceScore.rate = [self.brand.tPriceScore doubleValue];
    priceScore.ratingViewEnabled = NO;
}

- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    
    self.indicator.text = @"请稍后..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService brandEvaSummaryUrl:self.brand.brandId] delegate:self];
    conn.tag = PROCESS_SUM;
    [conn start];
}

- (void)showEvaController
{
    if (![CustWebService sharedUser].isLogin) {
        self.indicator.text = @"您尚未登录";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0 complete:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFCATION_NOT_LOGIN object:nil];
        }];
        return;
    }
    
    ConsBrandEvaController * controller = [[ConsBrandEvaController alloc] initWithNibName:nil bundle:nil];
    controller.delegate = self;
    controller.brand = self.brand;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark CTTableView DataSource & Delegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadData];
    return YES;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 81;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.evaArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"SHOP_CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSArray* elements = [[NSBundle mainBundle] loadNibNamed:@"ConsCustEvaCell" owner:nil options:nil];
        
        cell = [elements objectAtIndex:0];
//        cell.frame = CGRectMake(0, 0, self.tv.frame.size.width, 80);
    }
    
    ConsBrand * item = [self.evaArray objectAtIndex:indexPath.row];
    
    UILabel * name = (UILabel *)[cell viewWithTag:1];
    UILabel * time = (UILabel *)[cell viewWithTag:2];
    CTRatingView * rate = (CTRatingView *)[cell viewWithTag:3];
    UILabel * score = (UILabel *)[cell viewWithTag:4];
    name.text = item.userName;
    time.text = item.surverTime;
    rate.rate = [item.tTotalScore doubleValue];
    rate.ratingViewEnabled = NO;
    score.text = [NSString stringWithFormat:@"%@分", item.tTotalScore];
    
    return cell;
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.tv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_SUM) {
        ConsBrand * item = [[ConsBrand alloc] init];
        [item parseData:data complete:^(NSArray * array){
            ConsBrand * cc = [array objectAtIndex:0];
            self.brand.tTotalScore = cc.tTotalScore;
            self.brand.tTasteScore = cc.tTasteScore;
            self.brand.tPackageScore = cc.tPackageScore;
            self.brand.tPriceScore = cc.tPriceScore;
            self.brand.surverNum = cc.surverNum;
            
            [self showSummary];
            
            CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService brandEvaListUrl:self.brand.brandId] delegate:self];
            conn.tag = PROCESS_LIST;
            [conn start];
        }];
    } else {
        ConsBrand * item = [[ConsBrand alloc] init];
        [item parseData:data complete:^(NSArray * array){
            [self.evaArray removeAllObjects];
            [self.evaArray addObjectsFromArray:array];
            
            [self.tv reloadData];
            [self.indicator hide];
        }];
    }
}

#pragma mark -
#pragma mark ConsBrandEvaController Delegate
- (void)consBrandEvaControllerDidEvaluted:(ConsBrandEvaController *)controller
{
    [self loadData];
}

@end
