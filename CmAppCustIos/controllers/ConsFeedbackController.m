//
//  ConsFeedbackController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-28.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsFeedbackController.h"
#import "ConsPikcer.h"
#import "CustWebService.h"
#import "CustModule.h"
#import "CTScrollView.h"
#import "CustSystemMsg.h"
#import <QuartzCore/QuartzCore.h>
#import "CustModule.h"

#define PROCESS_LOAD_DATA 1
#define PROCESS_SUBMIT 2

@interface ConsFeedbackController () <UITextViewDelegate, CTScrollViewDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) CTScrollView * container;
@property (nonatomic, strong) ConsPikcer * picker;
@property (nonatomic, strong) UITextView * textView;

- (void)loadData;
- (void)submit;

@end

@implementation ConsFeedbackController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"意见反馈";
    }
    return self;
}

- (void)viewDidLoad {
    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_KHFW, CUST_MODULE_OPERATE_YJFK, nil]];
}

- (void)initUI
{
	UIButton * rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.backgroundColor = [UIColor clearColor];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"BtnSubmit"] forState:UIControlStateNormal];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"BtnSubmit"] forState:UIControlStateHighlighted];
    rightBtn.showsTouchWhenHighlighted = YES;
    [rightBtn addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn sizeToFit];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CONS_MODULE_YJFK, CONS_MODULE_YJFK, nil]];
    
    self.container = [[CTScrollView alloc] initWithFrame:self.view.bounds];
    self.container.contentSize = CGSizeMake(self.view.frame.size.width, self.view.bounds.size.height + 1);
    self.container.pullUpViewEnabled = NO;
    self.container.pullDownViewEnabled = YES;
    self.container.backgroundColor = [UIColor clearColor];
    self.container.ctScrollViewDelegate = self;
    [self.view addSubview:self.container];
    
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(10, 60, self.view.bounds.size.width - 20, 100)];
    self.textView.text = @"请输入内容";
    self.textView.font = [UIFont systemFontOfSize:14.0f];
    self.textView.textColor = [UIColor blackColor];
    self.textView.delegate = self;
    self.textView.layer.cornerRadius = 5.0;
    self.textView.layer.borderWidth = 0.5;
    self.textView.layer.borderColor = [[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:1] CGColor];
    [self.container addSubview:self.textView];
    
    self.picker = [[ConsPikcer alloc] initWithFrame:CGRectMake(80, 0, self.view.bounds.size.width - 80, 50)];
    [self.picker setTitle:@"无模块列表"];
    [self.picker setTitle:@"无模块列表"];
    
    [self.container addSubview:self.picker];
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 50)];
    label.backgroundColor = [UIColor colorWithRed:211.0/255.0 green:225.0/255.0 blue:214.0/255.0 alpha:1];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:15.0f];
    label.textColor = [UIColor blackColor];
    label.text = @"选择模块";
    [self.container addSubview:label];
    
    [self loadData];
}


- (void)loadData
{
    if (![CustWebService sharedUser].isLogin) {
        
        self.indicator.text = @"您尚未登录";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:NOT_LOGIN_NOTICE_INTERVAL complete:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFCATION_NOT_LOGIN object:nil];
        }];
        return;
    }
    
    if (self.indicator.showing){
        return;
    }
    
    self.indicator.text = @"获取模块列表..";
    self.indicator.backgroundTouchable = NO;
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService consModuleListUrl] delegate:self];
    conn.tag = PROCESS_LOAD_DATA;
    [conn start];
}

- (void)submit
{
    [self.picker hide];
    [self.textView resignFirstResponder];
    
    if (self.indicator.showing) {
        return;
    }
    
    if (self.picker.itemArray.count == 0) {
        self.indicator.text = @"无模块列表";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    
    NSString * content = [self.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    PickerItem * item = [self.picker.itemArray objectAtIndex:self.picker.selectedIndex];

    if (content.length == 0 || [content isEqualToString:@"请输入内容"]) {
        self.indicator.text = @"请输入内容描述";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    
    self.indicator.text = @"正在提交..";
    self.indicator.backgroundTouchable = NO;
    [self.indicator show];
    
    NSString * params = [NSString stringWithFormat:@"<CmappFeedback><userCode>%@</userCode><moduleCode>%@</moduleCode><userName>%@</userName><feedContent>%@</feedContent></CmappFeedback>", [CustWebService sharedUser].userCode, item.key, [CustWebService sharedUser].realName, content];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService submitFeedbackUrl] params:params delegate:self];
    conn.tag = PROCESS_SUBMIT;
    [conn start];
}

#pragma mark -
#pragma mark CTScrollView Delegate
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    [self.container ctScrollViewDidScroll];
//}
//
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//    [self.container ctScrollViewDidEndDragging];
//}

- (BOOL)ctScrollViewDidPullDownToRefresh:(CTScrollView *)ctScrollView
{
    [self loadData];
    return YES;
}

#pragma mark -
#pragma mark UITextView Delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"请输入内容"]) {
        textView.text = @"";
    }
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
    }
    return YES;
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"请求失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.container reload];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_LOAD_DATA) {
        CustModule * module = [[CustModule alloc] init];
        [module parseData:data complete:^(NSArray * array){
            NSMutableArray * itemArray = [[NSMutableArray alloc] init];
            for (int i = 0; i < array.count; i++) {
                PickerItem * item = [[PickerItem alloc] init];
                CustModule * cm = [array objectAtIndex:i];
                item.key = cm.code;
                item.value = cm.name;
                [itemArray addObject:item];
            }
//            PickerItem * item = [[PickerItem alloc] init];
//            item.key = @"-1";
//            item.value = @"其他";
//            [itemArray addObject:item];
            self.picker.itemArray = itemArray;
            self.picker.selectedIndex = 0;
//            [self.picker reloadData];
            [self.indicator hide];
            [self.container reload];
        }];
    } else {
        CustSystemMsg * msg = [[CustSystemMsg alloc] init];
        [msg parseData:data];
        if ([msg.code isEqualToString:@"1"]) {
            [self.indicator hide];
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"系统提示" message:@"反馈内容提交成功！" delegate:self cancelButtonTitle:@"好" otherButtonTitles: nil];
            [alert show];
        } else {
            self.indicator.text = msg.msg;
            [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
        }
    }
}

#pragma mark -
#pragma mark UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
