//
//  CustOrderAddController.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-21.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@class CustOrderAddController;

@protocol CustOrderAddControllerDelegate <NSObject>

@optional
- (void)custOrderAddControllerDidBrandAdded:(CustOrderAddController *)controller;

@end

@interface CustOrderAddController : CTViewController

@property (nonatomic, weak) id<CustOrderAddControllerDelegate> delegate;

@end
