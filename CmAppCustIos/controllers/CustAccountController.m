//
//  CustAccountController.m
//  CmAppCustIos
//
//  Created by Chenhc on 15/3/23.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustAccountController.h"
#import "CustInfo.h"
#import "CustWebService.h"
#import "ConsWebController.h"

@interface CustAccountController ()

@property (weak, nonatomic) IBOutlet UILabel *custLicenceCode;
@property (weak, nonatomic) IBOutlet UILabel *custName;
@property (weak, nonatomic) IBOutlet UILabel *custLevel;
@property (weak, nonatomic) IBOutlet UILabel *balMode;
@property (weak, nonatomic) IBOutlet UILabel *custOrderDate;
@property (weak, nonatomic) IBOutlet UILabel *custMgrName;
@property (weak, nonatomic) IBOutlet UIButton *custMgrMb;
@property (weak, nonatomic) IBOutlet UILabel *regiePersonName;
@property (weak, nonatomic) IBOutlet UIButton *regiePersonMb;
@property (weak, nonatomic) IBOutlet UIButton *distPersonMb;
@property (weak, nonatomic) IBOutlet UILabel *distPersonName;
@property (weak, nonatomic) IBOutlet UIButton *custLevelDetail;

- (IBAction)makePhoneCall:(UIButton *)sender;
- (IBAction)showCustLevel:(id)sender;
- (void)showDetail:(CustInfo *)info;

@end

@implementation CustAccountController

- (void)initUI
{
    self.title = @"帐号信息";
    self.indicator.text = @"加载中...";
    [self.indicator show];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService custAccountInfoUrl] delegate:self];
    conn.delegate = self;
    [conn start];
    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_CBL, CUST_MODULE_OPERATE_ZHXX, nil]];
}

- (IBAction)makePhoneCall:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", sender.titleLabel.text]]];
}

- (IBAction)showCustLevel:(id)sender
{
    ConsWebController * webController = [[ConsWebController alloc] init];
    webController.mainUrl = [CustWebService getCustLevelUrl];
    [self.navigationController pushViewController:webController animated:YES];
}

- (void)showDetail:(CustInfo *)info
{
    self.custLicenceCode.text = info.custLicenceCode;
    self.custName.text = info.custName;
    self.custLevel.text = info.custLevelName;
    self.balMode.text = info.balMode;
    if ([info.custOrderDate isEqualToString:@"1"]) {
        self.custOrderDate.text = @"星期一";
    } else if ([info.custOrderDate isEqualToString:@"2"]){
        self.custOrderDate.text = @"星期二";
    } else if ([info.custOrderDate isEqualToString:@"3"]){
        self.custOrderDate.text = @"星期三";
    } else if ([info.custOrderDate isEqualToString:@"4"]){
        self.custOrderDate.text = @"星期四";
    } else if ([info.custOrderDate isEqualToString:@"5"]){
        self.custOrderDate.text = @"星期五";
    } else {
       self.custOrderDate.text = info.custOrderDate;
    }
    
    self.custMgrName.text = info.custMgrName;
    self.regiePersonName.text = info.regiePersonName;
    self.distPersonName.text = info.distPersonName;
    [self.custLevelDetail setAttributedTitle:[[NSAttributedString alloc]initWithString:@"详情" attributes:@{NSUnderlineStyleAttributeName:[NSNumber numberWithInt:1]}] forState:UIControlStateNormal];
    [self.custLevelDetail setAttributedTitle:[[NSAttributedString alloc]initWithString:@"详情" attributes:@{NSUnderlineStyleAttributeName:[NSNumber numberWithInt:1]}] forState:UIControlStateHighlighted];
    [self.custMgrMb setAttributedTitle:[[NSAttributedString alloc]initWithString:info.mgrMb attributes:@{NSUnderlineStyleAttributeName:[NSNumber numberWithInt:1]}] forState:UIControlStateNormal];
    [self.custMgrMb setAttributedTitle:[[NSAttributedString alloc]initWithString:info.mgrMb attributes:@{NSUnderlineStyleAttributeName:[NSNumber numberWithInt:1]}] forState:UIControlStateHighlighted];
    [self.regiePersonMb setAttributedTitle:[[NSAttributedString alloc]initWithString:info.regieMb attributes:@{NSUnderlineStyleAttributeName:[NSNumber numberWithInt:1]}] forState:UIControlStateNormal];
    [self.regiePersonMb setAttributedTitle:[[NSAttributedString alloc]initWithString:info.regieMb attributes:@{NSUnderlineStyleAttributeName:[NSNumber numberWithInt:1]}] forState:UIControlStateHighlighted];
    [self.distPersonMb setAttributedTitle:[[NSAttributedString alloc]initWithString:info.distMb attributes:@{NSUnderlineStyleAttributeName:[NSNumber numberWithInt:1]}] forState:UIControlStateNormal];
    [self.distPersonMb setAttributedTitle:[[NSAttributedString alloc]initWithString:info.distMb attributes:@{NSUnderlineStyleAttributeName:[NSNumber numberWithInt:1]}] forState:UIControlStateHighlighted];
}

#pragma mark -
#pragma mark CTURLConnection Delegate -
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    CustInfo * info = [[CustInfo alloc] init];
    [info parseData:data];
    [self showDetail:info];
    [self.indicator hide];
}
@end
