//
//  CustMyPointRecordsViewController.h
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/24.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@class CustMyPointsDetail;

@interface CustMyPointRecordsViewController : CTViewController

@property (nonatomic, weak) CustMyPointsDetail *curPoint;

@end
