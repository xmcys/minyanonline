//
//  ConsAdController.h
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-29.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@class ConsAdController;

@protocol ConsAdControllerDelegate <NSObject>

@optional
- (void)consAdControllerDidDismiss:(ConsAdController *)controller;

@end

@interface ConsAdController : CTViewController

@property (nonatomic, weak) id<ConsAdControllerDelegate> delegate;
@property (nonatomic) BOOL isAd;

@end
