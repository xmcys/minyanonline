//
//  RYKRecordersofGoodsViewController.h
//  RYKForCustIos
//
//  Created by hsit on 14-6-24.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTViewController.h"



@interface CustRecordersofGoodsViewController : CTViewController

@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, strong) NSString *orderDate;

- (id)initWithOrder:(NSString *)orderId;



@end
