//
//  CustMyPointRecordsViewController.m
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/24.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustMyPointRecordsViewController.h"
#import "CTTableView.h"
#import "CustMyPointRecordsDetail.h"
#import "CustMyPointsDetail.h"
#import "CustWebService.h"

static NSString * const MyPointRecordCellIdentifier = @"MyPointRecordCell";

@interface CustMyPointRecordsViewController () <CTTableViewDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>

@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) NSMutableArray *array;

- (void)loadData;

@end

@implementation CustMyPointRecordsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"积分记录";
    self.array = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:CGRectMake(0, self.headerView.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height-self.headerView.frame.size.height)];
        self.tv.pullUpViewEnabled = NO;
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        [self.tv registerNib:[UINib nibWithNibName:@"CustMyPointRecordCell" bundle:nil] forCellReuseIdentifier:MyPointRecordCellIdentifier];
        [self.view addSubview:self.tv];
        [self loadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma  mark - http requests
- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中...";
    [self.indicator show];
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getPointRecordsDetailUrl:self.curPoint.supplierCode] delegate:self];
    [conn start];
    return;
}

//下拉刷新
#pragma  mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark - CTTableView Delegate *DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 41;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:MyPointRecordCellIdentifier forIndexPath:indexPath];
    
    // 背景色
    UIView *contentView = cell.contentView;
    if (indexPath.row % 2 == 0) {
        contentView.backgroundColor = XCOLOR(245, 245, 245, 1);
    } else {
        contentView.backgroundColor = XCOLOR(238, 236, 239, 1);
    }
    
    CustMyPointRecordsDetail *curRecord = [self.array objectAtIndex:indexPath.row];
    // 积分日期
    UILabel *labDate = (UILabel *)[cell viewWithTag:1];
    labDate.text = curRecord.pointDate;
    // 备注
    UILabel *labDes = (UILabel *)[cell viewWithTag:2];
    labDes.text = [NSString stringWithFormat:@"备注：%@", curRecord.pointDesc];
    // 增加积分
    UILabel *labAddPoint = (UILabel *)[cell viewWithTag:3];
    labAddPoint.text = curRecord.addPoint;
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    if (self.indicator.showing) {
        return NO;
    }
    [self loadData];
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
#pragma mark - CTURLConnection Delegate

- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
    [self.tv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    CustMyPointRecordsDetail *recordParser = [[CustMyPointRecordsDetail alloc] init];
    [recordParser parseData:data complete:^(NSArray *array) {
        [self.indicator hide];
        [self.array removeAllObjects];
        [self.array addObjectsFromArray:array];
        [self.tv reloadData];
    }];
    return;
}

@end
