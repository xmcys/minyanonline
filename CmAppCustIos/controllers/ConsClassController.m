//
//  ConsClassController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-1.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsClassController.h"
#import "ConsClassView.h"
#import "ConsMsgDetailController.h"
#import "ConsClass.h"
#import "ConsClassTradeMarkView.h"
#import "ConsSupplier.h"
#import "ConsSupplierDetailController.h"

@interface ConsClassController () <ConsClassViewDelegate, ConsClassTradeMarkViewDelegate>

@property (nonatomic, strong) IBOutlet UIView * topTab;             // 顶部tab栏
@property (nonatomic, strong) IBOutlet UIButton * tabFun;           // 烟趣
@property (nonatomic, strong) IBOutlet UIButton * tabDistinguish;   // 真伪鉴别
@property (nonatomic, strong) IBOutlet UIButton * tabTradeMark;     // 品牌百科
@property (nonatomic, strong) ConsClassView * funView;           // 烟趣视图
@property (nonatomic, strong) ConsClassView * distinguishView;   // 真伪鉴别
@property (nonatomic, strong) ConsClassTradeMarkView * tradeMarkView; // 品牌百科

- (IBAction)onTabChanged:(UIButton *)btn;

@end

@implementation ConsClassController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setTabBarItemWithTitle:@"资讯中心" titleColor:[UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1] titleSelectedColor:[UIColor colorWithRed:0.0/255.0 green:118.0/255.0 blue:68.0/255.0 alpha:1] image:[UIImage imageNamed:@"CustTabInfoNormal"] selectedImage:[UIImage imageNamed:@"CustTabInfoSelected"]];
    }
    return self;
}

- (void)onTabChanged:(UIButton *)btn
{
    UIImage * img = [[UIImage imageNamed:@"ClassTabSelected"] resizableImageWithCapInsets:UIEdgeInsetsMake(7, 5, 7, 5)];
    [self.tabFun setBackgroundImage:nil forState:UIControlStateNormal];
    [self.tabFun setBackgroundImage:nil forState:UIControlStateHighlighted];
    [self.tabDistinguish setBackgroundImage:nil forState:UIControlStateNormal];
    [self.tabDistinguish setBackgroundImage:nil forState:UIControlStateHighlighted];
    [self.tabTradeMark setBackgroundImage:nil forState:UIControlStateNormal];
    [self.tabTradeMark setBackgroundImage:nil forState:UIControlStateHighlighted];
    [self.tabFun setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.tabFun setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateHighlighted];
    [self.tabDistinguish setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.tabDistinguish setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateHighlighted];
    [self.tabTradeMark setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.tabTradeMark setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateHighlighted];
    self.funView.hidden = YES;
    self.distinguishView.hidden = YES;
    self.tradeMarkView.hidden = YES;
    
    [btn setBackgroundImage:img forState:UIControlStateNormal];
    [btn setBackgroundImage:img forState:UIControlStateHighlighted];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    if (btn == self.tabFun) {
        [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_ZXZX, CUST_MODULE_OPERATE_LSHJT, nil]];
        self.funView.hidden = NO;
    } else if (btn == self.tabDistinguish){
        [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_ZXZX, CUST_MODULE_OPERATE_JYZD, nil]];
        self.distinguishView.hidden = NO;
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_ZXZX, CUST_MODULE_OPERATE_PPBK, nil]];
        self.tradeMarkView.hidden = NO;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.title = @"资讯中心";
    self.topTab.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ClassTabBg"]];
    
    if (self.funView == nil) {
        self.funView = [[ConsClassView alloc] initWithFrame:CGRectMake(0, self.topTab.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height - self.topTab.frame.size.height)];
        self.funView.delegate = self;
        self.funView.showType = ClassCustTeach;
        [self.view addSubview:self.funView];
        
        [self.funView loadData];
    }
    
    if (self.distinguishView == nil) {
        self.distinguishView = [[ConsClassView alloc] initWithFrame:CGRectMake(0, self.topTab.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height - self.topTab.frame.size.height)];
        self.distinguishView.delegate = self;
        self.distinguishView.showType = ClassOperateGuide;
        [self.view addSubview:self.distinguishView];
        
        [self.distinguishView loadData];
    }
    
    if (self.tradeMarkView == nil) {
        self.tradeMarkView = [[ConsClassTradeMarkView alloc] initWithFrame:CGRectMake(0, self.topTab.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height - self.topTab.frame.size.height)];
        self.tradeMarkView.delegate = self;
        [self.view addSubview:self.tradeMarkView];
        [self.tradeMarkView loadData];
        [self onTabChanged:self.tabFun];
    }
}

#pragma mark -
#pragma mark ConsClassFunView Delegate
- (void)consClassView:(ConsClassView *)classView didSelectedWithItem:(ConsClass *)consClass
{
    ConsMsgDetailController * controller = [[ConsMsgDetailController alloc] initWithNibName:nil bundle:nil];
    [controller setConsClass:consClass];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -
#pragma mark ConsTradeMarkView Delegate
- (void)consClassTradeMarkView:(ConsClassTradeMarkView *)tradeMarkView didSelectedWithItem:(ConsSupplier *)supplier
{
    ConsSupplierDetailController * controller = [[ConsSupplierDetailController alloc] initWithNibName:nil bundle:nil];
    controller.supplier = supplier;
    [self.navigationController pushViewController:controller animated:YES];
}
@end
