//
//  CustLeftMenuController.m
//  CmAppCustIos
//
//  Created by Chenhc on 15/3/19.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustLeftMenuController.h"

@interface CustLeftMenu:NSObject

@property (nonatomic, strong) NSString * title;
@property (nonatomic) NSUInteger functionCode;
@property (nonatomic, strong) NSString * imageName;
@property (nonatomic) BOOL expanded;
@property (nonatomic, strong) NSMutableArray * childFunction;

@end

@implementation CustLeftMenu

@end

@interface CustLeftMenuController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UIImage * icon;
@property (nonatomic, strong) UIImageView * orgIcon;
@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) NSMutableArray * funcArray;

- (void)setMenuExpanded:(id)sender;
- (void)showAd:(id)sender;

@end

@implementation CustLeftMenuController

- (void)initUI
{
    self.view.backgroundColor = [UIColor blackColor];
    
    self.funcArray = [[NSMutableArray alloc] init];
    
    CustLeftMenu * section1 = [[CustLeftMenu alloc] init];
    section1.title = @"推荐";
    section1.imageName = @"menu_icon_search";
    section1.expanded = YES;
    section1.childFunction = [[NSMutableArray alloc] init];
    
    CustLeftMenu * s1f1 = [[CustLeftMenu alloc] init];
    s1f1.title = @"帐号信息";
    s1f1.functionCode = CustLeftMenuFunctionAccount;
    [section1.childFunction addObject:s1f1];
    
    CustLeftMenu * s1f2 = [[CustLeftMenu alloc] init];
    s1f2.title = @"我的兑换";
    s1f2.functionCode = CustLeftMenuFunctionExchange;
    [section1.childFunction addObject:s1f2];
    
    CustLeftMenu * s1f3 = [[CustLeftMenu alloc] init];
    s1f3.title = @"设置";
    s1f3.functionCode = CustLeftMenuFunctionSetting;
    [section1.childFunction addObject:s1f3];
    
//    CustLeftMenu * s1f4 = [[CustLeftMenu alloc] init];
//    s1f4.title = @"关于";
//    s1f4.functionCode = CustLeftMenuFunctionAbout;
//    [section1.childFunction addObject:s1f4];
    
    [self.funcArray addObject:section1];
    
    
    if ([UIDevice currentDevice].systemVersion.floatValue < 7.0) {
        self.orgIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 34, 32)];
    } else {
        self.orgIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 30, 34, 32)];
    }
    
    self.orgIcon.image = _icon;
    [self.view addSubview:self.orgIcon];
    
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(self.orgIcon.frame.origin.x + self.orgIcon.frame.size.width + 10, self.orgIcon.frame.origin.y, self.view.frame.size.width - (self.orgIcon.frame.origin.x + self.orgIcon.frame.size.width + 10 + 80 + 10), self.orgIcon.frame.size.height)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0f];
    label.textColor = [UIColor whiteColor];
    label.text = @"闽烟在线";
    [self.view addSubview:label];
    
    UIView * sep = [[UIView alloc] initWithFrame:CGRectMake(10, label.frame.origin.y + label.frame.size.height + 10, self.view.frame.size.width - 80 - 20, 1)];
    sep.backgroundColor = [UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:216.0/255.0 alpha:0.3];
    [self.view addSubview:sep];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, sep.frame.origin.y + sep.frame.size.height, self.view.frame.size.width - 80, self.view.frame.size.height - (sep.frame.origin.y + sep.frame.size.height + 40)) style:UITableViewStylePlain];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.scrollsToTop = NO;
    [self.view addSubview:self.tableView];
    
    UIButton * btnAd = [UIButton buttonWithType:UIButtonTypeCustom];
    btnAd.frame = CGRectMake(0, self.tableView.frame.size.height + self.tableView.frame.origin.y + 5, 60, 30);
    [btnAd setImage:[UIImage imageNamed:@"ShowAd"] forState:UIControlStateNormal];
    [btnAd setImage:[UIImage imageNamed:@"ShowAd"] forState:UIControlStateHighlighted];
    [btnAd addTarget:self action:@selector(showAd:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnAd];
}

- (void)setOrgIconImage:(UIImage *)image
{
    if (self.orgIcon != nil) {
        self.orgIcon.image = image;
    } else {
        self.icon = image;
    }
}

- (void)setMenuExpanded:(id)sender
{
    int index = ((UIControl *)sender).tag;
    CustLeftMenu * section = [self.funcArray objectAtIndex:index];
    section.expanded = !(section.expanded);
    [self.tableView reloadData];
}

- (void)showAd:(id)sender
{
    if ([self.menuDelegate respondsToSelector:@selector(custLeftMenuController:didClickFunction:)]) {
        [self.menuDelegate custLeftMenuController:self didClickFunction:CustLeftMenuFunctionAd];
    }
}

#pragma mark -
#pragma mark UITableView DataSource & Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.funcArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    CustLeftMenu * menu = [self.funcArray objectAtIndex:section];
    if (menu.expanded) {
        return menu.childFunction.count;
    } else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CustLeftMenu * menu = [self.funcArray objectAtIndex:section];
    
    UIControl * view = [[UIControl alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 50)];
    view.backgroundColor = self.view.backgroundColor;
    
    UIImageView * icon = [[UIImageView alloc] initWithFrame:CGRectMake(10, (view.frame.size.height - 24) / 2, 24, 24)];
    icon.image = [UIImage imageNamed:menu.imageName];
    [view addSubview:icon];
    
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(icon.frame.origin.x + icon.frame.size.width + 10, 0, view.frame.size.width - (icon.frame.origin.x + icon.frame.size.width + 10 + 20), view.frame.size.height)];
    label.font = [UIFont systemFontOfSize:17.0f];
    label.textColor = [UIColor whiteColor];
    label.text = menu.title;
    [view addSubview:label];
    
    UIImageView * indicator = [[UIImageView alloc] initWithFrame:CGRectMake(view.frame.size.width - 30, (view.frame.size.height - 12) / 2, 12, 12)];
    if (menu.expanded) {
        indicator.image = [UIImage imageNamed:@"LeftMenuMinus"];
    } else {
        indicator.image = [UIImage imageNamed:@"LeftMenuPlus"];
    }
    
    [view addSubview:indicator];
    
    UIView * sep = [[UIView alloc] initWithFrame:CGRectMake(10, view.frame.size.height - 1, self.tableView.frame.size.width - 20, 1)];
    sep.backgroundColor = [UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:216.0/255.0 alpha:0.3];
    [view addSubview:sep];
    
    view.tag = section;
    [view addTarget:self action:@selector(setMenuExpanded:) forControlEvents:UIControlEventTouchUpInside];
    
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CELL";
    UITableViewCell * cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont systemFontOfSize:15.0f];
        cell.textLabel.textColor = [UIColor whiteColor];
        
        UIView * sep = [[UIView alloc] initWithFrame:CGRectMake(10, 39, self.tableView.frame.size.width - 20, 1)];
        sep.backgroundColor = [UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:216.0/255.0 alpha:0.3];
        [cell addSubview:sep];
    }
    
    CustLeftMenu * section = [self.funcArray objectAtIndex:indexPath.section];
    CustLeftMenu * menu = [section.childFunction objectAtIndex:indexPath.row];
    
    cell.textLabel.text = menu.title;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CustLeftMenu * section = [self.funcArray objectAtIndex:indexPath.section];
    CustLeftMenu * menu = [section.childFunction objectAtIndex:indexPath.row];
    if ([self.menuDelegate respondsToSelector:@selector(custLeftMenuController:didClickFunction:)]) {
        [self.menuDelegate custLeftMenuController:self didClickFunction:menu.functionCode];
    }
}

@end
