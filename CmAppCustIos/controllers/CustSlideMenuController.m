//
//  CustSlideMenuController.m
//  CmAppCustIos
//
//  Created by Chenhc on 15/3/19.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustSlideMenuController.h"
#import "CTSlideMenuView.h"
#import "CustLeftMenuController.h"
#import "CustMainController.h"
#import "CTNavigationController.h"
#import "CustWebService.h"
#import "CustSignInController.h"
#import "CustAboutController.h"
#import "ConsAdController.h"
#import "ConsMyExchangeController.h"
#import "ConsSettingController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "ConsSetting.h"
#import "ConsMsgController.h"
#import "CustAccountController.h"
#import "CustVersion.h"
#import "CustActivity.h"
// 游戏类活动
//#import "TransitController.h"

#define PROCESS_VERSION 11201
#define PROCESS_AD 11202

@interface CustSlideMenuController () <CustMainControllerDelegate, CTNavigationDelegate, CustLeftMenuDelegate>

@property (nonatomic, strong) CTSlideMenuView * slideMenu;
@property (nonatomic, strong) CustVersion * version;
@property (nonatomic, assign) BOOL isInit;

- (void)logout:(NSNotification *)notification;
- (void)showHelp:(NSNotification *)notification;
- (void)checkUpdate;

@end

@implementation CustSlideMenuController

- (void)initUI
{
    if ([ConsSetting musicEnable]) {
        SystemSoundID soundId;
        NSString *path = [[NSBundle mainBundle] pathForResource:@"loginVoice" ofType:@"mp3"];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:path], &soundId);
        AudioServicesPlaySystemSound (soundId);
    }
    
    CustLeftMenuController * leftMenu = [[CustLeftMenuController alloc] initWithNibName:nil bundle:nil];
    NSString *imgName = [[CustWebService sharedUser].userCode substringToIndex:4];
    [leftMenu setOrgIconImage:[UIImage imageNamed:[NSString stringWithFormat:@"CustMenuLogo%@",imgName]]];
    leftMenu.menuDelegate = self;
    
    CustMainController * center = [[CustMainController alloc] initWithNibName:nil bundle:nil];
    center.delegate = self;
    CTNavigationController * nav = [[CTNavigationController alloc] initWithRootViewController:center];
    nav.ctNavDelegate = self;
    [nav.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor whiteColor], UITextAttributeTextColor,nil]];
    [nav setNavigationBarBackgroundImage:@"NavBg-44" imgName64:@"NavBg-64"];
    self.slideMenu = [[CTSlideMenuView alloc] initWithFrame:self.view.bounds leftViewController:leftMenu centerViewController:nav];
    [self.slideMenu setTapGestureEnabled:NO];
    [self.view addSubview:self.slideMenu];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logout:) name:NOTIFCATION_LOGOUT object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showHelp:) name:NOTIFCATION_SHOW_HELP object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(allActivityEnter:) name:NOTIFICATION_ALL_ACTIVITY object:nil];
    
    [self checkUpdate];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (!self.isInit) {
        [self loadAd];
        self.isInit = YES;
    }
}

// 游戏类活动
- (void)allActivityEnter:(NSNotification *)notification
{
    CustActivity *act = notification.object;
    // 未开启下载的活动，不让用户进入，调试可以在测试库先下载好包，切换成正式库关闭这个判断进行调试
    if (act.url.length <= 0) {
        return;
    }
    
//    [TransitController setUserId:[CustWebService sharedUser].userId userCode:[CustWebService sharedUser].userCode userName:[CustWebService sharedUser].realName userType:[CustWebService sharedUser].userType activityId:act.actId cdnUrl:act.url activityName:act.actTitle versionNo:act.versionNo];
//    TransitController *ts =[[TransitController alloc]init];
//    [self presentViewController:ts animated:YES completion:nil];
}

- (void)logout:(NSNotification *)notification
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)showHelp:(NSNotification *)notification
{
    ConsAdController * controller = [[ConsAdController alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)checkUpdate
{
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService cmappVersionCheckUrl] delegate:self];
    conn.tag = PROCESS_VERSION;
    [conn start];
}

- (void)loadAd
{
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService consAdUrl] delegate:self];
    conn.tag = PROCESS_AD;
    [conn start];
}

#pragma mark -
#pragma mark CustMainControllerDelegate
- (void)custMainControllerLeftMenuButtonDidClicked:(CustMainController *)controller
{
    if (self.slideMenu.slideMenuState == SlideMenuStateClosed) {
        [self.slideMenu setLeftViewOpened:YES];
    } else {
        [self.slideMenu setLeftViewOpened:NO];
    }
    
}

- (void)custMainControllerRightMenuButtonDidClicked:(CustMainController *)controller
{
    ConsMsgController * msg = [[ConsMsgController alloc] initWithNibName:nil bundle:nil];
    [(CTNavigationController *)self.slideMenu.centerViewController pushViewController:msg animated:YES];
}

#pragma mark -
#pragma mark CTNavigationDelegate
- (void)ctNavigationDidChangeCurrentController:(CTNavigationController *)navController animateFinished:(BOOL)finished
{
    if (!finished) {
        [self.slideMenu setLeftViewOpened:NO];
    } else {
        if (navController.viewControllers.count < 2) {
            [self.slideMenu setPanGestureEnabled:YES];
        } else {
            [self.slideMenu setPanGestureEnabled:NO];
        }
    }
    
}

#pragma mark -
#pragma mark CustLeftMenuDelegate
- (void)custLeftMenuController:(CustLeftMenuController *)controller didClickFunction:(CustLeftMenuFunction)function
{
    [self.slideMenu setLeftViewOpened:NO];
    switch (function) {
        case CustLeftMenuFunctionAbout:
        {
            CustAboutController * controller = [[CustAboutController alloc] initWithNibName:@"CustAboutController" bundle:nil];
            [(CTNavigationController *)self.slideMenu.centerViewController pushViewController:controller animated:YES];
        }
            break;
            
        case CustLeftMenuFunctionAd:
        {
            ConsAdController * controller = [[ConsAdController alloc] initWithNibName:nil bundle:nil];
            controller.isAd = YES;
            [self presentViewController:controller animated:YES completion:nil];
        }
            break;
            
        case CustLeftMenuFunctionAccount:
        {
            CustAccountController * controller = [[CustAccountController alloc] initWithNibName:@"CustAccountController" bundle:nil];
            [(CTNavigationController *)self.slideMenu.centerViewController pushViewController:controller animated:YES];
        }
            break;
            
        case CustLeftMenuFunctionExchange:
        {
            ConsMyExchangeController * controller = [[ConsMyExchangeController alloc] initWithNibName:nil bundle:nil];
            [(CTNavigationController *)self.slideMenu.centerViewController pushViewController:controller animated:YES];
        }
            break;
            
        case CustLeftMenuFunctionSetting:
        {
            ConsSettingController * controller = [[ConsSettingController alloc] initWithNibName:@"ConsSettingController" bundle:nil];
            [(CTNavigationController *)self.slideMenu.centerViewController pushViewController:controller animated:YES];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    if (connection.tag == PROCESS_VERSION) {
        connection.delegate = nil;
        connection = nil;
    }
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_VERSION) {
        self.version = [[CustVersion alloc] init];
        [self.version parseData:data];
        NSDictionary* infoDict =[[NSBundle mainBundle] infoDictionary];
        int current = [[infoDict objectForKey:@"CFBundleVersion"] intValue];
        [self.indicator hide];
        if (self.version.buildCode != nil && [self.version.buildCode intValue] > current) {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"有新版本啦  %@", self.version.versionNo] message:[NSString stringWithFormat:@"更新内容：%@", self.version.content] delegate:self cancelButtonTitle:@"下次再说" otherButtonTitles:@"马上更新", nil];
            alert.tag = 1;
            [alert show];
        }
    } else if (connection.tag == PROCESS_AD){
        CustActivity * act = [[CustActivity alloc] init];
        [act parseData:data complete:^(NSArray *array){
            
            if (array.count != 0) {
                ConsAdController * controller = [[ConsAdController alloc] initWithNibName:nil bundle:nil];
                controller.isAd = YES;
                [self presentViewController:controller animated:YES completion:nil];
            } else {
                NSLog(@"暂无广告");
            }
        }];
        
    }
}

#pragma mark -
#pragma mark UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1 && buttonIndex == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.version.url]];
    }
}



@end
