//
//  CustTrackController.m
//  CmAppCustIos
//
//  Created by Arcesaka on 14/8/25.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustTrackController.h"
#import "CTTableView.h"
#import "CTIndicateView.h"
#import "CustWebService.h"
#import "CTURLConnection.h"
#import "CTIndicateView.h"
#import "CustServiceMsg.h"
#import "CustSystemMsg.h"
#import "CustTrackUndealController.h"
#import "CustTrackDetailController.h"
#import "CustTrackAssessController.h"
#import "CustServiceCount.h"
#import "CustServiceMsgList.h"
#import "CTButton.h"


#define BTN_TYPE_UNDEAL 1
#define BTN_TYPE_DEALING 2
#define BTN_TYPE_DEALED 3

#define PROCESS_LOADING_DEALSUM  4
#define PROCESS_LOADING_UNDEAL   5
#define PROCESS_LOADING_DEALING 6
#define PROCESS_LOADING_DEALED  7
#define PROCESS_DELETE_INFO    8

@interface CustTrackController ()<UITableViewDelegate,UITableViewDataSource,CTTableViewDelegate,CTURLConnectionDelegate,UIScrollViewDelegate,CustTrackControllerDelegate,UIAlertViewDelegate>

@property (nonatomic, strong) IBOutlet UIView   *  topTab;
@property (nonatomic, strong) IBOutlet UIButton *  undealbtn;
@property (nonatomic, strong) IBOutlet UIButton *  dealingbtn;
@property (nonatomic, strong) IBOutlet UIButton *  dealedbtn;

@property (nonatomic, strong) CTTableView       *  undealtv;  //未处理
@property (nonatomic, strong) CTTableView       *  dealingtv; //已处理
@property (nonatomic, strong) CTTableView       *  dealedtv;  //待评价
@property (nonatomic, strong) NSMutableArray    *  undealarray;
@property (nonatomic, strong) NSMutableArray    *  dealingarray;
@property (nonatomic, strong) NSMutableArray    *  dealedarray;
@property (nonatomic, strong) CTIndicateView    *  indicator;
@property (nonatomic, strong) NSString *params;    //删除
@property (nonatomic) NSInteger curType;
@property (nonatomic) NSInteger deleteIndex;
@property (nonatomic) BOOL isLoadDealed;

- (IBAction)onTabChange:(UIButton *)btn;
- (void)showAlert:(NSString *)title withMessage:(NSString *)message;

@end

@implementation CustTrackController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)showAlert:(NSString *)title withMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.undealarray = [[NSMutableArray alloc] init];
    self.dealingarray = [[NSMutableArray alloc] init];
    self.dealedarray = [[NSMutableArray alloc] init];
    self.curType = BTN_TYPE_UNDEAL;
    self.deleteIndex = -1;
    self.isLoadDealed = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.topTab.backgroundColor = [UIColor colorWithRed:219.0/255.0 green:211.0/255.0 blue:226.0/255.0 alpha:1];
    self.topTab.frame = CGRectMake(0, 0, self.view.frame.size.width, self.topTab.frame.size.height);
    [self.view addSubview:self.topTab];
    [self loaddealSumData];
    if (self.undealtv == nil) {
        self.indicator = [[CTIndicateView alloc] initInView:self.view];
        self.indicator.backgroundTouchable = YES;
        CGRect mframe = CGRectMake(0, self.topTab.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - self.topTab.frame.size.height);
        self.undealtv = [[CTTableView alloc] initWithFrame:mframe];
        self.undealtv.backgroundColor = [UIColor clearColor];
        self.undealtv.delegate = self;
        self.undealtv.dataSource = self;
        self.undealtv.ctTableViewDelegate = self;
        self.undealtv.tag = 20 + BTN_TYPE_UNDEAL;
        [self.undealtv setPullUpViewEnabled:NO];
        [self.view addSubview:self.undealtv];
        
        self.dealingtv = [[CTTableView alloc] initWithFrame:mframe];
        self.dealingtv.backgroundColor = [UIColor clearColor];
        self.dealingtv.delegate = self;
        self.dealingtv.dataSource = self;
        self.dealingtv.ctTableViewDelegate = self;
        self.dealingtv.tag = 20 + BTN_TYPE_DEALING;
        [self.dealingtv setPullUpViewEnabled:NO];
        [self.view addSubview:self.dealingtv];
        
        self.dealedtv = [[CTTableView alloc] initWithFrame:mframe];
        self.dealedtv.backgroundColor = [UIColor clearColor];
        self.dealedtv.delegate = self;
        self.dealedtv.dataSource = self;
        self.dealedtv.ctTableViewDelegate = self;
        self.dealedtv.tag = 20 + BTN_TYPE_DEALED;
        [self.dealedtv setPullUpViewEnabled:NO];
        [self.view addSubview:self.dealedtv];
    }
    if (self.curType == BTN_TYPE_UNDEAL) {
        [self onTabChange:self.undealbtn];
    } else if (self.curType == BTN_TYPE_DEALING) {
        [self onTabChange:self.dealingbtn];
    } else {
        [self onTabChange:self.dealedbtn];
    }
}

- (void)loaddealSumData
{
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getServiceCount:[CustWebService sharedUser].userCode] delegate:self];
    conn.tag = PROCESS_LOADING_DEALSUM;
    [conn start];
    return;
}

- (void)loadUndealData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"加载中...";
    [self.indicator show];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getServiceInfo:[CustWebService sharedUser].userCode status:@"1"] delegate:self];
    conn.tag = PROCESS_LOADING_UNDEAL;
    [conn start];
    return;
}

- (void)loadDealingData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"加载中...";
    [self.indicator show];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getServiceInfo:[CustWebService sharedUser].userCode status:@"2"] delegate:self];
    conn.tag = PROCESS_LOADING_DEALING;
    [conn start];
    return;
}

- (void)loadDealedData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"加载中...";
    [self.indicator show];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getServiceInfo:[CustWebService sharedUser].userCode status:@"3"] delegate:self];
    conn.tag = PROCESS_LOADING_DEALED;
    [conn start];
    return;
}

- (void)onTabChange:(UIButton *)btn
{
    [self.undealbtn setTitleColor:XCOLOR(142.0, 86.0, 159.0, 1) forState:UIControlStateNormal];
    [self.undealbtn setTitleColor:XCOLOR(142.0, 86.0, 159.0, 1) forState:UIControlStateHighlighted];
    [self.dealingbtn setTitleColor:XCOLOR(142.0, 86.0, 159.0, 1) forState:UIControlStateNormal];
    [self.dealingbtn setTitleColor:XCOLOR(142.0, 86.0, 159.0, 1) forState:UIControlStateHighlighted];
    [self.dealedbtn setTitleColor:XCOLOR(142.0, 86.0, 159.0, 1) forState:UIControlStateNormal];
    [self.dealedbtn setTitleColor:XCOLOR(142.0, 86.0, 159.0, 1) forState:UIControlStateHighlighted];
    [btn setBackgroundColor:XCOLOR(194.0, 173.0, 202.0, 1)];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    UIColor *normalcolor = XCOLOR(209.0, 211.0, 226.0, 1);
    UIColor *selectedcolor = XCOLOR(194.0, 173.0, 202.0, 1);
    self.undealtv.hidden = YES;
    self.dealingtv.hidden = YES;
    self.dealedtv.hidden = YES;
    if (btn == self.undealbtn) {
        [self.undealbtn setBackgroundColor:selectedcolor];
        [self.dealingbtn setBackgroundColor:normalcolor];
        [self.dealedbtn setBackgroundColor:normalcolor];
        self.curType = BTN_TYPE_UNDEAL;
        self.undealtv.hidden = NO;
        [self loadUndealData];
    } else if (btn == self.dealingbtn) {
        [self.undealbtn setBackgroundColor:normalcolor];
        [self.dealingbtn setBackgroundColor:selectedcolor];
        [self.dealedbtn setBackgroundColor:normalcolor];
        self.curType = BTN_TYPE_DEALING;
        self.dealingtv.hidden = NO;
        [self loadDealingData];
    } else if (btn == self.dealedbtn) {
        [self.undealbtn setBackgroundColor:normalcolor];
        [self.dealingbtn setBackgroundColor:normalcolor];
        [self.dealedbtn setBackgroundColor:selectedcolor];
        self.curType = BTN_TYPE_DEALED;
        self.dealedtv.hidden = NO;
        [self loadDealedData];
    }
}
#pragma mark - CTTableView Delegate & DataSource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    if (self.curType == BTN_TYPE_UNDEAL) {
        [self loadUndealData];
    } else if (self.curType == BTN_TYPE_DEALING) {
        [self loadDealingData];
    } else {
        [self loadDealedData];
    }
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
    if (tableView.tag == 20 + BTN_TYPE_UNDEAL) {
        rows = self.undealarray.count;
    } else if (tableView.tag == 20 + BTN_TYPE_DEALING) {
        rows = self.dealingarray.count;
    } else {
        rows = self.dealedarray.count;
    }
    return rows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 88;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"CELL";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        NSString * cellxibname = @"CustTrackCell";
        if (tableView.tag == 20 + BTN_TYPE_DEALING) {
            cellxibname = @"CustAllCell";
        }
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:cellxibname owner:nil options:nil];
        UIView * view = [elements objectAtIndex:0];
        view.frame = CGRectMake(5, 8, tableView.frame.size.width - 5 * 2, 80);
        view.layer.borderColor = [[UIColor colorWithRed:202.0/255.0 green:201.0/255.0 blue:202.0/255.0 alpha:1] CGColor];
        view.layer.borderWidth = 1;
        [cell addSubview:view];
        
    }
    if (tableView.tag == 20 + BTN_TYPE_UNDEAL) {
        CustServiceMsgList * msg = [self.undealarray objectAtIndex:indexPath.row];
        UILabel * titlelab = (UILabel *)[cell viewWithTag:1];
        CTButton *changebtn = (CTButton *)[cell viewWithTag:2];
        CTButton *deletebtn = (CTButton *)[cell viewWithTag:3];
        UILabel  * datelab = (UILabel *)[cell viewWithTag:4];
        
        changebtn.index = indexPath.row;
        [changebtn addTarget:self action:@selector(changeClick:) forControlEvents:UIControlEventTouchUpInside];
        deletebtn.index = indexPath.row;
        [deletebtn addTarget:self action:@selector(deleteClick:) forControlEvents:UIControlEventTouchUpInside];
        titlelab.text = msg.demandTitle;
        datelab.text = msg.demandTime;
    } else if (tableView.tag == 20 + BTN_TYPE_DEALING) {
        CustServiceMsgList * msg = [self.dealingarray objectAtIndex:indexPath.row];
        UILabel * titlelab = (UILabel *)[cell viewWithTag:1];
        UILabel * statelab = (UILabel *)[cell viewWithTag:2];
        UILabel  * datelab = (UILabel *)[cell viewWithTag:4];
        titlelab.text = msg.demandTitle;
        statelab.text = @"处理中";
        datelab.text = msg.demandTime;
    } else {
        CustServiceMsgList * msg = [self.dealedarray objectAtIndex:indexPath.row];
        UILabel * titlelab = (UILabel *)[cell viewWithTag:1];
        UIButton *changebtn = (UIButton *)[cell viewWithTag:2];
        UIButton *deletebtn = (UIButton *)[cell viewWithTag:3];
        UILabel  * datelab = (UILabel *)[cell viewWithTag:4];
        UIImageView *image = (UIImageView *)[cell viewWithTag:50];
        image.hidden = NO;
        changebtn.hidden = YES;
        deletebtn.hidden = YES;
        titlelab.text = msg.demandTitle;
        datelab.text = msg.demandTime;
    }
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL canDelete;
    if (self.curType == BTN_TYPE_UNDEAL) {
        canDelete = YES;
    } else if (self.curType == BTN_TYPE_DEALING) {
        canDelete = NO;
    } else {
        canDelete = NO;
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.deleteIndex = indexPath.row;
    CustServiceMsgList * msg = [self.undealarray objectAtIndex:indexPath.row];
    self.params = [NSString stringWithFormat:@"<ServiceAccepted><demandId>%@</demandId></ServiceAccepted>",msg.demandId];
    
    [self showAlert:@"提示" withMessage:[NSString stringWithFormat:@"确定删除<%@>?",msg.demandTitle]];
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.curType == BTN_TYPE_UNDEAL) {
        return;
    } else if (self.curType == BTN_TYPE_DEALING) {
        CustTrackDetailController *controller = [[CustTrackDetailController alloc] initWithNibName:@"CustTrackDetailController" bundle:nil];
        controller.msg = [self.dealingarray objectAtIndex:indexPath.row];
        controller.title = @"服务受理";
        [self.nc pushViewController:controller animated:YES];
    } else if (self.curType == BTN_TYPE_DEALED) {
        CustTrackAssessController  *controller = [[CustTrackAssessController alloc] initWithNibName:@"CustTrackAssessController" bundle:nil];
        controller.msg = [self.dealedarray objectAtIndex:indexPath.row];
        controller.title = @"服务评价";
        controller.delegate = self;
        [self.nc pushViewController:controller animated:YES];
    }
}
#pragma mark - CustTrackDelegate
- (void)ismustRefresh:(CustTrackAssessController *)controller isFresh:(BOOL)isfresh
{
    if (isfresh == YES) {
        [self loaddealSumData];
        self.isLoadDealed = isfresh;
    }
}

#pragma  mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.curType == BTN_TYPE_UNDEAL) {
        [self.undealtv ctTableViewDidScroll];
    } else if (self.curType == BTN_TYPE_DEALING) {
        [self.dealingtv ctTableViewDidScroll];
    } else {
        [self.dealedtv ctTableViewDidScroll];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (self.curType == BTN_TYPE_UNDEAL) {
        [self.undealtv ctTableViewDidEndDragging];
    } else if (self.curType == BTN_TYPE_DEALING) {
        [self.dealingtv ctTableViewDidEndDragging];
    } else {
        [self.dealedtv ctTableViewDidEndDragging];
    }
}

#pragma mark - BtnClickEvent
- (void)changeClick:(CTButton *)btn
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btn.index inSection:0];
    
    CustServiceMsgList * msg = [self.undealarray objectAtIndex:indexPath.row];
    CustTrackUndealController * controller = [[CustTrackUndealController alloc] init];
    controller.title = @"服务受理";
    controller.msg = msg;
    [self.nc pushViewController:controller animated:YES];
}

- (void)deleteClick:(CTButton *)btn
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btn.index inSection:0];

    self.deleteIndex = indexPath.row;
    CustServiceMsgList * msg = [self.undealarray objectAtIndex:indexPath.row];
    self.params = [NSString stringWithFormat:@"<ServiceAccepted><demandId>%@</demandId></ServiceAccepted>",msg.demandId];
    
    [self showAlert:@"提示" withMessage:[NSString stringWithFormat:@"确定删除<%@>?",msg.demandTitle]];
}
#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService deleteServiceInfo] params:self.params delegate:self];
        conn.tag = PROCESS_DELETE_INFO;
        [conn start];
    }
}
#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0f];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_LOADING_DEALSUM) {
        CustServiceCount * msg = [[CustServiceCount alloc] init];
        [msg parseData:data complete:^(CustServiceCount * msg){
            [self drawDealSum:(CustServiceCount *)msg];
            if (self.isLoadDealed == YES) {
                [self loadDealedData];
            }
        }];
    } else if (connection.tag == PROCESS_LOADING_UNDEAL) {
        CustServiceMsgList * msg = [[CustServiceMsgList alloc] init];
        [msg parseData:data complete:^(NSArray *array){
            [self.indicator hide];
            [self.undealarray removeAllObjects];
            [self.undealarray addObjectsFromArray:array];
            
            self.undealarray = [NSMutableArray arrayWithArray:[self.undealarray sortedArrayUsingFunction:sortDate context:nil]];

            [self.undealtv reloadData];
        }];
    } else if (connection.tag == PROCESS_LOADING_DEALING) {
        CustServiceMsgList * msg = [[CustServiceMsgList alloc] init];
        [msg parseData:data complete:^(NSArray *array){
            [self.indicator hide];
            [self.dealingarray removeAllObjects];
            [self.dealingarray addObjectsFromArray:array];
            
            self.dealingarray = [NSMutableArray arrayWithArray:[self.dealingarray sortedArrayUsingFunction:sortDate context:nil]];

            
            [self.dealingtv reloadData];
        }];
    } else if (connection.tag == PROCESS_LOADING_DEALED) {
        CustServiceMsgList * msg = [[CustServiceMsgList alloc] init];
        [msg parseData:data complete:^(NSArray *array){
            [self.indicator hide];
            [self.dealedarray removeAllObjects];
            [self.dealedarray addObjectsFromArray:array];
            
            self.dealedarray = [NSMutableArray arrayWithArray:[self.dealedarray sortedArrayUsingFunction:sortDate context:nil]];

            [self.dealedtv reloadData];
        }];
    } else if (connection.tag == PROCESS_DELETE_INFO) {
        CustSystemMsg * msg = [[CustSystemMsg alloc] init];
        [msg parseData:data];
        if ([msg.code isEqualToString:@"1"]) {
            [self.undealarray removeObjectAtIndex:self.deleteIndex];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.deleteIndex inSection:0];
            [self.undealtv deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self loaddealSumData];
        } else {
            self.indicator.text = msg.msg;
            [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0f];
       }
    }
}
#pragma mark - DrawDealSum
- (void)drawDealSum:(CustServiceCount *)msg
{    
    [self.undealbtn setTitle:[NSString stringWithFormat:@"未处理(%@)",msg.noDeal] forState:UIControlStateNormal];
    [self.dealingbtn setTitle:[NSString stringWithFormat:@"处理中(%@)",msg.inDeal] forState:UIControlStateNormal];
    [self.dealedbtn setTitle:[NSString stringWithFormat:@"待评价(%@)",msg.byEval] forState:UIControlStateNormal];
    return;
}
#pragma mark - SortDate
NSInteger sortDate(id s1,id s2,void *cha)
{
    CustServiceMsgList *sx1 = (CustServiceMsgList *)s1;
    CustServiceMsgList *sx2 = (CustServiceMsgList *)s2;
    NSDate *date1 = (NSDate *)sx1.demandTime;
    NSDate *date2 = (NSDate *)sx2.demandTime;
    
    NSComparisonResult result = [date1 compare:date2];
    
    if (result == NSOrderedAscending) {
        return NSOrderedDescending;
    } else if (result == NSOrderedDescending) {
        return NSOrderedAscending;
    } else {
        return NSOrderedSame;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
