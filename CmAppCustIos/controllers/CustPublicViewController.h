//
//  RYKPublicViewController.h
//  RYKForCustIos
//
//  Created by hsit on 14-6-20.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTViewController.h"
#import "CustAddPublicViewController.h"

@interface CustPublicViewController : CTViewController<CustRefreshPubDelegate>

@end
