//
//  RYKRecordsofonsViewController.h
//  RYKForCustIos
//
//  Created by hsit on 14-6-23.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTViewController.h"
#import "CustCustName.h"

@interface CustRecordsofonsViewController : CTViewController

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *mobilephone;

- (id)initWithuserId:(NSString *)userId userName:(NSString *)username mobilePhone:(NSString *)mobilephone;


@end
