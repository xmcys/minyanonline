//
//  ConsBrandDetailController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-24.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsBrandDetailController.h"
#import "CTScrollView.h"
#import "CustWebService.h"
#import "ConsBrand.h"
#import "CTRatingView.h"
#import "CTImageView.h"
#import "ConsBrandEvaListController.h"
#import "ConsBrandEvaController.h"
#import <ShareSDK/ShareSDK.h>
//#import "ConsCustSearchController.h"

#define PROCESS_LOAD_REINFO 1
#define PROCESS_LOAD_DETAIL 2

@interface ConsBrandDetailController () <UIActionSheetDelegate, CTScrollViewDelegate>

@property (nonatomic, strong) IBOutlet CTScrollView * container;  // 主容器
@property (nonatomic, strong) IBOutlet CTImageView * banner;     // 顶部图片
@property (nonatomic, strong) IBOutlet UILabel * announceLabel;   // 公告
@property (nonatomic, strong) IBOutlet UILabel * qtyTarLabel;     // 焦油含量
@property (nonatomic, strong) IBOutlet UILabel * priceLabel;      // 零售价
@property (nonatomic, strong) IBOutlet UILabel * pkgColorLabel;   // 主色调
@property (nonatomic, strong) IBOutlet UILabel * pkgStyleLabel;   // 包装形式
@property (nonatomic, strong) IBOutlet UILabel * supNameLabel;    // 产地
@property (nonatomic, strong) IBOutlet CTRatingView * brandRate;     // 星星
@property (nonatomic, strong) IBOutlet UIButton * btnEvaList;     // 用户评论
@property (nonatomic, strong) IBOutlet UILabel * introduceLabel;  // 品牌文化
@property (nonatomic, strong) IBOutlet UIView  * introduceBg;     // 品牌文化背景
@property (nonatomic, strong) IBOutlet UIView  * sep1;     // 分割线1
@property (nonatomic, strong) IBOutlet UIView  * sep2;     // 分割线2
@property (nonatomic, strong) IBOutlet UIView  * sep3;     // 分割线3
@property (nonatomic, strong) IBOutlet UIView  * bottom;     // 底部 推荐卷烟 我要评价 父控件

- (void)share;
- (IBAction)showSuggestCust;
- (IBAction)showEvaList;
- (IBAction)addEva;
- (void)showBrandDetail;
- (void)announceAnimation;
- (void)loadReInfo;

@end

@implementation ConsBrandDetailController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"卷烟详情";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton * rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.backgroundColor = [UIColor clearColor];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"BtnShare"] forState:UIControlStateNormal];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"BtnShare"] forState:UIControlStateHighlighted];
    rightBtn.showsTouchWhenHighlighted = YES;
    [rightBtn addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn sizeToFit];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    
    self.sep1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Dot"]];
    self.sep2.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Dot"]];
    self.sep3.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"DotV"]];
    
//    [self.view addSubview:self.container];
    self.container.backgroundColor = [UIColor clearColor];
    self.container.pullDownViewEnabled = YES;
    self.container.ctScrollViewDelegate = self;
    self.banner.contentMode = UIViewContentModeScaleToFill;
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CONS_MODULE_JYXQ, CONS_MODULE_JYXQ, nil]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = self.brand.brandName;
    self.container.frame = self.view.bounds;
    
    [self loadReInfo];
}

- (void)showBrandDetail
{
    self.title = self.brand.brandName;
    self.banner.url = [CustWebService fileFullUrl:self.brand.picUrl];
    self.banner.contentMode = UIViewContentModeScaleAspectFit;
    [self.banner loadImage];
    
    self.priceLabel.text = [NSString stringWithFormat:@"%@元/条", self.brand.retailPrice];
    self.qtyTarLabel.text = [NSString stringWithFormat:@"%@mg", self.brand.qtyTar];
    self.pkgColorLabel.text = self.brand.mainPackColor;
    self.pkgStyleLabel.text = self.brand.brandPackstyleName;
    self.supNameLabel.text = self.brand.supplierName;
    
    self.brandRate.rate = [self.brand.tTotalScore doubleValue];
    self.brandRate.ratingViewEnabled = NO;
    
    [self.btnEvaList setTitle:[NSString stringWithFormat:@"(%@) >>", self.brand.surverNum] forState:UIControlStateNormal];
    [self.btnEvaList setTitle:[NSString stringWithFormat:@"(%@) >>", self.brand.surverNum] forState:UIControlStateHighlighted];
    
//    CGSize size = [self.brand.totalFeature sizeWithFont:self.introduceLabel.font constrainedToSize:CGSizeMake(self.introduceLabel.frame.size.width, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
//    size = CGSizeMake(size.width, size.height + 20);
//    
//    self.introduceLabel.frame = CGRectMake(self.introduceLabel.frame.origin.x, self.introduceLabel.frame.origin.y, self.introduceLabel.frame.size.width, size.height);
//    self.introduceBg.frame = CGRectMake(self.introduceBg.frame.origin.x, self.introduceBg.frame.origin.y, self.introduceBg.frame.size.width, self.introduceLabel.frame.origin.y + size.height + 10);
    self.introduceLabel.text = self.brand.totalFeature;
//
//    self.bottom.frame = CGRectMake(self.bottom.frame.origin.x, self.introduceBg.frame.origin.y + self.introduceBg.frame.size.height, self.bottom.frame.size.width, self.bottom.frame.size.height);
//    
//    self.container.contentSize = CGSizeMake(self.container.frame.size.width, self.bottom.frame.origin.y + self.bottom.frame.size.height);
    
    self.announceLabel.text = self.brand.reInfo;
    [self.announceLabel sizeToFit];
    self.announceLabel.frame = CGRectMake(0, 0, self.announceLabel.frame.size.width, 32);
    
    [self announceAnimation];
}

// 活动公告滚动动画
- (void)announceAnimation
{
    
    CGRect frame = self.announceLabel.frame;
    frame.origin.y = frame.origin.y + 2;
    frame.origin.x = 241;
    self.announceLabel.frame = frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationDuration:10.0];
    [UIView setAnimationRepeatAutoreverses:NO];
    [UIView setAnimationRepeatCount:999];
    
    frame = self.announceLabel.frame;
    frame.origin.x = - frame.size.width;
    self.announceLabel.frame = frame;
    
    [UIView commitAnimations];
}

- (void)share
{
    NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"Logo512"  ofType:@"png"];
    
    
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:[NSString stringWithFormat:@"我在“闽烟在线”发现一款很不错的烟——“%@”，你也来试试～http://118.244.194.37:8080/app/myzx/home.html", self.brand.brandName]
                                       defaultContent:[NSString stringWithFormat:@"我在“闽烟在线”发现一款很不错的烟——“%@”，你也来试试～http://118.244.194.37:8080/app/myzx/home.html", self.brand.brandName]
                                                image:[ShareSDK imageWithPath:imagePath]
                                                title:@"闽烟在线"
                                                  url:@"http://118.244.194.37:8080/app/myzx/home.html"
                                          description:[NSString stringWithFormat:@"我在“闽烟在线”发现一款很不错的烟——“%@”，你也来试试～http://118.244.194.37:8080/app/myzx/home.html", self.brand.brandName]
                                            mediaType:SSPublishContentMediaTypeNews];
    
    [ShareSDK showShareActionSheet:nil
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions: nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                if (state == SSResponseStateSuccess)
                                {
                                    self.indicator.text = @"分享成功";
                                    [self.indicator autoHide:CTIndicateStateDone afterDelay:1.0];
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    self.indicator.text = @"分享失败";
                                    [self.indicator autoHide:CTIndicateStateDone afterDelay:1.0];
                                    NSLog(@"分享失败,错误码:%ld,错误描述:%@", [error errorCode], [error errorDescription]);
                                }
                            }];
}

- (void)showSuggestCust
{
//    ConsCustSearchController * controller = [[ConsCustSearchController alloc] initWithNibName:nil bundle:nil];
//    controller.brand = self.brand;
//    [self.navigationController pushViewController:controller animated:YES];
}

- (void)showEvaList
{
    ConsBrandEvaListController * controller = [[ConsBrandEvaListController alloc] initWithNibName:@"ConsCustEvaListController" bundle:nil];
    controller.brand = self.brand;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)addEva
{
    if (![CustWebService sharedUser].isLogin) {
        self.indicator.text = @"您尚未登录";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0 complete:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFCATION_NOT_LOGIN object:nil];
        }];
        return;
    }
    
    ConsBrandEvaController * controller = [[ConsBrandEvaController alloc] initWithNibName:nil bundle:nil];
    controller.brand = self.brand;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)loadReInfo
{
    if (self.indicator.showing) {
        return;
    }
    
    self.indicator.text = @"加载中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService brandReInfoUrl:self.brand.brandId] delegate:self];
    conn.tag = PROCESS_LOAD_REINFO;
    [conn start];
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_LOAD_REINFO) {
        ConsBrand * reInfo = [[ConsBrand alloc] init];
        [reInfo parseData:data complete:^(NSArray * array){
            self.brand.reInfo = ((ConsBrand *)[array objectAtIndex:0]).reInfo;
            
            CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService brandDetailUrl:self.brand.brandId] delegate:self];
            conn.tag = PROCESS_LOAD_DETAIL;
            [conn start];
        }];
    } else {
        ConsBrand * brand = [[ConsBrand alloc] init];
        [brand parseData:data complete:^(NSArray * array){
            ConsBrand * item = [array objectAtIndex:0];
            item.reInfo = self.brand.reInfo;
            self.brand = item;
            [self.indicator hide];
            [self showBrandDetail];
            [self.container reload];
        }];
    }
}

#pragma mark -
#pragma mark CTScrollView Delegate
- (BOOL)ctScrollViewDidPullDownToRefresh:(CTScrollView *)ctScrollView
{
    [self loadReInfo];
    return YES;
}

@end
