//
//  CustExchangeViewController.m
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/24.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustExchangeViewController.h"
#import "CTTableView.h"
#import "CustWebService.h"
#import "ConsPikcer.h"
#import "CTImageView.h"
#import "CTButton.h"
#import "CustCompanyCode.h"
#import "CustExchangeGift.h"
#import "ConsExDetailController.h"
#import "CustSumPoints.h"
#import "ConsExOrderController.h"
#import "CustModule.h"

# define PROCESS_GIFTDATA   1
# define PROCESS_POINTSCHECK    2

@interface CustExchangeViewController () <CTTableViewDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate, ConsPickerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) IBOutlet UIView *pickerPosView;
@property (nonatomic, strong) IBOutlet UIButton *btnSearch;
@property (nonatomic, strong) IBOutlet UITextField *tfStart;
@property (nonatomic, strong) IBOutlet UITextField *tfEnd;

@property (nonatomic, strong) ConsPikcer *picker;
@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) NSMutableArray *companyArray;

- (IBAction)btnSearchOnClick:(id)sender;
- (void)loadData;

@end

@implementation CustExchangeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"积分兑换";
    self.array = [[NSMutableArray alloc] init];
    self.companyArray = [[NSMutableArray alloc] init];
    
    [self.btnSearch setBackgroundImage:[[UIImage imageNamed:@"BtnExchange"] resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)] forState:UIControlStateNormal];
    [self.btnSearch setBackgroundImage:[[UIImage imageNamed:@"BtnExchange"] resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)] forState:UIControlStateHighlighted];
    
    // 读中烟工业公司定义文件
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *companyFilePath = [mainBundle pathForResource:@"company" ofType:@"xml"];
    NSError *error = nil;
    NSString *content = [NSString stringWithContentsOfFile:companyFilePath encoding:NSUTF8StringEncoding error:&error];
    // 解析公司数据
    CustCompanyCode *companyParser = [[CustCompanyCode alloc] init];
    [companyParser parseData:[content dataUsingEncoding:NSUTF8StringEncoding] complete:^(NSArray *array) {
        [self.companyArray removeAllObjects];
        [self.companyArray addObjectsFromArray:array];
    }];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_KHFW, CUST_MODULE_OPERATE_JFDH, nil]];
    return;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:CGRectMake(0, self.headerView.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height-self.headerView.frame.size.height)];
        self.tv.pullUpViewEnabled = NO;
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        [self.view addSubview:self.tv];
    }
    
    if (self.picker == nil) {
        CGRect pickerFrame = CGRectMake(self.pickerPosView.frame.origin.x, self.pickerPosView.frame.origin.y, [UIScreen mainScreen].bounds.size.width - self.pickerPosView.frame.origin.x - 10, self.pickerPosView.frame.size.height);
        self.picker = [[ConsPikcer alloc] initWithFrame:pickerFrame];
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        for (CustCompanyCode *curCompany in self.companyArray) {
            PickerItem *pickerItem = [[PickerItem alloc] init];
            pickerItem.key = curCompany.companyCode;
            pickerItem.value = curCompany.companyName;
            [tempArray addObject:pickerItem];
        }
        
        [self.picker setItemArray:tempArray];
        self.picker.delegate = self;
        [self.view addSubview:self.picker];
        [self.picker setSelectedIndex:0];
        [self loadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma  mark - http requests
- (void)loadData
{
    if (self.companyArray.count == 0) {
        return;
    }
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中...";
    [self.indicator show];
    
    PickerItem *curPickItem = self.picker.itemArray[self.picker.selectedIndex];
    NSString *supplierCode = curPickItem.key;
    NSString *start = self.tfStart.text;
    NSString *end = self.tfEnd.text;
    NSString *params = [CustWebService getAllGiftPostParams:supplierCode pointStart:start pointEnd:end];
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService getAllGiftPostUrl] params:params delegate:self];
    conn.tag = PROCESS_GIFTDATA;
    [conn start];
    return;
}

# pragma mark - button click events
- (IBAction)btnSearchOnClick:(id)sender
{
    [self.view endEditing:YES];
    [self loadData];
    return;
}

- (void)exchange:(CTButton *)btn
{
    if (self.indicator.showing) {
        return;
    }
    
    self.indicator.text = @"努力加载中...";
    [self.indicator show];
    
    // 验证积分是否足够
    NSString * url = [CustWebService getTotalPointsUrl];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:url delegate:self];
    conn.userInfo = [NSNumber numberWithInt:btn.index];
    conn.tag = PROCESS_POINTSCHECK;
    [conn start];
}

# pragma mark - ConsPikcer delegate
- (void)consPickerDelegate:(ConsPikcer *)picker didSelectRowAtIndex:(int)index
{
    return;
}

# pragma mark - ConsPikcer delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.tfStart]) {
        [self.tfEnd becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

//下拉刷新
#pragma  mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark - CTTableView Delegate *DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 125;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray* elements = [[NSBundle mainBundle] loadNibNamed:@"ConsGoodsCell" owner:nil options:nil];
    UITableViewCell * cell = [elements objectAtIndex:0];
    UIView *mainView = [cell viewWithTag:99];
    mainView.layer.borderWidth = 1;
    mainView.layer.borderColor = [[UIColor colorWithRed:202.0/255.0 green:201.0/255.0 blue:202.0/255.0 alpha:1] CGColor];
    
    CustExchangeGift * gift = [self.array objectAtIndex:indexPath.row];
    
    CTImageView * img = (CTImageView *)[cell viewWithTag:1];
    img.url = [CustWebService fileFullUrl:gift.giftPic];
    [img loadImage];
    
    UILabel * name = (UILabel *)[cell viewWithTag:2];
    name.text = gift.giftName;
    
    UILabel * price = (UILabel *)[cell viewWithTag:3];
    price.text = [NSString stringWithFormat:@"%@元", gift.advisePrice];
    
    UILabel * point = (UILabel *)[cell viewWithTag:4];
    point.text = gift.changePoint;
    
    CTButton * btn = (CTButton *)[cell viewWithTag:5];
    [btn setBackgroundImage:[[UIImage imageNamed:@"BtnExchange"] resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)] forState:UIControlStateNormal];
    [btn setBackgroundImage:[[UIImage imageNamed:@"BtnExchange"] resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)] forState:UIControlStateHighlighted];
    btn.index = indexPath.row;
    btn.specialId = gift.giftCode;
    [btn addTarget:self action:@selector(exchange:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    if (self.indicator.showing) {
        return NO;
    }
    [self loadData];
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CustExchangeGift *gift = [self.array objectAtIndex:indexPath.row];
    ConsExDetailController *controller = [[ConsExDetailController alloc] initWithNibName:@"ConsExDetailController" bundle:nil];
    controller.gift = gift;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - CTURLConnection Delegate

- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
    [self.tv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_GIFTDATA) {
        CustExchangeGift *giftParser = [[CustExchangeGift alloc] init];
        [giftParser parseData:data complete:^(NSArray *array) {
            [self.array removeAllObjects];
            [self.array addObjectsFromArray:array];
            [self.indicator hide];
            [self.tv reloadData];
        }];
    } else if (connection.tag == PROCESS_POINTSCHECK) {
        CustSumPoints *sumPointsParser = [[CustSumPoints alloc] init];
        [sumPointsParser parseData:data complete:^(CustSumPoints *sumPoints) {
            NSInteger index = [connection.userInfo integerValue];
            CustExchangeGift *curGift = [self.array objectAtIndex:index];
            // 积分不足
            if ([sumPoints.sumPoints integerValue] < [curGift.changePoint integerValue]) {
                self.indicator.text = [NSString stringWithFormat:@"您当前可用积分余额为%@，不足以兑换此商品。", sumPoints.sumPoints];
                [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
            } else {
                [self.indicator hide];
                ConsExOrderController * controller = [[ConsExOrderController alloc] initWithNibName:@"ConsExOrderController" bundle:nil];
                controller.gift = curGift;
                controller.myPoints = sumPoints.sumPoints;
                [self.navigationController pushViewController:controller animated:YES];
            }
        }];
    }
    return;
}

@end
