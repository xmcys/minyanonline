//
//  ConsMsgDetailController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-3.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//


#import "ConsMsgDetailController.h"
#import "CustWebService.h"
#import "ConsMsg.h"
#import "CustActivity.h"
#import "ConsClass.h"
#import "ConsOnSaleActivity.h"

@interface ConsMsgDetailController ()

@property (nonatomic, strong) UILabel * titleLabel;        // 详情标题
@property (nonatomic, strong) UILabel * publishTimeLabel;  // 时间
@property (nonatomic, strong) UIImageView * clockIcon;     // 时钟图标
@property (nonatomic, strong) UIWebView * contentView;     // 内容视图

@property (nonatomic, strong) ConsMsg * item;   // 消息
@property (nonatomic, strong) CustActivity * act;  // 活动
@property (nonatomic, strong) ConsClass * consClass; // 烟草学堂
@property (nonatomic, weak) ConsOnSaleActivity * onSaleAct;  // 促销活动

- (void)showMsgContent;
- (void)showActContent;
- (void)showClassContent;
- (void)showOnSaleActContent;

@end

@implementation ConsMsgDetailController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"消息详情";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.titleLabel == nil) {
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.view.bounds.size.width - 10, 60)];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.numberOfLines = 2;
        [self.view addSubview:self.titleLabel];
        
        self.publishTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        self.publishTimeLabel.font = [UIFont systemFontOfSize:10.0f];
        self.publishTimeLabel.textColor = [UIColor lightGrayColor];
        self.publishTimeLabel.numberOfLines = 1;
        self.publishTimeLabel.backgroundColor = [UIColor clearColor];
        self.publishTimeLabel.text = @"2000-01-01 00:00:00";
        [self.publishTimeLabel sizeToFit];
        self.publishTimeLabel.center = CGPointMake(self.view.bounds.size.width - self.publishTimeLabel.frame.size.width / 2 - 10, 60 + self.publishTimeLabel.frame.size.height / 2);
        [self.view addSubview:self.publishTimeLabel];
        
        self.clockIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Clock"]];
        self.clockIcon.frame = CGRectMake(self.publishTimeLabel.frame.origin.x - 9 - 5, self.publishTimeLabel.frame.origin.y + 1, 9, 9);
        [self.view addSubview:self.clockIcon];
        
        UIView * dotLine = [[UIView alloc] initWithFrame:CGRectMake(5, self.publishTimeLabel.frame.origin.y + self.publishTimeLabel.frame.size.height + 5, self.view.bounds.size.width - 10, 1)];
        dotLine.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Dot"]];
        [self.view addSubview:dotLine];
        
        self.contentView = [[UIWebView alloc] initWithFrame:CGRectMake(5, dotLine.frame.origin.y + dotLine.frame.size.height + 5, self.view.bounds.size.width - 10, self.view.bounds.size.height - dotLine.frame.size.height - dotLine.frame.origin.y - 5 - 5)];
        self.contentView.backgroundColor = [UIColor clearColor];
        self.contentView.dataDetectorTypes = UIDataDetectorTypeNone;
        self.contentView.opaque = NO;
        [[[self.contentView subviews] objectAtIndex:0] setBounces:NO];
        [self.view addSubview:self.contentView];
    }
    
    if (self.item != nil) {
        self.title = @"消息详情";
        self.titleLabel.text = self.item.msgTitle;
        self.publishTimeLabel.text = self.item.publishTime;
        [self.publishTimeLabel sizeToFit];
        self.publishTimeLabel.center = CGPointMake(self.view.bounds.size.width - self.publishTimeLabel.frame.size.width / 2 - 10, 60 + self.publishTimeLabel.frame.size.height / 2);
        [self.view addSubview:self.publishTimeLabel];
        self.clockIcon.frame = CGRectMake(self.publishTimeLabel.frame.origin.x - 9 - 5, self.publishTimeLabel.frame.origin.y + 1, 9, 9);
        self.indicator.text = @"加载中...";
        [self.indicator show];
        CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService messageDetailUrl:self.item.msgId] delegate:self];
        [conn start];
    } else if (self.act != nil) {
        self.title = @"活动详情";
        self.titleLabel.text = self.act.actTitle;
        self.publishTimeLabel.text = [NSString stringWithFormat:@"%@ - %@", self.act.beginTime, self.act.endTime];
        [self.publishTimeLabel sizeToFit];
        self.publishTimeLabel.center = CGPointMake(self.view.bounds.size.width - self.publishTimeLabel.frame.size.width / 2 - 10, 60 + self.publishTimeLabel.frame.size.height / 2);
        [self.view addSubview:self.publishTimeLabel];
        self.clockIcon.frame = CGRectMake(self.publishTimeLabel.frame.origin.x - 9 - 5, self.publishTimeLabel.frame.origin.y + 1, 9, 9);
        [self showActContent];
    } else if (self.consClass != nil){
        if (self.consClass.showType == ClassFun) {
            self.title = @"零售户讲堂";
        } else {
            self.title = @"经营指导";
        }
        
        self.titleLabel.text = self.consClass.schoolTitle;
        self.publishTimeLabel.text = self.consClass.publishTime;
        [self.publishTimeLabel sizeToFit];
        self.publishTimeLabel.center = CGPointMake(self.view.bounds.size.width - self.publishTimeLabel.frame.size.width / 2 - 10, 60 + self.publishTimeLabel.frame.size.height / 2);
        [self.view addSubview:self.publishTimeLabel];
        self.clockIcon.frame = CGRectMake(self.publishTimeLabel.frame.origin.x - 9 - 5, self.publishTimeLabel.frame.origin.y + 1, 9, 9);
        [self showClassContent];
    } else if (self.onSaleAct != nil){
        self.title = @"活动详情";
        
        self.titleLabel.text = self.onSaleAct.actTitle;
        self.publishTimeLabel.text = self.onSaleAct.publishTime;
        [self.publishTimeLabel sizeToFit];
        self.publishTimeLabel.center = CGPointMake(self.view.bounds.size.width - self.publishTimeLabel.frame.size.width / 2 - 10, 60 + self.publishTimeLabel.frame.size.height / 2);
        [self.view addSubview:self.publishTimeLabel];
        self.clockIcon.frame = CGRectMake(self.publishTimeLabel.frame.origin.x - 9 - 5, self.publishTimeLabel.frame.origin.y + 1, 9, 9);
        [self showOnSaleActContent];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_XX, CUST_MODULE_OPERATE_XXXQ, nil]];
}

- (void)setConsMsg:(ConsMsg *)item
{
    self.item = item;
}

- (void)setConsAct:(CustActivity *)item
{
    self.act = item;
}

- (void)setConsOnSaleAct:(ConsOnSaleActivity *)item;
{
    self.onSaleAct = item;
}

- (void)setConsconsClass:(ConsClass *)item
{
    self.consClass = item;
}

- (void)showMsgContent
{
    NSString * cont = self.item.msgCont;
    cont = [cont stringByReplacingOccurrencesOfString:@"        " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"       " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"      " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"     " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"    " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@" " withString:@"</br>"];
    
    NSMutableString * imgString = [[NSMutableString alloc] init];
    if (self.item.msgImg01.length != 0 && ![self.item.msgImg01 isEqualToString:@"/"]) {
        [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [CustWebService fileFullUrl:self.item.msgImg01], self.view.bounds.size.width - 20]];
    }
    if (self.item.msgImg02.length != 0 && ![self.item.msgImg02 isEqualToString:@"/"]) {
        [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [CustWebService fileFullUrl:self.item.msgImg02], self.view.bounds.size.width - 20]];
    }
    if (self.item.msgImg03.length != 0 && ![self.item.msgImg03 isEqualToString:@"/"]) {
        [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [CustWebService fileFullUrl:self.item.msgImg03], self.view.bounds.size.width - 20]];
    }
    if (self.item.msgImg04.length != 0 && ![self.item.msgImg04 isEqualToString:@"/"]) {
        [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [CustWebService fileFullUrl:self.item.msgImg04], self.view.bounds.size.width - 20]];
    }
    if (self.item.msgImg05.length != 0 && ![self.item.msgImg05 isEqualToString:@"/"]) {
        [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [CustWebService fileFullUrl:self.item.msgImg05], self.view.bounds.size.width - 20]];
    }
    
    NSMutableString * fileString = [[NSMutableString alloc] init];
    if (self.item.msgUrl.length != 0 && ![self.item.msgUrl isEqualToString:@"/"]) {
        NSRange range = [self.item.msgUrl rangeOfString:@"Resource/"];
        NSString * name = [self.item.msgUrl substringFromIndex:range.location + range.length];
        [fileString appendString:[NSString stringWithFormat:@"<a href='%@'>《%@》</a>", [CustWebService fileFullUrl:self.item.msgUrl], name]];
    }
    
    
    
    [self.contentView loadHTMLString:[NSString stringWithFormat:@"%@</br><span style='font-size:14px; color:#3D3D3D;'>%@</span></br></br>%@", imgString, cont, fileString] baseURL:nil];
}

- (void)showActContent
{
    NSString * cont = self.act.actCont;
    
    NSMutableString * imgString = [[NSMutableString alloc] init];
    if (self.act.actImgUrl.length != 0 && ![self.act.actImgUrl isEqualToString:@"/"]) {
        [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [CustWebService fileFullUrl:self.act.actImgUrl], self.view.bounds.size.width - 20]];
    }
    
    [self.contentView loadHTMLString:[NSString stringWithFormat:@"%@</br><span style='font-size:14px; color:#3D3D3D;'>%@</span></br></br>", imgString, cont] baseURL:nil];
}

- (void)showClassContent
{
    NSString * cont = self.consClass.schoolCont;
    cont = [cont stringByReplacingOccurrencesOfString:@"        " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"       " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"      " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"     " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"    " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@" " withString:@"</br>"];
    
    NSMutableString * imgString = [[NSMutableString alloc] init];
    if (self.consClass.showType == ClassFun) {
        if (self.consClass.schoolImg01.length != 0 && ![self.consClass.schoolImg01 isEqualToString:@"/"]) {
            [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [CustWebService fileFullUrl:self.consClass.schoolImg01], self.view.bounds.size.width - 20]];
        }
        if (self.consClass.schoolImg02.length != 0 && ![self.consClass.schoolImg02 isEqualToString:@"/"]) {
            [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [CustWebService fileFullUrl:self.consClass.schoolImg02], self.view.bounds.size.width - 20]];
        }
        if (self.consClass.schoolImg03.length != 0 && ![self.consClass.schoolImg03 isEqualToString:@"/"]) {
            [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [CustWebService fileFullUrl:self.consClass.schoolImg03], self.view.bounds.size.width - 20]];
        }
        if (self.consClass.schoolImg04.length != 0 && ![self.consClass.schoolImg04 isEqualToString:@"/"]) {
            [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [CustWebService fileFullUrl:self.consClass.schoolImg04], self.view.bounds.size.width - 20]];
        }
        if (self.consClass.schoolImg05.length != 0 && ![self.consClass.schoolImg05 isEqualToString:@"/"]) {
            [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [CustWebService fileFullUrl:self.consClass.schoolImg05], self.view.bounds.size.width - 20]];
        }
    } else {
        if (self.consClass.schoolImg01.length != 0 && ![self.consClass.schoolImg01 isEqualToString:@"/"]) {
            [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [CustWebService fileFullUrl:self.consClass.schoolImg01], self.view.bounds.size.width - 20]];
        }
    }
    
    [self.contentView loadHTMLString:[NSString stringWithFormat:@"%@</br><span style='font-size:14px; color:#3D3D3D;'>%@</span></br></br>", imgString, cont] baseURL:nil];
}

- (void)showOnSaleActContent
{
    NSString * cont = self.onSaleAct.actCont;
    cont = [cont stringByReplacingOccurrencesOfString:@"        " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"       " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"      " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"     " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"    " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@" " withString:@"</br>"];
    
    NSMutableString * imgString = [[NSMutableString alloc] init];
    if (self.onSaleAct.msgImg01.length != 0 && ![self.onSaleAct.msgImg01 isEqualToString:@"/"]) {
        [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [CustWebService fileFullUrl:self.onSaleAct.msgImg01], self.view.bounds.size.width - 20]];
    }
    if (self.onSaleAct.msgImg02.length != 0 && ![self.onSaleAct.msgImg02 isEqualToString:@"/"]) {
        [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [CustWebService fileFullUrl:self.onSaleAct.msgImg02], self.view.bounds.size.width - 20]];
    }
    if (self.onSaleAct.msgImg03.length != 0 && ![self.onSaleAct.msgImg03 isEqualToString:@"/"]) {
        [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [CustWebService fileFullUrl:self.onSaleAct.msgImg03], self.view.bounds.size.width - 20]];
    }
    if (self.onSaleAct.msgImg04.length != 0 && ![self.onSaleAct.msgImg04 isEqualToString:@"/"]) {
        [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [CustWebService fileFullUrl:self.onSaleAct.msgImg04], self.view.bounds.size.width - 20]];
    }
    if (self.onSaleAct.msgImg05.length != 0 && ![self.onSaleAct.msgImg05 isEqualToString:@"/"]) {
        [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [CustWebService fileFullUrl:self.onSaleAct.msgImg05], self.view.bounds.size.width - 20]];
    }
    
    
    
    [self.contentView loadHTMLString:[NSString stringWithFormat:@"%@</br><span style='font-size:14px; color:#3D3D3D;'>%@</span></br></br>", imgString, cont] baseURL:nil];
    return;
}


#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"请求失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    [self.item parseData:data];
    [self showMsgContent];
    [self.indicator hide];
}

@end
