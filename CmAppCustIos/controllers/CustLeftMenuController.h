//
//  CustLeftMenuController.h
//  CmAppCustIos
//
//  Created by Chenhc on 15/3/19.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@class CustLeftMenuController;

typedef NS_ENUM(NSUInteger, CustLeftMenuFunction)
{
    CustLeftMenuFunctionAccount = 20100,
    CustLeftMenuFunctionExchange,
    CustLeftMenuFunctionSetting,
    CustLeftMenuFunctionAbout,
    CustLeftMenuFunctionAd
};

@protocol CustLeftMenuDelegate <NSObject>

@optional
- (void)custLeftMenuController:(CustLeftMenuController *)controller didClickFunction:(CustLeftMenuFunction)function;

@end

@interface CustLeftMenuController : CTViewController

@property (nonatomic, weak) id<CustLeftMenuDelegate> menuDelegate;

- (void)setOrgIconImage:(UIImage *)image;

@end
