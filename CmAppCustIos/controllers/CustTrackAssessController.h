//
//  CustTrackAssessController.h
//  CmAppCustIos
//
//  Created by hsit on 14-8-27.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"
#import "CustServiceMsgList.h"
#import "CTRatingView.h"

@class CustTrackAssessController;

@protocol CustTrackControllerDelegate <NSObject>

@optional

- (void)ismustRefresh:(CustTrackAssessController *)controller isFresh:(BOOL)isfresh;

@end

@interface CustTrackAssessController : CTViewController<CTRatingViewDelegate>

@property (nonatomic, strong) CustServiceMsgList * msg;
@property (nonatomic, weak) id<CustTrackControllerDelegate> delegate;

@end
