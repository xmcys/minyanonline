//
//  CustOrderHistoryController.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustOrderHistoryController.h"
#import "CTTableView.h"
#import "CustOrder.h"
#import "CTIndicateView.h"
#import "CTUtil.h"
#import "CTURLConnection.h"
#import "CustWebService.h"
#import "CustOrderHistoryDetailController.h"
#import "CustModule.h"

#define PROCESS_SUMMARY 0
#define PROCESS_LIST 1

@interface CustOrderHistoryController () <UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, CTTableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, CTURLConnectionDelegate, CustOrderDelegate>

@property (strong, nonatomic) IBOutlet UIView *header;
@property (weak, nonatomic) IBOutlet UIView *selectorBg;
@property (weak, nonatomic) IBOutlet UIButton *btnMonthSelector;
@property (weak, nonatomic) IBOutlet UIView *summaryBg;
@property (weak, nonatomic) IBOutlet UILabel *qtyOrder;
@property (weak, nonatomic) IBOutlet UILabel *qtyOrderSum;
@property (weak, nonatomic) IBOutlet UILabel *amtOrderSum;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (nonatomic, strong) CTTableView * cttv;
@property (nonatomic, strong) NSMutableArray * orderArray;
@property (nonatomic, strong) CTIndicateView * ctIndicate;
@property (nonatomic, strong) UIPickerView * picker;
@property (nonatomic, strong) UIControl * cover;
@property (nonatomic) int months;
@property (nonatomic) int process;

- (IBAction)showOrHidePicker;
- (IBAction)search;

@end

@implementation CustOrderHistoryController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"历史订单";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.summaryBg.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CustOrderDetailLimitBg"]];
    self.selectorBg.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CustOrderAddSearchBg"]];
    [self.btnMonthSelector setBackgroundImage:[[UIImage imageNamed:@"InputBoxBg"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 5.5, 14, 5.5)] forState:UIControlStateNormal];
    [self.btnMonthSelector setBackgroundImage:[[UIImage imageNamed:@"InputBoxBg"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 5.5, 14, 5.5)] forState:UIControlStateHighlighted];
    
    [self.btnSearch setBackgroundImage:[[UIImage imageNamed:@"BtnNormal"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 5, 10, 5)] forState:UIControlStateNormal];
    [self.btnSearch setBackgroundImage:[[UIImage imageNamed:@"BtnPressed"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 5, 10, 5)] forState:UIControlStateHighlighted];
    
    self.months = 1;
    self.orderArray = [[NSMutableArray alloc] init];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_ORDER_HIS_MAIN, CUST_MODULE_ORDER_HIS_LIST, nil]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.cttv == nil) {
        
        self.ctIndicate = [[CTIndicateView alloc] initInView:self.view];
//        self.ctIndicate.backgroundTouchable = NO;
        
        self.cttv = [[CTTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.cttv.delegate = self;
        self.cttv.dataSource = self;
        self.cttv.ctTableViewDelegate = self;
        self.cttv.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.cttv.pullUpViewEnabled = NO;
        self.cttv.pullDownViewEnabled = NO;
        
        [self.view addSubview:self.cttv];
    }
    
    if (self.picker == nil) {
        
        self.cover = [[UIControl alloc] initWithFrame:self.view.bounds];
        self.cover.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
        [self.cover addTarget:self action:@selector(showOrHidePicker) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.cover];
        self.cover.hidden = YES;
        
        self.picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height + 40, self.view.bounds.size.width, 216)];
        self.picker.dataSource = self;
        self.picker.delegate = self;
        self.picker.showsSelectionIndicator = YES;
        if (!VERSION_LESS_THAN_IOS7) {
            self.picker.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1];
        }

        [self.view addSubview:self.picker];
        
    }

    [self.btnMonthSelector setImageEdgeInsets:UIEdgeInsetsMake(0, self.view.frame.size.width - 98, 0, 0)];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.orderArray.count == 0) {
        [self search];
    }

}

- (void)showOrHidePicker
{
    if (self.picker.frame.origin.y < self.view.bounds.size.height) {
        [UIView animateWithDuration:0.2 animations:^{
            self.picker.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height + self.picker.frame.size.height / 2 + 40);
            self.cover.hidden = YES;
        }];
    } else {
        [UIView animateWithDuration:0.2 animations:^{
            self.picker.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height - self.picker.frame.size.height / 2);
            self.cover.hidden = NO;
        }];
    }
}

- (void)search
{
    if (self.ctIndicate.showing) {
        return;
    }
    self.ctIndicate.text = @"加载中...";
    [self.ctIndicate show];
    self.process = PROCESS_SUMMARY;
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService custOrderHistorySummaryUrl:[CustWebService sharedUser].userCode months:self.months] delegate:self];
    [conn start];
}

#pragma mark -
#pragma mark UIPickerView DataSource & Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 3;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:16.0f];
    label.textColor = [UIColor blackColor];
    
    switch (row) {
        case 0:
            label.text = @"最近一个月";
            break;
            
        case 1:
            label.text = @"最近两个月";
            break;
            
        case 2:
            label.text = @"最近三个月";
            break;
            
        default:
            break;
    }
    
    return label;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self.btnMonthSelector setTitle:((UILabel *)[pickerView viewForRow:row forComponent:component]).text forState:UIControlStateNormal];
    [self.btnMonthSelector setTitle:((UILabel *)[pickerView viewForRow:row forComponent:component]).text forState:UIControlStateHighlighted];
    self.months = row + 1;
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.cttv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.cttv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark UITableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.orderArray.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return self.header.frame.size.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"BRAND_CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:236.0/255.0 blue:239.0/255.0 alpha:1];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        
        NSArray * elements = [[NSBundle mainBundle] loadNibNamed:@"CustBrandCell" owner:nil options:nil];
        UIView * bg = [elements objectAtIndex:0];
        bg.frame = CGRectMake(0, 0, self.view.frame.size.width, 36);
        bg.tag = 1;
        [cell addSubview:bg];
    }
    
    UIView * bg = [cell viewWithTag:1];
    if (indexPath.row % 2 != 0) {
        bg.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:236.0/255.0 blue:239.0/255.0 alpha:1];
    } else {
        bg.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1];
    }
    
    CustOrder * item = [self.orderArray objectAtIndex:indexPath.row];
    
    UILabel * orderDate = (UILabel *)[cell viewWithTag:2];
    orderDate.text = item.orderDate;
    
    UILabel * order = (UILabel *)[cell viewWithTag:3];
    order.text = [NSString stringWithFormat:@"%.0f", item.orderSum];
    
    UILabel * cash = (UILabel *)[cell viewWithTag:4];
    cash.text = [NSString stringWithFormat:@"%.1f", item.cashSum];
    
    UILabel * brands = (UILabel *)[cell viewWithTag:5];
    brands.text = item.qtyOrderBrands;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CustOrder * item = [self.orderArray objectAtIndex:indexPath.row];
    CustOrderHistoryDetailController * controller = [[CustOrderHistoryDetailController alloc] initWithNibName:@"CustOrderHistoryDetailController" bundle:nil];
    controller.orderId = item.orderId;
    controller.date = item.orderDate;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark-
#pragma mark CTTableView Delegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    return NO;
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.ctIndicate.text = @"数据获取失败";
    [self.ctIndicate hideWithSate:CTIndicateStateWarning afterDelay:0.5];
    [self.cttv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    CustOrder * item = [[CustOrder alloc] init];
    item.delegate = self;
    [item parseData:data];
}

#pragma mark -
#pragma mark CustOrder Delegate
- (void)custOrder:(CustOrder *)custOrder didFinishingParse:(NSArray *)array
{
    if (self.process == PROCESS_SUMMARY && array.count != 0) {
        CustOrder * item = [array objectAtIndex:0];
        self.qtyOrder.text = item.order;
        self.qtyOrderSum.text = [NSString stringWithFormat:@"%0.f", item.orderSum];
        self.amtOrderSum.text = [NSString stringWithFormat:@"%.1f", item.cashSum];
        self.process = PROCESS_LIST;
        CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService custOrderHistoryListUrl:[CustWebService sharedUser].userCode months:self.months] delegate:self];
        [conn start];
    } else if (self.process == PROCESS_LIST){
        if (array.count == 0) {
            self.ctIndicate.text = @"查无数据";
            [self.ctIndicate hideWithSate:CTIndicateStateWarning afterDelay:0.5];
        } else {
            [self.ctIndicate hide];
        }
        
        [self.orderArray removeAllObjects];
        [self.orderArray addObjectsFromArray:array];
        [self.cttv reloadData];
    }
    
}


@end
