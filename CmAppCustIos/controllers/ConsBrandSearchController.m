//
//  ConsBrandSearchController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsBrandSearchController.h"
#import "ConsPopup.h"
#import "CTTableView.h"
#import "CustWebService.h"
#import "CTImageView.h"
#import "ConsBrand.h"
#import "ConsBrandDetailController.h"
#import "ConsSupplier.h"
//#import "ConsCustSearchController.h"

#define PROCESS_POP_TYPE 1
#define PROCESS_POP_PRICE 2
#define PROCESS_POP_SORT 3
#define PROCESS_LOAD_DATA 4

@interface ConsBrandSearchController () <CTTableViewDelegate, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, ConsPopupDelegate>

@property (nonatomic, strong) ConsPopup * typePop;  // 类型下啦
@property (nonatomic, strong) ConsPopup * pricePop; // 价格下拉
@property (nonatomic, strong) ConsPopup * sortPop;  // 排序下啦
@property (nonatomic, strong) CTTableView * tv;
@property (nonatomic, strong) NSMutableArray * brandArray;

- (void)loadPricePop;
- (void)loadTypePop;
- (void)loadSortPop;
- (void)loadBrand;

@end

@implementation ConsBrandSearchController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"推荐卷烟";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.brandArray = [[NSMutableArray alloc] init];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CONS_MODULE_TJJY, CONS_MODULE_TJJY, nil]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.supplier == nil && self.typePop == nil) {
        
        self.typePop = [[ConsPopup alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width / 3 - 1, 45)];
        self.typePop.delegate = self;
        [self.view addSubview:self.typePop];
        
        self.pricePop = [[ConsPopup alloc] initWithFrame:CGRectMake(self.view.bounds.size.width / 3, 0, self.view.bounds.size.width / 3 - 1, 45)];
        self.pricePop.delegate = self;
        [self.view addSubview:self.pricePop];
        
        self.sortPop = [[ConsPopup alloc] initWithFrame:CGRectMake(self.view.bounds.size.width / 3 * 2, 0, self.view.bounds.size.width / 3 - 1, 45)];
        self.sortPop.delegate = self;
        [self.view addSubview:self.sortPop];
        
        UIImageView * sep1 = [[UIImageView alloc] initWithFrame:CGRectMake(self.typePop.frame.size.width - 1, 0, 2, 45)];
        sep1.image = [[UIImage imageNamed:@"SepV"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [self.view addSubview:sep1];
        
        UIImageView * sep2 = [[UIImageView alloc] initWithFrame:CGRectMake(self.pricePop.frame.origin.x + self.pricePop.frame.size.width - 1, 0, 2, 45)];
        sep2.image = [[UIImage imageNamed:@"SepV"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [self.view addSubview:sep2];
        
        UIImageView * sep3 = [[UIImageView alloc] initWithFrame:CGRectMake(self.sortPop.frame.origin.x + self.sortPop.frame.size.width - 1, 0, 2, 45)];
        sep3.image = [[UIImage imageNamed:@"SepV"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [self.view addSubview:sep3];
    }
    
    if (self.tv == nil) {
        if (self.supplier == nil) {
            self.tv = [[CTTableView alloc] initWithFrame:CGRectMake(0, 45, self.view.bounds.size.width, self.view.bounds.size.height - 45)];
        } else {
            self.tv = [[CTTableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        }
        
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.ctTableViewDelegate = self;
        self.tv.pullUpViewEnabled = NO;
        self.tv.dataSource = self;
        self.tv.delegate = self;
        
        [self.view addSubview:self.tv];
        
        if (self.supplier == nil) {
            [self loadPricePop];
        } else {
            self.title = self.supplier.supplierName;
            [self loadBrand];
        }
        
    }
    if (self.keyword != nil) {
        self.title = self.keyword;
    }
}

- (void)loadPricePop
{
    [self.typePop hidePopup];
    [self.pricePop hidePopup];
    [self.sortPop hidePopup];
    
    if (self.indicator.showing) {
        return;
    }
    
    self.indicator.text = @"请稍后..";
    self.indicator.backgroundTouchable = NO;
    [self.indicator show];
    
    // 如果下拉框的值已经有了，就不加载下拉框数据
    if (self.pricePop.itemArray.count > 1) {
        [self loadBrand];
    }
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService brandPriceUrl] delegate:self];
    conn.tag = PROCESS_POP_PRICE;
    [conn start];
}

- (void)loadTypePop
{
    self.indicator.text = @"请稍后..";
    self.indicator.backgroundTouchable = NO;
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService brandTypeUrl] delegate:self];
    conn.tag = PROCESS_POP_TYPE;
    [conn start];
}

- (void)loadSortPop
{
    self.indicator.text = @"请稍后..";
    self.indicator.backgroundTouchable = NO;
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService brandSortUrl] delegate:self];
    conn.tag = PROCESS_POP_SORT;
    [conn start];
}

- (void)loadBrand
{
    self.indicator.text = @"请稍后..";
    self.indicator.backgroundTouchable = NO;
    [self.indicator show];

    if (self.supplier != nil) {
        CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService tradeMarkBrandListUrl:self.supplier.supplierCode] delegate:self];
        conn.tag = PROCESS_LOAD_DATA;
        [conn start];
        
    } else if (self.keyword == nil) {
        NSString * type = ((PopupItem *)[self.typePop.itemArray objectAtIndex:self.typePop.selectedIndex]).key;
        NSString * price = ((PopupItem *)[self.pricePop.itemArray objectAtIndex:self.pricePop.selectedIndex]).key;
        NSString * sort = ((PopupItem *)[self.sortPop.itemArray objectAtIndex:self.sortPop.selectedIndex]).key;
        
        CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService brandListUrl:type price:price orderByFlag:sort] delegate:self];
        conn.tag = PROCESS_LOAD_DATA;
        [conn start];
    } else {
        NSString * params = [NSString stringWithFormat:@"<RecommendCigarette><brandName>%@</brandName></RecommendCigarette>", self.keyword];
        CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService brandListByNameUrl] params:params delegate:self];
        conn.tag = PROCESS_LOAD_DATA;
        [conn start];
    }
    
    
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark CTTableView Delegate * DataSource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    if (self.supplier != nil) {
        [self loadBrand];
    } else {
        [self loadPricePop];
    }
    
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.brandArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 125;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CELL";
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSArray* elements = [[NSBundle mainBundle] loadNibNamed:@"ConsBrandCell" owner:nil options:nil];
    
    cell = [elements objectAtIndex:0];
    
    UIView * view = [cell viewWithTag:999];
//    view.frame = CGRectMake(5, 5, self.tv.frame.size.width - 10, 120);
    view.layer.borderColor = [[UIColor colorWithRed:222.0/255.0 green:222.0/255.0 blue:222.0/255.0 alpha:1] CGColor];
    view.layer.borderWidth = 1;
//
//    [cell addSubview:view];
//
    
    ConsBrand * brand = [self.brandArray objectAtIndex:indexPath.row];
    
    CTImageView * img = (CTImageView *)[cell viewWithTag:1];
    img.contentMode = UIViewContentModeScaleAspectFit;
    img.url = [CustWebService fileFullUrl:brand.picUrl];
    [img loadImage];
    
    UILabel * name = (UILabel *)[cell viewWithTag:2];
    name.text = brand.brandName;
    
    UILabel * price = (UILabel *)[cell viewWithTag:3];
    price.text = [NSString stringWithFormat:@"%@元/条", brand.retailPrice];
    
//    UILabel * tarLabel = (UILabel *)[cell viewWithTag:901];
    
    UILabel * qtyTar = (UILabel *)[cell viewWithTag:4];
    qtyTar.text = [NSString stringWithFormat:@"%@mg", brand.qtyTar];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.supplier != nil) {
        ConsBrandDetailController * controller = [[ConsBrandDetailController alloc] initWithNibName:@"ConsBrandDetailController" bundle:nil];
        controller.brand = [self.brandArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:controller animated:YES];
    }
//    else {
//        ConsCustSearchController * controller = [[ConsCustSearchController alloc] initWithNibName:nil bundle:nil];
//        controller.brand = [self.brandArray objectAtIndex:indexPath.row];
//        [self.navigationController pushViewController:controller animated:YES];
//    }
    
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.tv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_POP_PRICE) {
        ConsBrand * brand = [[ConsBrand alloc] init];
        [brand parseData:data complete:^(NSArray *array){
            NSMutableArray * temp = [[NSMutableArray alloc] init];
            for (int i = 0; i < array.count; i++) {
                PopupItem * item = [[PopupItem alloc] init];
                ConsBrand * b = [array objectAtIndex:i];
                item.key = [NSString stringWithFormat:@"%@-%@", b.sPrice, b.ePrice];
                item.value = b.priceName;
                [temp addObject:item];
            }
            self.pricePop.itemArray = temp;
            
            [self loadSortPop];
            
        }];
    } else if (connection.tag == PROCESS_POP_SORT){
        ConsBrand * brand = [[ConsBrand alloc] init];
        [brand parseData:data complete:^(NSArray *array){
            NSMutableArray * temp = [[NSMutableArray alloc] init];
            for (int i = 0; i < array.count; i++) {
                PopupItem * item = [[PopupItem alloc] init];
                ConsBrand * b = [array objectAtIndex:i];
                item.key = b.orderNo;
                item.value = b.orderName;
                [temp addObject:item];
            }
            self.sortPop.itemArray = temp;
            
            [self loadTypePop];
            
        }];
    } else if (connection.tag == PROCESS_POP_TYPE){
        ConsBrand * brand = [[ConsBrand alloc] init];
        [brand parseData:data complete:^(NSArray *array){
            NSMutableArray * temp = [[NSMutableArray alloc] init];
            int index = -1;
            for (int i = 0; i < array.count; i++) {
                PopupItem * item = [[PopupItem alloc] init];
                ConsBrand * b = [array objectAtIndex:i];
                item.key = b.typeCode;
                item.value = b.typeName;
                [temp addObject:item];
                if (self.type != nil && [item.key isEqualToString:self.type.typeCode]) {
                    index = i;
                }
                if (self.defaultTypeName != nil && [self.defaultTypeName isEqualToString:item.value]) {
                    index = i;
                    self.defaultTypeName = nil;
                }
            }
            self.typePop.itemArray = temp;
            if (index != -1) {
                self.typePop.selectedIndex = index;
            }
            
            self.type = nil;
            
            [self loadBrand];
        }];
    }
    if (connection.tag == PROCESS_LOAD_DATA) {
        ConsBrand * brand = [[ConsBrand alloc] init];
        [brand parseData:data complete:^(NSArray *array){
            [self.brandArray removeAllObjects];
            [self.brandArray addObjectsFromArray:array];
            [self.tv reloadData];
            [self.indicator hide];
        }];
    }
}

#pragma mark -
- (void)consPopup:(ConsPopup *)consPopup popStateChanged:(BOOL)isShowing
{
    if (isShowing) {
        if (consPopup == self.typePop) {
            [self.pricePop hidePopup];
            [self.sortPop hidePopup];
        } else if (consPopup == self.pricePop){
            [self.typePop hidePopup];
            [self.sortPop hidePopup];
        } else {
            [self.typePop hidePopup];
            [self.pricePop hidePopup];
        }
    }
}

- (void)consPopup:(ConsPopup *)consPopup didSelecteInIndex:(NSInteger)index
{
    [self loadBrand];
}

@end
