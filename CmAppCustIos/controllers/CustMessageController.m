//
//  CustMessageController.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-25.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustMessageController.h"
#import "CustMessage.h"
#import "CustWebService.h"
#import "CustModule.h"

@interface CustMessageController ()

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * publishTimeLabel;
@property (nonatomic, strong) UIImageView * clockIcon;
@property (nonatomic, strong) UIWebView * contentView;

@end

@implementation CustMessageController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"公告";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.titleLabel == nil) {
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.view.bounds.size.width - 10, 44)];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont boldSystemFontOfSize:20.0f];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.adjustsFontSizeToFitWidth = YES;
        [self.view addSubview:self.titleLabel];
        
        self.publishTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        self.publishTimeLabel.font = [UIFont systemFontOfSize:10.0f];
        self.publishTimeLabel.textColor = [UIColor lightGrayColor];
        self.publishTimeLabel.numberOfLines = 1;
        self.publishTimeLabel.backgroundColor = [UIColor clearColor];
        self.publishTimeLabel.text = self.msg.publishTime;
        [self.publishTimeLabel sizeToFit];
        self.publishTimeLabel.center = CGPointMake(self.view.bounds.size.width - self.publishTimeLabel.frame.size.width / 2 - 10, 44 + self.publishTimeLabel.frame.size.height / 2);
        [self.view addSubview:self.publishTimeLabel];
        
        self.clockIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ClockIcon"]];
        [self.clockIcon sizeToFit];
        self.clockIcon.center = CGPointMake(self.publishTimeLabel.frame.origin.x - 5 - self.clockIcon.frame.size.width / 2, self.publishTimeLabel.center.y);
        [self.view addSubview:self.clockIcon];
        
        UIView * dotLine = [[UIView alloc] initWithFrame:CGRectMake(5, self.publishTimeLabel.frame.origin.y + self.publishTimeLabel.frame.size.height + 5, self.view.bounds.size.width - 10, 1)];
        dotLine.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"DotLine"]];
        [self.view addSubview:dotLine];
        
        self.contentView = [[UIWebView alloc] initWithFrame:CGRectMake(5, dotLine.frame.origin.y + dotLine.frame.size.height + 5, self.view.bounds.size.width - 10, self.view.bounds.size.height - dotLine.frame.size.height - dotLine.frame.origin.y - 5 - 5)];
        self.contentView.backgroundColor = [UIColor clearColor];
        self.contentView.dataDetectorTypes = UIDataDetectorTypeNone;
        self.contentView.opaque = NO;
        [[[self.contentView subviews] objectAtIndex:0] setBounces:NO];
        [self.view addSubview:self.contentView];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.titleLabel.text = self.msg.msgTitle;
    self.publishTimeLabel.text = self.msg.publishTime;
    NSString * cont = self.msg.msgCont;
    cont = [cont stringByReplacingOccurrencesOfString:@"        " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"       " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"      " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"     " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"    " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@" " withString:@"</br>"];
    
    NSMutableString * imgString = [[NSMutableString alloc] init];
    if (self.msg.msgImg01.length != 0 && ![self.msg.msgImg01 isEqualToString:@"/"]) {
        [imgString appendString:[NSString stringWithFormat:@"<img src='%@'/></br>", [CustWebService fileFullUrl:self.msg.msgImg01]]];
    }
    if (self.msg.msgImg02.length != 0 && ![self.msg.msgImg02 isEqualToString:@"/"]) {
        [imgString appendString:[NSString stringWithFormat:@"<img src='%@'/></br>", [CustWebService fileFullUrl:self.msg.msgImg02]]];
    }
    if (self.msg.msgImg03.length != 0 && ![self.msg.msgImg03 isEqualToString:@"/"]) {
        [imgString appendString:[NSString stringWithFormat:@"<img src='%@'/></br>", [CustWebService fileFullUrl:self.msg.msgImg03]]];
    }
    if (self.msg.msgImg04.length != 0 && ![self.msg.msgImg04 isEqualToString:@"/"]) {
        [imgString appendString:[NSString stringWithFormat:@"<img src='%@'/></br>", [CustWebService fileFullUrl:self.msg.msgImg04]]];
    }
    if (self.msg.msgImg05.length != 0 && ![self.msg.msgImg05 isEqualToString:@"/"]) {
        [imgString appendString:[NSString stringWithFormat:@"<img src='%@'/></br>", [CustWebService fileFullUrl:self.msg.msgImg05]]];
    }
    
    NSMutableString * fileString = [[NSMutableString alloc] init];
    if (self.msg.msgUrl.length != 0 && ![self.msg.msgUrl isEqualToString:@"/"]) {
        NSRange range = [self.msg.msgUrl rangeOfString:@"Resource/"];
        NSString * name = [self.msg.msgUrl substringFromIndex:range.location + range.length];
        [fileString appendString:[NSString stringWithFormat:@"<a href='%@'>《%@》</a>", [CustWebService fileFullUrl:self.msg.msgUrl], name]];
    }
    
    
    
    [self.contentView loadHTMLString:[NSString stringWithFormat:@"%@<span style='font-size:14px; color:#3D3D3D;'>%@</span></br></br>%@", imgString, cont, fileString] baseURL:nil];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_ORDER_NOT_MAIN, self.msg.msgId, nil]];
}

@end
