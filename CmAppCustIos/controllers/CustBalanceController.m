//
//  CustBalanceController.m
//  CmAppCustIos
//
//  Created by Chenhc on 15/3/19.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustBalanceController.h"
#import "CustWebService.h"
#import "CTTableView.h"
#import "CTButton.h"
#import "CustBalance.h"
#import "CTURLConnection.h"

@interface CustBalanceController ()<UITableViewDataSource, UITableViewDelegate, CTTableViewDelegate, UIScrollViewDelegate, CTURLConnectionDelegate>

@property (nonatomic, strong) CTTableView * tableView;
@property (nonatomic, strong) NSMutableArray * bankArray;
@property (nonatomic) int refreshIndex;

- (void)loadData:(CTButton *)btn;

@end

@implementation CustBalanceController

- (void)initUI
{
    self.title = @"余额查询";
    self.bankArray = [[NSMutableArray alloc] init];
    self.tableView = [[CTTableView alloc] initWithFrame:self.view.bounds];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.ctTableViewDelegate = self;
    self.tableView.pullUpViewEnabled = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    [self loadData:nil];
}

- (void)loadData:(CTButton *)btn
{
    if (self.indicator.showing) {
        return;
    }
    
    CustInfo * user = [CustWebService sharedUser];
    
    if (btn == nil) {
        _refreshIndex = -1;
        CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService custBalanceListUrl:user.userCode] delegate:self];
        [conn start];
    } else {
        _refreshIndex = btn.index;
        CustBalance * item = [self.bankArray objectAtIndex:btn.index];
        CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService custBalanceUrl:user.userId cardNo:item.sCardNo] delegate:self];
        [conn start];
    }
    
    
    self.indicator.text = @"加载中...";
    [self.indicator show];
}

#pragma mark -
#pragma mark UITableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 140;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.bankArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell * cell = [[[NSBundle mainBundle] loadNibNamed:@"CustBalanceCell" owner:self options:nil] lastObject];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    CustBalance * item = [self.bankArray objectAtIndex:indexPath.row];
    
    UIImageView * headerBg = (UIImageView *)[cell viewWithTag:1];
    if (fmod(indexPath.row, 2) == 0) {
        headerBg.image = [[UIImage imageNamed:@"bank_bg_blue"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 20, 5, 20)];
    } else {
        headerBg.image = [[UIImage imageNamed:@"bank_bg_green"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 20, 5, 20)];
    }
    
    UIImageView * bankIcon = (UIImageView *)[cell viewWithTag:2];
    if ([item.bankName rangeOfString:@"工商"].location != NSNotFound) {
        bankIcon.image = [UIImage imageNamed:@"bank_gsyh"];
    } else if ([item.bankName rangeOfString:@"邮政"].location != NSNotFound){
        bankIcon.image = [UIImage imageNamed:@"bank_zgyz"];
    } else if ([item.bankName rangeOfString:@"兴业"].location != NSNotFound){
        bankIcon.image = [UIImage imageNamed:@"bank_xyyh"];
    } else if ([item.bankName rangeOfString:@"交通"].location != NSNotFound){
        bankIcon.image = [UIImage imageNamed:@"bank_jtyh"];
    } else if ([item.bankName rangeOfString:@"农业"].location != NSNotFound){
        bankIcon.image = [UIImage imageNamed:@"bank_nyyh"];
    } else if ([item.bankName rangeOfString:@"建设"].location != NSNotFound){
        bankIcon.image = [UIImage imageNamed:@"bank_jsyh"];
    } else {
        bankIcon.image = nil;
    }
    
    UIImageView * lockedIcon = (UIImageView *)[cell viewWithTag:3];
    UILabel * defaultLabel = (UILabel *)[cell viewWithTag:4];
    
    if ([item.isDefaultName rangeOfString:@"默认"].location != NSNotFound) {
        lockedIcon.hidden = NO;
        defaultLabel.hidden = NO;
    } else {
        lockedIcon.hidden = YES;
        defaultLabel.hidden = YES;
    }
    
    CTButton * btn = (CTButton *)[cell viewWithTag:5];
    btn.index = indexPath.row;
    [btn addTarget:self action:@selector(loadData:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel * bankName = (UILabel *)[cell viewWithTag:6];
    bankName.text = [NSString stringWithFormat:@"%@：", item.bankName];
    
    UILabel * bankCard = (UILabel *)[cell viewWithTag:7];
    bankCard.text = item.sCardNo;
    
    UILabel * balance = (UILabel *)[cell viewWithTag:8];
    balance.text = item.balance;
    
    UILabel * lastModifyTime = (UILabel *)[cell viewWithTag:9];
    lastModifyTime.text = item.lastModifyTime;
    
    return cell;
}

#pragma mark -
#pragma mark CTTableView Delegate
- (BOOL)ctTableViewDidPullUpToLoad:(CTTableView *)ctTableView
{
    return NO;
}

- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadData:nil];
    return YES;
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tableView ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tableView ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"获取失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    CustBalance * item = [[CustBalance alloc] init];
    [item parseData:data complete:^(NSArray * array){
        if (_refreshIndex == -1) {
            [self.bankArray removeAllObjects];
            [self.bankArray addObjectsFromArray:array];
        } else if (array.count != 0){
            [self.bankArray replaceObjectAtIndex:_refreshIndex withObject:[array objectAtIndex:0]];
        }
        [self.tableView reloadData];
        [self.indicator hide];
    }];
}

@end
