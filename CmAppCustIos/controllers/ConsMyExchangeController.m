//
//  ConsMyExchangeController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-28.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsMyExchangeController.h"
#import "CTTableView.h"
#import "CustWebService.h"
#import "CTImageView.h"
#import "ConsMyExchangeItem.h"
#import "CustModule.h"

@interface ConsMyExchangeController () <CTTableViewDelegate, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>

@property (nonatomic, strong) CTTableView * tv;
@property (nonatomic, strong) NSMutableArray * array;

- (void)loadData;

@end

@implementation ConsMyExchangeController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"我的兑换";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.array = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_CBL, CUST_MODULE_OPERATE_WDDH, nil]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tv.pullUpViewEnabled = NO;
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.ctTableViewDelegate = self;
        self.tv.delegate = self;
        self.tv.dataSource = self;
        
        [self.view addSubview:self.tv];
        
        [self loadData];
    }
    
}

- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    
    self.indicator.text = @"请稍后..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getChangeDetailUrl] delegate:self];
    [conn start];
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark CTTableView Delegate & DataSource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    if (self.indicator.showing) {
        return NO;
    }
    [self loadData];
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 125;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray* elements = [[NSBundle mainBundle] loadNibNamed:@"ConsGoodsCell" owner:nil options:nil];
    UITableViewCell * cell = [elements objectAtIndex:0];
    UIView *mainView = [cell viewWithTag:99];
    mainView.layer.borderWidth = 1;
    mainView.layer.borderColor = [[UIColor colorWithRed:202.0/255.0 green:201.0/255.0 blue:202.0/255.0 alpha:1] CGColor];
    
    UILabel * timeLabel = (UILabel *)[cell viewWithTag:8];
    timeLabel.text = @"兑换时间：";
    
    UILabel * point = (UILabel *)[cell viewWithTag:7];
    point.text = @"合计积分：";
    
    [cell viewWithTag:6].hidden = NO;
    
    [cell viewWithTag:5].hidden = YES;
    
    ConsMyExchangeItem * curItem = [self.array objectAtIndex:indexPath.row];
    
    CTImageView * img = (CTImageView *)[cell viewWithTag:1];
    img.contentMode = UIViewContentModeScaleAspectFit;
    img.image = [UIImage imageNamed:@"GoodsSample"];
    
    UILabel * name = (UILabel *)[cell viewWithTag:2];
    name.text = curItem.giftName;
    
    UILabel * time = (UILabel *)[cell viewWithTag:3];
    time.text = [NSString stringWithFormat:@"%@", curItem.changeDate];
    
    UILabel * points = (UILabel *)[cell viewWithTag:4];
    points.text = curItem.changePoint;
    
    UILabel * qty = (UILabel *)[cell viewWithTag:6];
    qty.text = [NSString stringWithFormat:@"共计%@商品", curItem.changeQty];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark CTURLConecction Delegate & DataSource
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1];
    [self.tv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    ConsMyExchangeItem * exchangeItem = [[ConsMyExchangeItem alloc] init];
    [exchangeItem parseData:data complete:^(NSArray * array){
        [self.array removeAllObjects];
        [self.array addObjectsFromArray:array];
        
        [self.tv reloadData];
        [self.indicator hide];
    }];
}

@end
