//
//  ConsPwdModifyController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-28.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsPwdModifyController.h"
#import "CustWebService.h"
#import "CustSystemMsg.h"
#import "ConsSetting.h"

@interface ConsPwdModifyController () <UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UIView * container;
@property (nonatomic, strong) IBOutlet UITextField * pwdField;
@property (nonatomic, strong) IBOutlet UITextField * pwdNewField;
@property (nonatomic, strong) IBOutlet UITextField * pwdConfirmField;

- (void)submit;

@end

@implementation ConsPwdModifyController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"密码修改";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton * rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.backgroundColor = [UIColor clearColor];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"BtnSubmit"] forState:UIControlStateNormal];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"BtnSubmit"] forState:UIControlStateHighlighted];
    rightBtn.showsTouchWhenHighlighted = YES;
    [rightBtn addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn sizeToFit];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CONS_MODULE_MMXG, CONS_MODULE_MMXG, nil]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)submit
{
    [self.pwdField resignFirstResponder];
    [self.pwdNewField resignFirstResponder];
    [self.pwdConfirmField resignFirstResponder];
    
    if (self.indicator.showing) {
        return;
    }
    
    NSString * pwd = self.pwdField.text;
    NSString * pwdNew = self.pwdNewField.text;
    NSString * pwdConfirm = self.pwdConfirmField.text;
    
    if (pwd.length == 0) {
        self.indicator.text = @"请输入当前密码";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    if (![pwd isEqualToString:[ConsSetting password]]) {
        self.indicator.text = @"当前密码有误";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    if (pwdNew.length < 4 || pwdNew.length > 16){
        self.indicator.text = @"请输入4-16位新密码";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    if (![pwdConfirm isEqualToString:pwdNew]) {
        self.indicator.text = @"两次密码不一致";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    
    self.indicator.text = @"正在提交..";
    self.indicator.backgroundTouchable = NO;
    [self.indicator show];
    
    NSString * params = [NSString stringWithFormat:@"<Login><userId>%@</userId><userPwd>%@</userPwd></Login>", [CustWebService sharedUser].userId, pwdNew];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService savePasswordUrl] params:params delegate:self];
    [conn start];
}

#pragma mark -
#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.pwdField) {
        [self.pwdNewField becomeFirstResponder];
    } else if (textField == self.pwdNewField){
        [self.pwdConfirmField becomeFirstResponder];
    } else {
        [self.pwdConfirmField resignFirstResponder];
    }
    return YES;
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"提交失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    CustSystemMsg * msg = [[CustSystemMsg alloc] init];
    [msg parseData:data];
    if ([msg.code isEqualToString:@"1"]) {
        [ConsSetting setPassword:self.pwdNewField.text];
        self.indicator.text = @"修改成功";
        [self.indicator hideWithSate:CTIndicateStateDone afterDelay:1.0];
    } else {
        self.indicator.text = msg.msg;
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    }
}

@end
