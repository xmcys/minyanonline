//
//  CustOrderAddController.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-21.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustOrderAddController.h"
#import "ChineseString.h"
#import "Pinyin.h"
#import "CustOrder.h"
#import "CTTableView.h"
#import "CTIndicateView.h"
#import "CTCheckBox.h"
#import "CTSearchBar.h"
#import "CTSmoothChooseView.h"
#import "CTURLConnection.h"
#import "CustWebService.h"
#import "CustSystemMsg.h"
#import "CustModule.h"

#define PROCESS_LOADING 0
#define PROCESS_SAVEING 1

@interface Section : NSObject

@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSMutableArray * array;

@end

@implementation Section

- (id)init
{
    if (self = [super init]) {
        self.name = @"";
        self.array = [[NSMutableArray alloc] init];
    }
    return self;
}

@end

@interface CustOrderAddController () <UITextFieldDelegate, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, CTTableViewDelegate, CTCheckBoxDelegate, CTSmoothChooseViewDelegate, CTURLConnectionDelegate, CustOrderDelegate, CTSearchBarDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) CTTableView * cttv;
@property (nonatomic, strong) CTIndicateView * ctIndicate;
@property (nonatomic, strong) NSMutableArray * backupArray;
@property (nonatomic, strong) NSMutableArray * brandArray;
@property (nonatomic, strong) NSMutableArray * sectionArray;
@property (nonatomic, strong) CTSearchBar * ctSearchBar;
@property (nonatomic, strong) CTSmoothChooseView * smoothChoose;
@property (nonatomic, weak) CustOrder * processBrand; // 正在提交的卷烟
@property (nonatomic) int process;

- (void)sort;
- (void)reloadData;
- (void)loadData:(int)flag content:(NSString *)content;
- (void)saveConfirm;
- (void)localSearch;
- (void)submit;

@end

@implementation CustOrderAddController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"添加商品";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton * btnSave = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSave.titleLabel.font = [UIFont systemFontOfSize:16.5f];
    [btnSave setTitle:@"  保存  " forState:UIControlStateNormal];
    [btnSave setTitle:@"  保存  " forState:UIControlStateHighlighted];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnSave setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [btnSave sizeToFit];
    [btnSave addTarget:self action:@selector(saveConfirm) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSave];
    
    self.brandArray = [[NSMutableArray alloc] init];
    self.sectionArray = [[NSMutableArray alloc] init];
    self.backupArray = [[NSMutableArray alloc] init];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_ORDER_MAIN, CUST_MODULE_ORDER_ADD, nil]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.cttv == nil) {
        self.cttv = [[CTTableView alloc] initWithFrame:CGRectMake(0, 44, self.view.bounds.size.width, self.view.bounds.size.height - 44) style:UITableViewStylePlain];
        self.cttv.delegate = self;
        self.cttv.dataSource = self;
        self.cttv.ctTableViewDelegate = self;
        self.cttv.pullUpViewEnabled = NO;
        self.cttv.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.cttv];
        
        self.ctSearchBar = [[CTSearchBar alloc] initWithFrame:CGRectMake(0, 0, self.cttv.frame.size.width, 44)];
        self.ctSearchBar.delegate = self;
        self.ctSearchBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CustOrderAddSearchBg"]];
        [self.view addSubview:self.ctSearchBar];
    }
    
    if (self.ctIndicate == nil) {
        self.ctIndicate = [[CTIndicateView alloc] initInView:self.view];
        self.ctIndicate.backgroundTouchable = NO;
    }
    
    if (self.smoothChoose == nil) {
        self.smoothChoose = [[CTSmoothChooseView alloc] initInView:self.view];
        self.smoothChoose.topOffset = 49;
        self.smoothChoose.delegate = self;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.brandArray.count == 0) {
        [self loadData:0 content:@""];
    }
}

- (void)sort
{
    //Step2:获取字符串中文字的拼音首字母并与字符串共同存放
    NSMutableArray * chineseStringsArray = [NSMutableArray array];
    for(int i = 0; i < [self.brandArray count]; i++){
        ChineseString * chineseString = [[ChineseString alloc] init];
        
        CustOrder * order = [self.brandArray objectAtIndex:i];
        chineseString.string = [NSString stringWithString:order.brandName];
        chineseString.obj = order;
        
        if(chineseString.string == nil){
            chineseString.string = @"";
        }
        
        if(![chineseString.string isEqualToString:@""]){
            NSString * pinYinResult = [NSString string];
            for(int j = 0; j < chineseString.string.length; j++){
                NSString * singlePinyinLetter = [[NSString stringWithFormat:@"%c", pinyinFirstLetter([chineseString.string characterAtIndex:j])]uppercaseString];
                
                pinYinResult = [pinYinResult stringByAppendingString:singlePinyinLetter];
            }
            chineseString.pinYin = pinYinResult;
        }else{
            chineseString.pinYin = @"";
        }
        [chineseStringsArray addObject:chineseString];
    }
    
    //Step3:按照拼音首字母对这些Strings进行排序
    NSArray * sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"pinYin" ascending:YES]];
    [chineseStringsArray sortUsingDescriptors:sortDescriptors];
    
    [self.sectionArray removeAllObjects];
    
    Section * added = [[Section alloc] init];
    added.name = @"+";
    
    NSString * lastLetter = @"#";
    // Step4:把排序好的内容从ChineseString类中提取出来
    Section * section = [[Section alloc] init];
    section.name = lastLetter;
    [self.sectionArray addObject:section];
    int count = 0;
    for(int i = 0;i < [chineseStringsArray count];i++){
        ChineseString * cs = [chineseStringsArray objectAtIndex:i];
        if (((CustOrder *)cs.obj).selectedFlag == 1) {
            // 已添加的规格
            [added.array addObject:cs.obj];
        } else if (cs.string.length == 0 || [@"ABCDEFGHIJKLMNOPQRSTUVWXYZ" rangeOfString:[cs.pinYin substringToIndex:1]].location == NSNotFound) {
            // #号规格
            [((Section *)[self.sectionArray objectAtIndex:0]).array addObject:cs.obj];
        } else {
            // A-Z规格
            if ([section.name isEqualToString:[cs.pinYin substringToIndex:1]]) {
                [section.array addObject:cs.obj];
            } else {
                if (![section.name isEqualToString:@"#"]) {
                    [self.sectionArray addObject:section];
                }
                section = [[Section alloc] init];
                section.name = [cs.pinYin substringToIndex:1];
                [section.array addObject:cs.obj];
            }
        }
        count++;
    }
    if (![((Section *)[self.sectionArray lastObject]).name isEqualToString:section.name]) {
        [self.sectionArray addObject:section];
    }
    
    // 如果 #组规格为空，移除
    section = ((Section *)[self.sectionArray objectAtIndex:0]);
    if ([section.name isEqualToString:@"#"] && section.array.count == 0) {
        [self.sectionArray removeObject:section];
    }
    
    // 加入已添加的分组
    [self.sectionArray insertObject:added atIndex:0];
    [self performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

- (void)loadData:(int)flag content:(NSString *)content
{
    if (self.ctIndicate.showing) {
        return;
    }
    if (flag == 0) {
        [self.cttv showPullDownView];
    }
    self.ctSearchBar.textField.text = @"";
    self.process = PROCESS_LOADING;
    self.ctIndicate.text = @"加载中...";
    [self.ctIndicate show];
    NSString * params = [NSString stringWithFormat:@"<UsShoppintCartApp><brandName></brandName><userId>%@</userId></UsShoppintCartApp>", [CustWebService sharedUser].userId];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService custOrderAddListUrl] params:params delegate:self];
    [conn start];
}

- (void)reloadData
{
    NSMutableArray * letters = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.sectionArray.count; i++) {
        [letters addObject:((Section *)[self.sectionArray objectAtIndex:i]).name];
    }
    self.smoothChoose.words = letters;
    [self.cttv reloadData];
    [self.ctIndicate hide];
}

- (void)localSearch
{
    [self.brandArray removeAllObjects];
    NSString * keyword = self.ctSearchBar.textField.text;
    if (keyword.length == 0) {
        [self.brandArray addObjectsFromArray:self.backupArray];
    } else {
        for (int i = 0; i < self.backupArray.count; i++) {
            CustOrder * order = [self.backupArray objectAtIndex:i];
            if ([order.brandName rangeOfString:keyword].location != NSNotFound || order.selectedFlag == 1)
            {
                [self.brandArray addObject:order];
            }
        }
    }
    [self sort];
}

- (void)saveConfirm
{
    [self.brandArray removeAllObjects];
    int count = 0;
    NSString * names = @"";
    for (int i = 0; i < self.backupArray.count; i++) {
        CustOrder * order = [self.backupArray objectAtIndex:i];
        if (order.added && order.selectedFlag != 1) {
            [self.brandArray addObject:order];
            count++;
            names = [NSString stringWithFormat:@"%@\n%@", names, order.brandName];
        }
    }
    if (count == 0) {
        self.ctIndicate.text = @"您尚未选择任何规格";
        [self.ctIndicate autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"确定添加以下 %d 种品牌规格？", count] message:names delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
}

- (void)submit
{
    NSString * params = [NSString stringWithFormat:@"<UsShoppingCart><userId>%@</userId><brandId>%@</brandId><demandQty>0</demandQty></UsShoppingCart>", [CustWebService sharedUser].userId, self.processBrand.brandId];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService custOrderChangeUrl] params:params delegate:self];
    self.process = PROCESS_SAVEING;
    [conn start];
}

#pragma mark -
#pragma mark CTCheckBox Delegate
- (void)ctCheckBox:(CTCheckBox *)checkBox didValueChanged:(BOOL)checked
{
    Section * s = [self.sectionArray objectAtIndex:[checkBox.specialName intValue]];
    CustOrder * item = [s.array objectAtIndex:[checkBox.specialId intValue]];
    if (item.selectedFlag == 1) {
        self.ctIndicate.text = @"不能对已添加的规格进行操作";
        [self.ctIndicate autoHide:CTIndicateStateWarning afterDelay:1.0];
        [checkBox setChecked:YES];
    } else {
        item.added = checkBox.checked;
    }
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.cttv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.cttv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark UITableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sectionArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ((Section *)[self.sectionArray objectAtIndex:section]).array.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    Section * s = [self.sectionArray objectAtIndex:section];
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.cttv.frame.size.width, 24)];
    label.font = [UIFont systemFontOfSize:15.0f];
    if ([s.name isEqualToString:@"+"]) {
        label.text = @"    已添加的规格";
    } else {
        label.text = [NSString stringWithFormat:@"    %@", s.name];
    }
    
    label.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:238.0/255.0 blue:239.0/255.0 alpha:1];
    label.textColor = [UIColor blackColor];
    return label;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 24.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"BRAND_CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:236.0/255.0 blue:239.0/255.0 alpha:1];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIView * bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.cttv.frame.size.width, 36)];
        bg.tag = 1;
        bg.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1];
        [cell addSubview:bg];
        
        CTCheckBox * cb = [[CTCheckBox alloc] initWithFrame:CGRectMake(5, 0, 36, bg.frame.size.height)];
        cb.tag = 2;
        cb.delegate = self;
        [cell addSubview:cb];
        
        UILabel * brandName = [[UILabel alloc] initWithFrame:CGRectMake(5 + 36 + 5, 0, bg.frame.size.width - 5 - 36 - 5 - 5 - 60, bg.frame.size.height)];
        brandName.font = [UIFont systemFontOfSize:13.0f];
        brandName.adjustsFontSizeToFitWidth = YES;
        brandName.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
        brandName.tag = 3;
        brandName.backgroundColor = [UIColor clearColor];
        [cell addSubview:brandName];
        
        UILabel * cash = [[UILabel alloc] initWithFrame:CGRectMake(bg.frame.size.width - 60 - 5, 0, 60, bg.frame.size.height)];
        cash.font = [UIFont systemFontOfSize:13.0f];
        cash.adjustsFontSizeToFitWidth = YES;
        cash.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
        cash.tag = 4;
        cash.backgroundColor = [UIColor clearColor];
        [cell addSubview:cash];
        
    }
    
    CustOrder * item = [((Section *)[self.sectionArray objectAtIndex:indexPath.section]).array objectAtIndex:indexPath.row];
    
    CTCheckBox * cb = (CTCheckBox *)[cell viewWithTag:2];
    [cb setChecked:item.added];
    cb.specialId = [NSString stringWithFormat:@"%d", (int)indexPath.row];
    cb.specialName = [NSString stringWithFormat:@"%d", (int)indexPath.section];
    
    UILabel * brandName = (UILabel *)[cell viewWithTag:3];
    brandName.text = item.brandName;
    
    UILabel * cash = (UILabel *)[cell viewWithTag:4];
    cash.text = [NSString stringWithFormat:@"￥%@", item.tradePrice];
    
    return cell;
}

#pragma mark-
#pragma mark CTTableView Delegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadData:1 content:self.ctSearchBar.textField.text];
    return YES;
}

#pragma mark -
#pragma mark CTSmoothChooseView Delegate
- (void)ctSmoothChooseView:(CTSmoothChooseView *)smoothChooseView didTouchedAtIndex:(int)index
{
    Section * s = [self.sectionArray objectAtIndex:index];
    if (s.array.count != 0) {
        [self.cttv scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
    
}

#pragma mark -
#pragma mark CTSearchBar Delegate
- (void)ctSearchBar:(CTSearchBar *)ctSearchBar didSearchWithContent:(NSString *)content
{
    self.ctIndicate.text = @"请稍后...";
    [NSThread detachNewThreadSelector:@selector(localSearch) toTarget:self withObject:nil];
}

#pragma mark -
#pragma mark CTURLConnection Delegate
#pragma mark -
#pragma mark CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    if (self.process == PROCESS_LOADING) {
        self.ctIndicate.text = @"加载失败";
    } else if (self.process == PROCESS_SAVEING){
        self.ctIndicate.text = @"保存失败";
        
    }
    [self.ctIndicate hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    connection.delegate = nil;
    [self.cttv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (self.process == PROCESS_LOADING) {
        CustOrder * order = [[CustOrder alloc] init];
        order.delegate = self;
        [order parseData:data];
    } else if (self.process == PROCESS_SAVEING){
        CustSystemMsg * msg = [[CustSystemMsg alloc] init];
        [msg parseData:data];
        if (msg.code.length != 0) {
            self.ctIndicate.text = msg.msg;
            [self.ctIndicate hideWithSate:CTIndicateStateWarning afterDelay:1.5];
            return;
        }
        self.processBrand.selectedFlag = 1;
        [self.brandArray removeObject:self.processBrand];
        if (self.brandArray.count != 0) {
            self.processBrand = [self.brandArray objectAtIndex:0];
            [self submit];
        } else {
            self.ctIndicate.text = @"添加成功";
            [self.ctIndicate hideWithSate:CTIndicateStateDone afterDelay:1.0];
            [self localSearch];
            if ([self.delegate respondsToSelector:@selector(custOrderAddControllerDidBrandAdded:)]) {
                [self.delegate custOrderAddControllerDidBrandAdded:self];
            }
        }
    }
    
}

#pragma mark -
#pragma mark CustOrder Delegate
- (void)custOrder:(CustOrder *)custOrder didFinishingParse:(NSArray *)array
{
    [self.brandArray removeAllObjects];
    [self.brandArray addObjectsFromArray:array];
    [self.backupArray removeAllObjects];
    [self.backupArray addObjectsFromArray:array];
    [NSThread detachNewThreadSelector:@selector(sort) toTarget:self withObject:nil];
}

#pragma mark -
#pragma mark UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1 && self.brandArray.count != 0) {
        self.processBrand = [self.brandArray objectAtIndex:0];
        self.ctIndicate.text = @"请稍后...";
        [self.ctIndicate show];
        [self submit];
    }
    
}

@end
