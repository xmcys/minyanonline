//
//  CustOrderDetailController.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-20.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustOrderDetailController.h"
#import "CTTableView.h"
#import "CustOrder.h"
#import "CTIndicateView.h"
#import "CustOrderAddController.h"
#import "CustOrderSuggestController.h"
#import "CustOrderEditController.h"
#import "CTURLConnection.h"
#import "CustWebService.h"
#import "CustSystemMsg.h"
#import "CustModule.h"
#import "CustCountFlag.h"
#import "CustOrderPayViewController.h"

#define PROCESS_LOADING_LIMIT 0  // 数据加载 - 限量
#define PROCESS_LOADING_BRAND 1  // 数据加载 - 卷烟
#define PROCESS_DELETE_CAR 2     // 数据删除 - 购物车
#define PROCESS_DELETE_ORDER 3   // 数据删除 - 订单
#define PROCESS_SAVING 4         // 数据保存
#define PROCESS_CUSTFLAG 5       // 强制结算标识
#define PROCESS_USER_NORMAL   6   // 正常
#define PROCESS_USER_MANDATORY  7 // 强制



@interface CustOrderDetailController () <UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, CTTableViewDelegate, CTURLConnectionDelegate, CustOrderDelegate, UIActionSheetDelegate, CustSystemMsgDelegate, CustOrderAddControllerDelegate, CustOrderSuggestControllerDelegate, CustOrderEditControllerDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnAdd;      // 添加商品
@property (weak, nonatomic) IBOutlet UIButton *btnSuggest;  // 新品推荐
@property (weak, nonatomic) IBOutlet UIButton *btnSave;     // 保存订单
@property (weak, nonatomic) IBOutlet UIView *limitBg;       // 限量背景
@property (weak, nonatomic) IBOutlet UILabel *limitMonth;   // 剩余月限量
@property (weak, nonatomic) IBOutlet UILabel *limitDay;     // 日限量
@property (weak, nonatomic) IBOutlet UILabel *limitBrand;   // 单品限量
@property (strong, nonatomic) IBOutlet UIView *header;      // 包含按钮、限量、标题的视图
@property (strong, nonatomic) IBOutlet UIView *summary;     // 底部合计视图
@property (weak, nonatomic) IBOutlet UILabel *demand;       // 合计需求
@property (weak, nonatomic) IBOutlet UILabel *order;        // 合计订单
@property (weak, nonatomic) IBOutlet UILabel *cash;         // 合计金额
@property (nonatomic, strong) CTTableView * tv;             // 列表
@property (nonatomic, strong) CTIndicateView * ctIndicate;  // 指示器
@property (nonatomic, strong) NSMutableArray * brandArray;  // 对象数组
@property (nonatomic) int process;                          // 进度标识
@property (nonatomic) BOOL mustRefresh;                     // 标识视图重新显示时是否要自动刷新数据
@property (nonatomic) float orderSum;                       // 合计非异型烟订单
@property (nonatomic) float norOrderSum;                    // 合计异型烟订单
@property (nonatomic) float cashSum;                        // 合计金额
@property (nonatomic, strong) NSString * orderId;           // 订单号
@property (nonatomic, strong) NSString * curOrderId;        // 保存的订单号

// 保存订单
- (IBAction)saveOrder:(id)sender;
// 进入新品推荐
- (IBAction)showSuggest:(id)sender;
// 进入添加商品
- (IBAction)showAdd:(id)sender;
// 删除订单
- (IBAction)deleteOrder:(id)sender;
// 加载数据 flag = 0时显示下拉刷新视图
- (void)loadData:(int)flag;

@end

@implementation CustOrderDetailController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"在线订货";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.limitBg.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CustOrderDetailLimitBg"]];
    self.summary.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CustOrderDetailSummaryBg"]];
    
    [self.btnAdd setBackgroundImage:[[UIImage imageNamed:@"Mask"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)] forState:UIControlStateHighlighted];
    [self.btnSave setBackgroundImage:[[UIImage imageNamed:@"Mask"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)] forState:UIControlStateHighlighted];
    [self.btnSuggest setBackgroundImage:[[UIImage imageNamed:@"Mask"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)] forState:UIControlStateHighlighted];
    self.brandArray = [[NSMutableArray alloc] init];
    self.mustRefresh = YES;
    self.order.adjustsFontSizeToFitWidth = YES;
    self.demand.adjustsFontSizeToFitWidth = YES;
    self.cash.adjustsFontSizeToFitWidth = YES;
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_ORDER_MAIN, CUST_MODULE_ORDER_LIST, nil]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.tv == nil) {
        self.ctIndicate = [[CTIndicateView alloc] initInView:self.view];
        self.ctIndicate.backgroundTouchable = NO;
        self.tv = [[CTTableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - self.summary.bounds.size.height + 4) style:UITableViewStylePlain];
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        self.tv.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tv.pullUpViewEnabled = NO;
        [self.view addSubview:self.tv];
        self.summary.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height - self.summary.frame.size.height / 2);
        [self.view addSubview:self.summary];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.mustRefresh) {
        [self loadData:0];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)saveOrder:(id)sender {
    if (self.ctIndicate.showing) {
        return;
    }
    
    if (self.brandArray.count == 0) {
        self.ctIndicate.text = @"您尚未选购卷烟";
        [self.ctIndicate autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    UIActionSheet * action = [[UIActionSheet alloc] initWithTitle:@"确定保存订单？" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"保存" otherButtonTitles: nil];
    action.tag = 1;
    [action showInView:self.view];
}

- (IBAction)showSuggest:(id)sender {
    CustOrderSuggestController * controller = [[CustOrderSuggestController alloc] initWithNibName:@"CustOrderSuggestController" bundle:nil];
    controller.delegate = self;
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)showAdd:(id)sender {
    CustOrderAddController * controller = [[CustOrderAddController alloc] initWithNibName:nil bundle:nil];
    controller.delegate = self;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)loadData:(int)flag
{
    if (self.ctIndicate.showing) {
        return;
    }
    if (flag == 0) {
        [self.tv showPullDownView];
    }
    self.ctIndicate.text = @"加载中...";
    [self.ctIndicate show];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService custOrderLimitUrl:[CustWebService sharedUser].userId] delegate:self];
    self.process = PROCESS_LOADING_LIMIT;
    [conn start];
}

- (void)deleteOrder:(id)sender
{
    UIActionSheet * action = [[UIActionSheet alloc] initWithTitle:@"确定删除订单？" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"删除订单" otherButtonTitles:nil];
    action.tag = 0;
    [action showInView:self.view];
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark UITableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.brandArray.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return self.header.frame.size.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"BRAND_CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:236.0/255.0 blue:239.0/255.0 alpha:1];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"CustBrandCell" owner:nil options:nil];
        UIView * bg = [elements objectAtIndex:0];
        bg.frame = CGRectMake(0, 0, self.view.frame.size.width, 36);
        bg.tag = 1;
        [cell addSubview:bg];
        
//        UIView * bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tv.frame.size.width, 36)];
//        bg.tag = 1;
//        [cell addSubview:bg];
//        
//        UILabel * brandName = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 165, bg.frame.size.height)];
//        brandName.font = [UIFont systemFontOfSize:13.0f];
//        brandName.adjustsFontSizeToFitWidth = YES;
//        brandName.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
//        brandName.tag = 2;
//        brandName.backgroundColor = [UIColor clearColor];
//        [cell addSubview:brandName];
//        
//        UILabel * demand = [[UILabel alloc] initWithFrame:CGRectMake(170, 0, 50, bg.frame.size.height)];
//        demand.font = [UIFont systemFontOfSize:13.0f];
//        demand.adjustsFontSizeToFitWidth = YES;
//        demand.textAlignment = NSTextAlignmentCenter;
//        demand.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
//        demand.tag = 3;
//        demand.backgroundColor = [UIColor clearColor];
//        [cell addSubview:demand];
//        
//        UILabel * order = [[UILabel alloc] initWithFrame:CGRectMake(220, 0, 50, bg.frame.size.height)];
//        order.font = [UIFont systemFontOfSize:13.0f];
//        order.adjustsFontSizeToFitWidth = YES;
//        order.textAlignment = NSTextAlignmentCenter;
//        order.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
//        order.tag = 4;
//        order.backgroundColor = [UIColor clearColor];
//        [cell addSubview:order];
//        
//        UILabel * cash = [[UILabel alloc] initWithFrame:CGRectMake(270, 0, 50, bg.frame.size.height)];
//        cash.font = [UIFont systemFontOfSize:13.0f];
//        cash.adjustsFontSizeToFitWidth = YES;
//        cash.textAlignment = NSTextAlignmentCenter;
//        cash.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
//        cash.tag = 5;
//        cash.backgroundColor = [UIColor clearColor];
//        [cell addSubview:cash];
        
    }
    
    UIView * bg = [cell viewWithTag:1];
    if (indexPath.row % 2 != 0) {
        bg.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:236.0/255.0 blue:239.0/255.0 alpha:1];
    } else {
        bg.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1];
    }
    
    CustOrder * item = [self.brandArray objectAtIndex:indexPath.row];

    UILabel * brandName = (UILabel *)[cell viewWithTag:2];
    brandName.text = item.brandName;
    
    UILabel * demand = (UILabel *)[cell viewWithTag:3];
    demand.text = item.demand;
    
    UILabel * order = (UILabel *)[cell viewWithTag:4];
    order.text = item.order;
    
    UILabel * cash = (UILabel *)[cell viewWithTag:5];
    cash.text = item.cash;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CustOrderEditController * controller = [[CustOrderEditController alloc] initWithNibName:@"CustOrderEditController" bundle:nil];
    controller.brandArray = self.brandArray;
    controller.currentIndex = indexPath.row;
    controller.delegate = self;
    [self.navigationController pushViewController:controller animated:YES];
    controller.qtyOrder = [NSString stringWithFormat:@"%.0f [异%0.f]", self.orderSum, self.norOrderSum];
    controller.amtOrder = [NSString stringWithFormat:@"%.2f", self.cashSum];
}
#pragma mark-
#pragma mark CTTableView Delegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadData:1];
    return YES;
}

#pragma mark -
#pragma mark CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    if (self.process == PROCESS_LOADING_LIMIT) {
        self.ctIndicate.text = @"限量信息获取失败";
    } else if (self.process == PROCESS_LOADING_BRAND) {
        self.ctIndicate.text = @"限量信息获取失败";
    } else if (self.process == PROCESS_DELETE_CAR) {
        self.ctIndicate.text = @"购物车清空失败";
    } else if (self.process == PROCESS_DELETE_ORDER) {
        self.ctIndicate.text = @"订单删除失败";
    } else if (self.process == PROCESS_SAVING){
        self.ctIndicate.text = @"订单保存失败";
    } else if (self.process == PROCESS_CUSTFLAG){
        self.ctIndicate.text = @"订单保存失败";
    }
    [self.ctIndicate hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    connection.delegate = nil;
    [self.tv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    self.mustRefresh = NO;
    if (PROCESS_LOADING_BRAND == self.process || PROCESS_LOADING_LIMIT == self.process) {
        CustOrder * order = [[CustOrder alloc] init];
        order.delegate = self;
        [order parseData:data];
    }
    else if (PROCESS_DELETE_CAR == self.process || PROCESS_DELETE_ORDER == self.process){
        CustSystemMsg * msg = [[CustSystemMsg alloc] init];
        msg.delegate = self;
        [msg parseData:data];
    }
    else if (PROCESS_SAVING == self.process){
        CustSystemMsg * msg = [[CustSystemMsg alloc] init];
        [msg parseData:data];
        if ([msg.code isEqualToString:@"1"]) {
            self.ctIndicate.showing = NO;
            //厦门才启用判断强制结算
            if ([[CustWebService sharedUser].userCode hasPrefix:@"3502"]) {
                self.curOrderId = [msg.msg substringFromIndex:11];
                [self getCountFlag];
                return;
            }
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"订单保存成功" message:nil delegate:self cancelButtonTitle:@"好" otherButtonTitles: nil];
            alert.tag = 1000;
            [alert show];
        } else {
            self.ctIndicate.text = msg.msg;
            [self.ctIndicate hideWithSate:CTIndicateStateWarning afterDelay:1.5];
        }
    }
    else if (self.process == PROCESS_CUSTFLAG){
        CustCountFlag * msg = [[CustCountFlag alloc] init];
        [msg parseData:data complete:^(CustCountFlag *msg){
            [self.ctIndicate hide];
            if ([msg.value isEqualToString:@"0"]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"尊敬的零售客户，您的订单已经生成，是否立即支付？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"立即支付", nil];
                alert.tag = PROCESS_USER_NORMAL;
                [alert show];
            } else {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"尊敬的零售客户，您的订单已经生成，请于 %@ 之前完成订单支付，否则订单将会失效！",msg.value] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"立即支付", nil];
                alert.tag = PROCESS_USER_MANDATORY;
            [alert show];
            }
        }];
    }
}

#pragma mark -
#pragma mark CustOrder Delegate
- (void)custOrder:(CustOrder *)custOrder didFinishingParse:(NSArray *)array
{
    if (PROCESS_LOADING_LIMIT == self.process && array.count != 0) {
        self.ctIndicate.showing = NO;
        CustOrder * item = [array objectAtIndex:0];
        self.limitBrand.text = item.brandLimit;
        self.limitDay.text = item.dayLimit;
        self.limitMonth.text = item.qtyRemainMonthLimit;
        
        self.process = PROCESS_LOADING_BRAND;
        self.ctIndicate.text = @"加载中...";
        [self.ctIndicate show];
        CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService custOrderListUrl:[CustWebService sharedUser].userId] delegate:self];
        [conn start];
    } else if (PROCESS_LOADING_BRAND == self.process){
        [self.brandArray removeAllObjects];
        [self.brandArray addObjectsFromArray:array];
        [self.ctIndicate hide];
        [self.tv reloadData];
        self.orderSum = custOrder.orderSum;
        self.norOrderSum = custOrder.norOrderSum;
        self.cashSum = custOrder.cashSum;
        self.orderId = custOrder.orderId;
        self.order.text = [NSString stringWithFormat:@"%.0f [异%0.f]", self.orderSum, self.norOrderSum];
        self.cash.text = [NSString stringWithFormat:@"%.2f", self.cashSum];
        
        if (![custOrder.code isEqualToString:@"1"] && ![custOrder.code isEqualToString:@"-5"]) {
            self.ctIndicate.text = custOrder.msg;
            [self.ctIndicate autoHide:CTIndicateStateWarning afterDelay:2.0];
        }
    }
    
}

#pragma mark -
#pragma mark UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 0 && buttonIndex == 0) {
        self.ctIndicate.text = @"正在清空购物车...";
        [self.ctIndicate show];
        self.process = PROCESS_DELETE_CAR;
        NSString * params = [NSString stringWithFormat:@"<UsShoppingCart><userId>%@</userId><brandId></brandId></UsShoppingCart>", [CustWebService sharedUser].userId];
        CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService custOrderDeleteShoppingCarUrl] params:params delegate:self];
        [conn start];
//        [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_ORDER_MAIN, CUST_MODULE_ORDER_DEL, nil]];
        
    } else if (actionSheet.tag == 1 && buttonIndex == 0){
        self.ctIndicate.text = @"正在保存...";
        [self.ctIndicate show];
        self.process = PROCESS_SAVING;
        CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService custOrderSaveUrl:[CustWebService sharedUser].userId] delegate:self];
        [conn start];
//        [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_ORDER_MAIN, CUST_MODULE_ORDER_SAVE, nil]];
    }
}

#pragma mark - getCountFlag
- (void)getCountFlag
{
    CTURLConnection * connflag = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getCountFlag:[CustWebService sharedUser].userCode] delegate:self];
    self.process = PROCESS_CUSTFLAG;
    [connflag start];
}

#pragma mark -
#pragma mark CustSystemMsg Delegate
- (void)custSystemMsgDidFinishingParse:(CustSystemMsg *)custSystemMsg
{
    if (self.process == PROCESS_DELETE_CAR) {
        if ([custSystemMsg.code isEqualToString:@"1"] && ![self.orderId isEqualToString:@""]) {
            self.ctIndicate.text = @"删除成功";
            [self.ctIndicate hideWithSate:CTIndicateStateDone afterDelay:1.0f];
            [self.brandArray removeAllObjects];
            [self.tv reloadData];
            [self performSelector:@selector(loadData:) withObject:nil afterDelay:1.6];
        }
        else if ([custSystemMsg.code isEqualToString:@"1"]){
            self.process = PROCESS_DELETE_ORDER;
            self.ctIndicate.text = @"正在删除订单...";
            CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService custOrderDeleteUrl:[CustWebService sharedUser].userId orderId:self.orderId] delegate:self];
            [conn start];
        }
        else {
            self.ctIndicate.text = @"购物车清空失败";
            [self.ctIndicate hideWithSate:CTIndicateStateWarning afterDelay:1.0];
        }
        
    } else if (self.process == PROCESS_DELETE_ORDER){
        if ([custSystemMsg.code isEqualToString:@"1"]) {
            self.ctIndicate.text = @"删除成功";
            [self.ctIndicate hideWithSate:CTIndicateStateDone afterDelay:1.0f];
            [self.brandArray removeAllObjects];
            [self.tv reloadData];
            [self performSelector:@selector(loadData:) withObject:nil afterDelay:1.6];
        } else {
            self.ctIndicate.text = @"订单删除失败";
            [self.ctIndicate hideWithSate:CTIndicateStateWarning afterDelay:1.0];
        }
    }
}

#pragma mark - 
#pragma mark CustOrderAddController Delegate
- (void)custOrderAddControllerDidBrandAdded:(CustOrderAddController *)controller
{
    self.mustRefresh = YES;
}

#pragma mark -
#pragma mark CustOrderSuggestController Delegate
- (void)custOrderSuggestControllerDidBrandAdded:(CustOrderSuggestController *)controller
{
    self.mustRefresh = YES;
}

#pragma mark -
#pragma mark CustOrderEditController Delegate
- (void)custOrderEditController:(CustOrderEditController *)controller didDeleteBrandAtIndex:(int)index
{
    CustOrder * order = [self.brandArray objectAtIndex:index];
    
    if (order.norFlag == 0) {
        self.orderSum -= [order.order floatValue];
    } else {
        self.norOrderSum -= [order.order floatValue];
    }
    
    self.cashSum -= [order.cash floatValue];
    
    self.order.text = [NSString stringWithFormat:@"%.0f [异%0.f]", self.orderSum, self.norOrderSum];
    self.cash.text = [NSString stringWithFormat:@"%.2f", self.cashSum];
    
    [self.brandArray removeObjectAtIndex:index];
    [self.tv reloadData];
    
    controller.qtyOrder = [NSString stringWithFormat:@"%.0f [异%0.f]", self.orderSum, self.norOrderSum];
    controller.amtOrder = [NSString stringWithFormat:@"%.2f", self.cashSum];
}

- (void)custOrderEditController:(CustOrderEditController *)controller didSubmitDemandAtIndex:(int)index newOrder:(CustOrder *)newOrder
{
    CustOrder * order = [self.brandArray objectAtIndex:index];
    
    if (order.norFlag == 0) {
        self.orderSum -= [order.order floatValue];
        self.orderSum += [newOrder.order floatValue];
    } else {
        self.norOrderSum -= [order.order floatValue];
        self.norOrderSum += [newOrder.order floatValue];
    }
    
    self.cashSum -= [order.cash floatValue];
    self.cashSum += [newOrder.cash floatValue];
    
    self.order.text = [NSString stringWithFormat:@"%.0f [异%0.f]", self.orderSum, self.norOrderSum];
    self.cash.text = [NSString stringWithFormat:@"%.2f", self.cashSum];
    
    [self.tv reloadData];
    
    controller.qtyOrder = [NSString stringWithFormat:@"%.0f [异%0.f]", self.orderSum, self.norOrderSum];
    controller.amtOrder = [NSString stringWithFormat:@"%.2f", self.cashSum];
}

#pragma mark -
#pragma mark UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == PROCESS_USER_NORMAL && buttonIndex == 1) {
        CustOrderPayViewController *controller = [[CustOrderPayViewController alloc] initWithNibName:@"CustOrderPayViewController" bundle:nil];
        controller.orderId = self.curOrderId;
        [self.navigationController pushViewController:controller animated:YES];
    } else if (alertView.tag == PROCESS_USER_MANDATORY && buttonIndex == 1) {
        CustOrderPayViewController *controller = [[CustOrderPayViewController alloc] initWithNibName:@"CustOrderPayViewController" bundle:nil];
        controller.orderId = self.curOrderId;
        [self.navigationController pushViewController:controller animated:YES];
    } else if (alertView.tag == 1000 && buttonIndex == 1) {
        return;
    }
}
@end
