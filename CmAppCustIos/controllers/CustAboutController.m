//
//  CustAboutController.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-2-10.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustAboutController.h"
#import <QuartzCore/QuartzCore.h>
#import "CustWebService.h"
#import "CustAboutInfo.h"

@interface CustAboutController ()

@property (nonatomic, strong) IBOutlet UIView * contentView;
@property (nonatomic, strong) IBOutlet UIScrollView * sv;
@property (nonatomic, strong) IBOutlet UILabel * appName;
@property (nonatomic, strong) IBOutlet UILabel * appVersion;
@property (nonatomic, strong) IBOutlet UILabel * telLab;
@property (nonatomic, strong) IBOutlet UILabel * webLab;
@property (nonatomic, strong) IBOutlet UILabel * adrLab;
@property (nonatomic, strong) IBOutlet UILabel * zipLab;

- (void)back;

@end

@implementation CustAboutController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"关于";
        self.view.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:239.0/255.0 blue:238.0/255.0 alpha:1];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)initUI
{
    self.contentView.layer.borderWidth = 0.5;
    self.contentView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.contentView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    self.contentView.layer.shadowOpacity = 0.1;
    self.contentView.layer.shadowRadius = 1;
    
    NSDictionary* infoDict =[[NSBundle mainBundle] infoDictionary];
    self.appVersion.text = [NSString stringWithFormat:@"iPhone V%@", [infoDict objectForKey:@"CFBundleShortVersionString"]];
    
    [self.view addSubview:self.sv];
    
    self.sv.frame = self.view.bounds;
    self.sv.contentSize = CGSizeMake(self.sv.frame.size.width, 480);
    CustAboutInfo *info = [[CustAboutInfo alloc] init];
    [info validate:[CustWebService sharedUser].userCode complete:^(CustAboutInfo *item) {
        self.telLab.text = item.tel;
        self.adrLab.text = item.addr;
        self.webLab.text = item.web;
        self.zipLab.text = item.zip;
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
