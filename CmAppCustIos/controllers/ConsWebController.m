//
//  ConsWebController.m
//  CmAppCustIos
//
//  Created by Chenhc on 15/3/23.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "ConsWebController.h"

@interface ConsWebController () <UIWebViewDelegate>

@property (nonatomic, strong) UIWebView * webView;

- (void)back;

@end

@implementation ConsWebController

- (void)initUI
{
    
    
    self.webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    self.webView.delegate = self;
    self.webView.backgroundColor = [UIColor clearColor];
    self.webView.scalesPageToFit = YES;
    self.webView.dataDetectorTypes = UIDataDetectorTypeNone;
    self.webView.mediaPlaybackRequiresUserAction = YES;
    self.webView.allowsInlineMediaPlayback = YES;
    
    for (UIView * subView in self.webView.subviews) {
        if ([subView isKindOfClass:[UIScrollView class]]) {
            ((UIScrollView *)subView).bounces = NO;
        }
    }
    [self.view addSubview:self.webView];
    self.indicator.text = @"加载中..";
    [self.indicator show];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.mainUrl]]];
}

- (void)back
{
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }
}

#pragma mark -
#pragma mark UIWebView Delegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    NSLog(@"%@", error);
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    self.title = [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    [self.indicator hide];
    if ([self.webView canGoBack]) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"后退" style:UIBarButtonItemStyleBordered target:self action:@selector(back)];
    } else {
        self.navigationItem.rightBarButtonItem = nil;
    }
}

@end
