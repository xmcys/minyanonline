//
//  CustAllController.m
//  CmAppCustIos
//
//  Created by Arcesaka on 14/8/25.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustAllController.h"
#import "CTTableView.h"
#import "CTURLConnection.h"
#import "CTIndicateView.h"
#import "CustServiceMsgList.h"
#import "CustWebService.h"
#import "CustTrackAssessController.h"
#import "CustTrackDetailController.h"

@interface CustAllController ()<UITextFieldDelegate,CTTableViewDelegate,UITableViewDataSource,UITableViewDelegate,CTURLConnectionDelegate,UIScrollViewDelegate>

@property (nonatomic, strong) IBOutlet UIView * header;
@property (nonatomic, strong) IBOutlet UIView * searchView;
@property (nonatomic, strong) IBOutlet UITextField * textField;
@property (nonatomic, strong) IBOutlet UIButton * searchbtn;
@property (nonatomic, strong) CTTableView * tv;
@property (nonatomic, strong) CTIndicateView   * indicator;
@property (nonatomic, strong) NSMutableArray  * array;
@property (nonatomic, strong) NSMutableArray  * tmparray;

- (IBAction)searchClick:(id)sender;

@end

@implementation CustAllController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.array = [[NSMutableArray alloc] init];
    self.tmparray  = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.header.frame = CGRectMake(0, 0, self.view.frame.size.width, self.header.frame.size.height);
    self.header.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CustAllSearchBg"]];
    self.textField.delegate = self;
    self.searchView.layer.cornerRadius = 5.0f;
    self.searchView.layer.masksToBounds = YES;
    [self.view addSubview:self.header];
    if (self.tv == nil) {
        self.indicator = [[CTIndicateView alloc] initInView:self.view];
        self.indicator.backgroundTouchable = YES;
        self.tv = [[CTTableView alloc] initWithFrame:CGRectMake(0, self.header.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - self.header.frame.size.height)];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        [self.tv setPullUpViewEnabled:NO];
        [self.view addSubview:self.tv];
        [self loadData];
    }
}

- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"加载中...";
    [self.indicator show];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getServiceAllInfo:[CustWebService sharedUser].userCode] delegate:self];
    [conn start];
    return;
}

- (IBAction)searchClick:(id)sender
{
    [self.textField resignFirstResponder];
    if (self.textField.text.length == 0) {
        return;
    }
    NSString * str = self.textField.text;
    [self.array removeAllObjects];
    for (int i = 0; i < self.tmparray.count; i ++) {
        CustServiceMsgList * msg = [self.tmparray objectAtIndex:i];
        if ([str isEqualToString:msg.demandTitle] || [str isEqualToString:msg.demandStatus]) {
            [self.array addObject:msg];
        }
    }
    [self.tv reloadData];
    if (self.array.count == 0) {
        self.indicator.text = @"没有匹配的内容";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
    }
}
#pragma mark - TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.textField resignFirstResponder];
    if (self.textField.text.length == 0) {
        return 0;
    }
    NSString * str = self.textField.text;
    [self.array removeAllObjects];
    for (int i = 0; i < self.tmparray.count; i ++) {
        CustServiceMsgList * msg = [self.tmparray objectAtIndex:i];
        if ([str isEqualToString:msg.demandTitle] || [str isEqualToString:msg.demandStatus]) {
            [self.array addObject:msg];
        }
    }
    [self.tv reloadData];
    if (self.array.count == 0) {
        self.indicator.text = @"没有匹配的内容";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (self.textField.text.length == 0 && self.array.count <= 1) {
        [self loadData];
    }
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [self loadData];
    return YES;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textField resignFirstResponder];
}

#pragma mark - CTTableView DataSource & Delegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadData];
    return YES;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 88;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        NSArray * elements = [[NSBundle mainBundle] loadNibNamed:@"CustAllCell" owner:nil options:nil];
        UIView * view = [elements objectAtIndex:0];
        view.frame = CGRectMake(5, 8, self.view.frame.size.width - 5 * 2, 80);
        view.layer.borderColor = [UIColor colorWithRed:201.0/255.0 green:201.0/255.0 blue:201.0/255.0 alpha:1].CGColor;
        view.layer.borderWidth = 1;
        [cell addSubview:view];
    }
    UILabel * titlelab = (UILabel *)[cell viewWithTag:1];
    UILabel * statelab = (UILabel *)[cell viewWithTag:2];
    UILabel * datelab = (UILabel *)[cell viewWithTag:4];
    CustServiceMsgList *msg = [self.array objectAtIndex:indexPath.row];
    titlelab.text = msg.demandTitle;
    statelab.text = msg.demandStatus;
    datelab.text = [NSString stringWithFormat:@"提交时间：%@",msg.demandTime];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CustServiceMsgList *msg = [self.array objectAtIndex:indexPath.row];
    if (!msg) return;
    if ([msg.demandStatus isEqualToString:@"未处理"] || [msg.demandStatus isEqualToString:@"处理中"]) {
        CustTrackDetailController *controller = [[CustTrackDetailController alloc] init];
        controller.msg = msg;
        controller.title = @"服务受理详情";
        [self.nc pushViewController:controller animated:YES];
    } else if ([msg.demandStatus isEqualToString:@"待评价"] || [msg.demandStatus isEqualToString:@"已评价"]) {
        CustTrackAssessController *controller = [[CustTrackAssessController alloc] init];
        controller.msg = msg;
        controller.title = @"服务受理详情";
        [self.nc pushViewController:controller animated:YES];
    }
}
#pragma mark - Scrollview Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}
#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0f];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    CustServiceMsgList * msg = [[CustServiceMsgList alloc] init];
    [msg parseData:data complete:^(NSArray * array){
        [self.indicator hide];
        [self.array removeAllObjects];
        [self.array addObjectsFromArray:array];
        
        self.array = [NSMutableArray arrayWithArray:[self.array sortedArrayUsingFunction:sortType context:nil]];

        [self.tmparray removeAllObjects];
        [self.tmparray addObjectsFromArray:array];
        [self.tv reloadData];
    }];
}
#pragma mark - SortDateType
NSInteger sortType(id s1,id s2,void *cha)
{
    CustServiceMsgList *sx1 = (CustServiceMsgList *)s1;
    CustServiceMsgList *sx2 = (CustServiceMsgList *)s2;
    NSDate *date1 = (NSDate *)sx1.demandTime;
    NSDate *date2 = (NSDate *)sx2.demandTime;
    
    NSComparisonResult result = [date1 compare:date2];
    
    if (result == NSOrderedAscending) {
        return NSOrderedDescending;
    } else if (result == NSOrderedDescending) {
        return NSOrderedAscending;
    } else {
        return NSOrderedSame;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
