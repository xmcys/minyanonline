//
//  CustNewIvtViewController.m
//  CmAppCustIos
//
//  Created by ArcEsaka on 15/3/19.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustNewIvtViewController.h"
#import "CTTableView.h"
#import "CustWebService.h"
#import "CustIvt.h"

#define PROCESS_GET_SIGNAL  1
#define PROCESS_GET_ALL    2

@interface CustNewIvtViewController ()<UITableViewDataSource, UITableViewDelegate, CTTableViewDelegate, UITextFieldDelegate,CTURLConnectionDelegate>

@property (nonatomic, strong) IBOutlet UIView *headView;
@property (nonatomic, strong) IBOutlet UILabel *totolLab;
@property (nonatomic, strong) IBOutlet UITextField *textField;
@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) CTTableView *tv;

- (IBAction)searckClick:(id)sender;

@end

@implementation CustNewIvtViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"最新库存";
    self.array = [[NSMutableArray alloc] init];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)searckClick:(id)sender {
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"加载中...";
    [self.indicator show];
    NSString *params = [NSString stringWithFormat:@"<MyCustProductsStock><productName>%@</productName><userCode>%@</userCode></MyCustProductsStock>", self.textField.text, [CustWebService sharedUser].userCode];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService getShopMyConsumerInfo] params:params delegate:self];
    conn.tag = PROCESS_GET_SIGNAL;
    [conn start];
}

- (void)initUI {
    self.tv = [[CTTableView alloc] initWithFrame:CGRectMake(0, self.headView.frame.size.height + 10, self.view.frame.size.width, self.view.frame.size.height -  self.headView.frame.size.height)];
    self.tv.backgroundColor = [UIColor clearColor];
    self.tv.delegate = self;
    self.tv.dataSource = self;
    self.tv.ctTableViewDelegate = self;
    self.tv.pullUpViewEnabled = NO;
    [self.view addSubview:self.tv];
    [self loadData];
}

- (void)loadData {
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"加载中...";
    [self.indicator show];
    NSString *params = [NSString stringWithFormat:@"<MyCustProductsStock><userCode>%@</userCode></MyCustProductsStock>", [CustWebService sharedUser].userCode];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService getShopMyConsumerInfo] params:params delegate:self];
    conn.tag = PROCESS_GET_ALL;
    [conn start];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

#pragma mark - UITextField - Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.textField resignFirstResponder];
    if (self.indicator.showing) {
        return  0;
    }
    self.indicator.text = @"加载中...";
    [self.indicator show];
    NSString *params = [NSString stringWithFormat:@"<MyCustProductsStock><productName>%@</productName><userCode>%@</userCode></MyCustProductsStock>", self.textField.text, [CustWebService sharedUser].userCode];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService getShopMyConsumerInfo] params:params delegate:self];
    conn.tag = PROCESS_GET_SIGNAL;
    [conn start];
    return YES;
}

#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark - CTTableView Delegate & DataSource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadData];
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [[[NSBundle mainBundle] loadNibNamed:@"CustIvtCell" owner:self options:nil] lastObject];
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];

    UIView *bg = (UIView *)[cell viewWithTag:8];
    bg.frame = CGRectMake(5, 0, self.tv.frame.size.width - 5 * 2, 70);
    UIImageView * line = (UIImageView *)[cell viewWithTag:11];
    UIImageView * line2 = (UIImageView *)[cell viewWithTag:12];
    
    UILabel *labname = (UILabel *)[cell viewWithTag:13];
    UILabel *labtrpri = (UILabel *)[cell viewWithTag:14];
    UILabel *labstock = (UILabel *)[cell viewWithTag:15];
    UILabel *labrepri = (UILabel *)[cell viewWithTag:16];
    
    CustIvt *ivt = [self.array objectAtIndex:indexPath.row];
    labname.text = ivt.productName;
    labtrpri.text = ivt.tradePrice;
    labstock.text = ivt.stock;
    labrepri.text = ivt.retailPrice;
    
//    if  (indexPath.row != self.array.count -1) {
//        line2.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"line"]];
//    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.indicator.text = @"加载失败";
    [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    if (connection.tag ==  PROCESS_GET_SIGNAL) {
        CustIvt *ivt = [[CustIvt alloc] init];
        [ivt parseData:data complete:^(NSArray *array) {
            [self.indicator hide];
            [self.array removeAllObjects];
            [self.array addObjectsFromArray:array];
            [self.tv reloadData];
            self.totolLab.text = [NSString stringWithFormat:@"共%ld条记录", self.array.count];
        }];
    } else {
        CustIvt *ivt = [[CustIvt alloc] init];
        [ivt parseData:data complete:^(NSArray *array) {
            [self.indicator hide];
            [self.array removeAllObjects];
            [self.array addObjectsFromArray:array];
            [self.tv reloadData];
            self.totolLab.text = [NSString stringWithFormat:@"共%ld条记录", self.array.count];
        }];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
