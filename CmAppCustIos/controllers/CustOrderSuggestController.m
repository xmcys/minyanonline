//
//  CustOrderSuggestController.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-21.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustOrderSuggestController.h"
#import "CTCheckBox.h"
#import "CTIndicateView.h"
#import "CustOrder.h"
#import "CTTableView.h"
#import "CTURLConnection.h"
#import "CustWebService.h"
#import "CustSystemMsg.h"
#import "CustModule.h"

#define PROCESS_LOADING 0
#define PROCESS_SAVE 1

@interface CustOrderSuggestController () <UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, CTTableViewDelegate, CTCheckBoxDelegate, CTURLConnectionDelegate, CustOrderDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *brandInfo;
@property (strong, nonatomic) UIControl *cover;
@property (weak, nonatomic) IBOutlet UILabel *brandName;
@property (weak, nonatomic) IBOutlet UILabel *retailPrice;
@property (weak, nonatomic) IBOutlet UILabel *tradePrice;
@property (weak, nonatomic) IBOutlet UILabel *qtyTar;
@property (weak, nonatomic) IBOutlet UILabel *totalScore;
@property (nonatomic, strong) UIView * footer;
@property (nonatomic, strong) CTIndicateView * ctIndicate;
@property (nonatomic, strong) CTTableView * cttv;
@property (nonatomic, strong) NSMutableArray * brandArray;
@property (nonatomic, strong) NSMutableArray * selectedArray;
@property (nonatomic, weak) CustOrder * processBrand; // 正在提交的卷烟
@property (nonatomic) int process;

- (void)loadData:(int)flag;
- (void)saveConfirm;
- (void)showOrHideBrandInfo;
- (void)submit;

@end

@implementation CustOrderSuggestController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"新品推荐";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton * btnSave = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSave.titleLabel.font = [UIFont systemFontOfSize:16.5f];
    [btnSave setTitle:@"  保存  " forState:UIControlStateNormal];
    [btnSave setTitle:@"  保存  " forState:UIControlStateHighlighted];
    [btnSave setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnSave setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [btnSave sizeToFit];
    [btnSave addTarget:self action:@selector(saveConfirm) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSave];
    

    self.brandArray = [[NSMutableArray alloc] init];
    self.selectedArray = [[NSMutableArray alloc] init];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_ORDER_MAIN, CUST_MODULE_ORDER_SUG, nil]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.footer == nil) {
        self.footer = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 46, self.view.bounds.size.width, 46)];
        self.footer.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CustOrderDetailSummaryBg"]];
        
        CTCheckBox * ctcb = [[CTCheckBox alloc] initWithFrame:CGRectMake(11, (self.footer.frame.size.height - 36) / 2 + 3, 100, 36)];
        ctcb.delegate = self;
        ctcb.text = @"全选";
        ctcb.textHidden = NO;
        ctcb.specialId = @"checkAll";
        [self.footer addSubview:ctcb];
        [self.view addSubview:self.footer];
    }
    
    if (self.cttv == nil) {
        self.cttv = [[CTTableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - 46 + 5) style:UITableViewStylePlain];
        self.cttv.delegate = self;
        self.cttv.dataSource = self;
        self.cttv.ctTableViewDelegate = self;
        self.cttv.pullUpViewEnabled = NO;
        self.cttv.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.cttv];
    }
    
    if (self.ctIndicate == nil) {
        self.ctIndicate = [[CTIndicateView alloc] initInView:self.view];
        self.ctIndicate.backgroundTouchable = NO;
    }
    
    if (self.cover == nil) {
        self.cover = [[UIControl alloc] initWithFrame:self.view.bounds];
        self.cover.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
        [self.cover addTarget:self action:@selector(showOrHideBrandInfo) forControlEvents:UIControlEventTouchUpInside];
        self.cover.hidden = YES;
        [self.view addSubview:self.cover];
        
        self.brandInfo.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height  / 2 - 44);
        self.brandInfo.hidden = YES;
        [self.view addSubview:self.brandInfo];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.brandArray.count == 0) {
        [self loadData:0];
    }
}

- (void)loadData:(int)flag
{
    if (self.ctIndicate.showing) {
        return;
    }
    if (flag == 0) {
        [self.cttv showPullDownView];
    }
    self.ctIndicate.text = @"加载中...";
    [self.ctIndicate show];
    self.process = PROCESS_LOADING;
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService custOrderSuggestListUrl:[CustWebService sharedUser].userId] delegate:self];
    [conn start];
}

- (void)saveConfirm
{
    [self.selectedArray removeAllObjects];
    int count = 0;
    NSString * names = @"";
    for (int i = 0; i < self.brandArray.count; i++) {
        CustOrder * order = [self.brandArray objectAtIndex:i];
        if (order.selectedFlag == 0 && order.added) {
            [self.selectedArray addObject:order];
            count++;
            names = [NSString stringWithFormat:@"%@\n%@", names, order.brandName];
        }
    }
    if (count == 0) {
        self.ctIndicate.text = @"您尚未添加任何品牌规格";
        [self.ctIndicate autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"确定添加以下 %d 种品牌规格？", count] message:names delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
}

- (void)showOrHideBrandInfo
{
    self.brandInfo.hidden = !self.brandInfo.hidden;
    self.cover.hidden = !self.cover.hidden;
    [self.view bringSubviewToFront:self.cover];
    [self.view bringSubviewToFront:self.brandInfo];
}

- (void)submit
{
    NSString * params = [NSString stringWithFormat:@"<UsShoppingCart><userId>%@</userId><brandId>%@</brandId><demandQty>0</demandQty></UsShoppingCart>", [CustWebService sharedUser].userId, self.processBrand.brandId];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService custOrderChangeUrl] params:params delegate:self];
    self.process = PROCESS_SAVE;
    [conn start];
}

#pragma mark -
#pragma mark CTCheckBox Delegate
- (void)ctCheckBox:(CTCheckBox *)checkBox didValueChanged:(BOOL)checked
{
    if ([checkBox.specialId isEqualToString:@"checkAll"]) {
        if (checked) {
            checkBox.text = @"全不选";
        } else {
            checkBox.text = @"全选";
        }
        for (CustOrder * item in self.brandArray) {
            if (item.selectedFlag == 0) {
                item.added = checked;
            } else {
                item.added = YES;
            }
        }
        [self.cttv reloadData];
    } else {
        CustOrder * item = [self.brandArray objectAtIndex:[checkBox.specialId intValue]];
        if (item.selectedFlag == 1) {
            self.ctIndicate.text = @"不能对已添加的规格进行操作";
            [self.ctIndicate autoHide:CTIndicateStateWarning afterDelay:1.0];
            [checkBox setChecked:YES];
        } else {
            item.added = checkBox.checked;
        }
        
    }
    
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.cttv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.cttv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark UITableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.brandArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"BRAND_CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:236.0/255.0 blue:239.0/255.0 alpha:1];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIView * bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.cttv.frame.size.width, 36)];
        bg.tag = 1;
        [cell addSubview:bg];
        
        CTCheckBox * cb = [[CTCheckBox alloc] initWithFrame:CGRectMake(5, 0, 36, bg.frame.size.height)];
        cb.tag = 2;
        cb.delegate = self;
        [cell addSubview:cb];
        
        UILabel * brandName = [[UILabel alloc] initWithFrame:CGRectMake(5 + 36 + 5, 0, bg.frame.size.width - 5 - 36 - 5 - 5 - 50, bg.frame.size.height)];
        brandName.font = [UIFont systemFontOfSize:13.0f];
        brandName.adjustsFontSizeToFitWidth = YES;
        brandName.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
        brandName.tag = 3;
        brandName.backgroundColor = [UIColor clearColor];
        [cell addSubview:brandName];
        
        UILabel * cash = [[UILabel alloc] initWithFrame:CGRectMake(bg.frame.size.width - 50 - 5, 0, 50, bg.frame.size.height)];
        cash.font = [UIFont systemFontOfSize:13.0f];
        cash.adjustsFontSizeToFitWidth = YES;
        cash.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
        cash.tag = 4;
        cash.backgroundColor = [UIColor clearColor];
        [cell addSubview:cash];
        
    }
    
    UIView * bg = [cell viewWithTag:1];
    if (indexPath.row % 2 != 0) {
        bg.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:236.0/255.0 blue:239.0/255.0 alpha:1];
    } else {
        bg.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1];
    }
    
    CustOrder * item = [self.brandArray objectAtIndex:indexPath.row];
    
    CTCheckBox * cb = (CTCheckBox *)[cell viewWithTag:2];
    [cb setChecked:item.added];
    cb.specialId = [NSString stringWithFormat:@"%d", (int)indexPath.row];
    
    UILabel * brandName = (UILabel *)[cell viewWithTag:3];
    brandName.text = item.brandName;
    
    UILabel * cash = (UILabel *)[cell viewWithTag:4];
    cash.text = [NSString stringWithFormat:@"￥%@", item.tradePrice];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CustOrder * order = [self.brandArray objectAtIndex:indexPath.row];
    self.brandName.text = order.brandName;
    self.tradePrice.text = [NSString stringWithFormat:@"批发价：%@元", order.tradePrice];
    self.retailPrice.text = [NSString stringWithFormat:@"零售价：%@元", order.retailPrice];
    self.qtyTar.text = [NSString stringWithFormat:@"焦油含量：%@mg", order.qtyTar];
    self.totalScore.text = [NSString stringWithFormat:@"综合评分：%@分", order.totalScore];
    [self showOrHideBrandInfo];
}

#pragma mark-
#pragma mark CTTableView Delegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadData:1];
    return YES;
}

#pragma mark -
#pragma mark CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    if (self.process == PROCESS_LOADING) {
        self.ctIndicate.text = @"加载失败";
    } else {
        self.ctIndicate.text = @"保存失败";
    }
    [self.ctIndicate hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    
    connection.delegate = nil;
    [self.cttv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (self.process == PROCESS_LOADING) {
        CustOrder * order = [[CustOrder alloc] init];
        order.delegate = self;
        [order parseData:data];
    } else {
        CustSystemMsg * msg = [[CustSystemMsg alloc] init];
        [msg parseData:data];
        if (msg.code.length != 0) {
            self.ctIndicate.text = msg.msg;
            [self.ctIndicate hideWithSate:CTIndicateStateWarning afterDelay:1.5];
            return;
        }
        self.processBrand.selectedFlag = 1;
        [self.selectedArray removeObject:self.processBrand];
        if (self.selectedArray.count != 0) {
            self.processBrand = [self.selectedArray objectAtIndex:0];
            [self submit];
        } else {
            self.ctIndicate.text = @"添加成功";
            [self.ctIndicate hideWithSate:CTIndicateStateDone afterDelay:1.0];
            if ([self.delegate respondsToSelector:@selector(custOrderSuggestControllerDidBrandAdded:)]) {
                [self.delegate custOrderSuggestControllerDidBrandAdded:self];
            }
        }
    }
}

#pragma mark -
#pragma mark CustOrder Delegate
- (void)custOrder:(CustOrder *)custOrder didFinishingParse:(NSArray *)array
{
    [self.brandArray removeAllObjects];
    [self.brandArray addObjectsFromArray:array];
    [self.ctIndicate hide];
    [self.cttv reloadData];
}

#pragma mark -
#pragma mark UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1 && self.selectedArray.count != 0) {
        self.processBrand = [self.selectedArray objectAtIndex:0];
        self.ctIndicate.text = @"请稍后...";
        [self.ctIndicate show];
        [self submit];
    }
    
}

@end
