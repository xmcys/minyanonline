//
//  CustMainController.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-20.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTTabBarController.h"

@class CustMainController;

@protocol CustMainControllerDelegate <NSObject>

- (void)custMainControllerLeftMenuButtonDidClicked:(CustMainController *)controller;
- (void)custMainControllerRightMenuButtonDidClicked:(CustMainController *)controller;

@end


@interface CustMainController : CTTabBarController

@property (nonatomic, weak) id<CustMainControllerDelegate> delegate;

// 显示或隐藏左侧侧滑菜单
- (void)showOrHideLeftMenu;

@end
