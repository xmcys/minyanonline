//
//  ConsMsgDetailController.h
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-3.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@class ConsMsg;
@class CustActivity;
@class ConsClass;
@class ConsOnSaleActivity;

@interface ConsMsgDetailController : CTViewController

// 从消息进来
- (void)setConsMsg:(ConsMsg *)item;
// 从活动进来
- (void)setConsAct:(CustActivity *)item;
// 从烟草学堂进来
- (void)setConsClass:(ConsClass *)item;
// 从促销活动进来
- (void)setConsOnSaleAct:(ConsOnSaleActivity *)item;

@end
