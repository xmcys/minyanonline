//
//  CustOrderHistoryDetailController.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-24.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@interface CustOrderHistoryDetailController : CTViewController

@property (nonatomic, strong) NSString * orderId;
@property (nonatomic, strong) NSString * date;

@end
