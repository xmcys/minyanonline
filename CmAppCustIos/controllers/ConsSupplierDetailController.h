//
//  ConsSupplierController.h
//  CmAppConsumerIos
//
//  Created by 宏超 陈 on 14-5-20.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@class ConsSupplier;

@interface ConsSupplierDetailController : CTViewController

@property (nonatomic, strong) ConsSupplier * supplier;

@end
