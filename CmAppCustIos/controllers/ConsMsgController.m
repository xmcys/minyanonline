//
//  ConsMsgController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-2.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsMsgController.h"
#import "ConsMsgDetailController.h"
#import "CTTableView.h"
#import "ConsMsg.h"
#import "CustWebService.h"
#import "CustSystemMsg.h"

#define PROCESS_LOAD   0
#define PROCESS_UPDATE 1

@interface ConsMsgController () <CTTableViewDelegate, UITableViewDataSource, UITableViewDelegate, ConsMsgDelegate, UIScrollViewDelegate, ConsMsgDelegate>

@property (nonatomic, strong) CTTableView * tv;
@property (nonatomic, strong) NSMutableArray * data;

- (void)loadData;
- (void)updateMessage:(NSString *)msgId;

@end

@implementation ConsMsgController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"我的消息";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.data = [[NSMutableArray alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData) name:NOTIFCATION_LOGIN object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tv.separatorColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1];
        self.tv.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        self.tv.ctTableViewDelegate = self;
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.pullUpViewEnabled = NO;
        [self.view addSubview:self.tv];
        
        [self loadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)loadData
{
    if (![CustWebService sharedUser].isLogin) {
        
        self.indicator.text = @"您尚未登录";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:NOT_LOGIN_NOTICE_INTERVAL complete:^{
            [self.tv reloadData];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFCATION_NOT_LOGIN object:nil];
        }];
        return;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_XX, CUST_MODULE_XX, nil]];
    
    if (self.indicator.showing) {
        return;
    }
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService messageListUrl] delegate:self];
    conn.tag = PROCESS_LOAD;
    [conn start];
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
}

- (void)updateMessage:(NSString *)msgId
{
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService messageUpdateUrl] params:[NSString stringWithFormat:@"<Message><userId>%@</userId><msgId>%@</msgId></Message>", [CustWebService sharedUser].userId, msgId] delegate:self];
    conn.tag = PROCESS_UPDATE;
    [conn start];
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark CTTableView DataSource & Delegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadData];
    return YES;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"MSG_CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        UILabel * publishTime = [[UILabel alloc] init];
        publishTime.backgroundColor = [UIColor clearColor];
        publishTime.text = @"2000-01-01 00:00:00";
        publishTime.font = [UIFont systemFontOfSize:12.0f];
        publishTime.textColor = [UIColor colorWithRed:124.0/255.0 green:134.0/255.0 blue:146.0/255.0 alpha:1];
        [publishTime sizeToFit];
        publishTime.tag = 3;
        publishTime.frame = CGRectMake(tableView.frame.size.width - 5 - publishTime.frame.size.width, 80 - publishTime.frame.size.height - 5, publishTime.frame.size.width, publishTime.frame.size.height);
        
        [cell addSubview:publishTime];
        
        UIImageView * imgColck = [[UIImageView alloc] initWithFrame:CGRectMake(publishTime.frame.origin.x - 5 - 9, publishTime.frame.origin.y + 3, 9, 9)];
        imgColck.contentMode = UIViewContentModeScaleAspectFill;
        imgColck.image = [UIImage imageNamed:@"Clock"];
        [cell addSubview:imgColck];
        
        
        UIImageView * imgNew = [[UIImageView alloc] initWithFrame:CGRectMake(imgColck.frame.origin.x - 5 - 19, publishTime.frame.origin.y + 2, 19, 11)];
        imgNew.contentMode = UIViewContentModeScaleAspectFill;
        imgNew.tag = 2;
        imgNew.image = [UIImage imageNamed:@"New"];
        [cell addSubview:imgNew];
        
        UILabel * title = [[UILabel alloc] initWithFrame:CGRectMake(13, 7, tableView.frame.size.width - 20, publishTime.frame.origin.y - 5)];
        title.backgroundColor = [UIColor clearColor];
        title.text = @"";
        title.font = [UIFont systemFontOfSize:15.0f];
        title.textColor = [UIColor blackColor];
        title.tag = 1;
        title.numberOfLines = 2;
        [cell addSubview:title];
    }
    
    ConsMsg * item = [self.data objectAtIndex:indexPath.row];
    
    UILabel * title = (UILabel *)[cell viewWithTag:1];
    title.text = item.msgTitle;
    
    UILabel * publishTime = (UILabel *)[cell viewWithTag:3];
    publishTime.text = item.publishTime;
    
    UIImageView * imgNew = (UIImageView *)[cell viewWithTag:2];
    if ([item.readFlag isEqualToString:@"0"]) {
        imgNew.hidden = NO;
    } else {
        imgNew.hidden = YES;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ConsMsg * item = [self.data objectAtIndex:indexPath.row];
    
    item.readFlag = @"1";
    [tableView reloadData];
    [self updateMessage:item.msgId];
    
    ConsMsgDetailController * controller = [[ConsMsgDetailController alloc] initWithNibName:nil bundle:nil];
    [controller setConsMsg:item];
    [self.navigationController pushViewController:controller animated:YES];
    
    
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    if (connection.tag == PROCESS_LOAD) {
        self.indicator.text = @"加载失败";
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
        [self.tv reloadData];
    }
    
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_LOAD) {
        ConsMsg * item = [[ConsMsg alloc] init];
        item.delegate = self;
        [item parseData:data];
    } else {
        CustSystemMsg * sysmsg = [[CustSystemMsg alloc] init];
        [sysmsg parseData:data];
        if ([sysmsg.code isEqualToString:@"1"]) {
            UNREAD_MESSAGE--;
            if (UNREAD_MESSAGE < 0) {
                UNREAD_MESSAGE = 0;
            }
            // 发送未读消息通知
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFCATION_NEW_MESSAGE object:[NSString stringWithFormat:@"%d", (int)UNREAD_MESSAGE]];
        }
    }
    
}

#pragma mark -
#pragma mark ConsMsgDelegate 
- (void)consMsgDidFinishingParse:(ConsMsg *)consMsg withArray:(NSArray *)array
{
    [self.indicator hide];
    [self.data removeAllObjects];
    [self.data addObjectsFromArray:array];
    [self.tv reloadData];
}

@end
