//
//  ConsExOrderController.h
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-10.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@class CustExchangeGift;

@interface ConsExOrderController : CTViewController

@property (nonatomic, strong) CustExchangeGift * gift;
@property (nonatomic, strong) NSString * myPoints;

@end
