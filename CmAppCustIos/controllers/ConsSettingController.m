//
//  ConsSettingController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-28.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsSettingController.h"
#import "ConsSetting.h"
#import "CustWebService.h"
#import "CustVersion.h"
#import "CustAboutController.h"
#import "ConsPwdModifyController.h"
#import "ConsFeedbackController.h"
#import "ConsAdController.h"
#import "CustAboutInfo.h"

@interface ConsSettingController () <UIAlertViewDelegate>

@property (nonatomic, strong) CustVersion * version;
@property (nonatomic, strong) IBOutlet UIScrollView * container;
@property (nonatomic, strong) IBOutlet UIButton * btnMusic;
@property (nonatomic, strong) IBOutlet UIButton * btnLogout;
@property (weak, nonatomic) IBOutlet UIButton *btnPhoneCall;

- (IBAction)useMusic:(UIButton *)btn;
- (IBAction)pwdModify:(UIButton *)btn;
- (IBAction)phoneCall:(UIButton *)btn;
- (IBAction)feedback:(UIButton *)btn;
- (IBAction)about:(UIButton *)btn;
- (IBAction)showHelp:(UIButton *)btn;
- (IBAction)loginOrLogout:(UIButton *)sender;

@end

@implementation ConsSettingController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"系统设置";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([ConsSetting musicEnable]) {
        [self.btnMusic setImage:[UIImage imageNamed:@"SwitchOn"] forState:UIControlStateNormal];
        [self.btnMusic setImage:[UIImage imageNamed:@"SwitchOn"] forState:UIControlStateHighlighted];
    } else {
        [self.btnMusic setImage:[UIImage imageNamed:@"SwitchOff"] forState:UIControlStateNormal];
        [self.btnMusic setImage:[UIImage imageNamed:@"SwitchOff"] forState:UIControlStateHighlighted];
    }
    [self.view addSubview:self.container];
    
    CustAboutInfo *info = [[CustAboutInfo alloc] init];
    [info validate:[CustWebService sharedUser].userCode complete:^(CustAboutInfo *item) {
        [self.btnPhoneCall setTitle:item.tel forState:UIControlStateNormal];
        [self.btnPhoneCall setTitle:item.tel forState:UIControlStateHighlighted];
    }];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_CBL, CUST_MODULE_OPERATE_SZ, nil]];
    
    [self.btnLogout setBackgroundImage:[[UIImage imageNamed:@"BtnRed"] resizableImageWithCapInsets:UIEdgeInsetsMake(16, 5, 16, 5)] forState:UIControlStateNormal];
    [self.btnLogout setBackgroundImage:[[UIImage imageNamed:@"BtnGreenNormal"] resizableImageWithCapInsets:UIEdgeInsetsMake(16, 5, 16, 5)] forState:UIControlStateHighlighted];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.container.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    if (self.btnLogout.frame.origin.y + self.btnLogout.frame.size.height > self.view.frame.size.height) {
        self.container.contentSize = CGSizeMake(self.container.frame.size.width, self.btnLogout.frame.origin.y + self.btnLogout.frame.size.height + 10);
    } else {
        self.container.contentSize = CGSizeMake(self.container.frame.size.width, self.view.bounds.size.height + 1);
    }
    
    if ([CustWebService sharedUser].isLogin) {
        [self.btnLogout setTitle:@"注 销" forState:UIControlStateNormal];
        [self.btnLogout setTitle:@"注 销" forState:UIControlStateHighlighted];
    } else {
        [self.btnLogout setTitle:@"登 录" forState:UIControlStateNormal];
        [self.btnLogout setTitle:@"登 录" forState:UIControlStateHighlighted];
    }
}

- (void)useMusic:(UIButton *)btn
{
    BOOL enable = [ConsSetting musicEnable];
    enable = !enable;
    if (enable) {
        [self.btnMusic setImage:[UIImage imageNamed:@"SwitchOn"] forState:UIControlStateNormal];
        [self.btnMusic setImage:[UIImage imageNamed:@"SwitchOn"] forState:UIControlStateHighlighted];
    } else {
        [self.btnMusic setImage:[UIImage imageNamed:@"SwitchOff"] forState:UIControlStateNormal];
        [self.btnMusic setImage:[UIImage imageNamed:@"SwitchOff"] forState:UIControlStateHighlighted];
    }
    [ConsSetting setMusicEnable:enable];
}

- (void)pwdModify:(UIButton *)btn
{
    if (![CustWebService sharedUser].isLogin) {
        
        self.indicator.text = @"您尚未登录";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:NOT_LOGIN_NOTICE_INTERVAL complete:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFCATION_NOT_LOGIN object:nil];
        }];
        return;
    }
    
    ConsPwdModifyController * controller = [[ConsPwdModifyController alloc] initWithNibName:@"ConsPwdModifyController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)phoneCall:(UIButton *)btn
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", self.btnPhoneCall.titleLabel.text]]];
}

- (void)feedback:(UIButton *)btn
{
    ConsFeedbackController * controller = [[ConsFeedbackController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}


- (IBAction)about:(UIButton *)btn
{
    CustAboutController * controller = [[CustAboutController alloc] initWithNibName:@"CustAboutController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)showHelp:(UIButton *)btn
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFCATION_SHOW_HELP object:nil];
}

- (void)loginOrLogout:(UIButton *)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFCATION_LOGOUT object:nil];
}

#pragma mark -
#pragma mark UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1 && buttonIndex == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.version.url]];
    }
}

@end
