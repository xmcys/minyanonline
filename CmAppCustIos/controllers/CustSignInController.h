//
//  CustSignInController.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-2-8.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@class CustSignInController;

@protocol CustSignInControllerDelegate <NSObject>

- (void)custSignInControllerDidSignIn:(CustSignInController *)signInController;

@end

@interface CustSignInController : CTViewController

@property (nonatomic, weak) id<CustSignInControllerDelegate> delegate;

@end
