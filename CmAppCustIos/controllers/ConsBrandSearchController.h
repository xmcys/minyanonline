//
//  ConsBrandSearchController.h
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@class ConsBrand;
@class ConsSupplier;

@interface ConsBrandSearchController : CTViewController

@property (nonatomic, strong) ConsBrand * type;
@property (nonatomic, strong) NSString * keyword;
@property (nonatomic, strong) ConsSupplier * supplier;
@property (nonatomic, strong) NSString * defaultTypeName;

@end
