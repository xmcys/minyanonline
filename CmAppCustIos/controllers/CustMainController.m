//
//  CustMainController.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-20.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustMainController.h"
#import "CustModule.h"
#import "CustWebService.h"
#import "CustAboutController.h"
#import "CustServiceController.h"
#import "CustOrderOnlineViewController.h"
#import "CustShopAssistantViewController.h"
#import "ConsClassController.h"

@interface CustMainController ()

- (void)custModuleRecord:(NSNotification *)notification;
- (void)showMessage:(UIButton *)btn;

@end

@implementation CustMainController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBar.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:239.0 alpha:255.0];
        self.tabBar.backgroundImage = nil;
        self.view.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:239.0/255.0 blue:238.0/255.0 alpha:1];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(custModuleRecord:) name:CUST_MODULE_NOTIFICATION_NAME object:nil];
    
    UIBarButtonItem * leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"NavLeftMenu"] style:UIBarButtonItemStyleBordered target:self action:@selector(showOrHideLeftMenu)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem * rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"NavMsg"] style:UIBarButtonItemStyleBordered target:self action:@selector(showMessage:)];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.viewControllers.count == 0) {
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        CustOrderOnlineViewController * order = [[CustOrderOnlineViewController alloc] initWithNibName:@"CustOrderOnlineViewController" bundle:nil];
        CustShopAssistantViewController * shop = [[CustShopAssistantViewController alloc] initWithNibName:@"CustShopAssistantViewController" bundle:nil];
        CustServiceController *service = [[CustServiceController alloc] initWithNibName:nil bundle:nil];
        ConsClassController * _class = [[ConsClassController alloc] initWithNibName:@"ConsClassController" bundle:nil];
        [tempArray addObject:order];
        if ([CustWebService getUserCodePrefix] != 3503) {
            // 莆田没有店铺助手
            [tempArray addObject:shop];
        }
        [tempArray addObject:service];
        [tempArray addObject:_class];
        
        self.viewControllers = [NSArray arrayWithArray:tempArray];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)custModuleRecord:(NSNotification *)notification
{
    NSArray * array = [[notification object] copy];
    if (array == nil || array.count < 2) {
        return;
    }
    [CustModule recordWithModCode:[array objectAtIndex:0] operateId:[array objectAtIndex:1]];
}

- (void)showOrHideLeftMenu
{
    if ([self.delegate respondsToSelector:@selector(custMainControllerLeftMenuButtonDidClicked:)]) {
        [self.delegate custMainControllerLeftMenuButtonDidClicked:self];
    }
}

- (void)showMessage:(UIButton *)btn
{
    if ([self.delegate respondsToSelector:@selector(custMainControllerRightMenuButtonDidClicked:)]) {
        [self.delegate custMainControllerRightMenuButtonDidClicked:self];
    }
}

@end
