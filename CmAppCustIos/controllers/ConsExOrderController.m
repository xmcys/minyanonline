//
//  ConsExOrderController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-10.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsExOrderController.h"
#import "CTImageView.h"
#import "CustExchangeGift.h"
#import "CustWebService.h"
#import "CustSystemMsg.h"

#define PROCESS_SUMMIT_ORDER 1

@interface ConsExOrderController () <UIAlertViewDelegate>

@property (nonatomic, strong) IBOutlet UIView * container;
@property (nonatomic, strong) IBOutlet UILabel * personName;
@property (nonatomic, strong) IBOutlet UIButton * btnPlus;
@property (nonatomic, strong) IBOutlet UIButton * btnMinus;
@property (nonatomic, strong) IBOutlet UIButton * btnNum;
@property (nonatomic, strong) IBOutlet UILabel * goodsName;
@property (nonatomic, strong) IBOutlet UILabel * goodsPoint;
@property (nonatomic, strong) IBOutlet CTImageView * goodsImg;
@property (nonatomic, strong) IBOutlet UILabel *labDes1;
@property (nonatomic, strong) IBOutlet UILabel *labDes2;
@property (nonatomic, assign) BOOL isInit;

- (void)submit;
- (IBAction)changeNum:(UIButton *)btn;

@end

@implementation ConsExOrderController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"订单确认";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.isInit = NO;
    UIButton * rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.backgroundColor = [UIColor clearColor];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"BtnSubmit"] forState:UIControlStateNormal];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"BtnSubmit"] forState:UIControlStateHighlighted];
    rightBtn.showsTouchWhenHighlighted = YES;
    [rightBtn addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn sizeToFit];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];

    [self.btnNum setBackgroundImage:[[UIImage imageNamed:@"NumBg"] resizableImageWithCapInsets:UIEdgeInsetsMake(13, 6, 13, 6)] forState:UIControlStateNormal];
    [self.btnNum setBackgroundImage:[[UIImage imageNamed:@"NumBg"] resizableImageWithCapInsets:UIEdgeInsetsMake(13, 6, 13, 6)] forState:UIControlStateHighlighted];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!self.isInit) {
        
        self.personName.text = [CustWebService sharedUser].realName;
        
        self.goodsName.text = self.gift.giftName;
        
        self.goodsImg.url = [CustWebService fileFullUrl:self.gift.giftPic];
        
        [self.goodsImg loadImage];
        self.goodsPoint.text = self.gift.changePoint;
        
        int count = [self.btnNum.titleLabel.text integerValue];
        self.labDes1.text = [NSString stringWithFormat:@"当前可参与兑换积分为%@", self.myPoints];
        self.labDes2.text = [NSString stringWithFormat:@"现兑换共计：%d个物品，%d积分", count, ([self.gift.changePoint integerValue]*count)];
    }
}

- (void)changeNum:(UIButton *)btn
{
    NSInteger tag = btn.tag;
    int count = [self.btnNum.titleLabel.text integerValue];
    if (tag == 1) {
        count--;
    } else {
        count++;
    }
    if (count <= 1) {
        count = 1;
    }
    
    [self.btnNum setTitle:[NSString stringWithFormat:@"%d", count] forState:UIControlStateNormal];
    [self.btnNum setTitle:[NSString stringWithFormat:@"%d", count] forState:UIControlStateHighlighted];
    self.labDes1.text = [NSString stringWithFormat:@"当前可参与兑换积分为%@", self.myPoints];
    self.labDes2.text = [NSString stringWithFormat:@"现兑换共计：%d个物品，%d积分", count, ([self.gift.changePoint integerValue]*count)];
}

- (void)submit
{
    
    if (self.indicator.showing) {
        return;
    }
    
    int exPoint = [self.btnNum.titleLabel.text integerValue] * [self.gift.changePoint integerValue];
    // 判断积分是否足够
    if ([self.myPoints integerValue] < exPoint) {
        self.indicator.text = [NSString stringWithFormat:@"您当前可用积分余额不足，不足以兑换%d个此商品", [self.btnNum.titleLabel.text integerValue]];
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    
    self.indicator.text = @"正在提交订单..";
    [self.indicator show];
    self.indicator.backgroundTouchable = NO;
    
    NSString * params = [NSString stringWithFormat:@"<IntegralChange><custLicenceCode>%@</custLicenceCode><supplierCode>%@</supplierCode><giftCode>%@</giftCode><ruleId>%@</ruleId><changePoint>%@</changePoint><changeQty>%@</changeQty></IntegralChange>", [CustWebService sharedUser].custLicenceCode, self.gift.supplierCode, self.gift.giftCode, self.gift.ruleId, self.gift.changePoint, self.btnNum.titleLabel.text];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService addGiftChangeUrl] params:params delegate:self];
    conn.tag = PROCESS_SUMMIT_ORDER;
    [conn start];
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"提交失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    
    if (connection.tag == PROCESS_SUMMIT_ORDER){
        [self.indicator hide];
        CustSystemMsg * sysMsg = [[CustSystemMsg alloc] init];
        [sysMsg parseData:data complete:^(CustSystemMsg *msg) {
            self.indicator.text = msg.msg;
            [self.indicator autoHide:CTIndicateStateDone afterDelay:1.0 complete:^{
                if ([msg.code isEqualToString:@"1"]) {
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
            }];
        }];
    }
}

@end
