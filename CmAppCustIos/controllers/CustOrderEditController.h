//
//  CustOrderEditController.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@class CustOrder;
@class CustOrderEditController;

@protocol CustOrderEditControllerDelegate <NSObject>

@optional
- (void)custOrderEditController:(CustOrderEditController *)controller didDeleteBrandAtIndex:(int)index;
- (void)custOrderEditController:(CustOrderEditController *)controller didSubmitDemandAtIndex:(int)index newOrder:(CustOrder *)newOrder;

@end

@interface CustOrderEditController : CTViewController

@property (nonatomic, weak) NSMutableArray * brandArray;
@property (nonatomic) NSInteger currentIndex;
@property (nonatomic, weak) id<CustOrderEditControllerDelegate> delegate;
@property (nonatomic, strong) NSString * qtyOrder;
@property (nonatomic, strong) NSString * amtOrder;

@end
