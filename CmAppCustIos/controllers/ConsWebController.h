//
//  ConsWebController.h
//  CmAppCustIos
//
//  Created by Chenhc on 15/3/23.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@interface ConsWebController : CTViewController

@property (nonatomic, strong) NSString * mainUrl;

@end
