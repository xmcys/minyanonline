//
//  CustTrackUndealController.m
//  CmAppCustIos
//
//  Created by hsit on 14-8-26.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustTrackUndealController.h"
#import "CTURLConnection.h"
#import "CustWebService.h"
#import "CTIndicateView.h"
#import "CustServiceMsg.h"
#import "CustSystemMsg.h"
#import "CTUtil.h"


#define PROCESS_LOADING_INFO 1
#define PROCESS_UPDATE_INFO  2

@interface CustTrackUndealController ()<UITextViewDelegate,CTURLConnectionDelegate>

@property (nonatomic, strong) IBOutlet UIView  * header;
@property (nonatomic, strong) IBOutlet UIView  * topbg;
@property (nonatomic, strong) IBOutlet UILabel * msgtitlelab;
@property (nonatomic, strong) IBOutlet UILabel * msgIdlab;
@property (nonatomic, strong) IBOutlet UILabel * msgdatelab;
@property (nonatomic, strong) IBOutlet UITextView * msgtextView;
@property (nonatomic, strong) IBOutlet UIButton * commitbtn;
@property (nonatomic, strong) CTIndicateView   * indicator;
@property (nonatomic, strong) CustServiceMsg   * info;

- (IBAction)commitClick:(id)sender;
@end

@implementation CustTrackUndealController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.header.backgroundColor = [UIColor clearColor];
    self.topbg.backgroundColor = [UIColor colorWithRed:252.0/255.0 green:245.0/255.0 blue:252.0/255.0 alpha:1];
    if (self.indicator == nil) {
        self.indicator = [[CTIndicateView alloc] initInView:self.view];
        self.indicator.backgroundTouchable = YES;
    }
    UIImage *normalimg = [[UIImage imageNamed:@"CustServiceCommitNormal"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 7, 7, 5)];
    UIImage *heligatedimg = [[UIImage imageNamed:@"CustServiceCommitHelighted"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
    [self.commitbtn setBackgroundImage:normalimg forState:UIControlStateNormal];
    [self.commitbtn setBackgroundImage:heligatedimg forState:UIControlStateHighlighted];
    self.msgtextView.layer.cornerRadius = 5;
    self.msgtextView.layer.masksToBounds = YES;
    self.msgtextView.delegate = self;
    [self.view addSubview:self.header];
    [self loadData];
 
    //注册通知,监听键盘出现
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidShow:)
                                                name:UIKeyboardDidShowNotification
                                              object:nil];
    //注册通知，监听键盘消失事件
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidHidden:)
                                                name:UIKeyboardDidHideNotification
                                              object:nil];
}

- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"加载中...";
    [self.indicator show];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getServiceDetailInfo:self.msg.demandId] delegate:self];
    conn.tag = PROCESS_LOADING_INFO;
    [conn start];
    return;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)drawView:(CustServiceMsg *)msg
{
    self.msgtitlelab.text = self.msg.demandTitle;
    self.msgIdlab.text = [NSString stringWithFormat:@"编       号：%@",self.msg.demandId];
    self.msgdatelab.text = [NSString stringWithFormat:@"提交时间：%@",self.msg.demandTime];
    self.msgtextView.text = msg.demandCont;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.msgtextView resignFirstResponder];
}
#pragma mark - KeyBoard
- (void)handleKeyboardDidShow:(NSNotification*)paramNotification
{
    //获取键盘高度
    NSValue *keyboardRectAsObject = [[paramNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect;
    [keyboardRectAsObject getValue:&keyboardRect];
    CGRect frame = self.msgtextView.frame;
    
    int offset = (frame.origin.y + frame.size.height) - (self.view.frame.size.height - keyboardRect.size.height);
    if (offset > 0) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2f];
        self.view.frame = CGRectMake(0, - 40, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }
}

- (void)handleKeyboardDidHidden:(NSNotification*)paramNotification
{
    //获取键盘高度
    NSValue *keyboardRectAsObject = [[paramNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect;
    [keyboardRectAsObject getValue:&keyboardRect];
    CGRect frame = self.msgtextView.frame;
    
    int offset = (frame.origin.y + frame.size.height) - (self.view.frame.size.height - keyboardRect.size.height);
    if (offset > 0) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2f];
        if (VERSION_LESS_THAN_IOS7) {
            self.view.frame = CGRectMake(0, 0 , self.view.frame.size.width, self.view.frame.size.height);
        } else {
            self.view.frame = CGRectMake(0, + 40 + 25, self.view.frame.size.width, self.view.frame.size.height);
        }
        [UIView commitAnimations];
    }
}
#pragma mark - UITextView Delegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [self.msgtextView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (IBAction)commitClick:(id)sender
{
    [self.msgtextView resignFirstResponder];
    if (self.indicator.showing) {
        return;
    }
    if ([self.info.demandCont isEqualToString:self.msgtextView.text]) {
        self.indicator.text = @"未做修改,无需提交";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
        return;
    }
    if (self.msgtextView.text.length > 500) {
        self.indicator.text = @"描述不能超过500个字";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
        return;
    }
    self.indicator.text = @"正在提交...";
    [self.indicator show];
    NSString * params = [NSString stringWithFormat:@"<ServiceAccepted><demandId>%@</demandId><demandCont>%@</demandCont></ServiceAccepted>",self.msg.demandId,self.msgtextView.text];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService updateServiceInfo] params:params delegate:self];
    conn.tag = PROCESS_UPDATE_INFO;
    [conn start];
}
#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    if (connection.tag == PROCESS_LOADING_INFO) {
        self.indicator.text = @"加载失败";
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0f];
    } else if (connection.tag == PROCESS_UPDATE_INFO) {
       self.indicator.text = @"提交失败";
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0f];
    }
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_LOADING_INFO) {
        self.info = [[CustServiceMsg alloc] init];
        [self.info parseData:data complete:^(CustServiceMsg *msg){
            [self.indicator hide];
            [self drawView:(CustServiceMsg *)msg];
        }];
    } else if (connection.tag == PROCESS_UPDATE_INFO) {
        CustSystemMsg * msg = [[CustSystemMsg alloc] init];
        [msg parseData:data];
        if ([msg.code isEqualToString:@"1"]) {
            self.indicator.text = msg.msg;
            [self.indicator hideWithSate:CTIndicateStateDone afterDelay:1.0f];
            [NSTimer scheduledTimerWithTimeInterval:0.8f target:self selector:@selector(popView) userInfo:nil repeats:NO];
        } else {
            self.indicator.text = msg.msg;
            [self.indicator hideWithSate:CTIndicateStateDone afterDelay:1.0f];
        }
    }
}

- (void)popView
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
