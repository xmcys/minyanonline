//
//  CustMessageController.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-25.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@class CustMessage;

@interface CustMessageController : CTViewController

@property (nonatomic, weak) CustMessage * msg;

@end
