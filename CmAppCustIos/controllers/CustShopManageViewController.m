//
//  RYKShopViewController.m
//  RYKForCustIos
//
//  Created by Arcesaka on 14-6-21.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CustShopManageViewController.h"
#import "CTTableView.h"
#import "CTEditorController.h"
#import "CustStoreInfo.h"
#import "CustWebService.h"
#import "CustDeliveryContent.h"
#import "CustSystemMsg.h"
#import "ZbCommonFunc.h"
#import "CustModule.h"


#define PROCESS_GET_STORE_MESSAGE 1
#define PROCESS_GET_DELIVER_MESSAGE 2
#define PROCESS_SAVE_STORE_MESSAGE 3
#define PROCESS_SAVE_DELIVER_MESSAGE 4

@interface CustShopManageViewController ()<CTTableViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,CTEditorControllerDelegate,UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIView   *toptab;     //顶部tab栏
@property (strong, nonatomic) IBOutlet UIButton *shopbtn;    //店铺信息
@property (strong, nonatomic) IBOutlet UIButton *servicebtn; //送货服务
@property (strong, nonatomic) CTTableView       *tableview;  //店铺信息视图
@property (strong, nonatomic) UITextView        *textview;   //送货服务视图
@property (strong, nonatomic) UILabel           *defautlab;
@property (strong, nonatomic) NSArray           *shopNameArray;  //左标题数组
@property (strong, nonatomic) NSMutableArray    *msgArray;
@property (strong, nonatomic) CustStoreInfo      *msg;

- (IBAction)onTabChanged:(UIButton *)btn;


@end

@implementation CustShopManageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"店铺管理";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.msgArray = [[NSMutableArray alloc] init];
    UIButton *btncommit = [UIButton buttonWithType:UIButtonTypeCustom];
    [btncommit setBackgroundImage:[UIImage imageNamed:@"BtnSubmit"] forState:UIControlStateNormal];
    btncommit.frame = CGRectMake(0, 0, 22, 22);
    [btncommit addTarget:self action:@selector(btncommitclick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnright = [[UIBarButtonItem alloc] initWithCustomView:btncommit];
    self.navigationItem.rightBarButtonItem = btnright;
    
    self.toptab.backgroundColor = XCOLOR(230, 245, 238, 1);
    self.toptab.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.toptab.frame.size.height);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_WDDP, CUST_MODULE_OPERATE_DPGL, nil]];
}
- (void)btncommitclick:(UIButton *)btn
{
    [self.textview resignFirstResponder];
    if (self.indicator.showing) {
        return;
    }
    NSString *params = [NSString stringWithFormat:@"<UsCustStoreNotice><userId>%@</userId><delvIntro>%@</delvIntro></UsCustStoreNotice>",[CustWebService sharedUser].userId,self.textview.text];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService getSaveDeliveryContent] params:params delegate:self];
    conn.tag = PROCESS_SAVE_DELIVER_MESSAGE;
    [conn start];
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    return;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.tableview == nil) {
        self.tableview = [[CTTableView alloc] initWithFrame:CGRectMake(0, self.toptab.frame.size.height+10, self.view.bounds.size.width, self.view.bounds.size.height-self.toptab.frame.size.height-10)];
        self.tableview.pullUpViewEnabled = NO;
        self.tableview.backgroundColor = [UIColor clearColor];
        self.tableview.delegate = self;
        self.tableview.dataSource = self;
        self.tableview.ctTableViewDelegate = self;
        [self.view addSubview:self.tableview];
        [self loadStoreInfo];
    }
    if (self.textview == nil) {
        self.textview = [[UITextView alloc] initWithFrame:CGRectMake(5, self.toptab.bounds.size.height+10, self.view.bounds.size.width-5*2, 210)];
        self.textview.backgroundColor = [UIColor whiteColor];
        self.textview.font = [UIFont systemFontOfSize:15.0];
        self.textview.textColor = [UIColor colorWithRed:127.0/255.0 green:127.0/255.0 blue:127.0/255.0 alpha:1];
        self.textview.layer.cornerRadius = 5;
        self.textview.layer.borderWidth = 1;
        self.textview.delegate=self;
        self.textview.layer.borderColor = [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1].CGColor;
        
        [self.view addSubview:self.textview];
        [self onTabChanged:self.shopbtn];
    }
}

- (void)loadStoreInfo
{
    if (self.indicator.showing) {
        return;
    }
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getReadStoreInfo:[CustWebService sharedUser].userId] delegate:self];
    conn.tag = PROCESS_GET_STORE_MESSAGE;
    [conn start];
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    return;
}

- (void)loadDeliverInfo
{
    if (self.indicator.showing) {
        return;
    }
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getDeliveryContent] delegate:self];
    conn.tag = PROCESS_GET_DELIVER_MESSAGE;
    [conn start];
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    return;
}

- (IBAction)onTabChanged:(UIButton *)btn
{
    UIImage *img = [[UIImage imageNamed:@"on"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 10, 5, 10)];
    
    UIImage *clearImage = [ZbCommonFunc getImage:CGSizeMake(1, 1) withColor:[UIColor clearColor]];
    [self.shopbtn setBackgroundImage:clearImage forState:UIControlStateNormal];
    [self.shopbtn setBackgroundImage:clearImage forState:UIControlStateHighlighted];
    [self.servicebtn setBackgroundImage:clearImage forState:UIControlStateNormal];
    [self.servicebtn setBackgroundImage:clearImage forState:UIControlStateHighlighted];
    
    [self.shopbtn setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.shopbtn setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateHighlighted];
    [self.servicebtn setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.servicebtn setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateHighlighted];
    
    
    [btn setBackgroundImage:img forState:UIControlStateNormal];
    [btn setBackgroundImage:img forState:UIControlStateHighlighted];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    if (btn == self.shopbtn) {
        self.tableview.hidden = NO;
        self.textview.hidden = YES;
        self.navigationItem.rightBarButtonItem.customView.hidden = YES;
        [self.textview resignFirstResponder];
        [self loadStoreInfo];
    } else if (btn == self.servicebtn) {
        self.tableview.hidden = YES;
        self.textview.hidden = NO;
        self.navigationItem.rightBarButtonItem.customView.hidden = NO;
        [self loadDeliverInfo];
    }
}

#pragma mark - UITextView Delegate
- (void)textViewDidChange:(UITextView *)textView
{
    if (self.textview.text.length == 0) {
        self.defautlab.text = @"编辑送货服务说明";
    } else {
        self.defautlab.text = @"";
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textview resignFirstResponder];
}

//下拉刷新
#pragma  mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tableview ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tableview ctTableViewDidEndDragging];
}

#pragma mark - CTTableView Delegate *DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CELL";
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = UITableViewCellSelectionStyleNone;
    NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"CustShopInfoCell" owner:nil options:nil];
    UIView *view = [elements objectAtIndex:0];
    view.frame = CGRectMake(0, 0, tableView.frame.size.width, 50);
    [cell addSubview:view];
    self.shopNameArray = [NSArray arrayWithObjects:@"商铺店名",@"固定电话",@"移动电话",@"地址",@"营业时间",@"商家介绍", nil];
    UILabel *infolab = (UILabel *)[cell viewWithTag:1];
    infolab.text = [self.shopNameArray objectAtIndex:indexPath.row];
    UILabel *detaillab = (UILabel *)[cell viewWithTag:2];
    if (self.msgArray.count > 0) {
        CustStoreInfo *info = (CustStoreInfo *)[self.msgArray objectAtIndex:0];
        if (indexPath.row == 0) {
            detaillab.text = info.userName;
       } else if (indexPath.row == 1) {
            detaillab.text = info.linkPhone;
       } else if (indexPath.row == 2) {
           detaillab.text = info.mobilphone;
       } else if (indexPath.row == 3) {
           detaillab.text = info.userAddr;
       } else if (indexPath.row == 4) {
           detaillab.text = info.businessTime;
       } else if (indexPath.row == 5) {
           detaillab.text = info.storeIntro;
       }
    }
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadStoreInfo];
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.msgArray.count <= 0) {
        return;
    }
    CTEditorController *controller = [[CTEditorController alloc] init];
    controller.delegate = self;
    self.msg = [self.msgArray objectAtIndex:0];
    controller.title = [self.shopNameArray objectAtIndex:indexPath.row];
    
    if (indexPath.row == 0) {
        [self.navigationController pushViewController:controller animated:YES];
        controller.editorType = CTEditorTypeTextField;
        controller.view.tag = 0;
        [controller setDefaultValue:self.msg.userName];
    }
   else if (indexPath.row == 1) {
        [self.navigationController pushViewController:controller animated:YES];
        controller.editorType = CTEditorTypeTextField;
        [controller setKeyboardType:UIKeyboardTypePhonePad];
        controller.view.tag = 1;
        [controller setDefaultValue:self.msg.linkPhone];
    }
   else if (indexPath.row == 2) {
        [self.navigationController pushViewController:controller animated:YES];
        controller.editorType = CTEditorTypeTextField;
        [controller setKeyboardType:UIKeyboardTypeNumberPad];
        controller.view.tag = 2;
        [controller setDefaultValue:self.msg.mobilphone];
    }
   else if (indexPath.row == 3) {
        [self.navigationController pushViewController:controller animated:YES];
        controller.editorType = CTEditorTypeTextField;
        controller.view.tag = 3;
        [controller setDefaultValue:self.msg.userAddr];
    }
   else if (indexPath.row == 4) {
        [self.navigationController pushViewController:controller animated:YES];
        controller.editorType = CTEditorTypeTextField;
        controller.view.tag = 4;
        [controller setDefaultValue:self.msg.businessTime];
    }
   else if (indexPath.row == 5) {
        [self.navigationController pushViewController:controller animated:YES];
        controller.editorType = CTEditorTypeTextView;
        controller.view.tag = 5;
        [controller setDefaultValue:self.msg.storeIntro];
    }
}

#pragma mark - CTEditorController  Delegate
- (void)ctEditorController:(CTEditorController *)controller editDoneWithText:(NSString *)text
{
    if (self.indicator.showing) {
        return;
    }
    NSInteger tag = controller.view.tag;
    self.msg = [self.msgArray objectAtIndex:0];
    NSString *oriString = [NSString stringWithFormat:@"<UsCustStoreNotice><userId>%@</userId><userName>%@</userName><linkPhone>%@</linkPhone><mobilphone>%@</mobilphone><userAddr>%@</userAddr><businessTime>%@</businessTime><storeIntro>%@</storeIntro></UsCustStoreNotice>",[CustWebService sharedUser].userId,self.msg.userName,self.msg.linkPhone,self.msg.mobilphone,self.msg.userAddr,self.msg.businessTime,self.msg.storeIntro];
    switch (tag) {
        case SHOP_INFO_USERNAME:
          oriString = [NSString stringWithFormat:@"<UsCustStoreNotice><userId>%@</userId><userName>%@</userName><linkPhone>%@</linkPhone><mobilphone>%@</mobilphone><userAddr>%@</userAddr><businessTime>%@</businessTime><storeIntro>%@</storeIntro></UsCustStoreNotice>",[CustWebService sharedUser].userId,text,self.msg.linkPhone,self.msg.mobilphone,self.msg.userAddr,self.msg.businessTime,self.msg.storeIntro];
            break;
        case SHOP_INFO_LINKPHONE:
            oriString = [NSString stringWithFormat:@"<UsCustStoreNotice><userId>%@</userId><userName>%@</userName><linkPhone>%@</linkPhone><mobilphone>%@</mobilphone><userAddr>%@</userAddr><businessTime>%@</businessTime><storeIntro>%@</storeIntro></UsCustStoreNotice>",[CustWebService sharedUser].userId,self.msg.userName,text,self.msg.mobilphone,self.msg.userAddr,self.msg.businessTime,self.msg.storeIntro];
            break;
        case SHOP_INFO_MOBILPHONE:
            oriString = [NSString stringWithFormat:@"<UsCustStoreNotice><userId>%@</userId><userName>%@</userName><linkPhone>%@</linkPhone><mobilphone>%@</mobilphone><userAddr>%@</userAddr><businessTime>%@</businessTime><storeIntro>%@</storeIntro></UsCustStoreNotice>",[CustWebService sharedUser].userId,self.msg.userName,self.msg.linkPhone,text,self.msg.userAddr,self.msg.businessTime,self.msg.storeIntro];
            break;
        case SHOP_INFO_USERADDR:
          oriString = [NSString stringWithFormat:@"<UsCustStoreNotice><userId>%@</userId><userName>%@</userName><linkPhone>%@</linkPhone><mobilphone>%@</mobilphone><userAddr>%@</userAddr><businessTime>%@</businessTime><storeIntro>%@</storeIntro></UsCustStoreNotice>",[CustWebService sharedUser].userId,self.msg.userName,self.msg.linkPhone,self.msg.mobilphone,text,self.msg.businessTime,self.msg.storeIntro];
            break;
        case SHOP_INFO_BUSSINESSTIME:
            oriString = [NSString stringWithFormat:@"<UsCustStoreNotice><userId>%@</userId><userName>%@</userName><linkPhone>%@</linkPhone><mobilphone>%@</mobilphone><userAddr>%@</userAddr><businessTime>%@</businessTime><storeIntro>%@</storeIntro></UsCustStoreNotice>",[CustWebService sharedUser].userId,self.msg.userName,self.msg.linkPhone,self.msg.mobilphone,self.msg.userAddr,text,self.msg.storeIntro];
                          break;
        case SHOP_INFO_STOREINTRO:
            oriString = [NSString stringWithFormat:@"<UsCustStoreNotice><userId>%@</userId><userName>%@</userName><linkPhone>%@</linkPhone><mobilphone>%@</mobilphone><userAddr>%@</userAddr><businessTime>%@</businessTime><storeIntro>%@</storeIntro></UsCustStoreNotice>",[CustWebService sharedUser].userId,self.msg.userName,self.msg.linkPhone,self.msg.mobilphone,self.msg.userAddr,self.msg.businessTime,text];
            break;
        default:
            break;
    }
    self.indicator.text = @"正在提交..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService getSaveStoreInfo] params:oriString delegate:self];
    conn.tag = PROCESS_SAVE_STORE_MESSAGE;
    [conn start];
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.tableview reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_GET_STORE_MESSAGE) {
            CustStoreInfo * msg = [[CustStoreInfo alloc] init];
            [msg parseData:data complete:^(NSArray *array){
                [self.indicator hide];
                [self.msgArray removeAllObjects];
                [self.msgArray addObjectsFromArray:array];
                [self.tableview reloadData];
        }];
    } else if(connection.tag == PROCESS_GET_DELIVER_MESSAGE){
            CustDeliveryContent *msg = [[CustDeliveryContent alloc] init];
            [msg parseData:data complete:^(CustDeliveryContent *msg){
                [self.indicator hide];
                self.textview.text = msg.delvIntro;
        }];
    
    } else if(connection.tag == PROCESS_SAVE_DELIVER_MESSAGE){
            CustSystemMsg *msg = [[CustSystemMsg alloc] init];
            [msg parseData:data];
            if ([msg.code isEqualToString:@"1"]) {
                self.indicator.text = @"修改成功";
                [self.indicator autoHide:CTIndicateStateDone afterDelay:1.0];
                return;
        }
    } else if(connection.tag == PROCESS_SAVE_STORE_MESSAGE){
            CustSystemMsg *msg = [[CustSystemMsg alloc] init];
            [msg parseData:data];
            if ([msg.code isEqualToString:@"1"]) {
                [self.indicator hide];
                self.indicator.text = @"修改成功";
                [self.indicator autoHide:CTIndicateStateDone afterDelay:1.0];
                self.indicator.showing = NO;
                [self loadStoreInfo];
                [self.navigationController popViewControllerAnimated:YES];
            }
    }
}




@end
