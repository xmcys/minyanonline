//
//  ConsBrandDetailController.h
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-24.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@class ConsBrand;

@interface ConsBrandDetailController : CTViewController

@property (nonatomic, strong) ConsBrand * brand;

@end
