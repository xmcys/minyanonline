//
//  CustSaleProfViewController.h
//  CmAppCustIos
//
//  Created by hsit on 15-3-20.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

typedef NS_ENUM(NSInteger, StatusTypeName){
    StatusTypeSale = 200,
    StatusTypeProf,
};

#import "CTViewController.h"

@interface CustSaleProfViewController : CTViewController

- (void)setSTatusType:(StatusTypeName)statusTypeName;

@end
