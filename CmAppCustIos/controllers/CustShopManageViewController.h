//
//  RYKShopViewController.h
//  RYKForCustIos
//
//  Created by Arcesaka on 14-6-21.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTViewController.h"

typedef NS_ENUM(NSInteger,ShopInfoEditType)
{
    SHOP_INFO_USERNAME,
    SHOP_INFO_LINKPHONE,
    SHOP_INFO_MOBILPHONE,
    SHOP_INFO_USERADDR,
    SHOP_INFO_BUSSINESSTIME,
    SHOP_INFO_STOREINTRO
};


@interface CustShopManageViewController : CTViewController
@property (nonatomic) ShopInfoEditType  editorType;


@end
