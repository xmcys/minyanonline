//
//  CustOrderOnlineViewController.m
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/17.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustOrderOnlineViewController.h"
#import "CTTableView.h"
#import "CustActivity.h"
#import "CTImageView.h"
#import "CustWebService.h"
#import "CustModule.h"
#import "CustOrderDetailController.h"
#import "CustOrderHistoryController.h"
#import "CustOrderTaticsController.h"
#import "CustOrderTrackViewController.h"
#import "CustSaleProfViewController.h"
#import "CustNewIvtViewController.h"
#import "CustOrderTrack.h"
#import "CustBalanceController.h"
#import "CTBannerView.h"
#import "ConsMsgDetailController.h"
#import "CustBannerInfo.h"
#import "ConsBrand.h"
#import "ConsBrandDetailController.h"
#import "CustDefaultBank.h"

#define PROCESS_LOADING_BANNER     1  // 宣传
#define PROCESS_LOADING_TRACK      2  // 订单跟踪、历史订单
#define PROCESS_LOADING_MONEY      3  // 余额
#define PROCESS_LOADING_ACTIVITY   4  // 活动
#define PROCESS_LOADING_XX         5  // 待定

@interface CustOrderOnlineViewController () <UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, CTTableViewDelegate, CTImageViewDelegate>

@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) IBOutlet UILabel *labOrderTrackState;  // 订单跟踪状态
@property (nonatomic, strong) IBOutlet UILabel *labHisOrderAmount;   // 历史订单量
@property (nonatomic, strong) IBOutlet UILabel *labMoneySum;         // 余额

@property (nonatomic, strong) IBOutlet UILabel *labSaleAmount;       // 销量
@property (nonatomic, strong) IBOutlet UILabel *labSaleAmountName;   // 销量文字
@property (nonatomic, strong) IBOutlet UILabel *labProfit;           // 毛利
@property (nonatomic, strong) IBOutlet UILabel *labProfitName;       // 毛利文字
@property (nonatomic, strong) IBOutlet UILabel *labStock;            // 库存
@property (nonatomic, strong) IBOutlet UILabel *labGuestAmount;      // 顾客数
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcInfoViewHeight; // 信息高度
@property (nonatomic, strong) IBOutlet UIView *infoView;             // 信息view

@property (retain,nonatomic) NSMutableArray *imgArray;               // 轮播图数据

@property (nonatomic, strong) CTBannerView *banner;                  // 宣传banner
@property (nonatomic, strong) CTTableView *tv;                       // 活动tv
@property (nonatomic, strong) NSMutableArray *array;                 // 活动数据array

- (void)loadData;
- (void)loadBannerData;
- (void)loadTrackData;
- (void)loadBankListData;
- (void)loadActivityData;

- (IBAction)btnBeginOrderOnClick:(id)btn;  // 开始订货
- (IBAction)btnTacticsOnClick:(id)btn;     // 货源策略
- (IBAction)btnOrderTrackOnClick:(id)btn;  // 订单跟踪
- (IBAction)btnHisOrderOnClick:(id)btn;    // 历史订单
- (IBAction)btnMoneyQueryOnClick:(id)btn;  // 余额查询
- (IBAction)btnSaleAmountOnClick:(id)btn;  // 销量点击
- (IBAction)btnProfitOnClick:(id)btn;      // 毛利点击
- (IBAction)btnStockOnClick:(id)btn;       // 库存点击
- (IBAction)btnGuestAmountOnClick:(id)btn; // 顾客点击

@end

@implementation CustOrderOnlineViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setTabBarItemWithTitle:@"在线订货" titleColor:[UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1] titleSelectedColor:[UIColor colorWithRed:4.0/255.0 green:168.0/255.0 blue:117.0/255.0 alpha:1] image:[UIImage imageNamed:@"CustTabOrderNormal"] selectedImage:[UIImage imageNamed:@"CustTabOrderSelected"]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.array = [[NSMutableArray alloc] init];
    self.imgArray = [[NSMutableArray alloc] init];
    
    self.labHisOrderAmount.adjustsFontSizeToFitWidth = YES;
    self.labMoneySum.adjustsFontSizeToFitWidth = YES;
    self.labOrderTrackState.adjustsFontSizeToFitWidth = YES;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_ZXDH, CUST_MODULE_ZXDH, nil]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.title = @"闽烟在线";
    
    // 广告轮播view
    if (self.banner == nil) {
        self.banner = [[CTBannerView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width, 150)];
        [self.banner setIndicateColor:[UIColor lightGrayColor] highlightedColor:XCOLOR(4, 168, 117, 1)];
        [self.headerView addSubview:self.banner];
    }
    
    // 活动tableview
    if (self.tv == nil) {
        // 莆田没有销售、毛利等信息
        if ([CustWebService getUserCodePrefix] == 3503) {
            CGRect headerFrame = self.headerView.frame;
            headerFrame.size.height = headerFrame.size.height - 47;
            self.headerView.frame = headerFrame;
            self.alcInfoViewHeight.constant = 0;
            self.infoView.hidden = YES;
        }
        
        self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.tv.pullUpViewEnabled = NO;
        self.tv.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        [self.tv setTableHeaderView:self.headerView];
        self.tv.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.tv];
        
        [self loadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

# pragma mark - http requests
- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中...";
    [self.indicator show];
    
    [self loadBannerData];
    return;
}

- (void)loadBannerData
{
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService mobileConsumerNew:@"1" position:@"1"] delegate:self];
    conn.tag = PROCESS_LOADING_BANNER;
    [conn start];
}

- (void)loadTrackData
{
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService queryOrderStatus:@"0" userId:[CustWebService sharedUser].userId] delegate:self];
    conn.tag = PROCESS_LOADING_TRACK;
    [conn start];
    return;
}

- (void)loadBankListData
{
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService custDefaultBalanceUrl] delegate:self];
    conn.tag = PROCESS_LOADING_MONEY;
    [conn start];
    return;
}

- (void)loadActivityData
{
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService custActivityListUrl] delegate:self];
    conn.tag = PROCESS_LOADING_ACTIVITY;
    [conn start];
    return;
}

# pragma mark - button click events
- (IBAction)btnBeginOrderOnClick:(id)btn
{
    // 开始订货点击
    CustOrderDetailController * detail = [[CustOrderDetailController alloc] initWithNibName:@"CustOrderDetailController" bundle:nil];
    [self.navigationController pushViewController:detail animated:YES];
    return;
}

- (IBAction)btnTacticsOnClick:(id)btn
{
    // 货源策略点击
    CustOrderTaticsController * tatics = [[CustOrderTaticsController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:tatics animated:YES];
    return;
}

- (IBAction)btnOrderTrackOnClick:(id)btn
{
    // 订单追踪点击
    CustOrderTrackViewController *track = [[CustOrderTrackViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:track animated:YES];
    return;
}

- (IBAction)btnHisOrderOnClick:(id)btn
{
    // 历史订单点击
    CustOrderHistoryController * history = [[CustOrderHistoryController alloc] initWithNibName:@"CustOrderHistoryController" bundle:nil];
    [self.navigationController pushViewController:history animated:YES];
    return;
}

- (IBAction)btnMoneyQueryOnClick:(id)btn
{
    // 余额点击
    CustBalanceController * controller = [[CustBalanceController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

- (IBAction)btnSaleAmountOnClick:(id)btn
{
    // 销量点击
//    CustSaleProfViewController *spf = [[CustSaleProfViewController alloc] initWithNibName:nil bundle:nil];
//    [spf setSTatusType:StatusTypeSale];
//    [self.navigationController pushViewController:spf animated:YES];
    [self showInfo];
    return;
}

- (IBAction)btnProfitOnClick:(id)btn
{
    // 毛利点击
//    CustSaleProfViewController *spf = [[CustSaleProfViewController alloc] initWithNibName:nil bundle:nil];
//    [spf setSTatusType:StatusTypeProf];
//    [self.navigationController pushViewController:spf animated:YES];
    [self showInfo];
    return;
}

- (IBAction)btnStockOnClick:(id)btn
{
    // 库存点击
//    CustNewIvtViewController *ivt = [[CustNewIvtViewController alloc] initWithNibName:nil bundle:nil];
//    [self.navigationController pushViewController:ivt animated:YES];
    [self showInfo];
    return;
}

- (IBAction)btnGuestAmountOnClick:(id)btn
{
    // 顾客点击
    [self showInfo];
    return;
}

- (void)showInfo {
    self.indicator.text = @"此功能尚在研发中,后续将会开放...";
    [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
}

#pragma mark - CTImageView delegate
- (void)ctImageViewDidClicked:(CTImageView *)ctImageView
{
    CustBannerInfo *banner = [self.imgArray objectAtIndex:ctImageView.tag];
    if ([banner.proType integerValue] == 0) {
        ConsBrand *brand = [[ConsBrand alloc] init];
        brand.brandId = banner.infoId;
        ConsBrandDetailController * controller = [[ConsBrandDetailController alloc] initWithNibName:@"ConsBrandDetailController" bundle:nil];
        controller.brand = brand;
        [self.navigationController pushViewController:controller animated:YES];
    }
    return;
}

- (void)ctImageViewLoadDone:(CTImageView *)ctImageView withResult:(CTImageViewState)state
{
    return;
}


#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([scrollView isEqual:self.tv]) {
        [self.tv ctTableViewDidScroll];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if ([scrollView isEqual:self.tv]) {
        [self.tv ctTableViewDidEndDragging];
    }
}

#pragma mark -
#pragma mark UITableView Delegate & DataSource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    if (self.indicator.showing) {
        return NO;
    }
    [self loadData];
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 110.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"MSG_CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIView * container = [[UIView alloc] initWithFrame:CGRectMake(5, 5, self.tv.frame.size.width - 10, 105)];
        container.backgroundColor = [UIColor clearColor];
        [cell addSubview:container];
        
        UIImageView * bg = [[UIImageView alloc] initWithFrame:container.bounds];
        bg.image = [[UIImage imageNamed:@"ActivityItemBg"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 3, 3, 3)];
        [container addSubview:bg];
        
        CTImageView * img = [[CTImageView alloc] initWithFrame:CGRectMake(7, 7, 131, 90)];
        img.tag = 1;
        [bg addSubview:img];
        
        UILabel * title = [[UILabel alloc] initWithFrame:CGRectMake(7 + 131 + 7, 7, container.frame.size.width - 7 - 131 - 7 - 7, 20)];
        title.textColor = [UIColor blackColor];
        title.font = [UIFont systemFontOfSize:16.0f];
        title.backgroundColor = [UIColor clearColor];
        title.tag = 2;
        [container addSubview:title];
        
        UILabel * time = [[UILabel alloc] initWithFrame:CGRectMake(7 + 131 + 7, 7 + title.frame.size.height + 3, container.frame.size.width - 7 - 131 - 7 - 7, 15)];
        time.textColor = [UIColor colorWithRed:254/255.0 green:161/255.0 blue:30/255.0 alpha:1];
        time.font = [UIFont systemFontOfSize:14.0f];
        time.backgroundColor = [UIColor clearColor];
        time.tag = 3;
        time.adjustsFontSizeToFitWidth = YES;
        [container addSubview:time];
        
        UILabel * content = [[UILabel alloc] initWithFrame:CGRectMake(7 + 131 + 7, 7 + title.frame.size.height + 3 + time.frame.size.height + 3, container.frame.size.width - 7 - 131 - 7 - 7, 34)];
        content.textColor = [UIColor colorWithRed:106/255.0 green:106/255.0 blue:106/255.0 alpha:1];
        content.font = [UIFont systemFontOfSize:14.0f];
        content.backgroundColor = [UIColor clearColor];
        content.tag = 5;
        content.numberOfLines = 2;
        [container addSubview:content];
        
        UILabel * personLabel = [[UILabel alloc] initWithFrame:CGRectMake(container.frame.size.width - 5, 7 + title.frame.size.height + 3 + time.frame.size.height + 3 + content.frame.size.height + 3, 40, 11)];
        personLabel.textColor = [UIColor colorWithRed:155/255.0 green:94/255.0 blue:13/255.0 alpha:1];
        personLabel.font = [UIFont systemFontOfSize:10.0f];
        personLabel.backgroundColor = [UIColor clearColor];
        personLabel.text = @"人参与";
        [personLabel sizeToFit];
        personLabel.frame = CGRectMake(container.frame.size.width - personLabel.frame.size.width - 5, 7 + title.frame.size.height + 3 + time.frame.size.height + 3 + content.frame.size.height + 3, personLabel.frame.size.width, personLabel.frame.size.height);
        [container addSubview:personLabel];
        
        UILabel * person = [[UILabel alloc] initWithFrame:CGRectMake(container.frame.size.width - 5 - personLabel.frame.size.width - 3 - 42, 8 + title.frame.size.height + 3 + time.frame.size.height + 3 + content.frame.size.height + 3, 42, 11)];
        person.textColor = [UIColor colorWithRed:155/255.0 green:94/255.0 blue:13/255.0 alpha:1];
        person.font = [UIFont systemFontOfSize:10.0f];
        person.backgroundColor = [UIColor clearColor];
        person.textAlignment = NSTextAlignmentCenter;
        person.adjustsFontSizeToFitWidth = YES;
        person.tag = 4;
        person.backgroundColor = [UIColor colorWithPatternImage:[[UIImage imageNamed:@"CustActityPersonBg"] resizableImageWithCapInsets:UIEdgeInsetsMake(5.5, 15, 5.5, 15)]];
        [container addSubview:person];
    }
    
    CustActivity * act = [self.array objectAtIndex:indexPath.row];
    
    CTImageView * img = (CTImageView *)[cell viewWithTag:1];
    img.url = [CustWebService fileFullUrl:act.actPicUrl];
    [img loadImage];
    img.contentMode = UIViewContentModeScaleToFill;
    
    UILabel * title = (UILabel *)[cell viewWithTag:2];
    title.text = act.actTitle;
    
    UILabel * time = (UILabel *)[cell viewWithTag:3];
    time.text = [NSString stringWithFormat:@"%@ 至 %@", act.beginTime, act.endTime];
    
    UILabel * person = (UILabel *)[cell viewWithTag:4];
    person.text = [NSString stringWithFormat:@"%@", act.actNum];
    
    UILabel * content = (UILabel *)[cell viewWithTag:5];
    content.text = act.actCont;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CustActivity * item = [self.array objectAtIndex:indexPath.row];
    
    if ([item.actUrl isEqualToString:@"ALL_ACT_LIST"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_ALL_ACTIVITY object:item];
        return;
    }
    
    ConsMsgDetailController * controller = [[ConsMsgDetailController alloc] initWithNibName:nil bundle:nil];
    [controller setConsAct:item];
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0f];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_LOADING_BANNER) {
        CustBannerInfo *bannerParser = [[CustBannerInfo alloc] init];
        [bannerParser parseData:data complete:^(NSArray *array) {
            [self.imgArray removeAllObjects];
            [self.imgArray addObjectsFromArray:array];
            
            NSMutableArray *imgViewArray = [[NSMutableArray alloc] init];
            for (CustBannerInfo *banner in self.imgArray) {
                CTImageView *iv = [[CTImageView alloc] initWithFrame:self.banner.bounds];
                iv.url = [CustWebService fileFullUrl:banner.proImg];
                [iv loadImage];
                iv.delegate = self;
                iv.tag = [self.imgArray indexOfObject:banner];
                iv.contentMode = UIViewContentModeScaleToFill;
                [imgViewArray addObject:iv];
            }
            self.banner.viewArray = [NSArray arrayWithArray:imgViewArray];
            [self loadTrackData];
        }];
    } else if (connection.tag == PROCESS_LOADING_TRACK) {
        // 解析订单跟踪
        CustOrderTrack *trackParser = [[CustOrderTrack alloc] init];
        [trackParser parseData:data complete:^(CustOrderTrack *tagTrack) {
            self.labOrderTrackState.text = tagTrack.statusName;
            // 历史订单数据即为最后一次订单数
            self.labHisOrderAmount.text = tagTrack.qtyOrderSum;
        }];
        [self loadBankListData];
    } else if (connection.tag == PROCESS_LOADING_MONEY) {
        CustDefaultBank *bankParser = [[CustDefaultBank alloc] init];
        [bankParser parseData:data complete:^(CustDefaultBank *bank) {
            self.labMoneySum.text = [NSString stringWithFormat:@"%@", bank.custBalance];
        }];
        [self loadActivityData];
    } else if (connection.tag == PROCESS_LOADING_ACTIVITY) {
        [self.indicator hide];
        CustActivity *activityParser = [[CustActivity alloc] init];
        [activityParser parseData:data complete:^(NSArray *array) {
            [self.array removeAllObjects];
            [self.array addObjectsFromArray:array];
        }];
        [self.tv reloadData];
    }
    return;
}


@end
