//
//  CustOrderTrackViewController.m
//  CmAppCustIos
//
//  Created by hsit on 15-3-18.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustOrderTrackViewController.h"
#import "CTTableView.h"
#import "CustWebService.h"
#import "CustOrderTrack.h"
#import "CustOrderTrDtl.h"
#import "CustModule.h"

#define PROCESS_ORDER_STATUS 1
#define PROCESS_ORDER_LOGISTICS 2


@interface CustOrderTrackViewController ()<CTTableViewDelegate, UITableViewDataSource, UITableViewDelegate, CTURLConnectionDelegate>

@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) IBOutlet UIView *fView;
@property (nonatomic, strong) IBOutlet UIView *sView;
@property (nonatomic, strong) IBOutlet UILabel *orderDatelab;
@property (nonatomic, strong) IBOutlet UILabel *orderIdlab;
@property (nonatomic, strong) IBOutlet UILabel *amtOrderSumlab;
@property (nonatomic, strong) IBOutlet UILabel *qtyDemandSumlab;
@property (nonatomic, strong) IBOutlet UILabel *qtyOrderSumlab;
@property (nonatomic, strong) IBOutlet UILabel *balModeNamelab;
@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) CTIndicateView *indicator;

@end

@implementation CustOrderTrackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.array = [[NSMutableArray alloc] init];
    self.title = @"跟踪订单";
    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_ZXDH, CUST_MODULE_OPERATE_DDGZ, nil]];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initUI {
    self.fView.backgroundColor = XCOLOR(230, 245, 238, 1);
    CGFloat xHeight = self.headerView.frame.size.height;
    self.tv = [[CTTableView alloc] initWithFrame:CGRectMake(0, xHeight, self.view.frame.size.width, self.view.frame.size.height - xHeight)];
    self.tv.backgroundColor = [UIColor clearColor];
    self.tv.delegate = self;
    self.tv.dataSource = self;
    self.tv.ctTableViewDelegate = self;
    self.tv.pullUpViewEnabled = NO;
    self.tv.pullDownViewEnabled = NO;
    self.tv.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.tv];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.array.count == 0 && !self.indicator.showing) {
        self.indicator.text = @"加载中...";
        [self.indicator show];
        CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService queryOrderStatus:@"0" userId:[CustWebService sharedUser].userId] delegate:self];
        conn.tag = PROCESS_ORDER_STATUS;
        [conn start];
    }
}

- (void)loadData {
    if (!self.indicator.showing) {
        self.indicator.text = @"加载中...";
        [self.indicator show];
        CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService queryOrderLogistics:@"0" userId:[CustWebService sharedUser].userId] delegate:self];
        conn.tag = PROCESS_ORDER_LOGISTICS;
        [conn start];
    }
}
#pragma mark CTTableView Delegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    return NO;
}

#pragma mark - CTTableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"CELL";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        UIView *bg = [[[NSBundle mainBundle] loadNibNamed:@"CustOrTrack" owner:nil options:nil] lastObject];
        bg.frame = CGRectMake(0, 0, self.tv.frame.size.width, 60);
        bg.backgroundColor = [UIColor clearColor];
        [cell addSubview:bg];
    }
    CustOrderTrDtl *item = [self.array objectAtIndex:indexPath.row];
    
    UIImageView *wlTop = (UIImageView *)[cell viewWithTag:1];
    
    UIImageView *wlMid = (UIImageView *)[cell viewWithTag:2];
    wlMid.layer.cornerRadius = 5;
    wlMid.layer.masksToBounds = YES;
    
    UIImageView *wlBot = (UIImageView *)[cell viewWithTag:3];

    
    UILabel *stautsName = (UILabel *)[cell viewWithTag:22];
    stautsName.text = item.statusName;
    
    UILabel *statusTime = (UILabel *)[cell viewWithTag:23];
    
    UIImageView *line = (UIImageView *)[cell viewWithTag:25];
    if (indexPath.row == self.array.count - 1) {
        line.frame = CGRectMake(0, 59, self.tv.frame.size.width, 1);
        wlBot.hidden = YES;
    } else if (indexPath.row == 0) {
        wlTop.hidden = YES;
        wlMid.image =[UIImage imageNamed:@"ICON_CJD"];
    }
    if ([item.statusTime isEqualToString:@"null"]) {
        item.statusTime = @"";
    }
    statusTime.text = item.statusTime;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -Connection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.indicator.text = @"加载失败";
    [self.tv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    if (connection.tag == PROCESS_ORDER_STATUS) {
        CustOrderTrack *track = [[CustOrderTrack alloc] init];
        [track parseData:data complete:^(CustOrderTrack *track) {
            self.orderDatelab.text = track.orderDate;
            self.orderIdlab.text = track.orderId;
            self.amtOrderSumlab.text = track.amtOrderSum;
            self.qtyDemandSumlab.text = track.qtyDemandSum;
            self.qtyOrderSumlab.text = track.qtyOrderSum;
            self.balModeNamelab.text = track.balModeName;
            self.indicator.showing = NO;
            [self loadData];
        }];
    } else if (connection.tag == PROCESS_ORDER_LOGISTICS) {
        CustOrderTrDtl *item = [[CustOrderTrDtl alloc] init];
        [item parseData:data complete:^(NSArray *array) {
            [self.array removeAllObjects];
            for (NSInteger i = array.count - 1; i > 0; i--) {
                [self.array addObject:[array objectAtIndex:i]];
            }
            [self.tv reloadData];
            [self.indicator hide];
        }];
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
