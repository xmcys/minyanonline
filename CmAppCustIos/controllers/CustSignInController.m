//
//  CustSignInController.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-2-8.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustSignInController.h"
#import "CustWebService.h"
#import "CTURLConnection.h"
#import "CTIndicateView.h"
#import "ConsSetting.h"
#import "ConsAdController.h"

@interface CustSignInController () <UITextFieldDelegate, CTURLConnectionDelegate>

@property (nonatomic, strong) UIImageView * bg;
@property (nonatomic, strong) UIImageView * logo;
@property (nonatomic, strong) IBOutlet UIView * inputView;
@property (nonatomic, strong) IBOutlet UIImageView * inputBg;
@property (nonatomic, strong) IBOutlet UIButton * btnSignIn;
@property (nonatomic, strong) IBOutlet UITextField * userCode;
@property (nonatomic, strong) IBOutlet UITextField * pwd;
@property (nonatomic, strong) UIControl * cover;
@property (nonatomic, strong) CTIndicateView * indicator;

- (void)hideKeyboard;
- (IBAction)signIn:(UIButton *)sender;

@end

@implementation CustSignInController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)initUI{
    
    self.view.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:239.0/255.0 blue:238.0/255.0 alpha:1];
    self.inputBg.image = [[UIImage imageNamed:@"CustSignInInputBg"] resizableImageWithCapInsets:UIEdgeInsetsMake(35, 26, 35, 26)];
    [self.btnSignIn setBackgroundImage:[[UIImage imageNamed:@"BtnGreenNormal"] resizableImageWithCapInsets:UIEdgeInsetsMake(19, 5, 19, 5)] forState:UIControlStateNormal];
    [self.btnSignIn setBackgroundImage:[[UIImage imageNamed:@"BtnRed"] resizableImageWithCapInsets:UIEdgeInsetsMake(19, 5, 19, 5)] forState:UIControlStateHighlighted];
    
    NSString * userCode = [ConsSetting userName];
    NSString * pwd = [ConsSetting password];
    if (userCode != nil) {
        self.userCode.text = userCode;
        self.pwd.text = pwd;
    }
    
    if (self.bg == nil) {
        self.bg = [[UIImageView alloc] initWithFrame:self.view.bounds];
        self.bg.contentMode = UIViewContentModeScaleAspectFill;
        self.bg.image = [UIImage imageNamed:@"CustSignInBg"];
        [self.view addSubview:self.bg];
    }
    
    if (self.logo == nil) {
        self.logo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 120, 160)];
        self.logo.contentMode = UIViewContentModeScaleAspectFill;
        self.logo.image = [UIImage imageNamed:@"CustSignInLogo"];
        self.logo.center = CGPointMake(self.view.bounds.size.width / 2, 40 + self.logo.frame.size.height / 2);
        [self.view addSubview:self.logo];
    }
    
    if (self.cover == nil) {
        self.cover = [[UIControl alloc] initWithFrame:self.view.bounds];
        [self.cover addTarget:self action:@selector(hideKeyboard) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.cover];
        
        self.inputView.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height - 45 - self.inputView.frame.size.height / 2);
        [self.view addSubview:self.inputView];

    }
    
    [self.view bringSubviewToFront:self.inputView];
    
    if (self.indicator == nil) {
        self.indicator = [[CTIndicateView alloc] initInView:self.view];
        self.indicator.backgroundTouchable = NO;
        [self.view addSubview:self.indicator];
    }
    
    [self.view bringSubviewToFront:self.indicator];
    
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSDictionary* infoDict =[[NSBundle mainBundle] infoDictionary];
    NSString * version = [infoDict objectForKey:@"CFBundleShortVersionString"];
    
    if (![[ConsSetting lastestGuideVersion] isEqualToString:version]) {
        ConsAdController * ad = [[ConsAdController alloc] initWithNibName:nil bundle:nil];
        ad.isAd = NO;
        [self presentViewController:ad animated:YES completion:nil];
        [ConsSetting setLastestGuideVersion:version];
    }
}

- (void)hideKeyboard
{
    [self.userCode resignFirstResponder];
    [self.pwd resignFirstResponder];
    [UIView animateWithDuration:0.2 animations:^{
        self.inputView.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height - 45 - self.inputView.frame.size.height / 2);
    }];
}

- (void)signIn:(UIButton *)sender
{
    [self hideKeyboard];
    
    if (self.indicator.showing) {
        return;
    }
    
    NSString * userCode = self.userCode.text;
    NSString * pwd = self.pwd.text;
    
    if (userCode.length != 12) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"请输入12位许可证号" message:nil delegate:nil cancelButtonTitle:@"好" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    if (pwd.length == 0) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"密码不能为空" message:nil delegate:nil cancelButtonTitle:@"好" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    [CustWebService setWebServiceHost:userCode];
    
    self.indicator.text = @"正在验证..";
    [self.indicator show];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService custSignInUrl:userCode pwd:pwd] delegate:self];
    [conn start];
}

#pragma mark -
#pragma mark UITextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (self.inputView.frame.origin.y != 115 + self.inputView.frame.size.height / 2) {
        [UIView animateWithDuration:0.2 animations:^{
            self.inputView.center = CGPointMake(self.view.bounds.size.width / 2, 115 + self.inputView.frame.size.height / 2);
        }];
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.userCode) {
        [self.pwd becomeFirstResponder];
    } else {
        [self.userCode resignFirstResponder];
        [self.pwd resignFirstResponder];
        [UIView animateWithDuration:0.2 animations:^{
            self.inputView.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height - 45 - self.inputView.frame.size.height / 2);
        }];
        [self signIn:self.btnSignIn];
    }
    return YES;
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"验证失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    CustInfo * info = [[CustInfo alloc] init];
    [info parseData:data];
    if (info.userId == nil || [info.userId isEqualToString:@""]) {
        self.indicator.text = @"用户名或密码有误";
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    } else {
        [self.indicator hide];
        [CustWebService setUser:info];
        [ConsSetting setUserName:self.userCode.text];
        [ConsSetting setPassword:self.pwd.text];
        if ([self.delegate respondsToSelector:@selector(custSignInControllerDidSignIn:)]) {
            [self.delegate custSignInControllerDidSignIn:self];
        }
    }
    
}

@end
