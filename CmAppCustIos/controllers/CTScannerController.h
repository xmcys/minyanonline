//
//  ConsScannerController.h
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-8.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"


@class CTScannerController;

@protocol CTScannerControllerDelegate <NSObject>

- (void)ctScannerController:(CTScannerController *)controller didReceivedScanResult:(NSString *)result;

@end

@interface CTScannerController : CTViewController

@property (nonatomic, weak) id<CTScannerControllerDelegate> delegate;

@end
