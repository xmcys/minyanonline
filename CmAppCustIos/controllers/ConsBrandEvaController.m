//
//  ConsBrandEvaController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-25.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsBrandEvaController.h"
#import "CTRatingView.h"
#import "CustWebService.h"
#import "ConsBrand.h"
#import "CustSystemMsg.h"

@interface ConsBrandEvaController () <UIAlertViewDelegate, CTRatingViewDelegate>

@property (nonatomic, strong) IBOutlet UIView * header;
@property (nonatomic, strong) IBOutlet UILabel * tasteScore;
@property (nonatomic, strong) IBOutlet UILabel * pkgScore;
@property (nonatomic, strong) IBOutlet UILabel * priceScore;
@property (nonatomic, strong) IBOutlet CTRatingView * tasteRate;
@property (nonatomic, strong) IBOutlet CTRatingView * pkgRate;
@property (nonatomic, strong) IBOutlet CTRatingView * priceRate;
@property (nonatomic, strong) IBOutlet UIButton * btnHideUserName;
@property (nonatomic) BOOL userNameEnable;

- (IBAction)showOrHideUser:(id)sender;

- (void)submit;

@end

@implementation ConsBrandEvaController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"我的评论";
    }
    return self;
}

- (void)initUI
{
	UIButton * rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.backgroundColor = [UIColor clearColor];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"BtnSubmit"] forState:UIControlStateNormal];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"BtnSubmit"] forState:UIControlStateHighlighted];
    rightBtn.showsTouchWhenHighlighted = YES;
    [rightBtn addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn sizeToFit];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    
    
    NSArray* elements = [[NSBundle mainBundle] loadNibNamed:@"ConsMyEva" owner:nil options:nil];
    
    self.header = [elements objectAtIndex:0];
    self.header.frame = CGRectMake(0, 0, self.view.frame.size.width, 210);
    UIImageView * img = (UIImageView *)[self.header viewWithTag:8];
    img.image = [[UIImage imageNamed:@"MyEvaBg"] resizableImageWithCapInsets:UIEdgeInsetsMake(7, 1, 5, 1)];
    
    [self.view addSubview:self.header];
    
    UILabel * tasteLabel = (UILabel *)[self.header viewWithTag:101];
    tasteLabel.text = @"口味：";
    self.tasteRate = (CTRatingView *)[self.header viewWithTag:1];
    self.tasteRate.delegate = self;
    self.tasteScore = (UILabel *)[self.header viewWithTag:4];
    
    UILabel * pkgLabel = (UILabel *)[self.header viewWithTag:102];
    pkgLabel.text = @"外观：";
    self.pkgRate = (CTRatingView *)[self.header viewWithTag:2];
    self.pkgRate.delegate = self;
    self.pkgScore = (UILabel *)[self.header viewWithTag:5];
    
    UILabel * priceLabel = (UILabel *)[self.header viewWithTag:103];
    priceLabel.text = @"性价比：";
    self.priceRate = (CTRatingView *)[self.header viewWithTag:3];
    self.priceRate.delegate = self;
    self.priceScore = (UILabel *)[self.header viewWithTag:6];
    
    self.btnHideUserName = (UIButton *)[self.header viewWithTag:7];
    [self.btnHideUserName addTarget:self action:@selector(showOrHideUser:) forControlEvents:UIControlEventTouchUpInside];
    
    self.userNameEnable = YES;
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CONS_MODULE_JYWDPJ, CONS_MODULE_JYWDPJ, nil]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)showOrHideUser:(id)sender
{
    self.userNameEnable = !self.userNameEnable;
    
    if (self.userNameEnable) {
        [self.btnHideUserName setImage:[UIImage imageNamed:@"SwitchOff"] forState:UIControlStateNormal];
        [self.btnHideUserName setImage:[UIImage imageNamed:@"SwitchOff"] forState:UIControlStateHighlighted];
    } else {
        [self.btnHideUserName setImage:[UIImage imageNamed:@"SwitchOn"] forState:UIControlStateNormal];
        [self.btnHideUserName setImage:[UIImage imageNamed:@"SwitchOn"] forState:UIControlStateHighlighted];
    }
}

- (void)submit
{
    if (self.indicator.showing) {
        return;
    }
    
    double taste = self.tasteRate.rate;
    double pkg = self.pkgRate.rate;
    double price = self.priceRate.rate;
    
    if (taste == 0 || pkg == 0 || price == 0) {
        self.indicator.text = @"您还有内容未评价哦！";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    
    self.indicator.text = @"正在提交..";
    self.indicator.backgroundTouchable = NO;
    [self.indicator show];
    
    NSString * params = [NSString stringWithFormat:@"<RecommendCigarette><brandId>%@</brandId><userId>%@</userId><tasteScore>%.1f</tasteScore><packageScore>%.1f</packageScore><priceScore>%.1f</priceScore><isAnonymous>%d</isAnonymous></RecommendCigarette>", self.brand.brandId, [CustWebService sharedUser].userId, taste, pkg, price, !self.userNameEnable];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService brandEvaSaveUrl] params:params delegate:self];
    [conn start];
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"提交失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    CustSystemMsg * msg = [[CustSystemMsg alloc] init];
    [msg parseData:data];
    if ([msg.code isEqualToString:@"1"]) {
        [self.indicator hide];
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"评论提示" message:@"评论成功！" delegate:self cancelButtonTitle:@"好" otherButtonTitles: nil];
        [alert show];
        if ([self.delegate respondsToSelector:@selector(consBrandEvaControllerDidEvaluted:)]) {
            [self.delegate consBrandEvaControllerDidEvaluted:self];
        }
    } else {
        self.indicator.text = msg.msg;
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    }
}

#pragma mark -
#pragma mark UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark CTRatingView Delegate
- (void)ctRatingViewDidRating:(CTRatingView *)ratingView withmRate:(float)mRate
{
    if (ratingView == self.tasteRate) {
        self.tasteScore.text = [NSString stringWithFormat:@"%.1f分", self.tasteRate.rate];
    } else if (ratingView == self.pkgRate){
        self.pkgScore.text = [NSString stringWithFormat:@"%.1f分", self.pkgRate.rate];
    } else {
        self.priceScore.text = [NSString stringWithFormat:@"%.1f分", self.priceRate.rate];
    }
}

@end
