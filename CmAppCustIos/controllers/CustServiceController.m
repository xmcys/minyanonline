//
//  CustServiceController.m
//  CmAppCustIos
//
//  Created by Arcesaka on 14/8/25.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustServiceController.h"
#import "CustMyPointViewController.h"
#import "CustExchangeViewController.h"
#import "CustSerEvaluViewController.h"
#import "ConsFeedbackController.h"
#import "CTBannerView.h"
#import "CustWebService.h"
#import "CustBannerInfo.h"
#import "CTImageView.h"
#import "ConsBrand.h"
#import "ConsBrandDetailController.h"
#import "CustModule.h"
#import "ConsWebController.h"

@interface CustServiceController ()<CTURLConnectionDelegate, CTImageViewDelegate>

@property (nonatomic, strong) CTBannerView *banner;
@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) NSMutableArray *imgArray;

- (IBAction)queryClick:(id)btn;     //积分查询
- (IBAction)exchangeClick:(id)btn;  //积分兑换
- (IBAction)starClick:(id)btn;      //星级
- (IBAction)evaluateClick:(id)btn;  //服务评价
- (IBAction)suggestClick:(id)btn;   //意见反馈

@end

@implementation CustServiceController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setTabBarItemWithTitle:@"客户服务" titleColor:[UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1] titleSelectedColor:[UIColor colorWithRed:4.0/255.0 green:168.0/255.0 blue:117.0/255.0 alpha:1] image:[UIImage imageNamed:@"CustTabServiceNormal"] selectedImage:[UIImage imageNamed:@"CustTabServiceSelected"]];
    }
    return self;
}

- (IBAction)queryClick:(id)btn    //积分查询
{
    CustMyPointViewController *controller = [[CustMyPointViewController alloc] initWithNibName:@"CustMyPointViewController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

- (IBAction)exchangeClick:(id)btn //积分兑换
{
    CustExchangeViewController *controller = [[CustExchangeViewController alloc] initWithNibName:@"CustExchangeViewController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

- (IBAction)starClick:(id)btn      //星级
{
    ConsWebController * webController = [[ConsWebController alloc] init];
    webController.mainUrl = [CustWebService getCustLevelUrl];
    [self.navigationController pushViewController:webController animated:YES];
    return;
}

- (IBAction)evaluateClick:(id)btn  //服务评价
{
    CustSerEvaluViewController *sev = [[CustSerEvaluViewController alloc] initWithNibName:@"CustSerEvaluViewController" bundle:nil];
    [self.navigationController pushViewController:sev animated:YES];
    return;
}

- (IBAction)suggestClick:(id)btn  //意见反馈
{
    ConsFeedbackController *controller = [[ConsFeedbackController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
    return;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.imgArray = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_KHFW, CUST_MODULE_KHFW, nil]];

}

- (void)initUI {
    return;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.title = @"客户服务";
    // 广告轮播view
    if (self.banner == nil) {
        self.banner = [[CTBannerView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 150)];
        [self.banner setIndicateColor:[UIColor lightGrayColor] highlightedColor:XCOLOR(4, 168, 117, 1)];
        [self.headerView addSubview:self.banner];
    }
    [self loadBannerData];
}

- (void)loadBannerData {
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"加载中...";
    [self.indicator show];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService mobileConsumerNew:@"1" position:@"3"] delegate:self];
    [conn start];
}
#pragma mark - CTImage - Delegate
// 图片加载完毕后，会调用此方法
- (void)ctImageViewLoadDone:(CTImageView *)ctImageView withResult:(CTImageViewState)state {
    return;
}
// 图片点击事件，需启用userInteractionEnabled并且实现本方法才可触发
- (void)ctImageViewDidClicked:(CTImageView *)ctImageView {
    CustBannerInfo *banner = [self.imgArray objectAtIndex:ctImageView.tag];
    if ([banner.proType integerValue] == 0) {
        ConsBrand *brand = [[ConsBrand alloc] init];
        brand.brandId = banner.infoId;
        ConsBrandDetailController * controller = [[ConsBrandDetailController alloc] initWithNibName:@"ConsBrandDetailController" bundle:nil];
        controller.brand = brand;
        [self.navigationController pushViewController:controller animated:YES];
    }
    return;
}
#pragma mark - CTURLConnection - Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.indicator.text = @"加载失败";
    [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
}
- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
        CustBannerInfo *bannerParser = [[CustBannerInfo alloc] init];
        [bannerParser parseData:data complete:^(NSArray *array) {
            [self.imgArray removeAllObjects];
            [self.imgArray addObjectsFromArray:array];
            
            NSMutableArray *imgViewArray = [[NSMutableArray alloc] init];
            for (CustBannerInfo *banner in self.imgArray) {
                CTImageView *iv = [[CTImageView alloc] initWithFrame:self.banner.bounds];
                iv.url = [CustWebService fileFullUrl:banner.proImg];
                [iv loadImage];
                iv.delegate = self;
                iv.tag = [self.imgArray indexOfObject:banner];
                iv.contentMode = UIViewContentModeScaleToFill;
                [imgViewArray addObject:iv];
            }
            self.banner.viewArray = [NSArray arrayWithArray:imgViewArray];
        }];
    [self.indicator hide];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
