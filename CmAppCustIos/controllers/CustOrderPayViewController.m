//
//  CustOrderPayViewController.m
//  CmAppCustIos
//
//  Created by hsit on 14-7-31.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustOrderPayViewController.h"
#import "CTTableView.h"
#import "CTIndicateView.h"
#import "CTURLConnection.h"
#import "CustWebService.h"
#import "CustBankList.h"
#import "CustAmtOrderSum.h"
#import "CustSystemMsg.h"

#define PROCESS_BANKORDER  1
#define PROCESS_BANKCARDLIST 2
#define PROCESS_REALTIMETRAN 3

@interface CustOrderPayViewController ()<CTTableViewDelegate,UITableViewDelegate,UITableViewDataSource,CTURLConnectionDelegate>

@property (nonatomic, strong) IBOutlet UIView * headerView;
@property (nonatomic, strong) CTTableView     * tv;
@property (nonatomic, strong) CTIndicateView  * indicator;
@property (nonatomic, strong) NSMutableArray  * bankCardArray;
@property (nonatomic, strong) UIView          * borderView;
@property (nonatomic, strong) NSString        * cardNo;
@property (nonatomic) int process;

@end

@implementation CustOrderPayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"立即支付";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.bankCardArray = [[NSMutableArray alloc] init];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame =CGRectMake(0, 0, 20, 20);
    [btn setImage:[UIImage imageNamed:@"Commit"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(realTimeTran:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = right;
}

- (void)realTimeTran:(UIButton *)btn
{
    if (self.indicator.showing) {
        return;
    }
    if (self.cardNo == nil) {
        self.indicator.text = @"请点击选中银行卡,再进行支付";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    self.indicator.text = @"支付确认中...";
    [self.indicator show];
    NSString *params = [NSString stringWithFormat:@"<TranOrder><userId>%@</userId><orderId>%@</orderId><cardNo>%@</cardNo></TranOrder>",[CustWebService sharedUser].userId,self.orderId,self.cardNo];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService realTimeTran] params:params delegate:self];
    self.process = PROCESS_REALTIMETRAN;
    [conn start];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.tv == nil) {
        self.indicator = [[CTIndicateView alloc] initInView:self.view];
        self.indicator.backgroundTouchable = NO;
        self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.pullUpViewEnabled = NO;
        [self.tv setTableHeaderView:self.headerView];
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        [self.view addSubview:self.tv];
    }
    [self loadBankOrder];
}

- (void)loadBankOrder
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"加载中...";
    [self.indicator show];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getBankOrder:self.orderId] delegate:self];
    self.process = PROCESS_BANKORDER;
    [conn start];
}

- (void)loadBankList
{
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getBankBalance:[CustWebService sharedUser].userId] delegate:self];
    self.process = PROCESS_BANKCARDLIST;
    [conn start];
}
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark - CTTableView Delegate & DataSource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadBankOrder];
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.bankCardArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 68;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"CustBankCardListCell" owner:nil options:nil];
        self.borderView = [elements objectAtIndex:0];
        self.borderView.frame = CGRectMake(5, 8, tableView.frame.size.width - 5 * 2, 60);
        self.borderView.layer.borderColor = [[UIColor colorWithRed:202.0/255.0 green:201.0/255.0 blue:202.0/255.0 alpha:1] CGColor];
        self.borderView.layer.borderWidth = 1;
        [cell addSubview:self.borderView];
    }
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:@"card1", @"农业银行", @"card2", @"兴业银行", @"card3", @"邮政银行", @"card4", @"工商银行", @"card5", @"建设银行", @"card6", @"交通银行", nil];
    UIImageView *imgBank = (UIImageView *)[cell viewWithTag:1];
    UILabel *labCard = (UILabel *)[cell viewWithTag:2];
    UILabel *labBanlace = (UILabel *)[cell viewWithTag:3];
    
    CustBankList *list = [self.bankCardArray objectAtIndex:indexPath.row];
//    if ([list.isDefault isEqualToString:@"1"]) {
//        NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
//        [self.tv selectRowAtIndexPath:selectedIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
//    }
    labBanlace.text = list.balance;
    labCard.text = list.cardNo;
    imgBank.image = [UIImage imageNamed:[dict objectForKey:list.bankName]];
    if ([dict objectForKey:list.bankName] == nil) {
        imgBank.image = nil;
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.borderView.layer.borderColor = [[UIColor orangeColor] CGColor];
    CustBankList *list = [self.bankCardArray objectAtIndex:indexPath.row];
    self.cardNo = list.cardNo;
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    if (self.process == PROCESS_BANKORDER) {
        self.indicator.text = @"加载失败...";
    } else if (self.process == PROCESS_BANKCARDLIST) {
        self.indicator.text = @"加载失败...";
    } else if (self.process == PROCESS_REALTIMETRAN) {
        self.indicator.text = @"支付失败";
    }
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.tv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (self.process == PROCESS_BANKORDER) {
        CustAmtOrderSum * order = [[CustAmtOrderSum alloc] init];
        [order parseData:data];
        self.indicator.showing = NO;
        UILabel *labSum = (UILabel *)[self.headerView viewWithTag:1];
        labSum.text = order.amtOrderSum;
        [self loadBankList];
    } else if (self.process == PROCESS_BANKCARDLIST){
        CustBankList * list = [[CustBankList alloc] init];
        [list parseData:data complete:^(NSArray *array){
            [self.indicator hide];
            [self.bankCardArray removeAllObjects];
            [self.bankCardArray addObjectsFromArray:array];
            [self.tv reloadData];
        }];
    } else if (self.process == PROCESS_REALTIMETRAN){
        CustSystemMsg * msg = [[CustSystemMsg alloc] init];
        [msg parseData:data];
        if ([msg.code isEqualToString:@"1"]){
            self.indicator.text = msg.msg;
            [self.indicator hideWithSate:CTIndicateStateDone afterDelay:1.0];
        } else {
            self.indicator.text = msg.msg;
            [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
