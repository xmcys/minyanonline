//
//  CustMyShopViewController.m
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/19.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustMyShopViewController.h"
#import "CTTableView.h"
#import "CustShopManageViewController.h"
#import "CustPublicViewController.h"
#import "CustCustomerInfoViewController.h"
#import "CustSaleInfoViewController.h"
#import "CustModule.h"
#import "CustWebService.h"
#import "CustSumer.h"

#define PROCESS_GET_CONSUMER 1
#define PROCESS_GET_PUBLIC   2

static NSString * const PublicCellIdentifier = @"PublicCell";
static NSString * const CustomerCellIdentifier = @"CustomerCell";

@interface CustMyShopViewController () <UITableViewDataSource, UITableViewDelegate, CTTableViewDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) IBOutlet UIView *headerView;

@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) NSMutableArray *publicArray;
@property (nonatomic, strong) NSMutableArray *customerArray;

- (IBAction)btnPubClick:(id)sender;
- (IBAction)btnShpClick:(id)sender;
- (IBAction)btnCifClick:(id)sender;
- (IBAction)btnSimClick:(id)sender;

- (void)loadData;

@end

@implementation CustMyShopViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"我的店铺";
    self.publicArray = [[NSMutableArray alloc] init];
    self.customerArray = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_WDDP, CUST_MODULE_WDDP, nil]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.tv.pullUpViewEnabled = NO;
        self.tv.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        [self.tv setTableHeaderView:self.headerView];
        [self.tv registerNib:[UINib nibWithNibName:@"CustPublicCell" bundle:nil] forCellReuseIdentifier:PublicCellIdentifier];
        [self.tv registerNib:[UINib nibWithNibName:@"CustCustomerCell" bundle:nil] forCellReuseIdentifier:CustomerCellIdentifier];
        self.tv.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.tv];
        
        [self loadData];
    }
}

#pragma mark - http requests
- (void)loadData
{
//    self.customerArray = [NSMutableArray arrayWithArray:@[@"",@"",@"",@"",@"",@""]];
//    self.publicArray = [NSMutableArray arrayWithArray:@[@"",@"",@"",@"",@"",@"",@"",@""]];
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中...";
    [self.indicator show];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getMyConsumerThisWeek:[CustWebService sharedUser].userCode] delegate:self];
    conn.tag = PROCESS_GET_CONSUMER;
    [conn start];
    return;

    return;
}

#pragma mark - button click events
-(IBAction)btnPubClick:(id)sender
{
    CustPublicViewController *controller = [[CustPublicViewController alloc] initWithNibName:@"CustPublicViewController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

-(IBAction)btnShpClick:(id)sender
{
    CustShopManageViewController *controller = [[CustShopManageViewController alloc] initWithNibName:@"CustShopManageViewController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

-(IBAction)btnCifClick:(id)sender
{
    CustCustomerInfoViewController *controller = [[CustCustomerInfoViewController alloc] initWithNibName:@"CustCustomerInfoViewController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

-(IBAction)btnSimClick:(id)sender
{
    CustSaleInfoViewController *controller = [[CustSaleInfoViewController alloc] initWithNibName:@"CustSaleInfoViewController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

# pragma mark - UIScrollView delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}
#pragma mark UITableView Delegate & DataSource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    if (self.indicator.showing) {
        return NO;
    }
    [self loadData];
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return self.customerArray.count;
    }
    else {
        return self.publicArray.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 41.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 106;
    } else {
        return 75;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"CustCustomerSectionCell" owner:nil options:nil];
        UIView * customerHeader = [elements objectAtIndex:0];
        return customerHeader;
    }
    else {
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"CustPublicSectionCell" owner:nil options:nil];
        UIView * publicHeader = [elements objectAtIndex:0];
        return publicHeader;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CustomerCellIdentifier forIndexPath:indexPath];
        
        UILabel *lab = (UILabel *)[cell viewWithTag:3];
        UILabel *lab2 = (UILabel *)[cell viewWithTag:4];
        UILabel *lab3 = (UILabel *)[cell viewWithTag:5];
        UILabel *lab4 = (UILabel *)[cell viewWithTag:6];
        
        CustSumer *sum = [self.customerArray objectAtIndex:indexPath.row];
        lab.text = sum.vipName;
        lab3.text = sum.tel;
        lab4.text = sum.applyindate;
        
    }
    else {
        
        cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:PublicCellIdentifier forIndexPath:indexPath];
        
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.indicator.text = @"加载失败";
    [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    if (connection.tag == PROCESS_GET_CONSUMER) {
        CustSumer *sum = [[CustSumer alloc] init];
        [sum parseData:data complete:^(NSArray *array) {
            [self.customerArray removeAllObjects];
            [self.customerArray addObjectsFromArray:array];
            [self.tv reloadData];
            [self.indicator hide];
        }];
    }
}



@end
