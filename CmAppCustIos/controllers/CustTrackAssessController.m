//
//  CustTrackAssessController.m
//  CmAppCustIos
//
//  Created by hsit on 14-8-27.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustTrackAssessController.h"
#import "CTIndicateView.h"
#import "CustWebService.h"
#import "CTURLConnection.h"
#import "CustServiceMsg.h"
#import "CustSystemMsg.h"
#import "CTUtil.h"
#import "CTRatingView.h"

#define PROCESS_LOADING_INFO 1
#define PROCESS_UPDATE_INFO  2

@interface CustTrackAssessController ()<CTURLConnectionDelegate,UITextViewDelegate>

@property (nonatomic, strong) IBOutlet UIView * header;
@property (nonatomic, strong) IBOutlet UIView * answerView;
@property (nonatomic, strong) UIScrollView    * scrollview;
@property (nonatomic, strong) CTIndicateView  * indicator;
@property (nonatomic, strong) IBOutlet UIImageView * line;
@property (nonatomic, strong) IBOutlet UILabel *titlelab;
@property (nonatomic, strong) IBOutlet UILabel *idlab;
@property (nonatomic, strong) IBOutlet UILabel *timelab;
@property (nonatomic, strong) IBOutlet UILabel *desclab;
@property (nonatomic, strong) IBOutlet UILabel *personlab;
@property (nonatomic, strong) IBOutlet UILabel *dealtimelab;
@property (nonatomic, strong) IBOutlet UILabel *typelab;
@property (nonatomic, strong) IBOutlet UILabel *answconlab;

@property (nonatomic, strong) IBOutlet UIView * assentView;     //待评价视图
@property (nonatomic, strong) IBOutlet UITextView * textView;
@property (nonatomic, strong) IBOutlet UIButton * btncommit;
@property (nonatomic, strong) CTRatingView    * xysdlevel;
@property (nonatomic, strong) CTRatingView    * fwsdlevel;
@property (nonatomic, strong) CTRatingView    * fwxglevel;
@property (nonatomic, strong) IBOutlet UILabel * xysdlab;
@property (nonatomic, strong) IBOutlet UILabel * fwsdlab;
@property (nonatomic, strong) IBOutlet UILabel * fwxglab;

@property (nonatomic, strong) IBOutlet UIView * assentedView;   //已评价视图
@property (nonatomic, strong) IBOutlet UILabel * asslab;
@property (nonatomic, strong) CTRatingView    * xysdleveled;
@property (nonatomic, strong) CTRatingView    * fwsdleveled;
@property (nonatomic, strong) CTRatingView    * fwxgleveled;
@property (nonatomic, strong) IBOutlet UILabel * xysdedlab;
@property (nonatomic, strong) IBOutlet UILabel * fwsdedlab;
@property (nonatomic, strong) IBOutlet UILabel * fwxgedlab;
@property (nonatomic)BOOL keyboardIsShown;


- (IBAction)commitClick:(id)sender;

@end

@implementation CustTrackAssessController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.scrollview == nil) {
        self.indicator = [[CTIndicateView alloc] initInView:self.view];
        self.indicator.backgroundTouchable = YES;
        self.scrollview.backgroundColor = [UIColor clearColor];
        self.scrollview = [[UIScrollView alloc] initWithFrame:self.view.bounds];
        self.scrollview.contentSize = CGSizeMake(self.view.frame.size.width, self.header.frame.size.height + self.assentView.frame.size.height);
        self.scrollview.showsVerticalScrollIndicator = NO;
        [self.view addSubview:self.scrollview];
        
        self.header.backgroundColor = [UIColor colorWithRed:252.0/255.0 green:245.0/255.0 blue:252.0/255.0 alpha:1];
        self.line.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Dot"]];
        self.answerView.backgroundColor = [UIColor whiteColor];
        self.answerView.layer.cornerRadius = 5.0f;
        self.answerView.layer.masksToBounds = YES;
        [self.scrollview addSubview:self.header];
        
        self.assentView.backgroundColor = [UIColor clearColor];
        self.assentView.frame = CGRectMake(0, self.header.frame.size.height + self.header.frame.origin.y, self.assentView.frame.size.width, self.assentView.frame.size.height);
        self.textView.layer.cornerRadius = 5.0f;
        self.textView.layer.masksToBounds = YES;
        self.textView.delegate = self;
        self.xysdlevel = [[CTRatingView alloc] initWithFrame:CGRectMake(86, 136, 100, 21) image:@"StarBigNormal" highlightedImage:@"StarBigHelighted"];
        self.xysdlevel.ratingViewEnabled = YES;
        self.xysdlevel.tag = 1;
        self.xysdlevel.delegate = self;
        [self.assentView addSubview:self.xysdlevel];
        self.fwsdlevel = [[CTRatingView alloc] initWithFrame:CGRectMake(86, 162, 100, 21) image:@"StarBigNormal" highlightedImage:@"StarBigHelighted"];
        self.fwsdlevel.ratingViewEnabled = YES;
        self.fwsdlevel.tag = 2;
        self.fwsdlevel.delegate = self;
        [self.assentView addSubview:self.fwsdlevel];
        self.fwxglevel = [[CTRatingView alloc] initWithFrame:CGRectMake(86, 188, 100, 21) image:@"StarBigNormal" highlightedImage:@"StarBigHelighted"];
        self.fwxglevel.ratingViewEnabled = YES;
        self.fwxglevel.tag = 3;
        self.fwxglevel.delegate = self;
        [self.assentView addSubview:self.fwxglevel];
        UIImage *normalimg = [[UIImage imageNamed:@"CustServiceCommitNormal"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 7, 7, 5)];
        UIImage *heligatedimg = [[UIImage imageNamed:@"CustServiceCommitHelighted"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
        [self.btncommit setBackgroundImage:normalimg forState:UIControlStateNormal];
        [self.btncommit setBackgroundImage:heligatedimg forState:UIControlStateHighlighted];
        [self.scrollview addSubview:self.assentView];
        
        
        self.assentedView.backgroundColor = [UIColor whiteColor];
        self.assentedView.frame = CGRectMake(0, self.header.frame.size.height + self.header.frame.origin.y + 10, self.assentedView.frame.size.width , self.assentedView.frame.size.height);
        self.xysdleveled = [[CTRatingView alloc] initWithFrame:CGRectMake(86, 63, 100, 21) image:@"StarBigNormal" highlightedImage:@"StarBigHelighted"];
        self.xysdleveled.ratingViewEnabled = NO;
        [self.assentedView addSubview:self.xysdleveled];
        self.fwsdleveled = [[CTRatingView alloc] initWithFrame:CGRectMake(86, 91, 100, 21) image:@"StarBigNormal" highlightedImage:@"StarBigHelighted"];
        self.fwsdleveled.ratingViewEnabled = NO;
        [self.assentedView addSubview:self.fwsdleveled];
        self.fwxgleveled = [[CTRatingView alloc] initWithFrame:CGRectMake(86, 118, 100, 21) image:@"StarBigNormal" highlightedImage:@"StarBigHelighted"];
        self.fwxgleveled.ratingViewEnabled = NO;
        [self.assentedView addSubview:self.fwxgleveled];

        [self.scrollview addSubview:self.assentedView];
    }
    
    [self LoadData];
    
   if ([self.msg.demandStatus isEqualToString:@"待评价"]) {
       self.assentView.hidden = YES;
       self.assentedView.hidden = YES;
    } else if ([self.msg.demandStatus isEqualToString:@"已评价"]) {
        self.assentView.hidden = YES;
    } else if ([self.title isEqualToString:@"服务评价"]) {
        self.assentedView.hidden = YES;
    }
    //注册通知,监听键盘出现
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidShow:)
                                                name:UIKeyboardDidShowNotification
                                              object:nil];
    //注册通知，监听键盘消失事件
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidHidden:)
                                                name:UIKeyboardDidHideNotification
                                              object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)commitClick:(id)sender
{
    if (self.indicator.showing) return;
    
    NSString *evalSpeed = [self.xysdlab.text stringByReplacingOccurrencesOfString:@"分" withString:@""];
    NSString *evalAttitude = [self.fwsdlab.text stringByReplacingOccurrencesOfString:@"分" withString:@""];
    NSString *evalEffect = [self.fwxglab.text stringByReplacingOccurrencesOfString:@"分" withString:@""];
    
    int a = [evalSpeed intValue];
    int b = [evalAttitude intValue];
    int c = [evalEffect intValue];

    if (a <= 0 || b <= 0 || c <= 0) {
        self.indicator.text = @"评分不能为空!";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
        return;
    }
    
    self.indicator.text = @"正在提交...";
    [self.indicator show];
    
    NSString * params = [NSString stringWithFormat:@"<ServiceAccepted><demandId>%@</demandId><evalInfo>%@</evalInfo><evalSpeed>%@</evalSpeed><evalAttitude>%@</evalAttitude><evalEffect>%@</evalEffect></ServiceAccepted>",self.msg.demandId,self.textView.text,evalSpeed,evalAttitude,evalEffect];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService commitServiceInfo] params:params delegate:self];
    conn.tag = PROCESS_UPDATE_INFO;
    [conn start];
}

- (void)LoadData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"加载中...";
    [self.indicator show];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getServiceDetailInfo:self.msg.demandId] delegate:self];
    conn.tag = PROCESS_LOADING_INFO;
    [conn start];
    return;
}

- (void)drawView:(CustServiceMsg *)msg
{
    if ([msg.dealPersonName isEqualToString:@"null"]) {
        msg.dealPersonName = @"";
    }
    if ([msg.dealTime isEqualToString:@"null"]) {
        msg.dealTime = @"";
    }
    if ([msg.dealType isEqualToString:@"null"]) {
        msg.dealType = @"";
    }
    if ([msg.dealInfo isEqualToString:@"null"]) {
        msg.dealInfo = @"";
    }
    if ([msg.evalInfo isEqualToString:@"null"]) {
        msg.evalInfo = @"";
    }
    self.titlelab.text = msg.demandTitle;
    self.idlab.text = [NSString stringWithFormat:@"编 号：%@",msg.demandId];
    self.timelab.text = [NSString stringWithFormat:@"提交时间：%@",msg.demandTime];
    self.desclab.text = [NSString stringWithFormat:@"描述：%@",msg.demandCont];
    self.personlab.text = [NSString stringWithFormat:@"处理人：%@",msg.dealPersonName];
    self.dealtimelab.text = [NSString stringWithFormat:@"处理时间：%@",msg.dealTime];
    self.typelab.text = [NSString stringWithFormat:@"处理方式：%@",msg.dealType];
    self.answconlab.text = [NSString stringWithFormat:@"回复内容：%@",msg.dealInfo];
    
    self.asslab.text = msg.evalInfo;
    self.xysdleveled.rate = [msg.evalSpeed floatValue];
    self.fwsdleveled.rate = [msg.evalAttitude floatValue];
    self.fwxgleveled.rate = [msg.evalEffect floatValue];
    self.xysdedlab.text = [NSString stringWithFormat:@"%@分",msg.evalSpeed];
    self.fwsdedlab.text = [NSString stringWithFormat:@"%@分",msg.evalAttitude];
    self.fwxgedlab.text = [NSString stringWithFormat:@"%@分",msg.evalEffect];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textView resignFirstResponder];
}
#pragma mark Keyboard
- (void)handleKeyboardDidShow:(NSNotification*)paramNotification
{
    if (!VERSION_LESS_THAN_IOS7) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2f];
        self.view.frame = CGRectMake(0, - 60, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    } else {
        NSDictionary* info = [paramNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
        self.scrollview.contentInset = contentInsets;
        self.scrollview.scrollIndicatorInsets = contentInsets;
        
        CGRect aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!CGRectContainsPoint(aRect, self.textView.frame.origin) ) {
            CGPoint scrollPoint = CGPointMake(0.0, self.textView.frame.origin.y - kbSize.height);
            [self.scrollview setContentOffset:scrollPoint animated:YES];
        }
    }
}

- (void)handleKeyboardDidHidden:(NSNotification*)paramNotification
{
    if (!VERSION_LESS_THAN_IOS7) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2f];
        self.view.frame = CGRectMake(0, + 60, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    } else {
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        self.scrollview.contentInset = contentInsets;
        self.scrollview.scrollIndicatorInsets = contentInsets;
    }
}


#pragma mark - TextView Delegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [self.textView resignFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark - CTRateingView  Delegate
- (void)ctRatingViewDidRating:(CTRatingView *)ratingView withmRate:(float)mRate
{
    if (mRate == 0) {
        mRate = -1;
    }else if (mRate == 5) {
        mRate = 4;
    }
    if (ratingView.tag == 1) {
        self.xysdlab.text = [NSString stringWithFormat:@"%d分",(int)mRate +1];
    } else if (ratingView.tag == 2) {
        self.fwsdlab.text = [NSString stringWithFormat:@"%d分",(int)mRate + 1];
    } else if (ratingView.tag == 3) {
        self.fwxglab.text = [NSString stringWithFormat:@"%d分",(int)mRate + 1];
    }
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    if (connection.tag == PROCESS_LOADING_INFO) {
        self.indicator.text = @"加载失败";
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0f];
    } else if (connection.tag == PROCESS_UPDATE_INFO) {
        self.indicator.text = @"提交失败";
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0f];
    }
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_LOADING_INFO) {
        CustServiceMsg * msg = [[CustServiceMsg alloc] init];
        [msg parseData:data complete:^(CustServiceMsg *msg){
            [self.indicator hide];
            [self drawView:(CustServiceMsg *)msg];
        }];
    } else if (connection.tag == PROCESS_UPDATE_INFO) {
        CustSystemMsg * msg = [[CustSystemMsg alloc] init];
        [msg parseData:data];
        if ([msg.code isEqualToString:@"1"]) {
            self.indicator.text = msg.msg;
            [self.indicator autoHide:CTIndicateStateDone afterDelay:1.0f];
            [self performSelector:@selector(popView) withObject:nil afterDelay:0.8f];
        } else {
            self.indicator.text = msg.msg;
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
        }
    }
}

- (void)popView
{
    [self.navigationController popViewControllerAnimated:YES];
    if ([self.delegate respondsToSelector:@selector(ismustRefresh:isFresh:)]) {
        [self.delegate ismustRefresh:self isFresh:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
