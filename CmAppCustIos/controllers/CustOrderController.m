//
//  CustOrderController.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-20.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustOrderController.h"
#import "CTUtil.h"
#import "CustOrderDetailController.h"
#import "CustOrderHistoryController.h"
#import "CustOrderTaticsController.h"
#import "CTTableView.h"
#import "CTIndicateView.h"
#import "CustWebService.h"
#import "CTURLConnection.h"
#import "CustMessage.h"
#import "CTRatingView.h"
#import "CustMessageController.h"
#import "CustModule.h"

#define PROCESS_CUST_INFO 0
#define PROCESS_ORDER_STATUS 1
#define PROCESS_MESSAGE 2

@interface CustMsgControl : UIControl

@property (nonatomic, strong) NSString * specialId;
@property (nonatomic, strong) UILabel * msgTitle;
@property (nonatomic, strong) UILabel * publishTime;

@end

@implementation CustMsgControl

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        self.msgTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, frame.size.width - 20, 40)];
        self.msgTitle.font = [UIFont systemFontOfSize:15.0f];
        self.msgTitle.textColor = [UIColor blackColor];
        self.msgTitle.numberOfLines = 2;
        self.msgTitle.backgroundColor = [UIColor clearColor];
        [self addSubview:self.msgTitle];
        
        UIImageView * clock = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ClockIcon"]];
        [clock sizeToFit];
        clock.center = CGPointMake(10 + clock.frame.size.width / 2, self.msgTitle.frame.origin.y + self.msgTitle.frame.size.height + 10 + clock.frame.size.height / 2);
        [self addSubview:clock];
        
        self.publishTime = [[UILabel alloc] initWithFrame:CGRectMake(clock.frame.origin.x + clock.frame.size.width + 5, clock.frame.origin.y, frame.size.width - clock.frame.origin.x - clock.frame.size.width - 5 - 10, clock.frame.size.height)];
        self.publishTime.font = [UIFont systemFontOfSize:10.0f];
        self.publishTime.textColor = [UIColor lightGrayColor];
        self.publishTime.numberOfLines = 1;
        self.publishTime.backgroundColor = [UIColor clearColor];
        [self addSubview:self.publishTime];
    }
    return self;
}

@end


@interface CustOrderController () <UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, CTTableViewDelegate, CTURLConnectionDelegate, CustMessageDelegate>

@property (strong, nonatomic) IBOutlet UIView *header;
@property (weak, nonatomic) IBOutlet UILabel *custName;
@property (weak, nonatomic) IBOutlet UILabel *custLicenceCode;
@property (weak, nonatomic) IBOutlet UILabel *custOrderDate;
@property (weak, nonatomic) IBOutlet UILabel *balMode;
@property (weak, nonatomic) IBOutlet UILabel *orderStatus;
@property (nonatomic, strong) CTRatingView * custLevel;
@property (nonatomic, strong) CTTableView * cttv;
@property (nonatomic, strong) NSMutableArray * msgArray;
@property (nonatomic, strong) CTIndicateView * indicator;
@property (nonatomic) int process;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *alcControlWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *alcControlHeight;


// 查询数据
- (void)query:(int)flag;
// 在线订货
- (IBAction)showDetail;
// 历史订单
- (IBAction)showHistory;
// 本周策略
- (IBAction)showTatics;
// 显示用户信息
- (void)resetInfo;
// 显示消息详情
- (void)showMsgDetail:(CustMsgControl *)msgControl;

@end

@implementation CustOrderController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setTabBarItemWithTitle:@"订货" titleColor:[UIColor colorWithRed:43.0/255.0 green:43.0/255.0 blue:43.0/255.0 alpha:1] titleSelectedColor:[UIColor colorWithRed:4.0/255.0 green:168.0/255.0 blue:117.0/255.0 alpha:1] image:[UIImage imageNamed:@"CustTabOrderNormal"] selectedImage:[UIImage imageNamed:@"CustTabOrderSelected"]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:239.0/255.0 blue:238.0/255.0 alpha:1];
    self.msgArray = [[NSMutableArray alloc] init];
    self.custLevel = [[CTRatingView alloc] initWithFrame:CGRectMake(51, 78, 80, 21)];
    self.custLevel.ratingViewEnabled = NO;
    [self.header addSubview:self.custLevel];
    self.custName.adjustsFontSizeToFitWidth = YES;
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_TAB_ORDER, @"00", nil]];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.title = @"订货";
    [super viewWillAppear:animated];
    if (self.cttv == nil) {
        self.cttv = [[CTTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.cttv.pullUpViewEnabled = NO;
        self.cttv.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.cttv.delegate = self;
        self.cttv.dataSource = self;
        self.cttv.ctTableViewDelegate = self;
        
        CGFloat width = CGRectGetWidth(self.view.bounds) / 3;
        CGFloat wannaHeight = width * 119 / 107;
        CGFloat diff = width - self.alcControlWidth.constant;
        self.alcControlWidth.constant = width;
        self.alcControlHeight.constant = wannaHeight;
        CGRect headerFrame = self.header.frame;
        headerFrame.size.width = CGRectGetWidth(self.view.bounds);
        headerFrame.size.height = headerFrame.size.height + diff;
        self.header.frame = headerFrame;
        [self.cttv setTableHeaderView:self.header];

        
        [self.view addSubview:self.cttv];
    }
    
    if (self.indicator == nil) {
        self.indicator = [[CTIndicateView alloc] initInView:self.view];
        self.indicator.backgroundTouchable = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if ([CustWebService sharedUser].custLicenceCode == nil || [[CustWebService sharedUser].custLicenceCode isEqualToString:@""]) {
        [self query:0];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)showDetail
{
    CustOrderDetailController * detail = [[CustOrderDetailController alloc] initWithNibName:@"CustOrderDetailController" bundle:nil];
    [self.navigationController pushViewController:detail animated:YES];
}

- (void)showHistory
{
    CustOrderHistoryController * history = [[CustOrderHistoryController alloc] initWithNibName:@"CustOrderHistoryController" bundle:nil];
    [self.navigationController pushViewController:history animated:YES];
}

- (void)showTatics
{
    CustOrderTaticsController * tatics = [[CustOrderTaticsController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:tatics animated:YES];
}

- (void)query:(int)flag
{
    if (self.indicator.showing) {
        return;
    }
    if (flag == 0) {
        [self.cttv showPullDownView];
    }
    self.indicator.text = @"加载中...";
    [self.indicator show];
    self.process = PROCESS_CUST_INFO;
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService custOrderInfoUrl:[CustWebService sharedUser].userId] delegate:self];
    [conn start];
}

- (void)resetInfo
{
    self.custLicenceCode.text = [NSString stringWithFormat:@"许可证号：%@", [CustWebService sharedUser].custLicenceCode];
    if ([[CustWebService sharedUser].custOrderDate isEqualToString:@"1"]) {
        self.custOrderDate.text = @"订货日：星期一";
    } else if ([[CustWebService sharedUser].custOrderDate isEqualToString:@"2"]) {
        self.custOrderDate.text = @"订货日：星期二";
    } else if ([[CustWebService sharedUser].custOrderDate isEqualToString:@"3"]) {
        self.custOrderDate.text = @"订货日：星期三";
    } else if ([[CustWebService sharedUser].custOrderDate isEqualToString:@"4"]) {
        self.custOrderDate.text = @"订货日：星期四";
    } else if ([[CustWebService sharedUser].custOrderDate isEqualToString:@"5"]) {
        self.custOrderDate.text = @"订货日：星期五";
    } else if ([[CustWebService sharedUser].custOrderDate isEqualToString:@"6"]) {
        self.custOrderDate.text = @"订货日：星期六";
    } else if ([[CustWebService sharedUser].custOrderDate isEqualToString:@"7"]) {
        self.custOrderDate.text = @"订货日：星期日";
    } else {
        self.custOrderDate.text = @"订货日：其他";
    }
    self.balMode.text = [NSString stringWithFormat:@"是否电子结算户：%@", [CustWebService sharedUser].balMode];
    self.custName.text = [CustWebService sharedUser].custName;
    self.custLevel.rate = [[CustWebService sharedUser].custLevle floatValue];
}

- (void)showMsgDetail:(CustMsgControl *)msgControl;
{
    CustMessage * msg = [self.msgArray objectAtIndex:[msgControl.specialId intValue]];
    CustMessageController * controller = [[CustMessageController alloc] initWithNibName:nil bundle:nil];
    controller.msg = msg;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.cttv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.cttv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark UITableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.msgArray.count % 2 == 0 ? self.msgArray.count / 2 : (self.msgArray.count / 2 + 1);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"MSG_CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIView * bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.cttv.frame.size.width, 90)];
        bg.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:239.0/255.0 blue:238.0/255.0 alpha:1];
        bg.tag = 0;
        [cell addSubview:bg];
        
        CustMsgControl * left = [[CustMsgControl alloc] initWithFrame:CGRectMake(0, 0, self.cttv.frame.size.width / 2 - 1, 89)];
        left.tag = 1;
        [left addTarget:self action:@selector(showMsgDetail:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:left];
        
        CustMsgControl * right = [[CustMsgControl alloc] initWithFrame:CGRectMake(self.cttv.frame.size.width / 2, 0, self.cttv.frame.size.width / 2, 89)];
        right.tag = 2;
        [right addTarget:self action:@selector(showMsgDetail:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:right];
    }
    
    int leftIndex = (int)indexPath.row * 2;
    int rightIndex = (int)indexPath.row * 2 + 1;
    
    CustMsgControl * left = (CustMsgControl *)[cell viewWithTag:1];
    if (leftIndex < self.msgArray.count) {
        CustMessage * msg = [self.msgArray objectAtIndex:leftIndex];
        left.msgTitle.text = msg.msgTitle;
        left.publishTime.text = msg.publishTime;
        left.specialId = [NSString stringWithFormat:@"%d", leftIndex];
    } else {
        left.hidden = YES;
    }
    
    CustMsgControl * right = (CustMsgControl *)[cell viewWithTag:2];
    if (rightIndex < self.msgArray.count) {
        CustMessage * msg = [self.msgArray objectAtIndex:rightIndex];
        right.msgTitle.text = msg.msgTitle;
        right.publishTime.text = msg.publishTime;
        right.specialId = [NSString stringWithFormat:@"%d", rightIndex];
    } else {
        right.hidden = YES;
    }
    
    return cell;
}
#pragma mark-
#pragma mark CTTableView Delegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self query:1];
    return YES;
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    if (self.process == PROCESS_CUST_INFO) {
        self.indicator.text = @"用户信息获取失败";
    } else if (self.process == PROCESS_MESSAGE){
        self.indicator.text = @"公告获取失败";
    } else if (self.process == PROCESS_ORDER_STATUS){
        self.indicator.text = @"订单状态获取失败";
    }
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.cttv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (self.process == PROCESS_CUST_INFO) {
        CustInfo * info = [[CustInfo alloc] init];
        [info parseData:data];
        [CustWebService sharedUser].custLicenceCode = info.custLicenceCode;
        [CustWebService sharedUser].custName = info.custName;
        [CustWebService sharedUser].custLevle = info.custLevle;
        [CustWebService sharedUser].mobilphone = info.mobilphone;
        [CustWebService sharedUser].balMode = info.balMode;
        [CustWebService sharedUser].custOrderDate = info.custOrderDate;
        [CustWebService sharedUser].orderStatus = info.orderStatus;
        
        [self resetInfo];
        
        self.process = PROCESS_MESSAGE;
        CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService custOrderMessageListUrl:[CustWebService sharedUser].userId] delegate:self];
        [conn start];
    } else if (self.process == PROCESS_MESSAGE){
        CustMessage * msg = [[CustMessage alloc] init];
        msg.delegate = self;
        [msg parseData:data];
    } else if (self.process == PROCESS_ORDER_STATUS){
        CustInfo * info = [[CustInfo alloc] init];
        [info parseData:data];
        [CustWebService sharedUser].orderStatus = info.orderStatus;
        self.orderStatus.text = [CustWebService sharedUser].orderStatus;
        [self.indicator hide];
    }
}

#pragma mark -
#pragma mark CustMessage Delegate
- (void)custMessage:(CustMessage *)msg didFinishParsing:(NSArray *)array
{
    [self.msgArray removeAllObjects];
    [self.msgArray addObjectsFromArray:array];
    [self.cttv reloadData];
    self.process = PROCESS_ORDER_STATUS;
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService custOrderStatusUrl:[CustWebService sharedUser].userId] delegate:self];
    [conn start];
}

@end
