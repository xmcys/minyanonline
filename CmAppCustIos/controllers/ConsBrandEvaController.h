//
//  ConsBrandEvaController.h
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-25.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@class ConsBrand;

@class ConsBrandEvaController;

@protocol ConsBrandEvaControllerDelegate <NSObject>

- (void)consBrandEvaControllerDidEvaluted:(ConsBrandEvaController *)controller;

@end

@interface ConsBrandEvaController : CTViewController

@property (nonatomic, strong) ConsBrand * brand;
@property (nonatomic, weak) id<ConsBrandEvaControllerDelegate> delegate;

@end
