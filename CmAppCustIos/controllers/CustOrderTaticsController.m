//
//  CustOrderTaticsController.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-25.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustOrderTaticsController.h"
#import "CTIndicateView.h"
#import "CustWebService.h"
#import "CTURLConnection.h"
#import "CTTableView.h"
#import "CustTatics.h"
#import "CustModule.h"
#import "ZbCommonFunc.h"
#import "CustPerTatics.h"

#define PROCESS_PER 1
#define PROCESS_ALL 2

@interface CustOrderTaticsController () <UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, CTTableViewDelegate, CTURLConnectionDelegate, CustTaticsDelegate>

@property (nonatomic, strong) UIScrollView * sv;
@property (nonatomic, strong) CTTableView * leftTv;
@property (nonatomic, strong) CTTableView * rightTv;
@property (nonatomic, strong) CTIndicateView * indicator;
@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) NSMutableArray * taticsArray;
@property (nonatomic, strong) NSMutableArray * pertatiArray;
@property (nonatomic, strong) IBOutlet UIView *headView;
@property (nonatomic, strong) IBOutlet UIView *headTvView;
@property (nonatomic, strong) IBOutlet UIButton *perBtn;
@property (nonatomic, strong) IBOutlet UIButton *allBtn;

- (IBAction)onTabChange:(UIButton *)btn;
- (NSString *)dateFormate:(NSString *)string;

@end

@implementation CustOrderTaticsController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"货源早知道";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.taticsArray = [[NSMutableArray alloc] init];
    self.pertatiArray = [[NSMutableArray alloc] init];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_ORDER_TATICS_MAIN, CUST_MODULE_ORDER_TATICS_DETAIL, nil]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_ZXDH, CUST_MODULE_OPERATE_HYCL, nil]];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)onTabChange:(UIButton *)btn
{
    UIImage *img = [[UIImage imageNamed:@"on"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 10, 5, 10)];
    UIImage *clearImage = [ZbCommonFunc getImage:CGSizeMake(1, 1) withColor:[UIColor clearColor]];
    [self.perBtn setBackgroundImage:clearImage forState:UIControlStateNormal];
    [self.perBtn setBackgroundImage:clearImage forState:UIControlStateHighlighted];
    [self.allBtn setBackgroundImage:clearImage forState:UIControlStateNormal];
    [self.allBtn setBackgroundImage:clearImage forState:UIControlStateHighlighted];
    
    [self.perBtn setTitleColor:XCOLOR(44, 44, 44, 1) forState:UIControlStateNormal];
    [self.perBtn setTitleColor:XCOLOR(44, 44, 44, 1) forState:UIControlStateHighlighted];
    [self.allBtn setTitleColor:XCOLOR(44, 44, 44, 1) forState:UIControlStateNormal];
    [self.allBtn setTitleColor:XCOLOR(44, 44, 44, 1) forState:UIControlStateHighlighted];
    
    [btn setBackgroundImage:img forState:UIControlStateNormal];
    [btn setBackgroundImage:img forState:UIControlStateHighlighted];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    if (btn == self.perBtn) {
        self.sv.hidden = YES;
        self.leftTv.hidden = YES;
        self.rightTv.hidden = YES;
        self.tv.hidden = NO;
        self.headTvView.hidden = NO;
        if (self.pertatiArray.count == 0 && !self.indicator.showing) {
            self.indicator.text = @"加载中...";
            [self.indicator show];
            CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService qtyUpperLimit:[CustWebService sharedUser].userCode] delegate:self];
            conn.tag = PROCESS_PER;
            [conn start];
        }
    } else if (btn == self.allBtn) {
        self.sv.hidden = NO;
        self.tv.hidden = YES;
        self.leftTv.hidden = NO;
        self.rightTv.hidden = NO;
        self.headTvView.hidden = YES;
        if (self.taticsArray.count == 0 && !self.indicator.showing) {
            self.indicator.text = @"加载中...";
            [self.indicator show];
            CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService custOrderTaticsUrl] delegate:self];
            conn.tag = PROCESS_ALL;
            [conn start];
        }
    }
}

- (void)initUI {
    return;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    CGFloat headHeight = self.headView.frame.size.height;
    if (self.sv == nil) {
        self.sv = [[UIScrollView alloc] initWithFrame:CGRectMake(120, headHeight, self.view.bounds.size.width - 120, self.view.bounds.size.height - headHeight)];
        self.sv.contentSize = CGSizeMake(350, self.view.bounds.size.height - headHeight);
        self.sv.showsHorizontalScrollIndicator = NO;
        self.sv.showsVerticalScrollIndicator = NO;
        self.sv.bounces = NO;
        self.sv.backgroundColor = [UIColor clearColor];
        
        self.leftTv = [[CTTableView alloc] initWithFrame:CGRectMake(0, headHeight, 120, self.sv.frame.size.height) style:UITableViewStylePlain];
        self.leftTv.delegate = self;
        self.leftTv.dataSource = self;
        self.leftTv.ctTableViewDelegate = self;
        self.leftTv.pullUpViewEnabled = NO;
        self.leftTv.pullDownViewEnabled = NO;
        self.leftTv.showsHorizontalScrollIndicator = NO;
        self.leftTv.showsVerticalScrollIndicator = NO;
        self.leftTv.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        self.rightTv = [[CTTableView alloc] initWithFrame:CGRectMake(0, 0, self.sv.contentSize.width, self.sv.contentSize.height ) style:UITableViewStylePlain];
        self.rightTv.delegate = self;
        self.rightTv.dataSource = self;
        self.rightTv.ctTableViewDelegate = self;
        self.rightTv.pullUpViewEnabled = NO;
        self.rightTv.pullDownViewEnabled = NO;
        self.rightTv.showsHorizontalScrollIndicator = NO;
        self.rightTv.showsVerticalScrollIndicator = NO;
        self.rightTv.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [self.view addSubview:self.leftTv];
        [self.sv addSubview:self.rightTv];
        
        [self.view addSubview:self.sv];
    }
    
    if (self.tv == nil) {
        CGFloat height = self.headTvView.frame.size.height;
        self.tv = [[CTTableView alloc] initWithFrame:CGRectMake(0, headHeight + height, self.view.frame.size.width, self.view.frame.size.height - headHeight - height)];
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.pullUpViewEnabled = NO;
        self.tv.pullDownViewEnabled = NO;
        self.tv.showsHorizontalScrollIndicator = NO;
        self.tv.showsVerticalScrollIndicator = NO;
//        self.tv.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.tv];
    }
    
    if (self.indicator == nil) {
        self.indicator = [[CTIndicateView alloc] initInView:self.view];
    }
    [self onTabChange:self.perBtn];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (NSString *)dateFormate:(NSString *)string
{
    NSString *str = [string stringByReplacingOccurrencesOfString:@"-" withString:@"."];
    return [str substringToIndex:10];
}
#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.leftTv == scrollView) {
        self.rightTv.contentOffset = self.leftTv.contentOffset;
    } else if (self.rightTv == scrollView){
        self.leftTv.contentOffset = self.rightTv.contentOffset;
    } else if (self.tv == scrollView) {
    
    }
}

#pragma mark -
#pragma mark UITableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.leftTv || tableView == self.rightTv) {
        if (self.taticsArray.count == 0) {
            return 0;
        } else {
            return self.taticsArray.count - 1;
        }
    } else {
        return self.pertatiArray.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView != self.tv) {
        return 40.0f;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == self.leftTv) {
        UILabel * brandName = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.leftTv.frame.size.width, 40.0)];
        brandName.backgroundColor = [UIColor whiteColor];
        brandName.font = [UIFont boldSystemFontOfSize:16.0f];
        brandName.textColor = [UIColor blackColor];
        if (self.taticsArray.count > 0) {
            CustTatics * ct = [self.taticsArray objectAtIndex:0];
            brandName.text = [NSString stringWithFormat:@" %@", ct.brandName];
        }
        return brandName;
    } else if (tableView == self.rightTv){
        UIView * bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.rightTv.frame.size.width, 36)];
        bg.backgroundColor = [UIColor whiteColor];
        int width = bg.frame.size.width / 7;
        
        UILabel * level51 = [[UILabel alloc] initWithFrame:CGRectMake(width * 0, 0, width, bg.frame.size.height)];
        level51.backgroundColor = [UIColor clearColor];
        level51.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
        level51.font = [UIFont boldSystemFontOfSize:16.0f];
        level51.textAlignment = NSTextAlignmentCenter;
        [bg addSubview:level51];
        
        UILabel * level52 = [[UILabel alloc] initWithFrame:CGRectMake(width * 1, 0, width, bg.frame.size.height)];
        level52.backgroundColor = [UIColor clearColor];
        level52.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
        level52.font = [UIFont boldSystemFontOfSize:16.0f];
        level52.textAlignment = NSTextAlignmentCenter;
        [bg addSubview:level52];
        
        UILabel * level41 = [[UILabel alloc] initWithFrame:CGRectMake(width * 2, 0, width, bg.frame.size.height)];
        level41.backgroundColor = [UIColor clearColor];
        level41.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
        level41.font = [UIFont boldSystemFontOfSize:16.0f];
        level41.textAlignment = NSTextAlignmentCenter;
        [bg addSubview:level41];
        
        UILabel * level42 = [[UILabel alloc] initWithFrame:CGRectMake(width * 3, 0, width, bg.frame.size.height)];
        level42.backgroundColor = [UIColor clearColor];
        level42.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
        level42.font = [UIFont boldSystemFontOfSize:16.0f];
        level42.textAlignment = NSTextAlignmentCenter;
        [bg addSubview:level42];
        
        UILabel * level3 = [[UILabel alloc] initWithFrame:CGRectMake(width * 4, 0, width, bg.frame.size.height)];
        level3.backgroundColor = [UIColor clearColor];
        level3.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
        level3.font = [UIFont boldSystemFontOfSize:16.0f];
        level3.textAlignment = NSTextAlignmentCenter;
        [bg addSubview:level3];
        
        UILabel * level2 = [[UILabel alloc] initWithFrame:CGRectMake(width * 5, 0, width, bg.frame.size.height)];
        level2.backgroundColor = [UIColor clearColor];
        level2.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
        level2.textAlignment = NSTextAlignmentCenter;
        level2.font = [UIFont boldSystemFontOfSize:16.0f];
        [bg addSubview:level2];
        
        UILabel * level1 = [[UILabel alloc] initWithFrame:CGRectMake(width * 6, 0, width, bg.frame.size.height)];
        level1.backgroundColor = [UIColor clearColor];
        level1.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
        level1.font = [UIFont systemFontOfSize:16.0f];
        level1.textAlignment = NSTextAlignmentCenter;
        level1.font = [UIFont boldSystemFontOfSize:16.0f];
        [bg addSubview:level1];
        
        if (self.taticsArray.count > 0) {
            CustTatics * ct = [self.taticsArray objectAtIndex:0];
            level51.text = ct.level51;
            level52.text = ct.level52;
            level41.text = ct.level41;
            level42.text = ct.level42;
            level3.text = ct.level3;
            level2.text = ct.level2;
            level1.text = ct.level1;
        }
        
        return bg;
    } else {
        return nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.leftTv) {
        static NSString * identifier = @"LEFT_CELL";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:236.0/255.0 blue:239.0/255.0 alpha:1];
            cell.contentView.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UIView * bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.leftTv.frame.size.width, 36)];
            bg.tag = 101;
            [cell addSubview:bg];
            
            UILabel * brandName = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 115, bg.frame.size.height)];
            brandName.font = [UIFont systemFontOfSize:13.0f];
            brandName.adjustsFontSizeToFitWidth = YES;
            brandName.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
            brandName.tag = 102;
            brandName.backgroundColor = [UIColor clearColor];
            [cell addSubview:brandName];
        }
        
        UIView * bg = [cell viewWithTag:101];
        if (indexPath.row % 2 != 0) {
            bg.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:236.0/255.0 blue:239.0/255.0 alpha:1];
        } else {
            bg.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1];
        }
        
        CustTatics * ct = [self.taticsArray objectAtIndex:indexPath.row + 1];
        
        UILabel * brandName = (UILabel *)[cell viewWithTag:102];
        brandName.text = ct.brandName;

        
        return cell;
    } else if (tableView == self.rightTv) {
        static NSString * identifier = @"RIGHT_CELL";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:236.0/255.0 blue:239.0/255.0 alpha:1];
            cell.contentView.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UIView * bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.rightTv.frame.size.width, 36)];
            bg.tag = 399;
            [cell addSubview:bg];
            
            int width = bg.frame.size.width / 7;
            
            UILabel * level51 = [[UILabel alloc] initWithFrame:CGRectMake(width * 0, 0, width, bg.frame.size.height)];
            level51.backgroundColor = [UIColor clearColor];
            level51.tag = 351;
            level51.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
            level51.font = [UIFont systemFontOfSize:13.0f];
            level51.adjustsFontSizeToFitWidth = YES;
            level51.textAlignment = NSTextAlignmentCenter;
            [bg addSubview:level51];
            
            UILabel * level52 = [[UILabel alloc] initWithFrame:CGRectMake(width * 1, 0, width, bg.frame.size.height)];
            level52.backgroundColor = [UIColor clearColor];
            level52.tag = 352;
            level52.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
            level52.font = [UIFont systemFontOfSize:13.0f];
            level52.adjustsFontSizeToFitWidth = YES;
            level52.textAlignment = NSTextAlignmentCenter;
            [bg addSubview:level52];
            
            UILabel * level41 = [[UILabel alloc] initWithFrame:CGRectMake(width * 2, 0, width, bg.frame.size.height)];
            level41.backgroundColor = [UIColor clearColor];
            level41.tag = 341;
            level41.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
            level41.font = [UIFont systemFontOfSize:13.0f];
            level41.adjustsFontSizeToFitWidth = YES;
            level41.textAlignment = NSTextAlignmentCenter;
            [bg addSubview:level41];
            
            UILabel * level42 = [[UILabel alloc] initWithFrame:CGRectMake(width * 3, 0, width, bg.frame.size.height)];
            level42.backgroundColor = [UIColor clearColor];
            level42.tag = 342;
            level42.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
            level42.font = [UIFont systemFontOfSize:13.0f];
            level42.adjustsFontSizeToFitWidth = YES;
            level42.textAlignment = NSTextAlignmentCenter;
            [bg addSubview:level42];
            
            UILabel * level3 = [[UILabel alloc] initWithFrame:CGRectMake(width * 4, 0, width, bg.frame.size.height)];
            level3.backgroundColor = [UIColor clearColor];
            level3.tag = 303;
            level3.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
            level3.font = [UIFont systemFontOfSize:13.0f];
            level3.adjustsFontSizeToFitWidth = YES;
            level3.textAlignment = NSTextAlignmentCenter;
            [bg addSubview:level3];
            
            UILabel * level2 = [[UILabel alloc] initWithFrame:CGRectMake(width * 5, 0, width, bg.frame.size.height)];
            level2.backgroundColor = [UIColor clearColor];
            level2.tag = 302;
            level2.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
            level2.font = [UIFont systemFontOfSize:13.0f];
            level2.adjustsFontSizeToFitWidth = YES;
            level2.textAlignment = NSTextAlignmentCenter;
            [bg addSubview:level2];
            
            UILabel * level1 = [[UILabel alloc] initWithFrame:CGRectMake(width * 6, 0, width, bg.frame.size.height)];
            level1.backgroundColor = [UIColor clearColor];
            level1.tag = 301;
            level1.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
            level1.font = [UIFont systemFontOfSize:13.0f];
            level1.adjustsFontSizeToFitWidth = YES;
            level1.textAlignment = NSTextAlignmentCenter;
            [bg addSubview:level1];
        }
        
        UIView * bg = [cell viewWithTag:399];
        if (indexPath.row % 2 != 0) {
            bg.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:236.0/255.0 blue:239.0/255.0 alpha:1];
        } else {
            bg.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1];
        }
        
        CustTatics * ct = [self.taticsArray objectAtIndex:indexPath.row + 1];
        
        UILabel * level51 = (UILabel *)[cell viewWithTag:351];
        level51.text = ct.level51;
        
        UILabel * level52 = (UILabel *)[cell viewWithTag:352];
        level52.text = ct.level52;
        
        UILabel * level41 = (UILabel *)[cell viewWithTag:341];
        level41.text = ct.level41;
        
        UILabel * level42 = (UILabel *)[cell viewWithTag:342];
        level42.text = ct.level42;
        
        UILabel * level3 = (UILabel *)[cell viewWithTag:303];
        level3.text = ct.level3;
        
        UILabel * level2 = (UILabel *)[cell viewWithTag:302];
        level2.text = ct.level2;
        
        UILabel * level1 = (UILabel *)[cell viewWithTag:301];
        level1.text = ct.level1;
        
        if (ct.level52.length == 0) {
            level51.frame = CGRectMake(level51.frame.origin.x, level51.frame.origin.y, self.rightTv.frame.size.width, level51.frame.size.height);
        } else {
            level51.frame = CGRectMake(level51.frame.origin.x, level51.frame.origin.y, self.rightTv.frame.size.width / 7, level51.frame.size.height);
        }
        
        return cell;
    } else {
        static NSString * identifier = @"PERCELL";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:236.0/255.0 blue:239.0/255.0 alpha:1];
            cell.contentView.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UIView * bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tv.frame.size.width, 36)];
            bg.tag = 200;
            [cell addSubview:bg];
            
            CGFloat xwidth = self.view.frame.size.width;
            
            UILabel * brandName = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 115, bg.frame.size.height)];
            brandName.font = [UIFont systemFontOfSize:13.0f];
            brandName.adjustsFontSizeToFitWidth = YES;
            brandName.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
            brandName.tag = 201;
            brandName.backgroundColor = [UIColor clearColor];
            [bg addSubview:brandName];
            
            UILabel * brandlimit = [[UILabel alloc] initWithFrame:CGRectMake(xwidth - (73 + 10) * 2 - 63, 0, 53, bg.frame.size.height)];
            brandlimit.font = [UIFont systemFontOfSize:13.0f];
            brandlimit.textAlignment = NSTextAlignmentCenter;
            brandlimit.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
            brandlimit.tag = 202;
            brandlimit.backgroundColor = [UIColor clearColor];
            [bg addSubview:brandlimit];
            
            UILabel * startdate = [[UILabel alloc] initWithFrame:CGRectMake(xwidth - (73 + 10) * 2, 0, 73, bg.frame.size.height)];
            startdate.font = [UIFont systemFontOfSize:13.0f];
            brandlimit.textAlignment = NSTextAlignmentCenter;
            startdate.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
            startdate.tag = 203;
            startdate.backgroundColor = [UIColor clearColor];
            [bg addSubview:startdate];
            
            UILabel * enddate = [[UILabel alloc] initWithFrame:CGRectMake(xwidth - 73 - 10, 0, 73, bg.frame.size.height)];
            enddate.font = [UIFont systemFontOfSize:13.0f];
            brandlimit.textAlignment = NSTextAlignmentCenter;
            enddate.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1];
            enddate.tag = 204;
            enddate.backgroundColor = [UIColor clearColor];
            [bg addSubview:enddate];
            
        }
        
        CustPerTatics *item = [self.pertatiArray objectAtIndex:indexPath.row];

        UILabel *namelab = (UILabel *)[cell viewWithTag:201];
        namelab.text = item.brandName;

        UILabel *limitlab = (UILabel *)[cell viewWithTag:202];
        limitlab.text = item.qtyUpperLimit;

        UILabel *startdate = (UILabel *)[cell viewWithTag:203];
        startdate.text = [self dateFormate:item.startDate];

        UILabel *enddate = (UILabel *)[cell viewWithTag:204];
        enddate.text = [self dateFormate:item.endDate];
        UIView *bg = [cell viewWithTag:200];

        if (indexPath.row % 2 != 0) {
            bg.backgroundColor = XCOLOR(238, 236, 239, 1);
        } else {
            bg.backgroundColor = XCOLOR(245, 245, 245, 1);
        }

        
        
        return cell;

    }
}

#pragma mark-
#pragma mark CTTableView Delegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    return NO;
}

#pragma mark -
#pragma mark CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:0.5];
    [self.leftTv reloadData];
    [self.rightTv reloadData];
    [self.tv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_PER) {
        CustPerTatics *item = [[CustPerTatics alloc] init];
        [item parseData:data complete:^(NSArray *array) {
            [self.pertatiArray removeAllObjects];
            [self.pertatiArray addObjectsFromArray:array];
            [self.tv reloadData];
            [self.indicator hide];
        }];
    } else if (connection.tag == PROCESS_ALL) {
        CustTatics * ct = [[CustTatics alloc] init];
        ct.delegate = self;
        [ct parseData:data];
    }
}

#pragma mark -
#pragma mark CustTatics Delegate
- (void)custTatics:(CustTatics *)custTatics didFinishParsing:(NSArray *)array
{
    [self.taticsArray removeAllObjects];
    [self.taticsArray addObjectsFromArray:array];
    [self.leftTv reloadData];
    [self.rightTv reloadData];
    [self.indicator hide];

}

@end
