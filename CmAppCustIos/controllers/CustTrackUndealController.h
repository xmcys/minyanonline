//
//  CustTrackUndealController.h
//  CmAppCustIos
//
//  Created by hsit on 14-8-26.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"
#import "CustServiceMsgList.h"

@interface CustTrackUndealController : CTViewController

@property (nonatomic, strong) CustServiceMsgList * msg;

@end
