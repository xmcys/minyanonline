//
//  CustSaleProfViewController.m
//  CmAppCustIos
//
//  Created by hsit on 15-3-20.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustSaleProfViewController.h"
#import "ZbCommonFunc.h"
#import "CTTableView.h"
#import "CustSaleProf.h"

@interface CustSaleProfViewController ()<UITableViewDataSource, UITableViewDelegate, CTTableViewDelegate>

@property (nonatomic, strong) IBOutlet UIView *headView;
@property (nonatomic, strong) IBOutlet UILabel *headLab;
@property (nonatomic, strong) IBOutlet UIButton *sortBtn;
@property (nonatomic, strong) IBOutlet UIButton *rankBtn;
@property (nonatomic) StatusTypeName statustypename;
@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) NSMutableArray *array;

- (IBAction)onTabChange:(UIButton *)btn;
- (IBAction)dateSegChange:(UISegmentedControl *)sender;

@end

@implementation CustSaleProfViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.array = [[NSMutableArray alloc] init];
    if (self.statustypename == StatusTypeSale) {
        self.title = @"销售汇总";
        self.headLab.text = @"销售汇总";
    } else {
        self.title = @"经营毛利";
        self.headLab.text = @"经营毛利";
    }
    
    CustSaleProf *item = [[CustSaleProf alloc] init];
    item.sm = @"一类卷烟";
    item.mo = @"100";
    item.ra = @"95%";
    CustSaleProf *item1 = [[CustSaleProf alloc] init];
    item1.sm = @"二类卷烟";
    item1.mo = @"100";
    item1.ra = @"100%";
    
    [self.array addObject:item];
    [self.array addObject:item1];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onTabChange:(UIButton *)btn
{
    UIImage *img = [[UIImage imageNamed:@"on"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 10, 5, 10)];
    UIImage *clearImage = [ZbCommonFunc getImage:CGSizeMake(1, 1) withColor:[UIColor clearColor]];
    [self.sortBtn setBackgroundImage:clearImage forState:UIControlStateNormal];
    [self.sortBtn setBackgroundImage:clearImage forState:UIControlStateHighlighted];
    [self.rankBtn setBackgroundImage:clearImage forState:UIControlStateNormal];
    [self.rankBtn setBackgroundImage:clearImage forState:UIControlStateHighlighted];
    
    [self.sortBtn setTitleColor:XCOLOR(44, 44, 44, 1) forState:UIControlStateNormal];
    [self.sortBtn setTitleColor:XCOLOR(44, 44, 44, 1) forState:UIControlStateHighlighted];
    [self.rankBtn setTitleColor:XCOLOR(44, 44, 44, 1) forState:UIControlStateNormal];
    [self.rankBtn setTitleColor:XCOLOR(44, 44, 44, 1) forState:UIControlStateHighlighted];
    
    [btn setBackgroundImage:img forState:UIControlStateNormal];
    [btn setBackgroundImage:img forState:UIControlStateHighlighted];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    if (btn == self.sortBtn) {
        
    } else if (btn == self.rankBtn) {
        
    }
}

- (IBAction)dateSegChange:(UISegmentedControl *)sender
{
    switch (sender.selectedSegmentIndex) {
        case 0:
            NSLog(@"1");
            break;
        case 1:
            NSLog(@"2");
            break;
        case 2:
            NSLog(@"3");
            break;
        case 3:
            NSLog(@"4");
            break;
            
        default:
            break;
    }
    return;
}


- (void)setSTatusType:(StatusTypeName)statusTypeName {
    self.statustypename = statusTypeName;
}

- (void)initUI {
    [self onTabChange:self.sortBtn];
    CGFloat xheight = self.headView.frame.size.height + 10;
    self.tv = [[CTTableView alloc] initWithFrame:CGRectMake(0, xheight, self.view.frame.size.width, self.view.frame.size.height - xheight)];
    self.tv.backgroundColor = [UIColor clearColor];
    self.tv.delegate = self;
    self.tv.dataSource = self;
    self.tv.ctTableViewDelegate = self;
    self.tv.pullUpViewEnabled = NO;
    [self.view addSubview:self.tv];
    [self loadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
//    self.indicator.text = @"加载中..";
//    [self.indicator show];
//    CTURLConnection *conn = [CTURLConnection alloc] initWithGetMethodUrl:<#(NSString *)#> delegate:(id<CTURLConnectionDelegate>)
//    [conn statrt];
}

- (void)expandClicked:(UIControl *)control {
    CustSaleProf *item = [self.array objectAtIndex:control.tag];
    item.expand = !item.expand;
    NSIndexSet *set = [[NSIndexSet alloc] initWithIndex:control.tag];
    [self.tv reloadSections:set withRowAnimation:UITableViewRowAnimationFade];
    return;
}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark - CTTableView Delegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    if (self.indicator.showing) {
        return NO;
    }
    [self loadData];
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    CustSaleProf *item = [self.array objectAtIndex:section];
    if (item.expand) {
        return self.array.count + 2;
    }
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger height;
    if (indexPath.row == 0) {
        height = 45;
    } else {
        height = 40;
    }
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"Cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];

    if (indexPath.row == 0) {
        UIView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"CustSaleHeadCell" owner:nil options:nil] lastObject];
        
        UIControl *backControl = (UIControl *)[headerView viewWithTag:1];
        backControl.tag = indexPath.section;
        backControl.layer.borderColor = [XCOLOR(216, 216, 216, 1) CGColor];
        backControl.layer.borderWidth = 1.0f;
        [backControl addTarget:self action:@selector(expandClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        NSArray *titleArray = [[NSArray alloc] initWithObjects:@"卷烟", @"非烟", nil];
        UILabel *labTit = (UILabel *)[headerView viewWithTag:2];
        labTit.text = [titleArray objectAtIndex:indexPath.section];
        UILabel *labMon = (UILabel *)[headerView viewWithTag:3];
        UIImageView *imageFlag = (UIImageView *)[headerView viewWithTag:4];
        
        CustSaleProf *item = [self.array objectAtIndex:indexPath.section];
        if (item.expand) {
            imageFlag.image = [UIImage imageNamed:@"arrowUp"];
        } else {
            imageFlag.image = [UIImage imageNamed:@"arrowDown"];
        }
        
        [cell addSubview:headerView];
    } else if (indexPath.row == 1) {
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"CustSaleProfCell" owner:nil options:nil];
        UIView *view = [elements objectAtIndex:0];
        [cell addSubview:view];
        
        UILabel *labSm = (UILabel *)[cell viewWithTag:1];
        UILabel *labMo = (UILabel *)[cell viewWithTag:2];
        UILabel *labRa = (UILabel *)[cell viewWithTag:3];
        
        labSm.text = @"分类";
        labMo.text = @"销售毛利";
        labRa.text = @"占比%";
        
    } else {
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"CustSaleProfCell" owner:nil options:nil];
        UIView *view = [elements objectAtIndex:0];
        [cell addSubview:view];
        
        UILabel *labSm = (UILabel *)[cell viewWithTag:1];
        UILabel *labMo = (UILabel *)[cell viewWithTag:2];
        UILabel *labRa = (UILabel *)[cell viewWithTag:3];
        
        
        CustSaleProf *item = [self.array objectAtIndex:indexPath.row - 2];
        labSm.text = item.sm;
        labMo.text = item.mo;
        labRa.text = item.ra;
        
//        if (indexPath.row % 2 != 0) {
//            view.backgroundColor = XCOLOR(238, 236, 239, 1);
//        } else {
//            view.backgroundColor = XCOLOR(245, 245, 245, 1);
//        }
    }
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"%ld",indexPath.row);
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.indicator.text = @"加载失败";
    [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
