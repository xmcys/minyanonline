//
//  CustServiceListController.m
//  CmAppCustIos
//
//  Created by Arcesaka on 14/8/25.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustServiceListController.h"
#import "CustCompleteController.h"
#import "CustTrackController.h"
#import "CustAllController.h"

@interface CustServiceListController ()

@property (nonatomic, strong) IBOutlet UIView * topTab;
@property (nonatomic, strong) IBOutlet UIButton * completetab;  //服务受理申报
@property (nonatomic, strong) IBOutlet UIButton * tracktab;     //服务受理跟踪
@property (nonatomic, strong) IBOutlet UIButton * alltab;       //查看全部
@property (nonatomic, strong) CustTrackController *track;
@property (nonatomic, strong) CustCompleteController *complete;
@property (nonatomic, strong) CustAllController * all;

- (IBAction)onTabChanged:(UIButton *)btn;

@end

@implementation CustServiceListController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"服务受理";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.topTab.frame = CGRectMake(0, 0, self.view.frame.size.width, self.topTab.frame.size.height);
    self.topTab.backgroundColor = [UIColor colorWithRed:252.0/255.0 green:245.0/255.0 blue:252.0/255.0 alpha:1];
    self.topTab.layer.cornerRadius = 5;
    self.topTab.layer.masksToBounds = YES;
    [self.view addSubview:self.topTab];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    CGRect subframe = CGRectMake(0, self.topTab.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height - self.topTab.frame.size.height);
    
    if (self.complete == nil) {
        self.complete = [[CustCompleteController alloc] initWithNibName:@"CustCompleteController" bundle:nil];
        self.complete.view.frame = subframe;
        [self.view addSubview:self.complete.view];
    }
    
    if (self.track == nil) {
        self.track = [[CustTrackController alloc] initWithNibName:@"CustTrackController" bundle:nil];
        self.track.view.frame = subframe;
        self.track.nc = self.navigationController;
        [self.view addSubview:self.track.view];
    }
    
    if (self.all == nil) {
        self.all = [[CustAllController alloc] initWithNibName:@"CustAllController" bundle:nil];
        self.all.view.frame = subframe;
        self.all.nc = self.navigationController;
        [self.view addSubview:self.all.view];
    }

    if (self.tag == self.completetab.tag) {
        [self onTabChanged:self.completetab];
    }   else if (self.tag == self.tracktab.tag) {
        [self onTabChanged:self.tracktab];
    }   else if (self.tag == self.alltab.tag) {
        [self onTabChanged:self.alltab];
    }
}

- (IBAction)onTabChanged:(UIButton *)btn
{
    UIImage * img = [[UIImage imageNamed:@"TabSelected"] resizableImageWithCapInsets:UIEdgeInsetsMake(7, 5, 7, 5)];
    [self.completetab setBackgroundImage:nil forState:UIControlStateNormal];
    [self.completetab setBackgroundImage:nil forState:UIControlStateHighlighted];
    [self.tracktab setBackgroundImage:nil forState:UIControlStateNormal];
    [self.tracktab setBackgroundImage:nil forState:UIControlStateHighlighted];
    [self.alltab setBackgroundImage:nil forState:UIControlStateNormal];
    [self.alltab setBackgroundImage:nil forState:UIControlStateHighlighted];
    [self.completetab setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.completetab setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateHighlighted];
    [self.tracktab setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.tracktab setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateHighlighted];
    [self.alltab setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateNormal];
    [self.alltab setTitleColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1] forState:UIControlStateHighlighted];

    self.complete.view.hidden = YES;
    self.track.view.hidden = YES;
    self.all.view.hidden = YES;

    [btn setBackgroundImage:img forState:UIControlStateNormal];
    [btn setBackgroundImage:img forState:UIControlStateHighlighted];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    self.tag = btn.tag;
    if (btn == self.completetab) {
        self.complete.view.hidden = NO;
        [self.complete viewWillAppear:NO];
    } else if (btn == self.tracktab){
        self.track.view.hidden = NO;
        [self.track viewWillAppear:NO];
    } else {
        self.all.view.hidden = NO;
        [self.all viewWillAppear:NO];
    }
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
