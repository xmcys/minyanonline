//
//  CustMyPointViewController.m
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/24.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustMyPointViewController.h"
#import "CTTableView.h"
#import "CustMyPointsDetail.h"
#import "CustWebService.h"
#import "CustMyPointRecordsViewController.h"
#import "CustModule.h"

static NSString * const MyPointCellIdentifier = @"MyPointCell";

@interface CustMyPointViewController () <CTTableViewDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>

@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) NSMutableArray *array;

- (void)loadData;

@end

@implementation CustMyPointViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"积分查询";
    self.array = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_KHFW, CUST_MODULE_OPERATE_JFCX, nil]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:CGRectMake(0, self.headerView.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height-self.headerView.frame.size.height)];
        self.tv.pullUpViewEnabled = NO;
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        [self.tv registerNib:[UINib nibWithNibName:@"CustMyPointCell" bundle:nil] forCellReuseIdentifier:MyPointCellIdentifier];
        [self.view addSubview:self.tv];
        [self loadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma  mark - http requests
- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中...";
    [self.indicator show];
    
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getPointsDetailUrl] delegate:self];
    [conn start];
    return;
}

//下拉刷新
#pragma  mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark - CTTableView Delegate *DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 41;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:MyPointCellIdentifier forIndexPath:indexPath];
    
    // 背景色
    UIView *contentView = cell.contentView;
    if (indexPath.row % 2 == 0) {
        contentView.backgroundColor = XCOLOR(245, 245, 245, 1);
    } else {
        contentView.backgroundColor = XCOLOR(238, 236, 239, 1);
    }
    
    CustMyPointsDetail *curPoint = [self.array objectAtIndex:indexPath.row];
    // 公司名
    UILabel *labName = (UILabel *)[cell viewWithTag:1];
    labName.text = curPoint.supplierName;
    // 历史积分
    UILabel *labPointTotal = (UILabel *)[cell viewWithTag:2];
    labPointTotal.text = curPoint.sumPoints;
    // 可用积分
    UILabel *labPointCurrent = (UILabel *)[cell viewWithTag:3];
    labPointCurrent.text = curPoint.usePoints;
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    if (self.indicator.showing) {
        return NO;
    }
    [self loadData];
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CustMyPointsDetail *curPoint = [self.array objectAtIndex:indexPath.row];
    CustMyPointRecordsViewController *controller = [[CustMyPointRecordsViewController alloc] initWithNibName:@"CustMyPointRecordsViewController" bundle:nil];
    controller.curPoint = curPoint;
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark - CTURLConnection Delegate

- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
    [self.tv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    CustMyPointsDetail *pointParser = [[CustMyPointsDetail alloc] init];
    [pointParser parseData:data complete:^(NSArray *array) {
        [self.indicator hide];
        [self.array removeAllObjects];
        [self.array addObjectsFromArray:array];
        [self.tv reloadData];
    }];
    return;
}

@end
