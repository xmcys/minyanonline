//
//  RYKRecordsofonsViewController.m
//  RYKForCustIos
//
//  Created by hsit on 14-6-23.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CustRecordsofonsViewController.h"
#import "CTTableView.h"
#import "CustRecordersofGoodsViewController.h"
#import "CustWebService.h"
#import "CustCustOrder.h"
#import "CustListRecords.h"

#define PROCESS_GET_ORDER    1
#define PROCESS_GET_ORDERDETAIL 2

@interface CustRecordsofonsViewController ()<UITableViewDelegate,UITableViewDataSource,CTTableViewDelegate>

@property (nonatomic, strong) CTTableView     *tableview;
@property (nonatomic, strong) IBOutlet UIView *headerview;
@property (nonatomic, strong) NSMutableArray  *array;
@property (nonatomic, strong) NSMutableArray  *msgarray;
@property (nonatomic, strong) IBOutlet UILabel *labname;
@property (nonatomic, strong) IBOutlet UILabel *labphone;
@property (nonatomic, strong) IBOutlet UILabel *labordersum;
@property (nonatomic, strong) IBOutlet UILabel *labpricesum;
@property (nonatomic, strong) NSMutableArray * tmparray;


@end

@implementation CustRecordsofonsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithuserId:(NSString *)userId userName:(NSString *)username mobilePhone:(NSString *)mobilephone
{
    if (self = [super init]) {
        self.userId = userId;
        self.username = username;
        self.mobilephone = mobilephone;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.array = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated 
{
    [super viewWillAppear:animated];
    self.title = @"消费记录查询";
    if (self.tableview == nil) {
        self.tableview = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tableview.backgroundColor = [UIColor clearColor];
        self.tableview.pullUpViewEnabled = NO;
        self.tableview.pullDownViewEnabled = NO;
        self.tableview.delegate = self;
        self.tableview.dataSource = self;
        self.tableview.ctTableViewDelegate = self;
        [self.tableview setTableHeaderView:self.headerview];
        [self.view addSubview:self.tableview];
    }
    [self drawUserInfo];
    [self loadData];
}

- (void)drawUserInfo
{
    self.labname.text = self.username;
    self.labphone.text = self.mobilephone;
}
- (void)drawUserOrder
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getTbcOrderCountDetail:self.userId userCode:[CustWebService sharedUser].userCode]  delegate:self];
    conn.tag = PROCESS_GET_ORDERDETAIL;
    [conn start];
    return;
}

- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getTbcOrderCount:self.userId userCode:[CustWebService sharedUser].userCode] delegate:self];
    conn.tag = PROCESS_GET_ORDER;
    [conn start];
    return;
}

- (NSString *)isEmptyString:(NSString *)string {
    if (string == nil || [string isEqualToString:@"null"]) {
        return @"";
    }
    return string;
}

#pragma mark - CTTableViewDelegate * Datasource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadData];
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 270;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"CELL";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"CustRecordersListCell" owner:nil        options:nil];
        UIView *view = [elements objectAtIndex:0];
        view.frame = CGRectMake(5, 10, tableView.frame.size.width-5 * 2, 260);
        view.layer.borderColor = [[UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1] CGColor];
        view.layer.borderWidth = 1;
        [cell addSubview:view];
    }
    UIView *xuxian = (UIView *)[cell viewWithTag:20];
    UILabel *labdate = (UILabel *)[cell viewWithTag:100];
    UILabel *labordersum = (UILabel *)[cell viewWithTag:101];
    UILabel *labamtordersum = (UILabel *)[cell viewWithTag:102];
    UILabel *labelname = (UILabel *)[cell viewWithTag:103];
    UILabel *labelunit = (UILabel *)[cell viewWithTag:105];
    UILabel *labelorder = (UILabel *)[cell viewWithTag:106];
    UILabel *labOtherGoodsCnt = (UILabel *)[cell viewWithTag:108];
    xuxian.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Dot"]];
    
    
    CustCustName *item = (CustCustName *)[self.array objectAtIndex:indexPath.row];
    labdate.text = item.operateDate;
    labamtordersum.text = item.quantity;
    labordersum.text = [NSString stringWithFormat:@"¥ %@", item.sumMoney];
    labelname.text = item.productName;
    labelunit.text = [NSString stringWithFormat:@"¥ %@/%@", item.currentPrice, item.quantity];
    labelorder.text = [NSString stringWithFormat:@"x%@", item.oNum];
    labOtherGoodsCnt.text = item.hj;
    return cell;
}

- (void)drawdetail
{


}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CustCustName *item = [self.array objectAtIndex:indexPath.row];
//    CustRecordersofGoodsViewController *controller = [[CustRecordersofGoodsViewController alloc] initWithOrder:msg.orderId];
//    controller.orderDate = msg.orderDate;
//    [self.navigationController pushViewController:controller animated:YES];
}


#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.tableview reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
     if (connection.tag == PROCESS_GET_ORDER) {
        CustCustOrder *msg = [[CustCustOrder alloc] init];
        [msg parseData:data complete:^(CustCustOrder *msg){
            [self.indicator hide];
            self.labordersum.text = [self isEmptyString:msg.orderNum];
            self.labpricesum.text = [self isEmptyString:msg.amt];
            [self drawUserOrder];
        }];
    } else if (connection.tag == PROCESS_GET_ORDERDETAIL) {
        CustCustName *msg = [[CustCustName alloc] init];
        [msg parseData:data complete:^(NSArray *array) {
            [self.indicator hide];
            [self.array removeAllObjects];
            [self.array addObjectsFromArray:array];
            [self.tableview reloadData];
        }];
    } }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
