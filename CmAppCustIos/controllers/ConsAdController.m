//
//  ConsAdController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-29.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsAdController.h"
#import "CTBannerView.h"
#import "CTImageView.h"
#import "CustWebService.h"
#import "CustActivity.h"

@interface ConsAdController () <CTBannerViewDelegate, CTURLConnectionDelegate>

@property (nonatomic, strong) CTBannerView * banner;
@property (nonatomic, strong) UIButton * btnClose;

- (void)dismiss;
- (void)closeEnable:(BOOL)enable;

@end

@implementation ConsAdController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)initUI
{
    self.banner = [[CTBannerView alloc] initWithFrame:self.view.bounds];
    self.banner.delegate = self;
    self.banner.autoScroll = NO;
    [self.banner setIndicateColor:[UIColor whiteColor] highlightedColor:[UIColor colorWithRed:0.0/255.0 green:118.0/255.0 blue:68.0/255.0 alpha:1]];
    [self.view addSubview:self.banner];
    
    if (self.isAd) {
        [self loadAd];
    } else {
        NSMutableArray * array = [[NSMutableArray alloc] init];
        
        UIImageView * img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.banner.frame.size.width, self.banner.frame.size.height)];
        img.image = [UIImage imageNamed:@"Guide1"];
        img.contentMode = UIViewContentModeScaleToFill;
        [array addObject:img];
        
        img = [[UIImageView alloc] initWithFrame:CGRectMake(self.banner.frame.size.width, 0, self.banner.frame.size.width, self.banner.frame.size.height)];
        img.image = [UIImage imageNamed:@"Guide2"];
        img.contentMode = UIViewContentModeScaleToFill;
        [array addObject:img];
        
        img = [[UIImageView alloc] initWithFrame:CGRectMake(self.banner.frame.size.width * 2, 0, self.banner.frame.size.width, self.banner.frame.size.height)];
        img.image = [UIImage imageNamed:@"Guide3"];
        img.contentMode = UIViewContentModeScaleToFill;
        [array addObject:img];
        
        self.banner.viewArray = array;
    }
    
    if (self.banner.viewArray.count <= 1) {
        [self closeEnable:YES];
    }
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

- (void)closeEnable:(BOOL)enable
{
    if (self.btnClose == nil) {
        if (_isAd) {
            self.btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
            self.btnClose.titleLabel.font = [UIFont systemFontOfSize:14.0f];
            self.btnClose.frame = CGRectMake(self.view.bounds.size.width - 60, 0, 60, 60);
            [self.btnClose setBackgroundImage:[UIImage imageNamed:@"Skip"] forState:UIControlStateNormal];
            [self.btnClose setBackgroundImage:[UIImage imageNamed:@"Skip"] forState:UIControlStateHighlighted];
            [self.btnClose addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
            self.btnClose.showsTouchWhenHighlighted = YES;
        } else {
            self.btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
            self.btnClose.titleLabel.font = [UIFont systemFontOfSize:14.0f];
            [self.btnClose setTitle:@"立即体验" forState:UIControlStateNormal];
            [self.btnClose setTitle:@"立即体验" forState:UIControlStateHighlighted];
            [self.btnClose setBackgroundColor:[UIColor clearColor]];
            [self.btnClose setTitleColor:[UIColor colorWithRed:0.0/255.0 green:118.0/255.0 blue:68.0/255.0 alpha:1] forState:UIControlStateNormal];
            [self.btnClose setTitleColor:[UIColor colorWithRed:0.0/255.0 green:118.0/255.0 blue:68.0/255.0 alpha:1] forState:UIControlStateHighlighted];
            self.btnClose.layer.borderColor = [[UIColor colorWithRed:0.0/255.0 green:118.0/255.0 blue:68.0/255.0 alpha:1] CGColor];
            self.btnClose.layer.borderWidth = 1;
            self.btnClose.layer.cornerRadius = 5.0;
            [self.btnClose addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
            self.btnClose.frame = CGRectMake((self.view.bounds.size.width - 120) / 2, self.view.bounds.size.height - 100, 120, 35);
            self.btnClose.showsTouchWhenHighlighted = YES;
        }
    }
    
    [self.btnClose removeFromSuperview];
    
    if (enable) {
        [self.view addSubview:self.btnClose];
    }
    
}

- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:^{
        if ([self.delegate respondsToSelector:@selector(consAdControllerDidDismiss:)]) {
            [self.delegate consAdControllerDidDismiss:self];
        }
    }];
}

- (void)loadAd
{
    self.indicator.text = @"加载中..";
    [self.indicator show];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService consAdUrl] delegate:self];
    [conn start];
}

#pragma mark -
#pragma mark CTBannerView Delegate
- (void)ctBannerViewDidScroll:(CTBannerView *)view
{
    if (view.currentPage == view.mMaxPage - 1) {
        [self closeEnable:YES];
    } else {
        [self closeEnable:NO];
    }
}

#pragma mark -
#pragma mark CTURLConenction Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"获取失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    CustActivity * act = [[CustActivity alloc] init];
    [act parseData:data complete:^(NSArray *array){
        
        if (array.count == 0) {
            self.indicator.text = @"暂无广告";
            [self.indicator hideWithSate:CTIndicateStateDone afterDelay:1.0 complete:^{
                [self dismiss];
            }];
            return ;
        }
        NSMutableArray * viewArray = [[NSMutableArray alloc] init];
        for (int i = 0; i < array.count; i++) {
            CustActivity * item = [array objectAtIndex:i];
            CTImageView * img = [[CTImageView alloc] initWithFrame:self.banner.bounds];
            img.contentMode = UIViewContentModeScaleToFill;
            img.url = [CustWebService fileFullUrl:item.proImg];
            [img loadImage];
            [viewArray addObject:img];
        }
        
        self.banner.viewArray = viewArray;

        [self.indicator hide];
    }];
}

@end
