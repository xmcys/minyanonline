//
//  CustSerEvaluViewController.m
//  CmAppCustIos
//
//  Created by hsit on 15-3-23.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustSerEvaluViewController.h"
#import "CTTableView.h"
#import "CTRatingView.h"
#import "CustWebService.h"
#import "CustSystemMsg.h"
#import "CustModule.h"
#import "CustInfo.h"

#define PROCESS_QUERY_MGR 1
#define PROCESS_SUBMIT_SCORE 2

@interface CustSerEvaluViewController ()<CTRatingViewDelegate, CTURLConnectionDelegate>

@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) IBOutlet UIView *fView;
@property (nonatomic, strong) UILabel *adLab;
@property (nonatomic, strong) IBOutlet UIView *kfjView;
@property (nonatomic, strong) IBOutlet UIView *ddyView;
@property (nonatomic, strong) IBOutlet UIView *shyView;
@property (nonatomic, strong) CTRatingView *kfjLevel;
@property (nonatomic, strong) CTRatingView *ddyLevel;
@property (nonatomic, strong) CTRatingView *shyLevel;
@property (nonatomic, strong) NSString *mgrSocre;
@property (nonatomic, strong) NSString *deptSocre;
@property (nonatomic, strong) NSString *delivSocre;
@property (nonatomic, strong) IBOutlet UILabel *marLab;
@property (nonatomic, strong) IBOutlet UILabel *regiLab;
@property (nonatomic, strong) IBOutlet UILabel *distLab;



@end

@implementation CustSerEvaluViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"服务评价";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BtnSubmit"] style:UIBarButtonItemStylePlain target:self action:@selector(confirmClick:)];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_KHFW, CUST_MODULE_OPERATE_FWPJ, nil]];
}

- (void)confirmClick:(UIBarButtonItem *)item {
    if ([self.indicator showing]) {
        return;
    }
    self.indicator.text = @"提交评分中...";
    [self.indicator show];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService addCustEvel:[CustWebService sharedUser].userId mgrScore:self.mgrSocre deptScore:self.deptSocre deliveryScore:self.delivSocre] delegate:self];
    conn.tag = PROCESS_SUBMIT_SCORE;
    [conn start];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initUI {
    self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds];
    self.tv.backgroundColor = [UIColor clearColor];
    self.tv.pullDownViewEnabled = NO;
    self.tv.pullUpViewEnabled = NO;
    [self.view addSubview:self.tv];
    self.adLab = [[UILabel alloc] initWithFrame:CGRectMake(8, 39, self.view.frame.size.width - 8 * 2, 80)];
    self.adLab.font = [UIFont systemFontOfSize:14.0f];
    self.adLab.textColor = [UIColor blackColor];
    self.adLab.numberOfLines = 0;
    self.adLab.textAlignment = NSTextAlignmentLeft;
    self.adLab.lineBreakMode = NSLineBreakByCharWrapping;
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:@"福州", @"3501", @"厦门", @"3502", @"莆田", @"3503", @"三明", @"3504", @"泉州", @"3505", @"漳州", @"3506", @"南平", @"3507", @"龙岩", @"3508", @"宁德", @"3509", nil];
    NSString *city = [dict objectForKey:[[CustWebService sharedUser].userCode substringToIndex:4]];
    self.adLab.text = [NSString stringWithFormat:@"   您好!首先感谢您长期以来对%@市烟草专卖局(公司)各项工作的大力支持和配合!为进一步提高服务质量,保障您的权益,市局(公司)开通了网上服务评价系统,请您对片区的客户经理,电访员的服务态度进行评价,我们将竭力做得更好!", city];
    [self.fView addSubview:self.adLab];
    [self.tv addSubview:self.headerView];
    [self loadData];
}

- (void)loadData {
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"加载中...";
    [self.indicator show];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService queryCustMgr:[CustWebService sharedUser].userCode] delegate:self];
    conn.tag = PROCESS_QUERY_MGR;
    [conn start];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.kfjLevel = [[CTRatingView alloc] initWithFrame:self.kfjView.bounds image:@"StarBigNormal" highlightedImage:@"StarBigHelighted"];
    self.kfjLevel.ratingViewEnabled = YES;
    self.kfjLevel.delegate = self;
    [self.kfjView addSubview:self.kfjLevel];
    
    self.ddyLevel = [[CTRatingView alloc] initWithFrame:self.ddyView.bounds image:@"StarBigNormal" highlightedImage:@"StarBigHelighted"];
    self.ddyLevel.ratingViewEnabled = YES;
    self.ddyLevel.delegate = self;
    [self.ddyView addSubview:self.ddyLevel];
    
    self.shyLevel = [[CTRatingView alloc] initWithFrame:self.shyView.bounds image:@"StarBigNormal" highlightedImage:@"StarBigHelighted"];
    self.shyLevel.ratingViewEnabled = YES;
    self.shyLevel.delegate = self;
    [self.shyView addSubview:self.shyLevel];
    
    CGSize size = [self.adLab.text sizeWithFont:self.adLab.font constrainedToSize:CGSizeMake(self.adLab.frame.size.width, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    size = CGSizeMake(size.width, size.height + 20);
    self.adLab.frame = CGRectMake(8, 39, self.adLab.frame.size.width - 8 * 2, size.height);

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (NSString *)isEmptyString:(NSString *)string {
    if (string == nil || [string isEqualToString:@"null"]) {
        return @"暂无";
    }
    return string;
}

- (void)showDetail:(CustInfo *)info {
    self.marLab.text =  [NSString stringWithFormat:@"客户经理:%@",[self isEmptyString:info.custMgrName ]];
    self.regiLab.text = [NSString stringWithFormat:@"订单员:%@", [self isEmptyString:info.regiePersonName]];
    self.distLab.text = [NSString stringWithFormat:@"送货员:%@", [self isEmptyString:info.distPersonName]];
}

#pragma mark - CTRateingView  Delegate
- (void)ctRatingViewDidRating:(CTRatingView *)ratingView withmRate:(float)mRate {
    if (mRate == 0) {
        mRate = -1;
    }else if (mRate == 5) {
        mRate = 4;
    }

    if (ratingView == self.kfjLevel) {
        self.mgrSocre = [NSString stringWithFormat:@"%d", (int)mRate + 1];
    } else if (ratingView == self.ddyLevel) {
        self.deptSocre = [NSString stringWithFormat:@"%d", (int)mRate + 1];
    } else if (ratingView == self.shyLevel) {
        self.delivSocre = [NSString stringWithFormat:@"%d", (int)mRate + 1];
    }
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error {
    self.indicator.text = @"加载失败";
    [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data {
    if (connection.tag == PROCESS_QUERY_MGR) {
        CustInfo * info = [[CustInfo alloc] init];
        [info parseData:data];
        [self showDetail:info];
        [self.indicator hide];

    } else if (connection.tag == PROCESS_SUBMIT_SCORE) {
        CustSystemMsg *msg = [[CustSystemMsg alloc] init];
        [msg parseData:data complete:^(CustSystemMsg *msg) {
            if ([msg.code isEqualToString:@"1"]) {
                self.indicator.text = msg.msg;
                [self.indicator autoHide:CTIndicateStateDone afterDelay:1.0f];
            } else if ([msg.code isEqualToString:@"-1"]) {
                self.indicator.text = msg.msg;
                [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
            } else {
                self.indicator.text = @"失败!";
                [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
            }
        }];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
