//
//  CustOrderEditController.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustOrderEditController.h"
#import "CustOrder.h"
#import "CustWebService.h"
#import "CustSystemMsg.h"
#import "CTURLConnection.h"
#import "CTIndicateView.h"

#define PROCESS_SUBMIT 0
#define PROCESS_DELETE 1
#define PROCESS_NEXT 2

@interface CustOrderEditController () <UIActionSheetDelegate, CTURLConnectionDelegate>

@property (weak, nonatomic) IBOutlet UIView *sumView;
@property (weak, nonatomic) IBOutlet UILabel *qtyOrderSum;
@property (weak, nonatomic) IBOutlet UILabel *amtOrderSum;
@property (strong, nonatomic) IBOutlet UIView * header;
@property (weak, nonatomic) IBOutlet UIView *inputBg;
@property (strong, nonatomic) UIView * numberPad;
@property (weak, nonatomic) IBOutlet UILabel *brandName;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *lastestBrandInfo;
@property (weak, nonatomic) IBOutlet UILabel *demand;
@property (nonatomic, strong) CTIndicateView * ctIndicate;
@property (nonatomic) int process;


- (void)btnOkClicked:(UIButton *)btn;
- (void)nextBrand:(UIButton *)btn;
- (void)numberPadClicked:(UIButton *)btn;
- (IBAction)btnPlusOrMinus:(UIButton *)btn;
- (IBAction)delete:(UIButton *)btn;
- (void)back;
- (void)changeBrand;

@end

@implementation CustOrderEditController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"在线订货";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:self.header];
    self.inputBg.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CustOrderEditInputBg"]];
    self.sumView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CustOrderDetailSummaryBg"]];
    
    UIButton * btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    btnNext.backgroundColor = [UIColor clearColor];
    btnNext.showsTouchWhenHighlighted = YES;
    btnNext.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    [btnNext setTitle:@"下个品牌  " forState:UIControlStateNormal];
    [btnNext setTitle:@"下个品牌  " forState:UIControlStateHighlighted];
    [btnNext setImage:[UIImage imageNamed:@"NavNext"] forState:UIControlStateNormal];
    [btnNext setImage:[UIImage imageNamed:@"NavNext"] forState:UIControlStateHighlighted];
    [btnNext setImageEdgeInsets:UIEdgeInsetsMake(0, 95, 0, 0)];
    [btnNext addTarget:self action:@selector(nextBrand:) forControlEvents:UIControlEventTouchUpInside];
    btnNext.frame = CGRectMake(0, 0, 120, 30);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnNext];
    
    self.demand.text = @"0";
    self.brandName.adjustsFontSizeToFitWidth = YES;
    [self.view addSubview:self.sumView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.sumView.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height - self.sumView.frame.size.height / 2);
    if (self.numberPad == nil) {
        self.numberPad = [[UIView alloc] initWithFrame:CGRectMake(0, self.header.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height - self.header.frame.size.height - self.sumView.frame.size.height)];
        self.numberPad.backgroundColor = [UIColor colorWithRed:239.0/255.0 green:238.0/255.0 blue:239.0/255.0 alpha:1];
        UIButton * btnOk = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnOk setBackgroundImage:[[UIImage imageNamed:@"CustOrderEditOk"] resizableImageWithCapInsets:UIEdgeInsetsMake(20, 6, 20, 6)] forState:UIControlStateNormal];
        [btnOk setBackgroundImage:[[UIImage imageNamed:@"CustOrderEditOk"] resizableImageWithCapInsets:UIEdgeInsetsMake(20, 6, 20, 6)] forState:UIControlStateHighlighted];
        btnOk.frame = CGRectMake(4, self.numberPad.frame.size.height - 44, self.numberPad.frame.size.width - 4 - 4, 44);
        btnOk.backgroundColor = [UIColor clearColor];
        btnOk.titleLabel.font = [UIFont systemFontOfSize:20.0f];
        [btnOk setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnOk setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [btnOk setTitle:@"确 定" forState:UIControlStateNormal];
        [btnOk setTitle:@"确 定" forState:UIControlStateHighlighted];
        [btnOk addTarget:self action:@selector(btnOkClicked:) forControlEvents:UIControlEventTouchUpInside];
        btnOk.showsTouchWhenHighlighted = YES;
        [self.numberPad addSubview:btnOk];
        
        float numberWidth = (self.numberPad.frame.size.width - 4 - 4 - 2 - 2) / 3;
        float numberHeight = (btnOk.frame.origin.y - 4 - 4 - 2 - 2 - 2) / 4;
        for (int i = 0; i < 12; i++) {
            int x = (i % 3) * (numberWidth + 2) + 4;
            int y = i / 3 * (numberHeight + 2) + 4;
            UIButton * item = [UIButton buttonWithType:UIButtonTypeCustom];
            item.frame = CGRectMake(x, y, numberWidth, numberHeight);
            item.backgroundColor = [UIColor colorWithRed:4.0/255.0 green:168.0/255.0 blue:117.0/255.0 alpha:1];
            item.titleLabel.font = [UIFont systemFontOfSize:30.0f];
            [item setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [item setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
            item.showsTouchWhenHighlighted = YES;
            if (i < 9) {
                [item setTitle:[NSString stringWithFormat:@"%d", i + 1] forState:UIControlStateNormal];
                [item setTitle:[NSString stringWithFormat:@"%d", i + 1] forState:UIControlStateHighlighted];
                item.tag = i + 1;
            } else if (i == 9){
                [item setTitle:@"C" forState:UIControlStateNormal];
                [item setTitle:@"C" forState:UIControlStateHighlighted];
                item.tag = -1;
            } else if (i == 10){
                [item setTitle:@"0" forState:UIControlStateNormal];
                [item setTitle:@"0" forState:UIControlStateHighlighted];
                item.tag = 0;
            } else {
                [item setImage:[[UIImage imageNamed:@"CustOrderEditBackspace"] resizableImageWithCapInsets:UIEdgeInsetsMake(20, 6, 20, 6)] forState:UIControlStateNormal];
                [item setImage:[[UIImage imageNamed:@"CustOrderEditBackspace"] resizableImageWithCapInsets:UIEdgeInsetsMake(20, 6, 20, 6)] forState:UIControlStateHighlighted];
                item.tag = -2;
            }
            item.titleLabel.shadowColor = [UIColor colorWithRed:239.0/255.0 green:238.0/255.0 blue:239.0/255.0 alpha:1];
            item.titleLabel.shadowOffset = CGSizeMake(1, 1);
            [item addTarget:self action:@selector(numberPadClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.numberPad addSubview:item];
        }
        [self.view addSubview:self.numberPad];
        
        [self changeBrand];
    }
    
    if (self.ctIndicate == nil) {
        self.ctIndicate = [[CTIndicateView alloc] initInView:self.view];
        self.ctIndicate.backgroundTouchable = NO;
    }
    
    self.qtyOrderSum.text = self.qtyOrder;
    self.amtOrderSum.text = self.amtOrder;
}

- (void)changeBrand
{
    if (self.currentIndex >= self.brandArray.count){
        self.currentIndex = self.brandArray.count - 1;
    }
    if (self.currentIndex < 0) {
        self.currentIndex = 0;
    }
    
    CustOrder * order = [self.brandArray objectAtIndex:self.currentIndex];
    self.brandName.text = order.brandName;
    self.price.text = [NSString stringWithFormat:@"￥%@", order.tradePrice];
    self.demand.text = order.demand;
    
    if (self.currentIndex != 0) {
        order = [self.brandArray objectAtIndex:(self.currentIndex - 1)];
        self.lastestBrandInfo.text = [NSString stringWithFormat:@"上个品牌：%@        订单量：%@", order.brandName, order.order];
    }

}

- (void)btnOkClicked:(UIButton *)btn
{
    CustOrder * order = [self.brandArray objectAtIndex:self.currentIndex];
    NSString * newDemand = self.demand.text;
    if ([order.demand isEqualToString:newDemand]) {
        self.ctIndicate.text = @"需求量无变更";
        [self.ctIndicate autoHide:CTIndicateStateWarning afterDelay:0.5];
        return;
    }
    if (self.ctIndicate.showing) {
        return;
    }
    self.ctIndicate.text = @"正在保存...";
    [self.ctIndicate show];
    self.process = PROCESS_SUBMIT;
    NSString * params = [NSString stringWithFormat:@"<UsShoppingCart><userId>%@</userId><brandId>%@</brandId><demandQty>%@</demandQty></UsShoppingCart>", [CustWebService sharedUser].userId, order.brandId, newDemand];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService custOrderChangeUrl] params:params delegate:self];
    [conn start];
}

- (void)nextBrand:(UIButton *)btn
{
    if (self.currentIndex + 1 == self.brandArray.count) {
        self.ctIndicate.text = @"已到最后一个品牌";
        [self.ctIndicate autoHide:CTIndicateStateWarning afterDelay:0.5];
        return;
    }
    CustOrder * order = [self.brandArray objectAtIndex:self.currentIndex];
    NSString * newDemand = self.demand.text;
    if ([newDemand isEqualToString:order.demand]) {
        self.currentIndex++;
        [self changeBrand];
        return;
    }
    if (self.ctIndicate.showing) {
        return;
    }
    self.ctIndicate.text = @"正在保存...";
    [self.ctIndicate show];
    self.process = PROCESS_NEXT;
    NSString * params = [NSString stringWithFormat:@"<UsShoppingCart><userId>%@</userId><brandId>%@</brandId><demandQty>%@</demandQty></UsShoppingCart>", [CustWebService sharedUser].userId, order.brandId, newDemand];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService custOrderChangeUrl] params:params delegate:self];
    [conn start];
}

- (void)numberPadClicked:(UIButton *)btn
{
    if (btn.tag >= 0 && [self.demand.text isEqualToString:@"0"]) {
        self.demand.text = [NSString stringWithFormat:@"%d", (int)btn.tag];
    } else if (btn.tag >= 0 && self.demand.text.length < 3){
        self.demand.text = [NSString stringWithFormat:@"%@%d", self.demand.text, (int)btn.tag];
    } else if (btn.tag == -1){
        self.demand.text = @"0";
    } else if (btn.tag == -2){
        int toIndex = (int)self.demand.text.length - 1;
        if (toIndex < 1) {
            self.demand.text = @"0";
        } else {
            self.demand.text = [self.demand.text substringToIndex:toIndex];
        }
    }
}

- (void)btnPlusOrMinus:(UIButton *)btn
{
    int d = [self.demand.text intValue];
    btn.tag == 0 ? d++ : d--;
    if (d > 999) {
        d = 999;
    }
    if (d < 0) {
        d = 0;
    }
    self.demand.text = [NSString stringWithFormat:@"%d", d];
}

- (IBAction)delete:(UIButton *)btn
{
    if (self.ctIndicate.showing) {
        return;
    }
    UIActionSheet * action = [[UIActionSheet alloc] initWithTitle:@"确定删除规格？" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"删除规格" otherButtonTitles: nil];
    [action showInView:self.view];
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setQtyOrder:(NSString *)qtyOrder
{
    _qtyOrder = qtyOrder;
    self.qtyOrderSum.text = self.qtyOrder;
}

- (void)setAmtOrder:(NSString *)amtOrder
{
    _amtOrder = amtOrder;
    self.amtOrderSum.text = self.amtOrder;
}

#pragma mark -
#pragma mark CTURLConnection
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    if (self.process == PROCESS_DELETE) {
        self.ctIndicate.text = @"删除失败";
    } else {
        self.ctIndicate.text = @"保存失败";
    }
    [self.ctIndicate hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    
    connection.delegate = nil;
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (self.process == PROCESS_DELETE) {
        CustSystemMsg * msg = [[CustSystemMsg alloc] init];
        [msg parseData:data];
        if ([msg.code isEqualToString:@"1"]) {
            self.ctIndicate.text = @"删除成功";
            [self.ctIndicate hideWithSate:CTIndicateStateDone afterDelay:1.0];
            if ([self.delegate respondsToSelector:@selector(custOrderEditController:didDeleteBrandAtIndex:)]) {
                [self.delegate custOrderEditController:self didDeleteBrandAtIndex:self.currentIndex];
            }
            [self performSelector:@selector(back) withObject:nil afterDelay:1.0];
        }
    } else if (self.process == PROCESS_SUBMIT || self.process == PROCESS_NEXT){
        CustSystemMsg * msg = [[CustSystemMsg alloc] init];
        [msg parseData:data];
        if (msg.code.length != 0) {
            self.ctIndicate.text = msg.msg;
            [self.ctIndicate hideWithSate:CTIndicateStateWarning afterDelay:1.5];
            return;
        }
        CustOrder * item = [[CustOrder alloc] init];
        [item parseData:data];
        
        if ([self.delegate respondsToSelector:@selector(custOrderEditController:didSubmitDemandAtIndex:newOrder:)]){
            [self.delegate custOrderEditController:self didSubmitDemandAtIndex:self.currentIndex newOrder:item];
        }
        
        CustOrder * order = (CustOrder *)[self.brandArray objectAtIndex:self.currentIndex];
        order.demand = item.demand;
        order.order = item.order;
        order.cash = item.cash;
        order.tradePrice = item.tradePrice;
        
        self.ctIndicate.text = @"保存成功";
        [self.ctIndicate hideWithSate:CTIndicateStateDone afterDelay:1.0];
        if (self.process == PROCESS_NEXT) {
            self.currentIndex++;
            [self performSelector:@selector(changeBrand) withObject:nil afterDelay:1.0];
        }
    }
}

#pragma mark -
#pragma mark UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        self.ctIndicate.text = @"正在删除...";
        [self.ctIndicate show];
        self.process = PROCESS_DELETE;
        NSString * params = [NSString stringWithFormat:@"<UsShoppingCart><userId>%@</userId><brandId>%@</brandId></UsShoppingCart>", [CustWebService sharedUser].userId, ((CustOrder *)[self.brandArray objectAtIndex:self.currentIndex]).brandId];
        CTURLConnection * conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService custOrderDeleteShoppingCarUrl] params:params delegate:self];
        [conn start];
    }
}

@end
