//
//  CustOrderHistoryDetailController.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-24.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustOrderHistoryDetailController.h"
#import "CTTableView.h"
#import "CTIndicateView.h"
#import "CustWebService.h"
#import "CTURLConnection.h"
#import "CustOrder.h"
#import "CustModule.h"

@interface CustOrderHistoryDetailController () <UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, CTTableViewDelegate, CTURLConnectionDelegate, CustOrderDelegate>
@property (strong, nonatomic) IBOutlet UIView *header;
@property (weak, nonatomic) IBOutlet UILabel *orderDate;
@property (strong, nonatomic) IBOutlet UIView *summaryView;
@property (weak, nonatomic) IBOutlet UILabel *orderSummary;
@property (weak, nonatomic) IBOutlet UILabel *cashSum;
@property (nonatomic, strong) CTTableView * cttv;
@property (nonatomic, strong) CTIndicateView * indicator;
@property (nonatomic, strong) NSMutableArray * brandArray;

- (void)query:(int)flag;

@end

@implementation CustOrderHistoryDetailController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"订单明细";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.orderDate.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CustOrderAddSearchBg"]];
    self.summaryView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CustOrderDetailSummaryBg"]];
    [self.view addSubview:self.summaryView];
    
    self.brandArray = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.summaryView.center = CGPointMake(self.view.bounds.size.width  / 2, self.view.bounds.size.height - self.summaryView.frame.size.height / 2);
    
    if (self.cttv == nil) {
        self.cttv = [[CTTableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - self.summaryView.frame.size.height + 4) style:UITableViewStylePlain];
        self.cttv.delegate = self;
        self.cttv.dataSource = self;
        self.cttv.ctTableViewDelegate = self;
        self.cttv.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.cttv.pullUpViewEnabled = NO;
        [self.view addSubview:self.cttv];
    }
    
    if (self.indicator == nil) {
        self.indicator = [[CTIndicateView alloc] initInView:self.view];
        self.indicator.backgroundTouchable = NO;
    }
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.brandArray.count == 0) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CUST_MODULE_ORDER_HIS_MAIN, self.orderId, nil]];
        [self query:0];
    }
    self.orderDate.text = [NSString stringWithFormat:@"   订单日期：%@", self.date];
}

- (void)query:(int)flag
{
    if (self.indicator.showing) {
        return;
    }
    
    if (flag == 0) {
        [self.cttv showPullDownView];
    }
    
    self.indicator.text = @"加载中...";
    [self.indicator show];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService custOrderHistoryDetailListUrl:self.orderId] delegate:self];
    [conn start];
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.cttv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.cttv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark UITableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.brandArray.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return self.header.frame.size.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"BRAND_CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:236.0/255.0 blue:239.0/255.0 alpha:1];
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        NSArray * elements = [[NSBundle mainBundle] loadNibNamed:@"CustBrandCell" owner:nil options:nil];
        UIView * bg = [elements objectAtIndex:0];
        bg.frame = CGRectMake(0, 0, self.view.frame.size.width, 36);
        bg.tag = 1;
        [cell addSubview:bg];

    }
    
    UIView * bg = [cell viewWithTag:1];
    if (indexPath.row % 2 != 0) {
        bg.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:236.0/255.0 blue:239.0/255.0 alpha:1];
    } else {
        bg.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1];
    }
    
    CustOrder * item = [self.brandArray objectAtIndex:indexPath.row];
    
    UILabel * orderDate = (UILabel *)[cell viewWithTag:2];
    orderDate.text = item.brandName;
    
    UILabel * order = (UILabel *)[cell viewWithTag:4];
    order.text = item.order;
    
    UILabel * cash = (UILabel *)[cell viewWithTag:5];
    cash.text = item.cash;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark-
#pragma mark CTTableView Delegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self query:1];
    return NO;
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"数据查询失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:0.5];
    [self.cttv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    CustOrder * item = [[CustOrder alloc] init];
    item.delegate = self;
    [item parseData:data];
}

#pragma mark -
#pragma mark CustOrder Delegate
- (void)custOrder:(CustOrder *)custOrder didFinishingParse:(NSArray *)array
{
    if (array.count == 0) {
        self.indicator.text = @"查无数据";
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:0.5];
    } else {
        [self.indicator hide];
    }
    
    self.orderSummary.text = [NSString stringWithFormat:@"%.0f [异%.0f]", custOrder.orderSum, custOrder.norOrderSum];
    self.cashSum.text = [NSString stringWithFormat:@"%.2f", custOrder.cashSum];
    [self.brandArray removeAllObjects];
    [self.brandArray addObjectsFromArray:array];
    [self.cttv reloadData];
    
}

@end
