//
//  CustTrackDetailController.m
//  CmAppCustIos
//
//  Created by hsit on 14-8-27.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustTrackDetailController.h"
#import "CTURLConnection.h"
#import "CustServiceMsg.h"
#import "CustWebService.h"
#import "CTIndicateView.h"

@interface CustTrackDetailController ()<CTURLConnectionDelegate>

@property (nonatomic, strong) UILabel * titlelab;
@property (nonatomic, strong) UILabel * datelab;
@property (nonatomic, strong) UILabel * idlab;
@property (nonatomic, strong) UIView  * line;
@property (nonatomic, strong) UILabel * contentlab;
@property (nonatomic, strong) UILabel * personlab;
@property (nonatomic, strong) CTIndicateView * indicator;


@end

@implementation CustTrackDetailController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor whiteColor];
    if (self.titlelab == nil) {
        self.indicator = [[CTIndicateView alloc] initInView:self.view];
        self.indicator.backgroundTouchable = YES;
        self.titlelab = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, self.view.frame.size.width - 5 * 2, 20)];
        self.titlelab.textAlignment = NSTextAlignmentCenter;
        self.titlelab.font = [UIFont systemFontOfSize:17.0f];
        [self.titlelab adjustsFontSizeToFitWidth];
        self.titlelab.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.titlelab];
        
        self.datelab = [[UILabel alloc] init];
        self.datelab.frame = CGRectOffset(self.titlelab.frame, 0, 30);
        self.datelab.textAlignment = NSTextAlignmentRight;
        self.datelab.font = [UIFont systemFontOfSize:14.0f];
        self.datelab.textColor = [UIColor colorWithRed:88.0/255.0 green:88.0/255.0 blue:88.0/255.0 alpha:1];
        self.datelab.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.datelab];
        
        self.idlab = [[UILabel alloc] init];
        self.idlab.frame = CGRectOffset(self.titlelab.frame, 0, 50);
        self.idlab.textAlignment = NSTextAlignmentRight;
        self.idlab.font = [UIFont systemFontOfSize:14.0f];
        self.idlab.textColor = [UIColor colorWithRed:88.0/255.0 green:88.0/255.0 blue:88.0/255.0 alpha:1];
        self.idlab.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.idlab];
        
        self.line = [[UIView alloc] init];
        self.line.frame = CGRectMake(5,self.idlab.frame.origin.y + self.idlab.frame.size.height , self.view.frame.size.width - 5 * 2, 1);
        self.line.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Dot"]];
        [self.view addSubview:self.line];
        
        self.contentlab = [[UILabel alloc] initWithFrame:CGRectMake(10, self.line.frame.origin.y + self.line.frame.size.height + 5,self.view.frame.size.width - 10 * 2, 50)];;
        self.contentlab.frame = CGRectOffset(self.line.frame, 0, 10);
        self.contentlab.backgroundColor = [UIColor clearColor];
        self.contentlab.numberOfLines = 0;
        self.contentlab.textAlignment = NSTextAlignmentLeft;
        self.contentlab.lineBreakMode = NSLineBreakByCharWrapping;
        self.contentlab.font = [UIFont systemFontOfSize:15.0f];
        self.contentlab.alpha = 0.6f;
        [self.view addSubview:self.contentlab];


        
        self.personlab = [[UILabel alloc] init];
        self.personlab.frame = CGRectMake(10, self.contentlab.frame.origin.y + self.contentlab.frame.size.height, self.view.frame.size.width - 10 * 2, 40);
        self.personlab.textAlignment = NSTextAlignmentRight;
        self.personlab.font = [UIFont systemFontOfSize:15.0f];
        self.personlab.backgroundColor = [UIColor clearColor];
        self.personlab.alpha = 0.6f;
        [self.view addSubview:self.personlab];
    }
    [self loadData];
}

- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"加载中...";
    [self.indicator show];
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getServiceDetailInfo:self.msg.demandId] delegate:self];
    [conn start];
    return;
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0f];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    CustServiceMsg * msg = [[CustServiceMsg alloc] init];
    [msg parseData:data complete:^(CustServiceMsg * msg){
        [self.indicator hide];
        self.titlelab.text = msg.demandTitle;
        self.idlab.text = [NSString stringWithFormat:@"NO: %@",msg.demandId];
        self.datelab.text = msg.demandTime;
        self.contentlab.text = [NSString stringWithFormat:@"   %@",msg.demandCont];
        self.personlab.text = [NSString stringWithFormat:@"处理人: %@",msg.dealPersonName];
        //lab自定义行高
        CGSize size = [self.contentlab.text sizeWithFont:self.contentlab.font constrainedToSize:CGSizeMake(self.contentlab.frame.size.width, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
        size = CGSizeMake(size.width, size.height + 20);
        self.contentlab.frame = CGRectMake(self.contentlab.frame.origin.x, self.line.frame.origin.y + self.line.frame.size.height + 5, self.contentlab.frame.size.width, size.height);

        
        if ([msg.dealPersonName isEqualToString:@"null"]) {
            self.personlab.text = @"";
        } else {
           self.personlab.frame = CGRectMake(10, self.contentlab.frame.origin.y + self.contentlab.frame.size.height, self.view.frame.size.width - 10 * 2, 40);
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
