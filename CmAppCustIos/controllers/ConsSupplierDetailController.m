//
//  ConsSupplierController.m
//  CmAppConsumerIos
//
//  Created by 宏超 陈 on 14-5-20.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsSupplierDetailController.h"
#import "CTScrollView.h"
#import "CTImageView.h"
#import "CTURLConnection.h"
#import "CustWebService.h"
#import "ConsSupplier.h"
#import "CTButton.h"
#import "ConsBrandSearchController.h"
#import <QuartzCore/QuartzCore.h>


@interface ConsSupplierDetailController () <CTScrollViewDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) CTScrollView * container;
@property (nonatomic, strong) UIView * tradeMarkContainer;
@property (nonatomic, strong) CTImageView * imgLogo;
@property (nonatomic, strong) UIView * introduceBg;  // 简介背景  制造内缩近
@property (nonatomic, strong) UILabel * introduce;
@property (nonatomic, strong) NSMutableArray * tradeMarks;  // 品牌列表

- (void)loadData;
- (void)refreshContainer;
- (void)showBrandList:(CTButton *)btn;

@end

@implementation ConsSupplierDetailController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"中烟详情";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tradeMarks = [[NSMutableArray alloc] init];
//    [[NSNotificationCenter defaultCenter] postNotificationName:CUST_MODULE_NOTIFICATION_NAME object:[NSArray arrayWithObjects:CONS_MODULE_YCXTBK, CONS_MODULE_YCXTBK, nil]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = self.supplier.supplierName;
    
    if (self.container == nil) {
        self.container = [[CTScrollView alloc] initWithFrame:self.view.bounds];
        self.container.pullUpViewEnabled = NO;
        self.container.pullDownViewEnabled = YES;
        self.container.ctScrollViewDelegate = self;
        self.container.delegate = self;
        self.container.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.container];
    }
    
    if (self.imgLogo == nil) {
        if (self.supplier.totalBrandPic == nil || self.supplier.totalBrandPic.length == 0) {
            self.imgLogo = [[CTImageView alloc] initWithFrame:CGRectMake(0, 0, self.container.frame.size.width, 0)];
        } else {
            self.imgLogo = [[CTImageView alloc] initWithFrame:CGRectMake(0, 0, self.container.frame.size.width, 133)];
        }
        
        self.imgLogo.contentMode = UIViewContentModeScaleToFill;
        self.imgLogo.url = [CustWebService fileFullUrl:self.supplier.totalBrandPic];
        [self.imgLogo loadImage];
        [self.container addSubview:self.imgLogo];
    }
    
    if (self.introduce == nil) {
        
        self.introduce = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, self.container.frame.size.width - 10, 0)];
        self.introduce.font = [UIFont systemFontOfSize:14.0f];
        self.introduce.textColor = [UIColor colorWithRed:32.0/255.0 green:32.0/255.0 blue:32.0/255.0 alpha:1];
        self.introduce.backgroundColor = [UIColor clearColor];
        self.introduce.numberOfLines = 0;
        
        CGSize size = [self.supplier.totalBrand sizeWithFont:self.introduce.font constrainedToSize:CGSizeMake(self.introduce.frame.size.width, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
        self.introduce.frame = CGRectMake(self.introduce.frame.origin.x, self.introduce.frame.origin.y, self.introduce.frame.size.width, size.height + 20);
        self.introduce.text = self.supplier.totalBrand;
        
        self.introduceBg = [[UIView alloc] initWithFrame:CGRectMake(0, self.imgLogo.frame.size.height + 5, self.container.frame.size.width, self.introduce.frame.size.height + 5)];
        self.introduceBg.backgroundColor = [UIColor whiteColor];
        self.introduceBg.layer.borderColor = [[UIColor colorWithRed:209.0/255.0 green:209.0/255.0 blue:209.0/255.0 alpha:1] CGColor];
        
        self.introduceBg.layer.borderWidth = 0.5;
        
        [self.introduceBg addSubview:self.introduce];
        
        [self.container addSubview:self.introduceBg];
        
        self.tradeMarkContainer = [[UIView alloc] initWithFrame:CGRectMake(0, self.introduceBg.frame.origin.y + self.introduceBg.frame.size.height + 10, self.container.frame.size.width, 0)];
        self.tradeMarkContainer.backgroundColor = [UIColor clearColor];
        [self.container addSubview:self.tradeMarkContainer];
        
        [self loadData];
    }
}

- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService supplierTradeMarkListUrl:self.supplier.supplierCode] delegate:self];
    [conn start];
}

- (void)refreshContainer
{
    for (UIView * view in self.tradeMarkContainer.subviews) {
        [view removeFromSuperview];
    }
    
    int y = 0;
    
    for (int i = 0; i < self.tradeMarks.count; i++) {
        ConsSupplier * item = [self.tradeMarks objectAtIndex:i];
        NSArray* elements = [[NSBundle mainBundle] loadNibNamed:@"ConsTradeMarkCell" owner:nil options:nil];
        UIView * view = [elements objectAtIndex:0];
        
        CTImageView * img = (CTImageView *)[view viewWithTag:1];
        img.url = [CustWebService fileFullUrl:item.totalBrandPic];
        [img loadImage];
        
        CTButton * btn = (CTButton *)[view viewWithTag:2];
        btn.index = i;
        [btn setTitle:[NSString stringWithFormat:@"%@ >>  ", item.supplierName] forState:UIControlStateNormal];
        [btn setTitle:[NSString stringWithFormat:@"%@ >>  ", item.supplierName] forState:UIControlStateHighlighted];
        
        [btn addTarget:self action:@selector(showBrandList:) forControlEvents:UIControlEventTouchUpInside];
        
        
        UILabel * intro = (UILabel *)[view viewWithTag:3];
        
        CGSize size = [item.totalBrand sizeWithFont:intro.font constrainedToSize:CGSizeMake(intro.frame.size.width, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
        size = CGSizeMake(size.width, size.height + 10);
        intro.frame = CGRectMake(intro.frame.origin.x, intro.frame.origin.y, intro.frame.size.width, size.height);
        intro.text = item.totalBrand;

        view.frame = CGRectMake(view.frame.origin.x, y, self.view.frame.size.width, intro.frame.origin.y + intro.frame.size.height + 10);
        view.layer.borderColor = [[UIColor colorWithRed:209.0/255.0 green:209.0/255.0 blue:209.0/255.0 alpha:1] CGColor];
        view.layer.borderWidth = 0.5;
        
        [self.tradeMarkContainer addSubview:view];
        y = view.frame.origin.y + view.frame.size.height + 10;
        
    }
    
    self.tradeMarkContainer.frame = CGRectMake(self.tradeMarkContainer.frame.origin.x, self.tradeMarkContainer.frame.origin.y, self.tradeMarkContainer.frame.size.width, y);
    
    y = self.tradeMarkContainer.frame.size.height + self.tradeMarkContainer.frame.origin.y + 10;
    
    if (y < self.container.frame.size.height) {
        y = self.container.frame.size.height + 1;
    }
    self.container.contentSize = CGSizeMake(self.container.frame.size.width, y);
}

- (void)showBrandList:(CTButton *)btn
{
    if (btn.index < self.tradeMarks.count) {
        ConsBrandSearchController * controller = [[ConsBrandSearchController alloc] initWithNibName:nil bundle:nil];
        controller.supplier = [self.tradeMarks objectAtIndex:btn.index];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark -
#pragma mark UISCrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.container ctScrollViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.container ctScrollViewDidEndDragging];
}

#pragma mark -
#pragma mark CTScrollView Delegate
- (BOOL)ctScrollViewDidPullDownToRefresh:(CTScrollView *)ctScrollView
{
    [self loadData];
    return YES;
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.container reload];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    ConsSupplier * item = [[ConsSupplier alloc] init];
    [item parseData:data complete:^(NSArray *array){
        [self.tradeMarks removeAllObjects];
        [self.tradeMarks addObjectsFromArray:array];
        [self.indicator hide];
        [self.container reload];
        [self refreshContainer];
        [self refreshContainer]; // 一个很奇怪的现象，多布局一次才能分开视图块
    }];
}

@end
