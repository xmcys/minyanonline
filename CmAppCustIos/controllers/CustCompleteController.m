//
//  CustCompleteController.m
//  CmAppCustIos
//
//  Created by Arcesaka on 14/8/25.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustCompleteController.h"
#import "CTIndicateView.h"
#import "CTURLConnection.h"
#import "CustWebService.h"
#import "CustSystemMsg.h"
#import "CTSoapConnection.h"
#import "CustServiceMsgList.h"

#define PROCESS_POST_INFO 1

@interface CustCompleteController () <UITextViewDelegate,UITextFieldDelegate,CTURLConnectionDelegate,CTSoapConnectionDelegate>
@property (nonatomic, strong) IBOutlet UIView      * header;
@property (nonatomic, strong) IBOutlet UIView      * textFieldBg;
@property (nonatomic, strong) IBOutlet UITextField * titleField;
@property (nonatomic, strong) IBOutlet UITextView  * contentTextView;
@property (nonatomic, strong) IBOutlet UILabel     * holderlab;
@property (nonatomic, strong) IBOutlet UIButton    * finshbtn;
@property (nonatomic, strong) CTIndicateView       * indicator;

- (IBAction)finshClick:(UIButton *)btn;

@end

@implementation CustCompleteController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.header.frame = CGRectMake(0, 0, self.view.frame.size.width, self.header.frame.size.height);
    self.textFieldBg.layer.cornerRadius = 5.0f;
    self.textFieldBg.layer.masksToBounds = YES;
    self.titleField.delegate = self;
    self.contentTextView.layer.cornerRadius = 5.0f;
    self.contentTextView.layer.masksToBounds = YES;
    self.contentTextView.delegate = self;
    
    UIImage *normalimg = [[UIImage imageNamed:@"CustServiceCommitNormal"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 7, 7, 5)];
    UIImage *heligatedimg = [[UIImage imageNamed:@"CustServiceCommitHelighted"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
    [self.finshbtn setBackgroundImage:normalimg forState:UIControlStateNormal];
    [self.finshbtn setBackgroundImage:heligatedimg forState:UIControlStateHighlighted];
    
    [self.view addSubview:self.header];
    if (self.indicator == nil) {
        self.indicator = [[CTIndicateView alloc] initInView:self.view];
        self.indicator.backgroundTouchable = YES;
    }
}

- (void)sendMsg:(NSString *)demandId
{
    NSDate *nowDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm"];
    
    NSString *sendPerson = [CustWebService sharedUser].userCode;
    NSString *destDateString = [dateFormatter stringFromDate:nowDate];
    NSString *msgContent = [NSString stringWithFormat:@"您收到零售户%@一条服务受理:%@,请尽快进入服务受理列表安排处理!",[CustWebService sharedUser].userCode,self.titleField.text];
    NSString *msgUrlPad = [NSString stringWithFormat:@"FWSLDB+%@",demandId];
    NSString *recPerson = [CustWebService sharedUser].custMgrCode;
    NSString *recDept = [CustWebService sharedUser].saleDeptCode;
    
    NSString *xmlStr = [NSString stringWithFormat:
                       @"&lt;MESEntity&gt;"
                        "&lt;MSG_ID&gt;%@&lt;/MSG_ID&gt;"
                        "&lt;BUS_ID&gt;&lt;/BUS_ID&gt;"
                        "&lt;MSG_TYPE&gt;1&lt;/MSG_TYPE&gt;"
                        "&lt;HANDLE_TYPE&gt;1&lt;/HANDLE_TYPE&gt;"
                        "&lt;SEND_TYPE&gt;1&lt;/SEND_TYPE&gt;"
                        "&lt;LEVEL_TYPE&gt;0&lt;/LEVEL_TYPE&gt;"
                        "&lt;OBJ_TYPE&gt;0&lt;/OBJ_TYPE&gt;"
                        "&lt;PORT_TYPE&gt;HTTP&lt;/PORT_TYPE&gt;"
                        "&lt;REC_TER&gt;0&lt;/REC_TER&gt; "
                        "&lt;ORG_CODE&gt;13500200&lt;/ORG_CODE&gt;"

                        "&lt;SYS_CODE&gt;107&lt;/SYS_CODE&gt;"
                        "&lt;MODEL_CODE&gt;LDYQFWSL&lt;/MODEL_CODE&gt;"
                        "&lt;PROCESS_ID&gt;&lt;/PROCESS_ID&gt;"
                        
                        "&lt;SEND_PERSON&gt;%@&lt;/SEND_PERSON&gt;"
                        "&lt;SEND_DEPT&gt;&lt;/SEND_DEPT&gt;"
                        "&lt;SEND_DUTY&gt;%@&lt;/SEND_DUTY&gt;"
                        "&lt;SEND_DATE&gt;%@&lt;/SEND_DATE&gt;"
                        "&lt;LAST_DATE&gt;&lt;/LAST_DATE&gt;"
                        
                        "&lt;MSG_TITLE&gt;%@&lt;/MSG_TITLE&gt;"
                        "&lt;MSG_CONTENT&gt;%@&lt;/MSG_CONTENT&gt;"
                        "&lt;MSG_URL&gt;&lt;/MSG_URL&gt;"
                        "&lt;MSG_URL_PHONE&gt;&lt;/MSG_URL_PHONE&gt;"
                        "&lt;MSG_URL_PAD&gt;%@&lt;/MSG_URL_PAD&gt;"

                        "&lt;MES_REC_LIST&gt; "
                        "&lt;MES_REC&gt; "
                        "&lt;REC_PERSON&gt;%@&lt;/REC_PERSON&gt; "
                        "&lt;REC_DEPT&gt;%@&lt;/REC_DEPT&gt; "
                        "&lt;REC_DUTY&gt;%@&lt;/REC_DUTY&gt; "
                        "&lt;/MES_REC&gt; "
                        "&lt;/MES_REC_LIST&gt; "
                        
                        "&lt;MES_ANNEX_LIST&gt; "
                        "&lt;/MES_ANNEX_LIST&gt; "
                        "&lt;/MESEntity&gt;"
                        ,@"",sendPerson,@"零售户",destDateString,@"有新的服务受理!",msgContent,msgUrlPad,recPerson,recDept,@"客户经理"
                        ];
    
    if (self.indicator.showing) return;
    NSString *params = [NSString stringWithFormat:
                        @"<xmlString>%@</xmlString>\n"
                        , xmlStr
                        ];
    
    CTSoapConnection * conn = [[CTSoapConnection alloc] initWithFunctionName:@"sendMsgToMQByXml" params:params delegate:self];
    conn.soapNameSpace = SOAP_NAMESPACE;
    conn.soapUrl = [CustWebService soapUrl];
    [conn start];
    return;
}

- (IBAction)finshClick:(UIButton *)btn
{
    if (self.titleField.text.length == 0) {
        self.indicator.text = @"请输入标题";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
        return;
    }
    if (self.contentTextView.text.length == 0) {
        self.indicator.text = @"请输入内容";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
        return;
    }
    if (self.titleField.text.length > 50) {
        self.indicator.text = @"标题不能超过50个字";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
        return;
    }
    if (self.contentTextView.text.length > 500) {
        self.indicator.text = @"描述不能超过500个字";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
        return;
    }
    if (self.indicator.showing) return;
    self.indicator.text = @"正在提交...";
    [self.indicator show];
    NSString * params = [NSString stringWithFormat:@"<ServiceAccepted><licenceCode>%@</licenceCode><demandTitle>%@</demandTitle><demandCont>%@</demandCont></ServiceAccepted>",[CustWebService sharedUser].userCode,self.titleField.text,self.contentTextView.text];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithPutMethodUrl:[CustWebService addServiceInfo] params:params delegate:self];
    conn.tag = PROCESS_POST_INFO;
    [conn start];
}

- (void)empty
{
    self.titleField.text = @"";
    self.contentTextView.text = @"";
    self.holderlab.hidden = NO;
}

#pragma mark TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.titleField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.titleField resignFirstResponder];
    [self.contentTextView resignFirstResponder];
}
#pragma mark - TextView Delegate
- (void)textViewDidChange:(UITextView *)textView
{
    if (self.contentTextView.text.length != 0) {
        self.holderlab.hidden = YES;
    } else {
        self.holderlab.hidden = NO;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [self.contentTextView resignFirstResponder];
        return NO;
    }
    return YES;
}


#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"提交失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0f];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == PROCESS_POST_INFO) {
        CustSystemMsg * msg = [[CustSystemMsg alloc] init];
        [msg parseData:data];
        if ([msg.code isEqualToString:@"1"]) {
            self.indicator.showing = NO;
            [self sendMsg:msg.value];
        } else {
            self.indicator.text = msg.msg;
            [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0f];
        }
    }
}
#pragma mark - CTSoapConnection DataSource & Delegate
- (void)soapConnection:(CTSoapConnection *)connection didFinishLoading:(NSData *)data
{
    NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if ([result isEqualToString:@"success"]) {
        self.indicator.text = @"提交成功";
        [self.indicator autoHide:CTIndicateStateDone afterDelay:1.0f];
        [self performSelector:@selector(empty) withObject:nil afterDelay:0.8f];
    } else {
        self.indicator.text = @"提交失败";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
    }
    return;
}

- (void)soapConnection:(CTSoapConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    return;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
