//
//  CustOrderSuggestController.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-21.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"

@class CustOrderSuggestController;

@protocol CustOrderSuggestControllerDelegate <NSObject>

@optional
- (void)custOrderSuggestControllerDidBrandAdded:(CustOrderSuggestController *)controller;

@end

@interface CustOrderSuggestController : CTViewController

@property (nonatomic, weak) id<CustOrderSuggestControllerDelegate> delegate;

@end
