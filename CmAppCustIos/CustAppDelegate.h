//
//  CustAppDelegate.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-20.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CTUtil.h"
#import "CustSignInController.h"
#import "CustSlideMenuController.h"

@interface CustAppDelegate : UIResponder <UIApplicationDelegate, CustSignInControllerDelegate>

@property (strong, nonatomic) UIWindow * window;
@property (strong, nonatomic) CustSignInController * signInController;

@end
