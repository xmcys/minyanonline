//
//  CustExchangeGift.h
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/24.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustExchangeGift : NSObject

@property (nonatomic, strong) NSString * giftCode;     // 礼品代码
@property (nonatomic, strong) NSString * giftName;     // 礼品名
@property (nonatomic, strong) NSString * giftUnit;     // 礼品单位
@property (nonatomic, strong) NSString * giftPic;      // 礼品图片
@property (nonatomic, strong) NSString * changePoint;  // 兑换积分
@property (nonatomic, strong) NSString * giftDesc;     // 礼品描述
@property (nonatomic, strong) NSString * ruleId;       // ruleId
@property (nonatomic, strong) NSString * supplierCode; // 公司代码
@property (nonatomic, strong) NSString * advisePrice;  // 价格
@property (nonatomic, strong) NSString * supplierName; // 公司名

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *))block;

@end
