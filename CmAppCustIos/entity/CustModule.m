//
//  CustModule.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-2-7.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustModule.h"
#import "CustWebService.h"
#import "CTURLConnection.h"

@interface CustModule () <CTURLConnectionDelegate, NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableData * data;
@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void(^block)(NSArray *array);

- (void)saveRecordWithModCode:(NSString *)modCode operateId:(NSString *)operateId;

@end

@implementation CustModule

+ (void)recordWithModCode:(NSString *)modCode operateId:(NSString *)operateId
{
    CustModule * cm = [[CustModule alloc] init];
    [cm saveRecordWithModCode:modCode operateId:operateId];
}

- (void)saveRecordWithModCode:(NSString *)modCode operateId:(NSString *)operateId
{

    NSDictionary* infoDict =[[NSBundle mainBundle] infoDictionary];
    NSString * version = [infoDict objectForKey:@"CFBundleShortVersionString"];
    
    NSURL * mUrl = [[NSURL alloc] initWithString:[CustWebService cmappLogInfoUrl:[CustWebService sharedUser].userId appType:APP_TYPE version:version modCode:modCode operateId:operateId]];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:mUrl.absoluteString delegate:self];
    [conn start];
}

-(void)parseData:(NSData *)data complete:(void (^)(NSArray *))block;
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"模块使用记录失败");
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    NSLog(@"模块使用记录成功：%@", [[NSString alloc] initWithData:self.data encoding:NSUTF8StringEncoding]);
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        CustModule * item = [[CustModule alloc] init];
        item.code = [attributeDict objectForKey:@"dictCode"];
        item.name = [attributeDict objectForKey:@"dictName"];
        [self.array addObject:item];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}
@end
