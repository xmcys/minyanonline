//
//  CustOrderTrDtl.m
//  CmAppCustIos
//
//  Created by hsit on 15-3-18.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustOrderTrDtl.h"

@implementation CustOrderTrDtl

-(void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}
#pragma mark - NSXMLParser Delegate
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if ([elementName isEqualToString:@"item"]) {
        CustOrderTrDtl * item = [[CustOrderTrDtl alloc] init];
        item.statusName = [attributeDict objectForKey:@"statusName"];
        item.statusTime = [attributeDict objectForKey:@"statusTime"];
        [self.array addObject:item];
    }
}

-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}


@end
