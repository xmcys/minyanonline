//
//  ConsGoods.h
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-9.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ConsGoods : NSObject

@property (nonatomic, strong) NSString * goodsId;
@property (nonatomic, strong) NSString * goodsCode;
@property (nonatomic, strong) NSString * goodsName;
@property (nonatomic, strong) NSString * mDescription;
@property (nonatomic, strong) NSString * imgUrl;
@property (nonatomic, strong) NSString * price;
@property (nonatomic, strong) NSString * points;
@property (nonatomic, strong) NSString * salesCount;
@property (nonatomic, strong) NSString * pic1;
@property (nonatomic, strong) NSString * pic2;
@property (nonatomic, strong) NSString * pic3;
@property (nonatomic, strong) NSString * pic4;
@property (nonatomic, strong) NSString * pic5;
@property (nonatomic, strong) NSString * orderNo;
@property (nonatomic, strong) NSString * orderDate;
@property (nonatomic, strong) NSString * exType;
@property (nonatomic, strong) NSString * orderAmount;
;@property (nonatomic, strong) NSString * exPoints;
@property (nonatomic, strong) NSString * pickupCode;
@property (nonatomic, strong) NSString * goodsDesc;
@property (nonatomic, strong) NSString * storeName;
@property (nonatomic, strong) NSString * storeAddr;
@property (nonatomic, strong) NSString * tel;
@property (nonatomic, strong) NSString * cnName;
@property (nonatomic, strong) NSString * cnPhoneNo;
@property (nonatomic, strong) NSString * orderStatus;
@property (nonatomic, strong) NSString * qty;



- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;

- (NSRange)getQtyPosition:(NSString *)value lastRange:(NSRange)range;

@end


@interface ConsExchangePoints : NSObject

@property (nonatomic, strong) NSString * points;

- (void)parseData:(NSData *)data complete:(void(^)(NSString *points))block;

@end

