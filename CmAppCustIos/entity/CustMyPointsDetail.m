//
//  CustMyPointsDetail.m
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/24.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustMyPointsDetail.h"

@interface CustMyPointsDetail () <NSXMLParserDelegate>

@property (nonatomic, strong) void(^block)(NSArray *);
@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation CustMyPointsDetail

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        CustMyPointsDetail * curPoint = [[CustMyPointsDetail alloc] init];
        curPoint.sumPoints = [attributeDict objectForKey:@"sumPoints"];
        curPoint.usePoints = [attributeDict objectForKey:@"usePoints"];
        curPoint.supplierName = [attributeDict objectForKey:@"supplierName"];
        curPoint.supplierCode = [attributeDict objectForKey:@"supplierCode"];
        
        [self.array addObject:curPoint];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
