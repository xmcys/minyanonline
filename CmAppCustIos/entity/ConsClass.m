//
//  ConsClassFun.m
//  CmAppConsumerIos
//
//  Created by 宏超 陈 on 14-5-20.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsClass.h"

@interface ConsClass () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void(^block)(NSArray * array);

@end

@implementation ConsClass

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        ConsClass * cc = [[ConsClass alloc] init];
        cc.schoolId = [attributeDict objectForKey:@"schoolId"];
        cc.schoolTitle = [attributeDict objectForKey:@"schoolTitle"];
        cc.schoolCont = [attributeDict objectForKey:@"schoolCont"];
        cc.schoolImg01 = [attributeDict objectForKey:@"schoolImg01"];
        cc.schoolImg02 = [attributeDict objectForKey:@"schoolImg02"];
        cc.schoolImg03 = [attributeDict objectForKey:@"schoolImg03"];
        cc.schoolImg04 = [attributeDict objectForKey:@"schoolImg04"];
        cc.schoolImg05 = [attributeDict objectForKey:@"schoolImg05"];
        cc.likeNums = [attributeDict objectForKey:@"likeNums"];
        cc.publishTime = [attributeDict objectForKey:@"publishTime"];
        
        if (cc.publishTime != nil) {
            cc.publishTime = [cc.publishTime stringByReplacingOccurrencesOfString:@" 00:00:00" withString:@""];
        }
        
        [self.array addObject:cc];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
