//
//  CustDefaultBank.h
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/27.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustDefaultBank : NSObject

@property (nonatomic, strong) NSString * custBalance;  // 余额

- (void)parseData:(NSData *)data complete:(void(^)(CustDefaultBank *bank))block;

@end
