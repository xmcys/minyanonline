//
//  CustWebService.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustInfo.h"

#define SOAP_NAMESPACE @"http://webService.icsshs.com/"

#define CUST_WEBSERVICE_CHECKVERSION @"http://118.244.194.37:8080"
//#define CUST_WEBSERVICE_HOST @"http://59.57.247.68:9097"
//#define CUST_WEBSERVICE_FILE_HOST @"http://xm.fj-tobacco.com:18000/assets"
//#define CUST_WEBSERVICE_TEST_HOST @"http://10.188.182.125:9080"

static NSString * CUST_WEBSERVICE_HOST = @"";               //主机地址
static NSString * CUST_WEBSERVICE_ACTIVITY_HOST = @"";      //主机下载式活地址
static NSString * CUST_WEBSERVICE_FILE_HOST = @"";          //消息里面的附件下载地址
static NSString * APP_TYPE = @"3";                          // 2 正版 3 越狱

double UNREAD_MESSAGE;

@interface CustWebService : NSObject

//设置主机地址
+ (void)setWebServiceHost:(NSString *)userCode;
+ (NSString *)WebServiceActivityHost;
+ (NSInteger)getUserCodePrefix;

+ (void)setUser:(CustInfo *)info;
+ (CustInfo *)sharedUser;

// 文件具体路径
+ (NSString *)fileFullUrl:(NSString *)path;
// 模块使用记录
+ (NSString *)cmappLogInfoUrl:(NSString *)userId appType:(NSString *)appType version:(NSString *)version modCode:(NSString *)modCode operateId:(NSString *)operateId;
// 版本检查
+ (NSString *)cmappVersionCheckUrl;
// 客户登陆
+ (NSString *)custSignInUrl:(NSString *)userCode pwd:(NSString *)pwd;
// soap地址
+ (NSString *)soapUrl;
// 订货首页 - 客户信息
+ (NSString *)custOrderInfoUrl:(NSString *)userId;
// 订货首页 - 订单状态
+ (NSString *)custOrderStatusUrl:(NSString *)userId;
// 订货首页 - 消息公告
+ (NSString *)custOrderMessageListUrl:(NSString *)userId;
// 在线订货 - 日、月限量
+ (NSString *)custOrderLimitUrl:(NSString *)userId;
// 在线订货 - 订单规格列表
+ (NSString *)custOrderListUrl:(NSString *)userId;
// 在线订货 - 新品推荐列表
+ (NSString *)custOrderSuggestListUrl:(NSString *)userId;
// 在线订货 - 添加商品列表
+ (NSString *)custOrderAddListUrl;
// 在线订货 - 修改需求
+ (NSString *)custOrderChangeUrl;
// 在线订货 - 删除购物车
+ (NSString *)custOrderDeleteShoppingCarUrl;
// 在线订货 - 删除订单
+ (NSString *)custOrderDeleteUrl:(NSString *)userId orderId:(NSString *)orderId;
// 在线订货 - 保存订单
+ (NSString *)custOrderSaveUrl:(NSString *)userId;
// 在线订货 - 订单跟踪
+ (NSString *)queryOrderStatus:(NSString *)shopId userId:(NSString *)userId;
// 在线订货 - 获取订单进度
+ (NSString *)queryOrderLogistics:(NSString *)shopId userId:(NSString *)userId;
// 历史订单 - 合计
+ (NSString *)custOrderHistorySummaryUrl:(NSString *)userCode months:(int)num;
// 历史订单 - 列表
+ (NSString *)custOrderHistoryListUrl:(NSString *)userCode months:(int)num;
// 历史订单 - 明细列表
+ (NSString *)custOrderHistoryDetailListUrl:(NSString *)orderId;
// 本周策略 - 个人策略
+ (NSString *)qtyUpperLimit:(NSString *)userCode;
// 本周策略 - 全星级策略
+ (NSString *)custOrderTaticsUrl;
// 活动广场 - Banner广告
+ (NSString *)custActivityBannerUrl;
// 活动广场 - 营销活动列表
+ (NSString *)custActivityListUrl;
// 刮刮卡 - 获取刮奖次数
+ (NSString *)guaGuaKaGameTimesUrl:(NSString *)userId;
// 刮刮卡 - 获取刮奖结果
+ (NSString *)guaGuaKaGameResultUrl:(NSString *)userId userCode:(NSString *)userCode termType:(NSString *)termType;
// 刮刮卡 - 保存刮奖结果
+ (NSString *)guaGuaKaGameSaveUrl:(NSString *)userId userCode:(NSString *)userCode scratchId:(NSString *)scratchId giftId:(NSString *)giftId ranNum:(NSString *)ranNum;
// 刮刮卡 - 获取奖品列表
+ (NSString *)guaGuaKaMyPriceUrl:(NSString *)userId startPage:(int)startPage endPage:(int)endPage;
// 强制结算标识
+ (NSString *)getCountFlag:(NSString *)userCode;
// 查询用户订单金额,支付状态
+ (NSString *)getBankOrder:(NSString *)orderId;
// 查询用户银行信息
+ (NSString *)getBankBalance:(NSString *)userId;
// 实时交易
+ (NSString *)realTimeTran;
// 客户服务 - 获取广告  userType 0消费者 1零售户 
+ (NSString *)mobileConsumerNew:(NSString *)userType position:(NSString *)position;
// 客户服务 - 获取客户经理代码
+ (NSString *)queryCustMgr:(NSString *)userCode;
// 客户服务 - 服务评价
+ (NSString *)addCustEvel:(NSString *)userId mgrScore:(NSString *)mgrScore deptScore:(NSString *)deptScore deliveryScore:(NSString *)deliveryScore;
// 服务受理 - 服务受理填报
+ (NSString *)addServiceInfo;
// 服务受理 - 服务受理跟踪数量总和
+ (NSString *)getServiceCount:(NSString *)userCode;
// 服务受理 - 查询服务受理跟踪里面的信息    status 1未处理 2处理中 3待评价
+ (NSString *)getServiceInfo:(NSString *)userCode status:(NSString *)status;
// 服务受理 - 查询服务受理详情信息
+ (NSString *)getServiceDetailInfo:(NSString *)demandId;
// 服务受理 - 更新服务受理信息
+ (NSString *)updateServiceInfo;
// 服务受理 - 提交服务评价信息
+ (NSString *)commitServiceInfo;
// 服务受理 - 删除服务受理信息
+ (NSString *)deleteServiceInfo;
// 服务受理 - 查看全部受理信息
+ (NSString *)getServiceAllInfo:(NSString *)userCode;
// 在线订货 - 余额查询（列表）
+ (NSString *)custBalanceListUrl:(NSString *)userCode;
// 在线订货 - 余额查询（单张）
+ (NSString *)custBalanceUrl:(NSString *)userId cardNo:(NSString *)cardNo;
// 在线订货 － 余额查询（返回默认卡）
+ (NSString *)custDefaultBalanceUrl;
// 侧边栏 - 广告
+ (NSString *)consAdUrl;
// 侧边栏 - 我的兑换
+ (NSString *)myExchangeUrl;
// 设置 - 修改密码
+ (NSString *)savePasswordUrl;
// 设置 - 意见反馈
+ (NSString *)submitFeedbackUrl;
// 设置 - 意见反馈 - 模块列表
+ (NSString *)consModuleListUrl;
// 消息 - 检查未读消息
+ (NSString *)checkUnreadMessage;
// 消息 - 消息列表
+ (NSString *)messageListUrl;
// 消息 - 消息详情
+ (NSString *)messageDetailUrl:(NSString *)msgId;
// 消息 - 更新已读消息
+ (NSString *)messageUpdateUrl;
// 侧边栏 - 帐号信息
+ (NSString *)custAccountInfoUrl;
// 店铺助手 - 获取本周新增关注
+ (NSString *)getMyConsumerThisWeek:(NSString *)userCode;
// 店铺助手 - 店铺信息数据读取
+ (NSString *)getReadStoreInfo:(NSString *)userId;
// 店铺助手 - 店铺信息数据保存
+ (NSString *)getSaveStoreInfo;
// 店铺助手 - 送货服务内容获取
+ (NSString *)getDeliveryContent;
// 店铺助手 - 送货服务内容保存
+ (NSString *)getSaveDeliveryContent;
// 查询公告信息和查询公告信息
+ (NSString *)queryUsMsgList:(NSString *)msg;
// 获取目标公告信息
+ (NSString *)getUsMsgListById:(NSString *)msgId publishman:(NSString *)publishman;
// 新增公告
+ (NSString *)saveUsMsgList;
// 删除公告
+ (NSString *)deleteUsMsgList:(NSString *)msgId;
// 更新公告信息
+ (NSString *)updateUsMsgList;
// 店铺助手 - 搜索目标消费者资料
+ (NSString *)getCustInfo;
// 店铺助手 - 目标消费者详细信息
+ (NSString *)getMyConsumerInfoDetail:(NSString *)vipId;
// 店铺助手 - 显示【消费者资料】界面的顾客的订单汇总信息
+ (NSString *)getOrderId:(NSString *)userId;
// 店铺助手 - 消费记录查询 - 订单汇总信息
+ (NSString *)getTbcOrderCountDetail:(NSString *)vipId userCode:(NSString *)userCode;
// 店铺助手 - 消费记录查询 - 总订单量,总金额
+ (NSString *)getTbcOrderCount:(NSString *)vipId userCode:(NSString *)userCode;
// 店铺助手 - 销售信息获取下拉框数据
+ (NSString *)getMyDict;
// 店铺助手 - 最新销量金额、经营毛利、最新库存
+ (NSString *)getCustBiView:(NSString *)userCode dateType:(NSString *)dateType;
// 店铺助手 - 最新销售金额排行  productSort 0非烟 1卷烟
+ (NSString *)getCustBiTopView:(NSString *)userCode dateType:(NSString *)dateType productSort:(NSString *)productSort;
// 店铺助手 - 当前系统时间最新库存信息
+ (NSString *)getShopMyConsumerInfo;
// 销售信息url
+ (NSString *)getSalesOrderListUrl:(NSString *)userCode vm:(NSString *)vm;
// 条码搜出销售信息url
+ (NSString *)getBarCodeSalesOrderListUrl;
// 总积分
+ (NSString *)getTotalPointsUrl;
// 积分明细
+ (NSString *)getPointsDetailUrl;
// 积分记录明晰
+ (NSString *)getPointRecordsDetailUrl:(NSString *)supplierCode;
// 品牌百科 - 品牌列表
+ (NSString *)supplierTradeMarkListUrl:(NSString *)supplierCode;
// 资讯中心 - 零售户讲堂
+ (NSString *)classFunListUrl;
// 资讯中心 - 经营指导
+ (NSString *)classDisinguishListUrl;
// 资讯中心 - 品牌百科
+ (NSString *)supplierListUrl;
// 品牌百科 - 卷烟价格
+ (NSString *)brandPriceUrl;
// 品牌百科 - 卷烟排序
+ (NSString *)brandSortUrl;
// 品牌百科 - 卷烟类型
+ (NSString *)brandTypeUrl;
// 品牌百科 - 工业品牌规格
+ (NSString *)tradeMarkBrandListUrl:(NSString *)tmId;
// 品牌百科 - 规格过滤
+ (NSString *)brandListUrl:(NSString *)cigarFlag price:(NSString *)price orderByFlag:(NSString *)orderByFlag;
// 品牌百科 - 规格名称过滤
+ (NSString *)brandListByNameUrl;
// 卷烟详情 - 活动信息
+ (NSString *)brandReInfoUrl:(NSString *)brandId;
// 卷烟详情 - 卷烟详情
+ (NSString *)brandDetailUrl:(NSString *)brandId;
// 获取所有礼品 带查询条件
+ (NSString *)getAllGiftPostUrl;
+ (NSString *)getAllGiftPostParams:(NSString *)supplierCode pointStart:(NSString *)pointStart pointEnd:(NSString *)pointEnd;
// 卷烟详情 - 评价提交
+ (NSString *)brandEvaSaveUrl;
// 卷烟详情 - 评价列表
+ (NSString *)brandEvaListUrl:(NSString *)custLicenceCode;
// 卷烟详情 - 评价统计
+ (NSString *)brandEvaSummaryUrl:(NSString *)custLicenceCode;
// 兑换礼品
+ (NSString *)addGiftChangeUrl;
// 兑换明细
+ (NSString *)getChangeDetailUrl;
// 资讯中心的前几个tab页。  0烟趣1真伪鉴别2零售户讲坛3经营指导
+ (NSString *)getInfoTypeUrl:(int)typeCode;
// 星级
+ (NSString *)getCustLevelUrl;

@end
