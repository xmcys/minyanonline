//
//  CustDict.h
//  CmAppCustIos
//
//  Created by hsit on 15-4-2.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustDict : NSObject

@property (nonatomic, strong) NSString *dictCode;
@property (nonatomic, strong) NSString *dictName;

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;

@end
