//
//  RYKCustOrder.h
//  RYKForCustIos
//
//  Created by hsit on 14-7-2.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustCustOrder : NSObject <NSXMLParserDelegate>

@property (nonatomic, strong) NSString *orderNum;  //总订单量
@property (nonatomic, strong) NSString *amt;   //订单金额
@property (nonatomic ,strong) void (^block)(CustCustOrder *msg);

- (void)parseData:(NSData *)data complete:(void(^)(CustCustOrder *msg))block;

@end
