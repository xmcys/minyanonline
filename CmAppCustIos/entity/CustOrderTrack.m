//
//  CustOrderTrack.m
//  CmAppCustIos
//
//  Created by hsit on 15-3-18.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustOrderTrack.h"

@implementation CustOrderTrack

- (void)parseData:(NSData *)data complete:(void (^)(CustOrderTrack *))block
{
    self.block = block;
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"items"]) {
        self.statusName = [attributeDict objectForKey:@"statusName"];
        self.balModeName = [attributeDict objectForKey:@"balModeName"];
        self.balStatus = [attributeDict objectForKey:@"balStatus"];
        self.balStatusName = [attributeDict objectForKey:@"balStatusName"];
        self.amtOrderSum = [attributeDict objectForKey:@"amtOrderSum"];
        self.qtyDemandSum = [attributeDict objectForKey:@"qtyDemandSum"];
        self.qtyOrderSum = [attributeDict objectForKey:@"qtyOrderSum"];
        self.orderDate = [attributeDict objectForKey:@"orderDate"];
        self.orderId = [attributeDict objectForKey:@"orderId"];
        self.flag = [attributeDict objectForKey:@"flag"];
    }
}

-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self);
}


@end
