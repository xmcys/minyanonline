//
//  RYKSaleOrder.h
//  RYKForCustIos
//
//  Created by 张斌 on 14-7-3.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustSaleOrder : NSObject

@property (nonatomic, strong) NSString * orderId;         // 订单id
@property (nonatomic, strong) NSString * orderDate;       // 订单日期
@property (nonatomic, strong) NSString * qtyOrderSum;     // 总数
@property (nonatomic, strong) NSString * amtOrderSum;     // 总额
@property (nonatomic, strong) NSString * contactName;     // 姓名
@property (nonatomic, strong) NSString * brandName;      // 商品名
@property (nonatomic, strong) NSString * brandUnit;       // 单位
@property (nonatomic, strong) NSString * brandCnt;        // 数量
@property (nonatomic, strong) NSString * price;           // 单价
@property (nonatomic, strong) NSString * leftBrandCnt;    // 剩余x件商品

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *))block;

@end
