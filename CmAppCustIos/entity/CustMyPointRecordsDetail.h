//
//  CustMyPointRecordsDetail.h
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/24.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustMyPointRecordsDetail : NSObject

@property (nonatomic, strong) NSString * pointDate;    // 积分日期
@property (nonatomic, strong) NSString * addPoint;     // 积分分数
@property (nonatomic, strong) NSString * pointDesc;    // 备注字段

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *))block;

@end
