//
//  CustWebService.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustWebService.h"

static CustInfo * USER; // 当前用户

@implementation CustWebService

+ (void)setWebServiceHost:(NSString *)userCode
{
    int cityCode = [[userCode substringToIndex:4] intValue];
    switch (cityCode) {
        case 3501://福州
           CUST_WEBSERVICE_HOST = @"";
            CUST_WEBSERVICE_FILE_HOST = @"";
            CUST_WEBSERVICE_ACTIVITY_HOST = @"";
            break;
            
        case 3502://厦门
            CUST_WEBSERVICE_HOST = @"http://59.57.247.68:9097";
            CUST_WEBSERVICE_FILE_HOST = @"http://www.xmtobacco.com:18000/assets";
            CUST_WEBSERVICE_ACTIVITY_HOST = @"59.57.247.68/ussWebservice";
            break;
            
        case 3503://莆田
            CUST_WEBSERVICE_HOST = @"http://59.58.128.60:9098";
            CUST_WEBSERVICE_FILE_HOST = @"http://www.fjptycw.com:9080/assets";
            CUST_WEBSERVICE_ACTIVITY_HOST = @"59.58.128.60/ussWebservice";
            break;
            
        case 3504://三明
            CUST_WEBSERVICE_HOST = @"";
            CUST_WEBSERVICE_FILE_HOST = @"";
            CUST_WEBSERVICE_ACTIVITY_HOST = @"";
            break;
            
        case 3505://泉州
            CUST_WEBSERVICE_HOST = @"";
            CUST_WEBSERVICE_FILE_HOST = @"";
            CUST_WEBSERVICE_ACTIVITY_HOST = @"";
            break;
           
        case 3506://漳州
            CUST_WEBSERVICE_HOST = @"";
            CUST_WEBSERVICE_FILE_HOST = @"";
            CUST_WEBSERVICE_ACTIVITY_HOST = @"";
            break;
            
        case 3507://南平
            CUST_WEBSERVICE_HOST = @"http://10.188.156.8:9098";
//            CUST_WEBSERVICE_HOST = @"http://220.160.203.26:9098";
            CUST_WEBSERVICE_FILE_HOST = @"http://www.npycw.com.cn:18000/assets";
            CUST_WEBSERVICE_ACTIVITY_HOST = @"220.160.203.26/ussWebservice";
            break;
            
        case 3508://龙岩
            CUST_WEBSERVICE_HOST = @"";
            CUST_WEBSERVICE_FILE_HOST = @"";
            CUST_WEBSERVICE_ACTIVITY_HOST = @"";
            break;
            
        case 3509://宁德
            CUST_WEBSERVICE_HOST = @"http://www.ndyc.cn:9089";
            CUST_WEBSERVICE_FILE_HOST = @"";
            CUST_WEBSERVICE_ACTIVITY_HOST = @"";
            break;
            
        default:
            CUST_WEBSERVICE_HOST = @"";
            CUST_WEBSERVICE_FILE_HOST = @"";
            CUST_WEBSERVICE_ACTIVITY_HOST = @"";
            break;
    }
}

+ (NSString *)WebServiceActivityHost
{
    return CUST_WEBSERVICE_ACTIVITY_HOST;
}

+ (NSInteger)getUserCodePrefix
{
    NSInteger cityCode = [[USER.userCode substringToIndex:4] integerValue];
    return cityCode;
}

+ (void)setUser:(CustInfo *)info
{
    USER = info;
}

+ (CustInfo *)sharedUser
{
    return USER;
}

+ (NSString *)fileFullUrl:(NSString *)path
{
    NSRange range = [path rangeOfString:@"Resource/"];
    if (range.location == NSNotFound) {
        range = [path rangeOfString:@"resource/"];
    }
    if (range.location == NSNotFound) {
        return @"";
    }
    NSString * name = [path substringFromIndex:range.location + range.length];
    name = [name stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [CUST_WEBSERVICE_FILE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@", [path substringToIndex:range.location + range.length], name]];
}

+ (NSString *)cmappVersionCheckUrl
{
    return [CUST_WEBSERVICE_CHECKVERSION stringByAppendingPathComponent:[NSString stringWithFormat:@"/app/appVerMgr.do?f=checkVer&appName=MYONLINE&appType=%@", APP_TYPE]];
}

+ (NSString *)cmappLogInfoUrl:(NSString *)userId appType:(NSString *)appType version:(NSString *)version modCode:(NSString *)modCode operateId:(NSString *)operateId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/moblieConsumer/cmappAppLogInfo/userId/%@/appType/%@/versionNo/%@/modCode/%@/operateId/%@", userId, appType, version, modCode, operateId]];
}

+ (NSString *)custSignInUrl:(NSString *)userCode pwd:(NSString *)pwd
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/moblieConsumer/getMobileCheckIn/userCode/%@/pwd/%@", userCode, pwd]];
}

// soap地址
+ (NSString *)soapUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:@"/MCP_Flex/webServices/IMsgCenterWS"];
}


+ (NSString *)custOrderInfoUrl:(NSString *)userId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/moblieConsumer/queryCustInfoApp/userId/%@", userId]];
}

+ (NSString *)custOrderStatusUrl:(NSString *)userId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/moblieConsumer/queryOrderInfoApp/userId/%@", userId]];
}

+ (NSString *)custOrderMessageListUrl:(NSString *)userId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/inteOrder/ws/rs/basic/allMsgList/userId/%@/publishTime/1", userId]];
}

+ (NSString *)custOrderLimitUrl:(NSString *)userId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/inteOrder/ws/rs/order/queryCustLmt/userId/%@", userId]];
}

+ (NSString *)custOrderListUrl:(NSString *)userId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/moblieConsumer/queryShoppingCartApp/userId/%@", userId]];
}

+ (NSString *)custOrderSuggestListUrl:(NSString *)userId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/moblieConsumer/queryNewBrandApp/userId/%@", userId]];
}

+ (NSString *)custOrderAddListUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/moblieConsumer/queryBrandListApp"];
}

+ (NSString *)custOrderChangeUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/inteOrder/ws/rs/order/addShoppingCart"];
}

+ (NSString *)custOrderDeleteShoppingCarUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/inteOrder/ws/rs/order/delShoppingCart"];
}

+ (NSString *)custOrderDeleteUrl:(NSString *)userId orderId:(NSString *)orderId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/inteOrder/ws/rs/order/delToTemp/userId/%@/orderId/%@", userId, orderId]];
}

+ (NSString *)queryOrderStatus:(NSString *)shopId userId:(NSString *)userId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/inteOrder/ws/rs/order/queryOrderStatus/shopId/%@/userId/%@", shopId, userId]];
}

+ (NSString *)queryOrderLogistics:(NSString *)shopId userId:(NSString *)userId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/inteOrder/ws/rs/order/queryOrderLogistics/shopId/%@/userId/%@", shopId, userId]];
}

+ (NSString *)custOrderSaveUrl:(NSString *)userId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/inteOrder/ws/rs/order/insertToTemp/shopId/12010036/userId/%@", userId]];
}

+ (NSString *)custOrderHistorySummaryUrl:(NSString *)userCode months:(int)num
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/moblieConsumer/queryBrandListApp/userCode/%@/num/%d", userCode, num]];
}

+ (NSString *)custOrderHistoryListUrl:(NSString *)userCode months:(int)num
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/moblieConsumer/queryOrderSumDetailApp/userCode/%@/num/%d", userCode, num]];
}

+ (NSString *)custOrderHistoryDetailListUrl:(NSString *)orderId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/moblieConsumer/queryOrderIdSumDetailApp/orderId/%@", orderId]];
}

+ (NSString *)qtyUpperLimit:(NSString *)userCode
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/marketCust/qtyUpperLimit/userCode/%@", userCode]];
}

+ (NSString *)custOrderTaticsUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/moblieConsumer/getExcel/developId/weekTactics"];
}

+ (NSString *)custActivityBannerUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/moblieConsumer/queryPropagandaInfo"];
}

+ (NSString *)custActivityListUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"ussWebservice/ws/rs/moblieConsumer/queryCustInfoApp/appType/%@", APP_TYPE]];
}

+ (NSString *)guaGuaKaGameTimesUrl:(NSString *)userId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/xmSaleAct/initQtyChange/userId/%@", userId]];
}

+ (NSString *)guaGuaKaGameResultUrl:(NSString *)userId userCode:(NSString *)userCode termType:(NSString *)termType
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/xmSaleAct/actScratchRanUsPrize/userId/%@/userCode/%@/termType/%@", userId, userCode, termType]];
}

+ (NSString *)guaGuaKaGameSaveUrl:(NSString *)userId userCode:(NSString *)userCode scratchId:(NSString *)scratchId giftId:(NSString *)giftId ranNum:(NSString *)ranNum
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/xmSaleAct/saveUserScratchData/userId/%@/userCode/%@/scratchId/%@/giftId/%@/ranNum/%@", userId, userCode, scratchId, giftId, ranNum]];
}

+ (NSString *)guaGuaKaMyPriceUrl:(NSString *)userId startPage:(int)startPage endPage:(int)endPage
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/xmSaleAct/queryCustActScratch/userId/%@/startPage/%d/endPage/%d", userId, startPage, endPage]];
}

+ (NSString *)getCountFlag:(NSString *)userCode
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/bankServer/getCountFlag/userCode/%@", userCode]];
}

+ (NSString *)getBankOrder:(NSString *)orderId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/inteOrder/ws/rs/order/getBankOrder/shopId/12010036/orderId/%@", orderId]];
}

+ (NSString *)getBankBalance:(NSString *)userId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/inteOrder/ws/rs/order/XsmCo/getBankBalance/userId/%@", userId]];
}

+ (NSString *)realTimeTran
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/inteOrder/ws/rs/order/realTimeTran"]];
}

+ (NSString *)mobileConsumerNew:(NSString *)userType position:(NSString *)position
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/mobileConsumerNew/getPropagandaInfo/addType/1/userType/%@/position/%@", userType, position]];
}

+ (NSString *)queryCustMgr:(NSString *)userCode
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/custForum/queryCustMgr/userCode/%@", userCode]];
}

+ (NSString *)addCustEvel:(NSString *)userId mgrScore:(NSString *)mgrScore deptScore:(NSString *)deptScore deliveryScore:(NSString *)deliveryScore
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/custForum/addCustEvel/userCode/%@/mgrScore/%@/deptScore/%@/deliveryScore/%@", userId, mgrScore, deptScore, deliveryScore]];
}

+ (NSString *)addServiceInfo
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/serviceAccepted/addServiceInfo"];
}

+ (NSString *)getServiceCount:(NSString *)userCode
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/serviceAccepted/getServiceCount/licence/%@", userCode]];
}

+ (NSString *)getServiceInfo:(NSString *)userCode status:(NSString *)status
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/serviceAccepted/getServiceInfo/licence/%@/status/%@", userCode, status]];
}

+ (NSString *)getServiceDetailInfo:(NSString *)demandId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/serviceAccepted/getServiceDetailInfo/demandId/%@", demandId]];
}

+ (NSString *)updateServiceInfo
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/serviceAccepted/updateServiceInfo"];
}

+ (NSString *)commitServiceInfo
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/serviceAccepted/commitServiceInfo"];
}

+ (NSString *)deleteServiceInfo
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/serviceAccepted/deleteServiceInfo"];
}

+ (NSString *)getServiceAllInfo:(NSString *)userCode
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/serviceAccepted/getServiceAllInfo/licence/%@", userCode]];
}

+ (NSString *)custBalanceListUrl:(NSString *)userCode
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/mobileConsumerNew/getCustBankInfo/userCode/%@", userCode]];
}

+ (NSString *)custBalanceUrl:(NSString *)userId cardNo:(NSString *)cardNo
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/inteOrder/ws/rs/order/getBankBalance/userId/%@/cardNo/%@", userId, cardNo]];
}

// 在线订货 － 余额查询（返回默认卡）
+ (NSString *)custDefaultBalanceUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/inteOrder/ws/rs/order/getCustBalance/userId/%@", USER.userId]];
}

+ (NSString *)consAdUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/mobileConsumerNew/getAdvertData/appType/1"];
}

+ (NSString *)myExchangeUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/marketPoint/queryHisChange/userId/%@", [CustWebService sharedUser].userId]];
}
+ (NSString *)savePasswordUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/mobileConsumerNew/saveNewPassword"];
}
+ (NSString *)submitFeedbackUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/moblieConsumer/addFeedbackContent"];
}
+ (NSString *)consModuleListUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/mobileConsumerNew/getModuleList"];
}
+ (NSString *)checkUnreadMessage
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/mobileConsumerNew/getUnreadMessages/userId/%@", [CustWebService sharedUser].userId]];
}

+ (NSString *)messageListUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/mobileConsumerNew/getMessageList/userId/%@", [CustWebService sharedUser].userId]];
}

+ (NSString *)messageDetailUrl:(NSString *)msgId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/mobileConsumerNew/getDetailMessage/userId/%@/msgId/%@", [CustWebService sharedUser].userId, msgId]];
}

+ (NSString *)messageUpdateUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/mobileConsumerNew/updateReadingStatus"];
}

+ (NSString *)custAccountInfoUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/custForum/getCustomer/userCode/%@", [CustWebService sharedUser].userCode]];
}

+ (NSString *)getMyConsumerThisWeek:(NSString *)userCode
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/custShopAssistant/getMyConsumerThisWeek/userCode/%@", userCode]];
}

+ (NSString *)getReadStoreInfo:(NSString *)userId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/custShopAssistant/getUsStoreNotice/userId/%@", userId]];
}

+ (NSString *)getSaveStoreInfo
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/custShopAssistant/saveUsStoreNotice"]];
}

+ (NSString *)getDeliveryContent
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/custShopAssistant/getUsStoreNotice/userId/%@",USER.userId]];
}

+ (NSString *)getSaveDeliveryContent
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/custShopAssistant/saveDelvIntro"]];
}

+ (NSString *)getUsMsgListById:(NSString *)msgId publishman:(NSString *)publishman
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/usMsgList/getUsMsgListById?msgId=%@&publishMan=%@",msgId, USER.userId]];
}

+ (NSString *)queryUsMsgList:(NSString *)msg
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/usMsgList/queryUsMsgList?msg=%@&publishMan=%@",msg,USER.userId]];
}

+ (NSString *)saveUsMsgList
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:@"/custMobile/ws/rs/usMsgList/saveUsMsgList"];
}

// 删除公告
+ (NSString *)deleteUsMsgList:(NSString *)msgId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/usMsgList/deleteUsMsgList?msgId=%@", msgId]];
}

+ (NSString *)updateUsMsgList
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:@"/custMobile/ws/rs/usMsgList/updateUsMsgList"];
}

+ (NSString *)getCustInfo
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:@"/ussWebservice/ws/rs/custShopAssistant/getMyConsumerInfo"];
}

+ (NSString *)getMyConsumerInfoDetail:(NSString *)vipId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/custShopAssistant/getMyConsumerInfoDetail/vipId/%@/userCode/%@",vipId , USER.userCode]];
}

+ (NSString *)getOrderId:(NSString *)userId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/CustInfo/getConsumerInfoOrder/vipUserId/%@",userId]];
}

+ (NSString *)getTbcOrderCountDetail:(NSString *)vipId userCode:(NSString *)userCode;
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/custShopAssistant/getTbcOrderCountDetail/vipId/%@/userCode/%@", vipId, userCode]];
}

+ (NSString *)getTbcOrderCount:(NSString *)vipId userCode:(NSString *)userCode
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/custShopAssistant/getTbcOrderCount/vipId/%@/userCode/%@", vipId, userCode]];
}

+ (NSString *)getMyDict {
    return [CUST_WEBSERVICE_HOST stringByAppendingString:@"/ussWebservice/ws/rs/custShopAssistant/ussWebservice/ws/rs/custShopAssistant/getMyDict"];
}

+ (NSString *)getCustBiView:(NSString *)userCode dateType:(NSString *)dateType {
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/custShopAssistant/getCustBiView/userCode/%@/dateType/%@", userCode, dateType]];
}

+ (NSString *)getCustBiTopView:(NSString *)userCode dateType:(NSString *)dateType productSort:(NSString *)productSort {
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/custShopAssistant/getCustBiTopView/userCode/%@/dateType/%@/productSort/%@", userCode, dateType, productSort]];
}

+ (NSString *)getShopMyConsumerInfo {
    return [CUST_WEBSERVICE_HOST stringByAppendingString:@"/ussWebservice/ws/rs/custShopAssistant/getMyConsumerInfo"];
}

// 销售信息url
+ (NSString *)getSalesOrderListUrl:(NSString *)userCode vm:(NSString *)vm
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/custShopAssistant/getTbcOrderCountDetail_M/userCode/%@/vm/%@", userCode, vm]];
}

// 条码搜出销售信息url
+ (NSString *)getBarCodeSalesOrderListUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/custMobile/ws/rs/salesInfo/getTotalSum"]];
}

// 总积分
+ (NSString *)getTotalPointsUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/retailer/getTotalPoints/custLicenceCode/%@", USER.userCode]];
}

// 积分明细
+ (NSString *)getPointsDetailUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/retailer/getPointsDetail/custLicenceCode/%@", USER.userCode]];
}

// 积分记录明晰
+ (NSString *)getPointRecordsDetailUrl:(NSString *)supplierCode
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/retailer/getPointRecordsDetail/custLicenceCode/%@/supplierCode/%@", USER.userCode, supplierCode]];
}

+ (NSString *)supplierTradeMarkListUrl:(NSString *)supplierCode
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/moblieConsumer/queryBrandCulture/id/%@", supplierCode]];
}
+ (NSString *)classFunListUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/tobaccoSchool/getSchoolContent"];
}

+ (NSString *)classDisinguishListUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/tobaccoSchool/getSchoolIdentify"];
}
+ (NSString *)supplierListUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/tobaccoSchool/getTobaccoDetails"];
}
+ (NSString *)brandPriceUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/mobileConsumerNew/getPriceRange"];
}

+ (NSString *)brandSortUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/mobileConsumerNew/getSortRule"];
}
+ (NSString *)brandTypeUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/mobileConsumerNew/getCigaretteClassification"];
}
+ (NSString *)tradeMarkBrandListUrl:(NSString *)tmId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/moblieConsumer/queryBrand/id/%@", tmId]];
}
+ (NSString *)brandListUrl:(NSString *)cigarFlag price:(NSString *)price orderByFlag:(NSString *)orderByFlag
{
    NSString * sPrice = [price substringToIndex:[price rangeOfString:@"-"].location];
    NSString * ePrice = [price substringFromIndex:[price rangeOfString:@"-"].location + 1];
    
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/mobileConsumerNew/getCigaretteList/cigarFlag/%@/startPrice/%@/endPrice/%@/orderByFlag/%@", cigarFlag, sPrice, ePrice, orderByFlag]];
}
+ (NSString *)brandListByNameUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/mobileConsumerNew/getBasicInfoFuzzy"];
}
+ (NSString *)brandReInfoUrl:(NSString *)brandId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/mobileConsumerNew/getRecommendInfo/brandId/%@", brandId]];
}
+ (NSString *)brandDetailUrl:(NSString *)brandId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/mobileConsumerNew/getBasicInfoAndBrandCulture/brandId/%@", brandId]];
}
// 获取所有礼品 带查询条件
+ (NSString *)getAllGiftPostUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/retailer/getAllGiftPost"]];
}

+ (NSString *)getAllGiftPostParams:(NSString *)supplierCode pointStart:(NSString *)pointStart pointEnd:(NSString *)pointEnd
{
    return [NSString stringWithFormat:@"<IntegralChange><supplierCode>%@</supplierCode><changePointStart>%@</changePointStart><changePointEnd>%@</changePointEnd></IntegralChange>", supplierCode, pointStart, pointEnd];
}
+ (NSString *)brandEvaSummaryUrl:(NSString *)brandId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/mobileConsumerNew/getRetailerInfo/brandId/%@", brandId]];
}

+ (NSString *)brandEvaListUrl:(NSString *)brandId
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/mobileConsumerNew/getCommentInfo/brandId/%@", brandId]];
}

+ (NSString *)brandEvaSaveUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:@"/ussWebservice/ws/rs/mobileConsumerNew/saveCommentsInfo"];
}
// 兑换礼品
+ (NSString *)addGiftChangeUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/retailer/addGiftChange"]];
}

// 兑换明细
+ (NSString *)getChangeDetailUrl
{
    return [CUST_WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/ussWebservice/ws/rs/retailer/getChangeDetail/custLicenceCode/%@", USER.userCode]];
}

+ (NSString *)getInfoTypeUrl:(int )typeCode
{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/ws/rs/tobaccoSchool/getSchoolForType/schoolType/%d", typeCode]];
}

+ (NSString *)getCustLevelUrl{
    return [CUST_WEBSERVICE_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/ussWebservice/pages/khfwStarLevel.html?userCode=%@", USER.userCode]];
}

@end
