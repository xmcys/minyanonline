//
//  RYKListRecords.m
//  RYKForCustIos
//
//  Created by hsit on 14-7-2.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CustListRecords.h"

@implementation CustListRecords

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *))block;
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
     if ([elementName isEqualToString:@"item"]) {
        CustListRecords *item = [[CustListRecords alloc] init];
        item.brandName = [attributeDict objectForKey:@"brandName"];
        item.brandUnit = [attributeDict objectForKey:@"brandUnit"];
        item.price = [attributeDict objectForKey:@"price"];
        item.qtyOrder = [attributeDict objectForKey:@"qtyOrder"];
        item.amt = [attributeDict objectForKey:@"amt"];
        item.contactName = [attributeDict objectForKey:@"contactName"];
        item.receiveAddr = [attributeDict objectForKey:@"receiveAddr"];
        item.mobilePhone = [attributeDict objectForKey:@"mobilePhone"];
        item.orderDesc = [attributeDict objectForKey:@"orderDesc"];
         item.orderDate = [attributeDict objectForKey:@"orderDate"];
         [self.array addObject:item];
     }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}



@end
