//
//  RYKDeliveryContent.h
//  RYKForCustIos
//
//  Created by hsit on 14-6-27.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustDeliveryContent : NSObject<NSXMLParserDelegate>
@property (nonatomic, strong) NSString * delvIntro;         //送货信息
@property (nonatomic, strong) void(^block)(CustDeliveryContent *);

- (void)parseData:(NSData *)data complete:(void(^)(CustDeliveryContent *))block;


@end
