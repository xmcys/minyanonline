//
//  CustOrder.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-21.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustOrder.h"

@interface CustOrder () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) NSString * tmp;

@end

@implementation CustOrder

- (void)parseData:(NSData *)data
{
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark - 
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        CustOrder * order = [[CustOrder alloc] init];
        order.orderId = [attributeDict objectForKey:@"orderId"];
        order.shoppingId = [attributeDict objectForKey:@"shoppingId"];
        order.brandId = [attributeDict objectForKey:@"brandId"];
        order.brandName = [[attributeDict objectForKey:@"brandName"] stringByReplacingOccurrencesOfString:@"★" withString:@""];
        order.demand = [attributeDict objectForKey:@"demandQty"];
        order.order = [attributeDict objectForKey:@"orderQty"];
        if ([attributeDict objectForKey:@"qtyOrder"] != nil) {
            order.order = [attributeDict objectForKey:@"qtyOrder"];
        }
        order.cash = [attributeDict objectForKey:@"orderAmt"];
        if ([attributeDict objectForKey:@"orderSumPrice"] != nil) {
            order.cash = [attributeDict objectForKey:@"orderSumPrice"];
        }
        order.tradePrice = [attributeDict objectForKey:@"tradePrice"];
        order.retailPrice = [attributeDict objectForKey:@"retailPrice"];
        order.qtyTar = [attributeDict objectForKey:@"qtyTar"];
        order.totalScore = [attributeDict objectForKey:@"tTotalScore"];
        order.qtyOrderBrands = [attributeDict objectForKey:@"qtyOrderBrands"];
        order.norFlag = [[attributeDict objectForKey:@"norFlag"] integerValue];
        order.newFlag = [[attributeDict objectForKey:@"newFlag"] integerValue];
        order.selectedFlag = [[attributeDict objectForKey:@"selectFlag"] integerValue];
        order.brandLimit = [attributeDict objectForKey:@"brandLimit"];
        order.dayLimit = [attributeDict objectForKey:@"dayLimit"];
        order.qtyMonthLimit = [attributeDict objectForKey:@"qtyMonthLimit"];
        order.qtyRemainMonthLimit = [attributeDict objectForKey:@"qtyRemainMonthLimit"];
        order.orderDate = [attributeDict objectForKey:@"orderDate"];
        order.orderSum = [[attributeDict objectForKey:@"qtyOrderSum"] floatValue];
        order.cashSum = [[attributeDict objectForKey:@"amtOrderSum"] floatValue];
        if ([attributeDict objectForKey:@"exFlag"] != nil) {
            order.norFlag = [[attributeDict objectForKey:@"exFlag"] integerValue];
        }
        if ([attributeDict objectForKey:@"orderNum"] != nil) {
            order.order = [attributeDict objectForKey:@"orderNum"];
        }
        if (order.selectedFlag == 1) {
            order.added = YES;
        }
        if (order.newFlag == 1) {
            order.added = YES;
        }
        if (order.norFlag == 1) {
            order.brandName = [order.brandName stringByAppendingString:@" [异]"];
        }
        self.demandSum += [order.demand floatValue];
        if (order.norFlag == 1) {
            self.norOrderSum += [order.order floatValue];
        } else {
            self.orderSum += [order.order floatValue];
        }
        self.cashSum += [order.cash floatValue];
        [self.array addObject:order];
    } else if ([elementName isEqualToString:@"items"]){
        self.orderId = [attributeDict objectForKey:@"orderId"];
        self.code= [attributeDict objectForKey:@"code"];
        self.msg = [attributeDict objectForKey:@"msg"];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    self.tmp = string;
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"brandId"]) {
        self.brandId = self.tmp;
    } else if ([elementName isEqualToString:@"brandName"]) {
        self.brandName = self.tmp;
    } else if ([elementName isEqualToString:@"count"]) {
        
    } else if ([elementName isEqualToString:@"demandQty"]) {
        self.demand = self.tmp;
    } else if ([elementName isEqualToString:@"gift"]) {
        
    } else if ([elementName isEqualToString:@"orderAmt"]) {
        self.cash = self.tmp;
    } else if ([elementName isEqualToString:@"orderQty"]) {
        self.order = self.tmp;
    } else if ([elementName isEqualToString:@"tradePrice"]) {
        self.tradePrice = self.tmp;
    } else if ([elementName isEqualToString:@"unitName"]) {
        
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    if ([self.delegate respondsToSelector:@selector(custOrder:didFinishingParse:)]) {
        [self.delegate custOrder:self didFinishingParse:self.array];
    }
}

@end
