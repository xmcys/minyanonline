//
//  CustBankList.m
//  CmAppCustIos
//
//  Created by hsit on 14-7-31.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustBankList.h"

@interface CustBankList () <NSXMLParserDelegate>

@property (nonatomic, strong) NSString * tmp;
@property (nonatomic, strong) NSMutableArray * array;

@end

@implementation CustBankList

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"BankBalance"]) {
        CustBankList *bankitem = [[CustBankList alloc] init];
        [self.array addObject:bankitem];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    self.tmp = string;
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if (self.array.count <= 0) {
        return;
    }
    
    CustBankList *curBank = [self.array objectAtIndex:self.array.count - 1];
    if ([elementName isEqualToString:@"bankName"]) {
        curBank.bankName = self.tmp;
    } else if ([elementName isEqualToString:@"balance"]) {
        curBank.balance = self.tmp;
    } else if ([elementName isEqualToString:@"cardNo"]) {
        curBank.cardNo = self.tmp;
    } else if ([elementName isEqualToString:@"cardNoDetail"]) {
        curBank.cardNoDetail = self.tmp;
    }else if ([elementName isEqualToString:@"isDefault"]) {
        curBank.isDefault = self.tmp;
    }
}


-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}


@end
