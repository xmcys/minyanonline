//
//  CustActivity.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-25.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustActivity : NSObject

@property (nonatomic, strong) NSString * actId;  // 活动ID
@property (nonatomic, strong) NSString * actTitle;  // 标题
@property (nonatomic, strong) NSString * actCont;   // 活动主题
@property (nonatomic, strong) NSString * beginTime;  // 活动开始时间
@property (nonatomic, strong) NSString * endTime;  // 活动结束时间
@property (nonatomic, strong) NSString * actUrl;  // 活动地址
@property (nonatomic, strong) NSString * actPicUrl;  // 小图片
@property (nonatomic, strong) NSString * actImgUrl;  // 大图片
@property (nonatomic, strong) NSString * actNum;  // 参与人数
@property (nonatomic, strong) NSString * proImg;  // banner图片地址
@property (nonatomic, strong) NSString * infoId;  // banner图片ID
@property (nonatomic, strong) NSString * proType;  // banner图片类型
@property (nonatomic, strong) NSString * appType;
@property (nonatomic, strong) NSString * versionNo;
@property (nonatomic, strong) NSString * url;

@property (nonatomic, strong) void (^block)(NSArray *array);

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;

@end
