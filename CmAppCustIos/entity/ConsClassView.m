//
//  ConsClassFunView.m
//  CmAppConsumerIos
//
//  Created by 宏超 陈 on 14-5-20.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsClassView.h"
#import "CTTableView.h"
#import "CTIndicateView.h"
#import "ConsClass.h"
#import "CTImageView.h"
#import "CustWebService.h"
#import "CTURLConnection.h"

@interface ConsClassView () <CTTableViewDelegate, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, CTURLConnectionDelegate>

@property (nonatomic, strong) CTTableView * tv;
@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) CTIndicateView * indicator;

@end

@implementation ConsClassView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        self.array = [[NSMutableArray alloc] init];
        
        self.tv = [[CTTableView alloc] initWithFrame:self.bounds];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        self.tv.pullUpViewEnabled = NO;
        [self addSubview:self.tv];
        
        self.indicator = [[CTIndicateView alloc] initInView:self];
    }
    return self;
}

- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService getInfoTypeUrl:self.showType] delegate:self];
    [conn start];
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark CTTableView Delegate * DataSource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadData];
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 175;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CLASS_CELL";
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    NSArray* elements = [[NSBundle mainBundle] loadNibNamed:@"ConsClassCell" owner:nil options:nil];
    
    UIView * view = [elements objectAtIndex:0];
    view.frame = CGRectMake(5, 5, tableView.frame.size.width - 10, 170);
    view.layer.borderColor = [[UIColor colorWithRed:222.0/255.0 green:222.0/255.0 blue:222.0/255.0 alpha:1] CGColor];
    view.layer.borderWidth = 1;
    
    [cell addSubview:view];
    
    ConsClass * item = [self.array objectAtIndex:indexPath.row];
    
    UILabel * cont = (UILabel *)[cell viewWithTag:4];
    CTImageView * img = (CTImageView *)[cell viewWithTag:3];
    img.userInteractionEnabled = NO;
    if (self.showType == ClassFun || self.showType == ClassCustTeach) {
        img.contentMode = UIViewContentModeScaleToFill;
        img.url = [CustWebService fileFullUrl:item.schoolImg01];
        [img loadImage];
        img.hidden = NO;
        cont.hidden = YES;
        item.showType = ClassFun;
    } else {
        img.hidden = YES;
        cont.hidden = NO;
        item.showType = ClassDistinguish;
        
        CGSize size = [item.schoolCont sizeWithFont:cont.font constrainedToSize:CGSizeMake(cont.frame.size.width, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
        
        if (size.height > cont.frame.size.height) {
            size = CGSizeMake(size.width, cont.frame.size.height);
        } else {
            size = CGSizeMake(size.width, size.height);
        }
        cont.text = [item.schoolCont stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
        cont.frame = CGRectMake(cont.frame.origin.x, cont.frame.origin.y, size.width, size.height);
    }
    
    UILabel * title = (UILabel *)[cell viewWithTag:1];
    title.text = item.schoolTitle;
    
    UILabel * time = (UILabel *)[cell viewWithTag:2];
    time.text = item.publishTime;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ConsClass * item = [self.array objectAtIndex:indexPath.row];
    
    if ([self.delegate respondsToSelector:@selector(consClassView:didSelectedWithItem:)]) {
        [self.delegate consClassView:self didSelectedWithItem:item];
    }
    
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.tv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    ConsClass * item = [[ConsClass alloc] init];
    [item parseData:data complete:^(NSArray * array){
        [self.array removeAllObjects];
        [self.array addObjectsFromArray:array];
        [self.tv reloadData];
        [self.indicator hide];
    }];
}

@end
