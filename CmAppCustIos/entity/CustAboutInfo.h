//
//  CustAboutInfo.h
//  CmAppCustIos
//
//  Created by hsit on 14-12-12.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustAboutInfo : NSObject

@property (nonatomic, strong) NSString *tel;  //客服电话
@property (nonatomic, strong) NSString *web;  //网址
@property (nonatomic, strong) NSString *addr; //地址
@property (nonatomic, strong) NSString *zip;  //邮编

- (void)validate:(NSString *)userCode complete:(void(^)(CustAboutInfo *))block;

@end
