//
//  CustBankList.h
//  CmAppCustIos
//
//  Created by hsit on 14-7-31.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustBankList : NSObject<NSXMLParserDelegate>

@property (nonatomic, strong) NSString * bankName;   //银行名称
@property (nonatomic, strong) NSString * balance;    //银行卡余额
@property (nonatomic, strong) NSString * cardNo;     //银行卡号简短
@property (nonatomic, strong) NSString * cardNoDetail; //详细银行卡号
@property (nonatomic, strong) NSString * isDefault;   //是否是默认付款银行卡 1.默认 0 不默认
@property (nonatomic, strong) NSString * isUsedFlag;  //是否正常  1正常 0停用

@property (nonatomic, strong) void (^block)(NSArray *array);

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;

@end
