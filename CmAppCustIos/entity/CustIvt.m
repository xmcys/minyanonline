//
//  CustIvt.m
//  CmAppCustIos
//
//  Created by hsit on 15-4-13.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustIvt.h"

@interface CustIvt ()<NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) void(^block)(NSArray *);

@end

@implementation CustIvt

- (void)parseData:(NSData *)date complete:(void (^)(NSArray *))block {
    self.array = [[NSMutableArray alloc] init];
    self.block = block;
    NSXMLParser *xml = [[NSXMLParser alloc] init];
    xml.delegate = self;
    [xml parse];
}

#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"item"]) {
        CustIvt *ivt = [[CustIvt alloc] init];
        ivt.productName = [attributeDict objectForKey:@"productName"];
        ivt.stock = [attributeDict objectForKey:@"stock"];
        ivt.tradePrice = [attributeDict objectForKey:@"tradePrice"];
        ivt.retailPrice = [attributeDict objectForKey:@"retailPrice"];
        [self.array addObject:ivt];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
