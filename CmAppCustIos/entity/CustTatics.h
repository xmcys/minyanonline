//
//  CustTatics.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-26.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CustTatics;

@protocol CustTaticsDelegate <NSObject>

@optional
- (void)custTatics:(CustTatics *)custTatics didFinishParsing:(NSArray *)array;

@end

@interface CustTatics : NSObject

@property (nonatomic, strong) NSString * brandName;  // 卷烟名称
@property (nonatomic, strong) NSString * level51;    // 五星 I
@property (nonatomic, strong) NSString * level52;    // 五星 II
@property (nonatomic, strong) NSString * level41;    // 四星 I
@property (nonatomic, strong) NSString * level42;    // 四星 II
@property (nonatomic, strong) NSString * level3;     // 三星
@property (nonatomic, strong) NSString * level2;     // 二星
@property (nonatomic, strong) NSString * level1;     // 一星
@property (nonatomic, weak) id<CustTaticsDelegate> delegate;

- (void)parseData:(NSData *)data;

@end
