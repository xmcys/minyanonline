//
//  CustOrderTrDtl.h
//  CmAppCustIos
//
//  Created by hsit on 15-3-18.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustOrderTrDtl : NSObject<NSXMLParserDelegate>

@property (nonatomic, strong) NSString * statusName;   //订单状态
@property (nonatomic, strong) NSString * statusTime;    //订单时间
@property (nonatomic, strong) NSMutableArray *array;

@property (nonatomic, strong) void (^block)(NSArray *array);

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;


@end
