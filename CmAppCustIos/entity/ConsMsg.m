//
//  ConsMsg.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-2.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsMsg.h"

@interface ConsMsg () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray * array;

- (NSString *)formatImgPath:(NSString *)path;


@end

@implementation ConsMsg

- (id)init
{
    self = [super init];
    if (self) {
        self.unreadCount = @"0";
    }
    return self;
}

- (void)parseData:(NSData *)data
{
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

- (NSString *)formatImgPath:(NSString *)path
{
    if ([path hasSuffix:@".png"] || [path hasSuffix:@".PNG"]
        || [path hasSuffix:@".jpg"] || [path hasSuffix:@".JPG"]
        || [path hasSuffix:@".jpeg"] || [path hasSuffix:@".JPEG"]) {
        return path;
    } else {
        return @"";
    }
}


#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        ConsMsg * item = [[ConsMsg alloc] init];
        item.msgId = [attributeDict objectForKey:@"msgId"];
        item.msgType = [attributeDict objectForKey:@"msgType"];
        item.msgTitle = [attributeDict objectForKey:@"msgTitle"];
        item.msgCont = [attributeDict objectForKey:@"msgCont"];
        item.msgUrl = [attributeDict objectForKey:@"msgUrl"];
        item.publishTime = [attributeDict objectForKey:@"publishTime"];
        item.readFlag = [self formatImgPath:[attributeDict objectForKey:@"readFlag"]];
        item.msgImg01 = [self formatImgPath:[attributeDict objectForKey:@"msgImg01"]];
        item.msgImg02 = [self formatImgPath:[attributeDict objectForKey:@"msgImg02"]];
        item.msgImg03 = [self formatImgPath:[attributeDict objectForKey:@"msgImg03"]];
        item.msgImg04 = [self formatImgPath:[attributeDict objectForKey:@"msgImg04"]];
        item.msgImg05 = [self formatImgPath:[attributeDict objectForKey:@"msgImg05"]];
        item.readFlag = [attributeDict objectForKey:@"readFlag"];
        item.unreadCount = [attributeDict objectForKey:@"readNums"];
        
        self.unreadCount = item.unreadCount;
        self.msgId = item.msgId;
        self.msgType = item.msgType;
        self.msgTitle = item.msgTitle;
        self.msgCont = item.msgCont;
        self.msgUrl = item.msgUrl;
        self.readFlag = item.readFlag;
        self.publishTime = item.publishTime;
        self.readFlag = item.readFlag;
        self.msgImg01 = item.msgImg01;
        self.msgImg02 = item.msgImg02;
        self.msgImg03 = item.msgImg03;
        self.msgImg04 = item.msgImg04;
        self.msgImg05 = item.msgImg05;
        
        [self.array addObject:item];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    if ([self.delegate respondsToSelector:@selector(consMsgDidFinishingParse:withArray:)]) {
        [self.delegate consMsgDidFinishingParse:self withArray:self.array];
    }
}


@end
