//
//  CustPerTatics.m
//  CmAppCustIos
//
//  Created by hsit on 15-3-17.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustPerTatics.h"

@interface CustPerTatics() <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation CustPerTatics

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    self.array = [[NSMutableArray alloc] init];
    parser.delegate = self;
    [parser parse];
}

#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        CustPerTatics *item = [[CustPerTatics alloc] init];
        item.brandName = [attributeDict objectForKey:@"brandName"];
        item.startDate = [attributeDict objectForKey:@"startDate"];
        item.endDate = [attributeDict objectForKey:@"endDate"];
        item.qtyUpperLimit = [attributeDict objectForKey:@"qtyUpperLimit"];
        [self.array addObject:item];
    }
}

-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}


@end
