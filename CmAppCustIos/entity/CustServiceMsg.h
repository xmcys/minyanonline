//
//  CustServiceMsg.h
//  CmAppCustIos
//
//  Created by hsit on 14-8-26.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustServiceMsg : NSObject<NSXMLParserDelegate>

@property (nonatomic, strong) NSString * demandId;      //服务ID
@property (nonatomic, strong) NSString * demandTitle;   //标题
@property (nonatomic, strong) NSString * demandTime;    //提出时间
@property (nonatomic, strong) NSString * demandCont;    //内容描述
@property (nonatomic, strong) NSString * dealType;      //处理方式
@property (nonatomic, strong) NSString * dealPersonName;//处理人员
@property (nonatomic, strong) NSString * dealTime;      //处理时间
@property (nonatomic, strong) NSString * dealInfo;      //回复内容
@property (nonatomic, strong) NSString * evalInfo;      //评价内容
@property (nonatomic, strong) NSString * evalSpeed;     //响应速度评价
@property (nonatomic, strong) NSString * evalAttitude;  //服务态度评价
@property (nonatomic, strong) NSString * evalEffect;    //服务效果评价
@property (nonatomic, strong) void (^block)(CustServiceMsg *);

- (void)parseData:(NSData *)data complete:(void (^)(CustServiceMsg *))block;

@end
