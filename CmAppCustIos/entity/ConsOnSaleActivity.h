//
//  ConsOnSaleActivity.h
//  CmAppConsumerIos
//
//  Created by 张斌 on 14-6-4.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConsOnSaleActivity : NSObject

@property (nonatomic, strong) NSString * actId;  // 活动ID
@property (nonatomic, strong) NSString * actTitle;  // 标题
@property (nonatomic, strong) NSString * actCont;   // 活动内容
@property (nonatomic, strong) NSString * publishTime;  // 活动开始时间
@property (nonatomic, strong) NSString * msgImg01;      // 消息图片
@property (nonatomic, strong) NSString * msgImg02;      // 消息图片
@property (nonatomic, strong) NSString * msgImg03;      // 消息图片
@property (nonatomic, strong) NSString * msgImg04;      // 消息图片
@property (nonatomic, strong) NSString * msgImg05;      // 消息图片

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;

@end
