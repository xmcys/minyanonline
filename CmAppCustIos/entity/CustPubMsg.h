//
//  RYKPubMsg.h
//  RYKForCustIos
//
//  Created by hsit on 14-6-26.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustPubMsg : NSObject<NSXMLParserDelegate>
@property (nonatomic, strong) NSString * msgId;             //公告序列号
@property (nonatomic, strong) NSString * msgTitle;         // 公告标题
@property (nonatomic, strong) NSString * publishTime;        //公告开始时间
@property (nonatomic, strong) NSString * validTime;        //结束时间
@property (nonatomic, strong) NSString * msgCont;          //消息内容
@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void(^block)(NSArray *array);

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;
- (void)MsgFromotherChanel:(CustPubMsg *)tagMsgs;



@end
