//
//  ConsPopup.h
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ConsPopup;

@protocol ConsPopupDelegate <NSObject>

- (void)consPopup:(ConsPopup *)consPopup popStateChanged:(BOOL)isShowing;
- (void)consPopup:(ConsPopup *)consPopup didSelecteInIndex:(NSInteger)index;

@end


@interface PopupItem : NSObject

@property (nonatomic, strong) NSString * key;
@property (nonatomic, strong) NSString * value;

@end

@interface ConsPopup : UIView

@property (nonatomic) NSInteger selectedIndex;
@property (nonatomic, strong) NSMutableArray * itemArray;
@property (nonatomic, weak) id<ConsPopupDelegate> delegate;

- (void)hidePopup;

@end
