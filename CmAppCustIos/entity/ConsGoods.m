//
//  ConsGoods.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-9.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsGoods.h"
#import "CustWebService.h"

@interface ConsGoods () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void(^block)(NSArray *array);

@end

@implementation ConsGoods

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

// 查找最后一个（号
- (NSRange)getQtyPosition:(NSString *)value lastRange:(NSRange)lastRange
{
    int loc = lastRange.location;
    NSRange range = [value rangeOfString:@"("];
    if (range.location != NSNotFound) {
        NSString * temp = [value substringFromIndex:range.location + 1];
        NSRange tr = [temp rangeOfString:@"("];
        if (tr.location == NSNotFound) {
            if (loc != NSNotFound) {
                return NSMakeRange(loc + range.location, range.length);
            } else {
                return range;
            }
        } else {
            if (loc == NSNotFound) {
                loc = 0;
            }
            return [self getQtyPosition:temp lastRange:NSMakeRange(loc + 1 + range.location, range.length)];
        }
    } else {
        return NSMakeRange(NSNotFound, 0);
    }
}

- (NSString *)formatImgPath:(NSString *)path
{
    if ([path hasSuffix:@".png"] || [path hasSuffix:@".PNG"]
        || [path hasSuffix:@".jpg"] || [path hasSuffix:@".JPG"]
        || [path hasSuffix:@".jpeg"] || [path hasSuffix:@".JPEG"]) {
        return path;
    } else {
        return @"";
    }
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        ConsGoods * item = [[ConsGoods alloc] init];
        item.goodsId = [attributeDict objectForKey:@"ruleId"];
        item.goodsCode = [attributeDict objectForKey:@"presentId"];
        item.goodsName = [attributeDict objectForKey:@"presentName"];
        item.mDescription = [attributeDict objectForKey:@"presentCont"];
        item.imgUrl = [self formatImgPath:[attributeDict objectForKey:@"presentUrl"]];
        item.imgUrl = [CustWebService fileFullUrl:item.imgUrl];
        item.price = [attributeDict objectForKey:@"presentPrice"];
        item.points = [attributeDict objectForKey:@"points"];
        item.salesCount = [attributeDict objectForKey:@"SalesCount"];
        item.pic1 = [attributeDict objectForKey:@"Pic1"];
        item.pic2 = [attributeDict objectForKey:@"Pic2"];
        item.pic3 = [attributeDict objectForKey:@"Pic3"];
        item.pic4 = [attributeDict objectForKey:@"Pic4"];
        item.pic5 = [attributeDict objectForKey:@"Pic5"];
        item.orderNo = [attributeDict objectForKey:@"OrderNo"];
        item.orderDate = [attributeDict objectForKey:@"OrderDate"];
        item.exType = [attributeDict objectForKey:@"ExType"];
        item.orderAmount = [attributeDict objectForKey:@"OrderAmount"];
        item.exPoints = [attributeDict objectForKey:@"ExPoints"];
        item.pickupCode = [attributeDict objectForKey:@"PickupCode"];
        item.goodsDesc = [attributeDict objectForKey:@"presentCont"];
        item.storeName = [attributeDict objectForKey:@"StoreName"];
        item.storeAddr = [attributeDict objectForKey:@"StoreAddr"];
        item.tel = [attributeDict objectForKey:@"Tel"];
        item.cnName = [attributeDict objectForKey:@"CnName"];
        item.cnPhoneNo = [attributeDict objectForKey:@"CnPhoneNo"];
        item.orderStatus = [attributeDict objectForKey:@"OrderStatus"];
        
        if (item.goodsDesc != nil && item.goodsDesc.length != 0) {
            NSRange range = [self getQtyPosition:item.goodsDesc lastRange:NSMakeRange(NSNotFound, 0)];
            if (range.location == NSNotFound) {
                item.qty = @"0件";
            } else {
                item.qty = [item.goodsDesc substringFromIndex:range.location + 1];
                item.qty = [item.qty substringToIndex:[item.qty rangeOfString:@")"].location];
                item.goodsDesc = [item.goodsDesc stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"(%@)", item.qty] withString:@""];
            }
        } else {
            item.qty = @"0件";
        }
        
        [self.array addObject:item];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}


@end

@interface ConsExchangePoints () <NSXMLParserDelegate>

@property (nonatomic, strong) NSString *curPoints;

@property (nonatomic, strong) void(^block)(NSString *points);

@end

@implementation ConsExchangePoints

- (void)parseData:(NSData *)data complete:(void (^)(NSString *))block
{
    self.block = block;
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    if (self.curPoints == nil || self.curPoints.length == 0 || [self.curPoints isEqualToString:@"null"]) {
        self.curPoints = @"0";
    }
    self.block(self.curPoints);
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        self.curPoints = [attributeDict objectForKey:@"value"];
    }
}


@end

