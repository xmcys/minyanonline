//
//  CustSumPoints.m
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/24.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustSumPoints.h"

@interface CustSumPoints () <NSXMLParserDelegate>

@property (nonatomic, strong) void(^block)(CustSumPoints *);
@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation CustSumPoints

- (void)parseData:(NSData *)data complete:(void (^)(CustSumPoints *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        self.sumPoints = [attributeDict objectForKey:@"sumPoints"];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self);
}

@end
