//
//  CustAboutInfo.m
//  CmAppCustIos
//
//  Created by hsit on 14-12-12.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustAboutInfo.h"

@implementation CustAboutInfo

- (void)validate:(NSString *)userCode complete:(void(^)(CustAboutInfo *))block;
{
    int cityCode = [[userCode substringToIndex:4] intValue];
    switch (cityCode) {
        case 3501://福州
            self.tel = @"暂无";
            self.web = @"暂无";
            self.addr = @"暂无";
            self.zip = @"暂无";
            break;
            
        case 3502://厦门
            self.tel = @"96329";
            self.web = @"http://xm.fj-tobacco.com";
            self.addr = @"厦门市思明区湖光路66-67号";
            self.zip = @"361004";
            break;
            
        case 3503://莆田
            self.tel = @"暂无";
            self.web = @"暂无";
            self.addr = @"暂无";
            self.zip = @"暂无";
            break;
            
        case 3504://三明
            self.tel = @"暂无";
            self.web = @"暂无";
            self.addr = @"暂无";
            self.zip = @"暂无";
            break;
            
        case 3505://泉州
            self.tel = @"暂无";
            self.web = @"暂无";
            self.addr = @"暂无";
            self.zip = @"暂无";
            break;
            
        case 3506://漳州
            self.tel = @"暂无";
            self.web = @"暂无";
            self.addr = @"暂无";
            self.zip = @"暂无";
            break;
            
        case 3507://南平
            self.tel = @"8876021";
            self.web = @"http://www.npycw.com.cn";
            self.addr = @"南平市延平区滨江路389号冠林大厦";
            self.zip = @"353000";
            break;
            
        case 3508://龙岩
            self.tel = @"暂无";
            self.web = @"暂无";
            self.addr = @"暂无";
            self.zip = @"暂无";
            break;
            
        case 3509://宁德
            self.tel = @"18905939361";
            self.web = @"http://www.ndyc.cn";
            self.addr = @"宁德市蕉城区蕉城南路95号";
            self.zip = @"352100";
            break;
            
        default:
            self.tel = @"暂无";
            self.web = @"暂无";
            self.addr = @"暂无";
            self.zip = @"暂无";
            break;
    }
    block(self);
}

@end
