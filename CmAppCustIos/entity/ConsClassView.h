//
//  ConsClassFunView.h
//  CmAppConsumerIos
//
//  Created by 宏超 陈 on 14-5-20.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>



@class ConsClassView;
@class ConsClass;

@protocol ConsClassViewDelegate <NSObject>

@optional
- (void)consClassView:(ConsClassView *)classView didSelectedWithItem:(ConsClass *)consClass;

@end


@interface ConsClassView : UIView

@property (nonatomic, weak) id<ConsClassViewDelegate> delegate;
@property (nonatomic) NSInteger showType;

- (void)loadData;

@end
