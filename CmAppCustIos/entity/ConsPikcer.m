//
//  ConsPikcer.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-11.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsPikcer.h"

@implementation PickerItem


@end


@interface ConsPikcer () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UIView * header;
@property (nonatomic, strong) UIButton * btnPicker;
@property (nonatomic, strong) UITableView * tv;

- (void)showOrHide;

@end


@implementation ConsPikcer

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 50)];
    if (self) {
        
        self.selectedIndex = -1;
        
        self.header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        self.header.backgroundColor = [UIColor colorWithRed:211.0/255.0 green:225.0/255.0 blue:214.0/255.0 alpha:1];
        [self addSubview:self.header];
        
        self.btnPicker = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btnPicker.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        [self.btnPicker setTitleColor:[UIColor colorWithRed:88.0/255.0 green:88.0/255.0 blue:88.0/255.0 alpha:1] forState:UIControlStateNormal];
        [self.btnPicker setTitleColor:[UIColor colorWithRed:188.0/255.0 green:188.0/255.0 blue:188.0/255.0 alpha:1] forState:UIControlStateHighlighted];
        self.btnPicker.frame = CGRectMake(7, 7, frame.size.width - 14, 35);
        [self.btnPicker setBackgroundImage:[[UIImage imageNamed:@"BtnDropDownWithShell"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateNormal];
        [self.btnPicker setBackgroundImage:[[UIImage imageNamed:@"BtnDropDownWithShell"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 14, 14, 14)] forState:UIControlStateHighlighted];
        [self.btnPicker setTitle:@"无可用积分" forState:UIControlStateNormal];
        [self.btnPicker setTitle:@"无可用积分" forState:UIControlStateHighlighted];
        self.btnPicker.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
        self.btnPicker.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self.btnPicker addTarget:self action:@selector(showOrHide) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.btnPicker];
        
        self.backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(7, 42, frame.size.width - 14, 108)];
        
        self.backgroundImage.image = [[UIImage imageNamed:@"PurpleShell"] resizableImageWithCapInsets:UIEdgeInsetsMake(9, 7, 10, 8)];
        self.backgroundImage.alpha = 0;
        [self addSubview:self.backgroundImage];
        
        self.itemArray = [[NSMutableArray alloc] init];
        
        self.tv = [[UITableView alloc] initWithFrame:CGRectMake(self.frame.origin.x + 7, CGRectGetMaxY(self.frame) - 8, frame.size.width - 14, 108)];
        self.tv.dataSource = self;
        self.tv.delegate = self;
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tv.alpha = 0;
    }
    return self;
}

- (void)show
{
    [self.superview addSubview:self.tv];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundImage.alpha = 1.0;
        self.tv.alpha = 1.0;
    }];
}

- (void)hide
{
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundImage.alpha = 0.0;
        self.tv.alpha = 0.0;
    } completion:^(BOOL finished){
        [self.tv removeFromSuperview];
    }];
}

- (void)showOrHide
{
    if (self.backgroundImage.alpha == 0) {
        [self show];
    } else {
        [self hide];
    }
}

- (void)reloadData
{
    if (self.selectedIndex < self.itemArray.count) {
        PickerItem * item = [self.itemArray objectAtIndex:self.selectedIndex];
        item.selected = YES;
        [self.btnPicker setTitle:item.value forState:UIControlStateNormal];
        [self.btnPicker setTitle:item.value forState:UIControlStateHighlighted];
    }
    [self.tv reloadData];
}

- (void)setSelectedIndex:(int)selectedIndex
{
    _selectedIndex = selectedIndex;
    for (int i = 0; i < self.itemArray.count; i++) {
        PickerItem * item = [self.itemArray objectAtIndex:i];
        item.selected = NO;
    }
    [self reloadData];
}

- (void)setTitle:(NSString *)title
{
    [self.btnPicker setTitle:title forState:UIControlStateNormal];
    [self.btnPicker setTitle:title forState:UIControlStateHighlighted];
}

#pragma mark -
#pragma mark UITableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.itemArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"PICKER_CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, tableView.frame.size.width - 15, 36)];
        label.font = [UIFont systemFontOfSize:14.0f];
        label.textColor = [UIColor colorWithRed:88.0/255.0 green:88.0/255.0 blue:88.0/255.0 alpha:1];
        label.backgroundColor = [UIColor clearColor];
        label.tag = 1;
        [cell addSubview:label];
    }
    
    PickerItem * item = [self.itemArray objectAtIndex:indexPath.row];
    
    UILabel * label = (UILabel *)[cell viewWithTag:1];
    label.text = item.value;
    if (item.selected) {
        label.textColor = [UIColor colorWithRed:90.0/255.0 green:187.0/255.0 blue:150.0/255.0 alpha:1];
        [self.btnPicker setTitle:item.value forState:UIControlStateNormal];
        [self.btnPicker setTitle:item.value forState:UIControlStateHighlighted];
    } else {
        label.textColor = [UIColor colorWithRed:88.0/255.0 green:88.0/255.0 blue:88.0/255.0 alpha:1];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    for (int i = 0; i < self.itemArray.count; i++) {
        PickerItem * item = [self.itemArray objectAtIndex:i];
        item.selected = NO;
    }
    
    PickerItem * item = [self.itemArray objectAtIndex:indexPath.row];
    item.selected = YES;
    self.selectedIndex = indexPath.row;
    [tableView reloadData];
    
    [self hide];
    
    
    if ([self.delegate respondsToSelector:@selector(consPickerDelegate:didSelectRowAtIndex:)]){
        [self.delegate consPickerDelegate:self didSelectRowAtIndex:indexPath.row];
    }
    
}

@end
