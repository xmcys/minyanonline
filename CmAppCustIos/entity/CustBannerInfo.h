//
//  CustBannerInfo.h
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/25.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustBannerInfo : NSObject

@property (nonatomic, strong) NSString * proType;  // 0卷烟1公告2营销活动
@property (nonatomic, strong) NSString * infoId;   // 卷烟ID,公告ID,活动ID
@property (nonatomic, strong) NSString * proImg;   // 宣传图片

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *))block;

@end
