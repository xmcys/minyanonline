//
//  ConsSupplier.h
//  CmAppConsumerIos
//
//  Created by 宏超 陈 on 14-5-20.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConsSupplier : NSObject

@property (nonatomic, strong) NSString * supplierCode;
@property (nonatomic, strong) NSString * supplierName;  // 工业名称
@property (nonatomic, strong) NSString * totalBrand;   // 工业介绍
@property (nonatomic, strong) NSString * userIcon;    // 工业图标
@property (nonatomic, strong) NSString * totalBrandPic;  // 工业大图

- (void)parseData:(NSData *)data complete:(void(^)(NSArray * array))block;

@end
