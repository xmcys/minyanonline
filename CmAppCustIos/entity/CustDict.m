//
//  CustDict.m
//  CmAppCustIos
//
//  Created by hsit on 15-4-2.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustDict.h"

@interface CustDict ()<NSXMLParserDelegate>

@property (nonatomic, strong) void (^block)(NSArray *);
@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation CustDict

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block {
    self.array = [[NSMutableArray alloc] init];
    self.block = block;
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"item"]) {
        CustDict *dict = [[CustDict alloc] init];
        dict.dictCode = [attributeDict objectForKey:@"dictCode"];
        dict.dictName = [attributeDict objectForKey:@"dictName"];
        [self.array addObject:dict];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}


@end
