//
//  CustServiceMsgList.h
//  CmAppCustIos
//
//  Created by hsit on 14-9-3.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustServiceMsgList : NSObject <NSXMLParserDelegate>

@property (nonatomic, strong) NSString * demandId;
@property (nonatomic, strong) NSString * demandTitle;
@property (nonatomic, strong) NSString * demandTime;
@property (nonatomic, strong) NSString * demandStatus;
@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void (^block)(NSArray *array);

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;

@end
