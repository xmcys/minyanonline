//
//  ConsMyExchangeItem.m
//  CmAppConsumerIos
//
//  Created by 张斌 on 14-6-18.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsMyExchangeItem.h"

@interface ConsMyExchangeItem () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void(^block)(NSArray *array);

@end

@implementation ConsMyExchangeItem

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        ConsMyExchangeItem * curItem = [[ConsMyExchangeItem alloc] init];
        curItem.changeDate = [attributeDict objectForKey:@"changeDate"];
        curItem.giftName = [attributeDict objectForKey:@"giftName"];
        curItem.changePoint = [attributeDict objectForKey:@"changePoint"];
        curItem.changeQty = [attributeDict objectForKey:@"changeQty"];
        curItem.giftUnit = [attributeDict objectForKey:@"giftUnit"];
        curItem.giftCode = [attributeDict objectForKey:@"giftCode"];
        curItem.supplierName = [attributeDict objectForKey:@"supplierName"];
        curItem.supplierCode = [attributeDict objectForKey:@"supplierCode"];
        
        [self.array addObject:curItem];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
