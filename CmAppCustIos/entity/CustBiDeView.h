//
//  CustBiDeView.h
//  CmAppCustIos
//
//  Created by hsit on 15-4-13.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustBiDeView : NSObject

@property (nonatomic, strong) NSString *amtSale;      //金额
@property (nonatomic, strong) NSString *productName;  //品牌
@property (nonatomic, strong) NSString *zb;           //占比

- (void)parseData:(NSData *)date complete:(void(^)(NSArray *array))block;

@end
