//
//  RYKCustOrder.m
//  RYKForCustIos
//
//  Created by hsit on 14-7-2.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CustCustOrder.h"

@implementation CustCustOrder

- (void)parseData:(NSData *)data complete:(void(^)(CustCustOrder *msg))block;
{
    self.block = block;
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    self.orderNum = [attributeDict objectForKey:@"orderNum"];
    self.amt = [attributeDict objectForKey:@"amt"];
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self);
}


@end
