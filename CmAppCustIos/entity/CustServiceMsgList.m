//
//  CustServiceMsgList.m
//  CmAppCustIos
//
//  Created by hsit on 14-9-3.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustServiceMsgList.h"

@implementation CustServiceMsgList

-(void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}
#pragma mark - NSXMLParser Delegate
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if ([elementName isEqualToString:@"item"]) {
        CustServiceMsgList * item = [[CustServiceMsgList alloc] init];
        item.demandId = [attributeDict objectForKey:@"demandId"];
        item.demandTitle = [attributeDict objectForKey:@"demandTitle"];
        item.demandTime = [attributeDict objectForKey:@"demandTime"];
        item.demandStatus = [attributeDict objectForKey:@"demandStatus"];
        [self.array addObject:item];
    }  
}
-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}


@end
