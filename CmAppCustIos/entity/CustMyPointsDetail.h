//
//  CustMyPointsDetail.h
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/24.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustMyPointsDetail : NSObject

@property (nonatomic, strong) NSString * sumPoints;       // 总积分
@property (nonatomic, strong) NSString * usePoints;       // 可用积分
@property (nonatomic, strong) NSString * supplierName;    // 中烟公司名
@property (nonatomic, strong) NSString * supplierCode;    // 中烟公司代码

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *))block;

@end
