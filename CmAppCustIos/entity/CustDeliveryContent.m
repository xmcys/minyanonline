//
//  RYKDeliveryContent.m
//  RYKForCustIos
//
//  Created by hsit on 14-6-27.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CustDeliveryContent.h"

@implementation CustDeliveryContent

- (void)parseData:(NSData *)data complete:(void (^)(CustDeliveryContent *))block
{
    self.block = block;
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

- (NSString *)isEmptyString:(NSString *)string {
    if (string == nil || [string isEqualToString:@"null"]) {
        return @"";
    }
    return string;
}

#pragma mark -NSXMLParser Delegate
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if ([elementName isEqualToString:@"item"]) {
        self.delvIntro = [self isEmptyString:[attributeDict objectForKey:@"delvIntro"]];
    }
}

-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self);
}


@end
