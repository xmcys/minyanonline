//
//  ConsClassTradeMarkView.m
//  CmAppConsumerIos
//
//  Created by 宏超 陈 on 14-5-20.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsClassTradeMarkView.h"
#import "CTTableView.h"
#import "CustWebService.h"
#import "CTURLConnection.h"
#import "CTIndicateView.h"
#import "ConsSupplier.h"
#import "CTImageView.h"
#import <QuartzCore/QuartzCore.h>

@interface ConsClassTradeMarkView () <CTTableViewDelegate, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, CTURLConnectionDelegate>


@property (nonatomic, strong) CTTableView * tv;
@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) CTIndicateView * indicator;

@end

@implementation ConsClassTradeMarkView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        self.array = [[NSMutableArray alloc] init];
        
        self.tv = [[CTTableView alloc] initWithFrame:self.bounds];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        self.tv.pullUpViewEnabled = NO;
        [self addSubview:self.tv];
        
        self.indicator = [[CTIndicateView alloc] initInView:self];
        
    }
    return self;
}

- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"努力加载中..";
    [self.indicator show];
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[CustWebService supplierListUrl] delegate:self];
    [conn start];
}

- (void)onSupplierClicked:(UIControl *)sender
{
    CTImageView * img = (CTImageView *)[sender viewWithTag:1];
    ConsSupplier * item = [self.array objectAtIndex:[img.imgId intValue]];
    if ([self.delegate respondsToSelector:@selector(consClassTradeMarkView:didSelectedWithItem:)]) {
        [self.delegate consClassTradeMarkView:self didSelectedWithItem:item];
    }
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark CTTableView Delegate * DataSource
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    [self loadData];
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.array.count % 4 == 0) {
        return self.array.count / 4;
    } else {
        return self.array.count / 4 + 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"SUPPLIER_CELL";
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    CGFloat spcace = (self.tv.frame.size.width - 80 * 4) / 5;
    for (int i = 0; i < 4; i++) {
        NSArray* elements = [[NSBundle mainBundle] loadNibNamed:@"ConsSupplierCell" owner:nil options:nil];
        UIControl * view = [elements objectAtIndex:0];
        view.frame = CGRectMake(spcace + i * (spcace + 80), 0, 80, 100);
        CTImageView * img = (CTImageView *)[view viewWithTag:1];
        img.contentMode = UIViewContentModeScaleToFill;
        img.layer.borderColor = [[UIColor colorWithRed:205.0/255.0 green:205.0/255.0 blue:205.0/255.0 alpha:1] CGColor];
        img.layer.borderWidth = 0.5f;
        img.userInteractionEnabled = NO;
        
        UILabel * name = (UILabel *)[view viewWithTag:2];
        [cell addSubview:view];
        
        if (indexPath.row * 4 + i >= self.array.count) {
            view.hidden = YES;
        } else {
            ConsSupplier * item = [self.array objectAtIndex:(indexPath.row * 4 + i)];
            name.text = item.supplierName;
            img.imgId = [NSString stringWithFormat:@"%d", (indexPath.row * 4 + i)];
            img.url = [CustWebService fileFullUrl:item.userIcon];
            [img loadImage];
            view.hidden = NO;
            [view addTarget:self action:@selector(onSupplierClicked:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    return cell;
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.tv reloadData];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    ConsSupplier * item = [[ConsSupplier alloc] init];
    [item parseData:data complete:^(NSArray * array){
        [self.array removeAllObjects];
        [self.array addObjectsFromArray:array];
        [self.indicator hide];
        [self.tv reloadData];
    }];
}

@end
