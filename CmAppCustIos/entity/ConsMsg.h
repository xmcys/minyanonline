//
//  ConsMsg.h
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-2.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ConsMsg;

@protocol ConsMsgDelegate <NSObject>

@optional

- (void)consMsgDidFinishingParse:(ConsMsg *)consMsg withArray:(NSArray *)array;

@end


@interface ConsMsg : NSObject

@property (nonatomic, weak) id<ConsMsgDelegate> delegate;
@property (nonatomic, strong) NSString * msgId;         // 消息ID
@property (nonatomic, strong) NSString * msgType;       // 消息类型
@property (nonatomic, strong) NSString * msgTitle;      // 消息标题
@property (nonatomic, strong) NSString * msgCont;       // 消息内容
@property (nonatomic, strong) NSString * msgUrl;        // 附件地址
@property (nonatomic, strong) NSString * publishTime;   // 发布时间
@property (nonatomic, strong) NSString * readFlag;      // 阅读标记
@property (nonatomic, strong) NSString * msgImg01;      // 消息图片
@property (nonatomic, strong) NSString * msgImg02;      // 消息图片
@property (nonatomic, strong) NSString * msgImg03;      // 消息图片
@property (nonatomic, strong) NSString * msgImg04;      // 消息图片
@property (nonatomic, strong) NSString * msgImg05;      // 消息图片
@property (nonatomic, strong) NSString * unreadCount;   // 未读消息数


-(void)parseData:(NSData *)data;

@end
