//
//  RYKStoreInfo.m
//  RYKForCustIos
//
//  Created by hsit on 14-6-26.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CustStoreInfo.h"

@implementation CustStoreInfo
-(void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

- (NSString *)isEmptyString:(NSString *)string {
    if (string == nil || [string isEqualToString:@"null"]) {
        return @"";
    }
    return string;
}

#pragma mark -NSXMLParser Delegate
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if ([elementName isEqualToString:@"item"]) {
        CustStoreInfo * item = [[CustStoreInfo alloc] init];
        item.userName = [self isEmptyString:[attributeDict objectForKey:@"userName"]];
        item.linkPhone = [self isEmptyString:[attributeDict objectForKey:@"linkPhone"]];
        item.mobilphone = [self isEmptyString:[attributeDict objectForKey:@"mobilphone"]];
        item.userAddr = [self isEmptyString:[attributeDict objectForKey:@"userAddr"]];
        item.businessTime = [self isEmptyString:[attributeDict objectForKey:@"businessTime"]];
        item.storeIntro = [self isEmptyString:[attributeDict objectForKey:@"storeIntro"]];
        
        [self.array addObject:item];
    }
}

-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
