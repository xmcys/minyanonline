//
//  CustDefaultBank.m
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/27.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustDefaultBank.h"

@interface CustDefaultBank () <NSXMLParserDelegate>

@property (nonatomic, strong) void(^block)(CustDefaultBank *);

@end

@implementation CustDefaultBank

- (void)parseData:(NSData *)data complete:(void (^)(CustDefaultBank *))block
{
    self.block = block;
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"brand"]) {
        self.custBalance = [attributeDict objectForKey:@"custBalance"];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self);
}

@end
