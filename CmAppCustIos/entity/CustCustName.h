//
//  RYKCustName.h
//  RYKForCustIos
//
//  Created by hsit on 14-7-2.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustCustName : NSObject<NSXMLParserDelegate>

@property (nonatomic, strong) NSString *orderCode;    //订单序列号
@property (nonatomic, strong) NSString *operateDate;  //订单日期
@property (nonatomic, strong) NSString *productName;  //商品名称
@property (nonatomic, strong) NSString *currentPrice; //单价
@property (nonatomic, strong) NSString *quantity;     //销售数量
@property (nonatomic, strong) NSString *sumMoney;     //金额
@property (nonatomic, strong) NSString *oNum;         //其他商品数
@property (nonatomic, strong) NSString *hj;           //总计
@property (nonatomic, strong) NSString *num;          //总数量数
@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void(^block)(NSArray *array);

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;



@end
