//
//  CustMessage.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-24.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CustMessage;

@protocol CustMessageDelegate <NSObject>

@optional
- (void)custMessage:(CustMessage *)msg didFinishParsing:(NSArray *)array;

@end

@interface CustMessage : NSObject

@property (nonatomic, weak) id<CustMessageDelegate> delegate;
@property (nonatomic, strong) NSString * msgId;         // 消息ID
@property (nonatomic, strong) NSString * msgType;       // 消息类型
@property (nonatomic, strong) NSString * msgTitle;      // 消息标题
@property (nonatomic, strong) NSString * msgCont;       // 消息内容
@property (nonatomic, strong) NSString * msgUrl;        // 附件地址
@property (nonatomic, strong) NSString * publishTime;   // 发布时间
@property (nonatomic, strong) NSString * readFlag;      // 阅读标记
@property (nonatomic, strong) NSString * msgImg01;      // 消息图片
@property (nonatomic, strong) NSString * msgImg02;      // 消息图片
@property (nonatomic, strong) NSString * msgImg03;      // 消息图片
@property (nonatomic, strong) NSString * msgImg04;      // 消息图片
@property (nonatomic, strong) NSString * msgImg05;      // 消息图片

- (void)parseData:(NSData *)data;

@end
