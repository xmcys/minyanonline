//
//  CustServiceMsg.m
//  CmAppCustIos
//
//  Created by hsit on 14-8-26.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustServiceMsg.h"

@implementation CustServiceMsg

- (void)parseData:(NSData *)data complete:(void (^)(CustServiceMsg *))block
{
    self.block = block;
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        self.demandId = [attributeDict objectForKey:@"demandId"];
        self.demandTitle = [attributeDict objectForKey:@"demandTitle"];
        self.demandTime = [attributeDict objectForKey:@"demandTime"];
        self.demandCont = [attributeDict objectForKey:@"demandCont"];
        self.dealType = [attributeDict objectForKey:@"dealType"];
        self.dealPersonName = [attributeDict objectForKey:@"dealPersonName"];
        self.dealTime = [attributeDict objectForKey:@"dealTime"];
        self.dealInfo = [attributeDict objectForKey:@"dealInfo"];
        self.evalInfo = [attributeDict objectForKey:@"evalInfo"];
        self.evalSpeed = [attributeDict objectForKey:@"evalSpeed"];
        self.evalAttitude = [attributeDict objectForKey:@"evalAttitude"];
        self.evalEffect = [attributeDict objectForKey:@"evalEffect"];
    }
}

-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self);
}


@end
