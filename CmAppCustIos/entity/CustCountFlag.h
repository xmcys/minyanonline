//
//  CustCountFlag.h
//  CmAppCustIos
//
//  Created by hsit on 14-7-30.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustCountFlag : NSObject <NSXMLParserDelegate>

@property (nonatomic, strong) NSString * value; //  强制结算标志
@property (nonatomic, strong) void (^block)(CustCountFlag *);

- (void)parseData:(NSData *)data complete:(void(^)(CustCountFlag *))block;

@end
