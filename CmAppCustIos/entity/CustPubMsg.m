//
//  RYKPubMsg.m
//  RYKForCustIos
//
//  Created by hsit on 14-6-26.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CustPubMsg.h"

@implementation CustPubMsg

- (id)init
{
    self = [super init];
    if (self) {
        self.msgId = @"";
        self.msgTitle = @"";
        self.publishTime = @"";
        self.validTime = @"";
        self.msgCont = @"";
        
    }
    return self;
}

- (void)MsgFromotherChanel:(CustPubMsg *)tagMsgs
{
    self.msgTitle = [NSString stringWithString:tagMsgs.msgTitle];
    self.publishTime = [NSString stringWithString:tagMsgs.publishTime];
    self.validTime = [NSString stringWithString:tagMsgs.validTime];
    self.msgCont = [NSString stringWithString:tagMsgs.msgCont];
    return;
}

-(void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}
#pragma mark -NSXMLParser Delegate
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if ([elementName isEqualToString:@"item"]) {
        CustPubMsg * item = [[CustPubMsg alloc] init];
        item.msgId = [attributeDict objectForKey:@"msgId"];
        item.publishTime = [attributeDict objectForKey:@"publishTime"];
        item.msgTitle = [attributeDict objectForKey:@"msgTitle"];
        item.validTime = [attributeDict objectForKey:@"validTime"];
        item.msgCont = [attributeDict objectForKey:@"msgCont"];
        [self.array addObject:item];
    }
}
-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}


@end
