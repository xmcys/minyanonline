//
//  CustExchangeGift.m
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/24.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustExchangeGift.h"

@interface CustExchangeGift () <NSXMLParserDelegate>

@property (nonatomic, strong) void(^block)(NSArray *);
@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation CustExchangeGift

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        CustExchangeGift * curGift = [[CustExchangeGift alloc] init];
        curGift.giftCode = [attributeDict objectForKey:@"giftCode"];
        curGift.giftName = [attributeDict objectForKey:@"giftName"];
        curGift.giftUnit = [attributeDict objectForKey:@"giftUnit"];
        curGift.giftPic = [attributeDict objectForKey:@"giftPic"];
        curGift.changePoint = [attributeDict objectForKey:@"changePoint"];
        curGift.giftDesc = [attributeDict objectForKey:@"giftDesc"];
        curGift.ruleId = [attributeDict objectForKey:@"ruleId"];
        curGift.supplierCode = [attributeDict objectForKey:@"supplierCode"];
        curGift.advisePrice = [attributeDict objectForKey:@"advisePrice"];
        curGift.supplierName = [attributeDict objectForKey:@"supplierName"];
        if (!curGift.giftDesc || [curGift.giftDesc isEqualToString:@"null"] || curGift.giftDesc.length == 0) {
            curGift.giftDesc = @"无";
        }
        
        [self.array addObject:curGift];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
