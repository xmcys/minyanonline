//
//  CustOrderTrack.h
//  CmAppCustIos
//
//  Created by hsit on 15-3-18.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustOrderTrack : NSObject<NSXMLParserDelegate>

@property (nonatomic, strong) NSString *statusName;
@property (nonatomic, strong) NSString *balModeName;    //付款方式
@property (nonatomic, strong) NSString *balStatus;      //付款状态
@property (nonatomic, strong) NSString *balStatusName;  //付款状态名
@property (nonatomic, strong) NSString *amtOrderSum;  //订单金额
@property (nonatomic, strong) NSString *qtyDemandSum; //总需求量
@property (nonatomic, strong) NSString *qtyOrderSum;  //总订单量
@property (nonatomic, strong) NSString *orderDate;    //订购日期
@property (nonatomic, strong) NSString *orderId;      //订单号
@property (nonatomic, strong) NSString *flag;      //物流结束标记 0未结束 1已结束
@property (nonatomic, strong) void (^block)(CustOrderTrack *);

- (void)parseData:(NSData *)data complete:(void (^)(CustOrderTrack *))block;

@end
