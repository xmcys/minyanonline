//
//  CustSaleProf.h
//  CmAppCustIos
//
//  Created by hsit on 15-3-23.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustSaleProf : NSObject

@property (nonatomic, strong) NSString *sm;
@property (nonatomic, strong) NSString *mo;
@property (nonatomic, strong) NSString *ra;

@property (nonatomic) BOOL expand;

@end
