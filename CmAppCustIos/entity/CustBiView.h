//
//  CustLiOrder.h
//  CmAppCustIos
//
//  Created by hsit on 15-4-7.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustBiView : NSObject

@property (nonatomic, strong) NSString *amtSale;  //销售汇总
@property (nonatomic, strong) NSString *amtProfit;//毛利汇总

- (void)parseData:(NSData *)data complete:(void(^)(CustBiView *biView))block;

@end
