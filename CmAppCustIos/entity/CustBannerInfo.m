//
//  CustBannerInfo.m
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/25.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustBannerInfo.h"

@interface CustBannerInfo () <NSXMLParserDelegate>

@property (nonatomic, strong) void(^block)(NSArray *);
@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation CustBannerInfo

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        CustBannerInfo * banner = [[CustBannerInfo alloc] init];
        banner.proType = [attributeDict objectForKey:@"proType"];
        banner.infoId = [attributeDict objectForKey:@"infoId"];
        banner.proImg = [attributeDict objectForKey:@"proImg"];
        
        [self.array addObject:banner];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
