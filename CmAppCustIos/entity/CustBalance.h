//
//  CustBalance.h
//  CmAppCustIos
//
//  Created by Chenhc on 15/3/19.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustBalance : NSObject

@property (nonatomic, strong) NSString * picUrl;
@property (nonatomic, strong) NSString * bankName;
@property (nonatomic, strong) NSString * cardNo;
@property (nonatomic, strong) NSString * sCardNo;
@property (nonatomic, strong) NSString * isDefaultName;
@property (nonatomic, strong) NSString * balance;
@property (nonatomic, strong) NSString * lastModifyTime;

- (void)parseData:(NSData *)xmlData complete:(void(^)(NSArray *))block;

@end
