//
//  ConsPopup.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsPopup.h"

#define ITEM_HEIGHT 45

@interface PopupItem ()

@end

@implementation PopupItem

- (id)init
{
    self = [super init];
    if (self) {
        self.key = @"-1";
        self.value = @"";
    }
    return self;
}

@end

@interface ConsPopup () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UIButton * btnPopup;
@property (nonatomic, strong) UITableView * tv;

- (void)showOrHideTableView;

@end

@implementation ConsPopup

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, ITEM_HEIGHT)];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:236.0/255.0 green:247.0/255.0 blue:243.0/255.0 alpha:1];
        self.btnPopup = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btnPopup.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        [self.btnPopup setBackgroundColor:[UIColor clearColor]];
        [self.btnPopup setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.btnPopup setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        self.btnPopup.showsTouchWhenHighlighted = YES;
        self.btnPopup.frame = CGRectMake(0, 0, frame.size.width, ITEM_HEIGHT);
        [self.btnPopup addTarget:self action:@selector(showOrHideTableView) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.btnPopup];
        
        self.itemArray = [[NSMutableArray alloc] init];
        PopupItem * item = [[PopupItem alloc] init];
        item.key = @"-1";
        item.value = @"全部";
        [self.itemArray addObject:item];
        
        
        self.tv = [[UITableView alloc] initWithFrame:CGRectMake(0, ITEM_HEIGHT, frame.size.width, ITEM_HEIGHT * 4) style:UITableViewStylePlain];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.separatorColor = [UIColor whiteColor];
        self.tv.hidden = YES;
        [self addSubview:self.tv];
        
        self.selectedIndex = 0;
        
    }
    return self;
}

- (void)showOrHideTableView
{
    if (self.frame.size.height == ITEM_HEIGHT) {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, ITEM_HEIGHT * 5);
        self.tv.hidden = NO;
        [self.superview bringSubviewToFront:self];
//        [self.tv reloadData];
    } else {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, ITEM_HEIGHT);
        self.tv.hidden = YES;
    }
    
    if ([self.delegate respondsToSelector:@selector(consPopup:popStateChanged:)]) {
        [self.delegate consPopup:self popStateChanged:!self.tv.hidden];
    }
}

- (void)hidePopup
{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, ITEM_HEIGHT);
    self.tv.hidden = YES;
}

- (void)setItemArray:(NSMutableArray *)itemArray
{
    _itemArray = itemArray;
    [self.tv reloadData];
    
    if (self.itemArray.count > self.selectedIndex) {
        [self.btnPopup setTitle:((PopupItem *)[self.itemArray objectAtIndex:_selectedIndex]).value forState:UIControlStateNormal];
        [self.btnPopup setTitle:((PopupItem *)[self.itemArray objectAtIndex:_selectedIndex]).value forState:UIControlStateHighlighted];
    }
    
}

- (void)setSelectedIndex:(NSInteger)selectedIndex
{
    _selectedIndex = selectedIndex;
    if (_selectedIndex >= self.itemArray.count) {
        _selectedIndex = 0;
    }
    
    if (self.itemArray.count > self.selectedIndex) {
        [self.btnPopup setTitle:((PopupItem *)[self.itemArray objectAtIndex:_selectedIndex]).value forState:UIControlStateNormal];
        [self.btnPopup setTitle:((PopupItem *)[self.itemArray objectAtIndex:_selectedIndex]).value forState:UIControlStateHighlighted];
    }
    [self.tv reloadData];
}

#pragma mark -
#pragma mark UITableView DataSource & Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ITEM_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.itemArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"POPUP_CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        
        UIImageView * img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, ITEM_HEIGHT)];
        img.tag = 1;
        img.image = [[UIImage imageNamed:@"PopupSel"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 7, 0, 0)];
        img.backgroundColor = [UIColor whiteColor];
        [cell addSubview:img];
        
        UILabel * label = [[UILabel alloc] init];
        label.frame = CGRectMake(0, 0, self.frame.size.width, ITEM_HEIGHT);
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont systemFontOfSize:14.0f];
        label.tag = 2;
        label.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:label];
    }
    
    PopupItem * item = [self.itemArray objectAtIndex:indexPath.row];
    
    UILabel * label = (UILabel *)[cell viewWithTag:2];
    label.text = item.value;
    
    UIImageView * img = (UIImageView *)[cell viewWithTag:1];
    
    if (self.selectedIndex == indexPath.row) {
        img.hidden = NO;
    } else {
        img.hidden = YES;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndex = indexPath.row;
    [self hidePopup];
    if ([self.delegate respondsToSelector:@selector(consPopup:didSelecteInIndex:)]) {
        [self.delegate consPopup:self didSelecteInIndex:self.selectedIndex];
    }
}

@end
