//
//  CustServiceCount.h
//  CmAppCustIos
//
//  Created by hsit on 14-9-3.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustServiceCount : NSObject<NSXMLParserDelegate>

@property (nonatomic, strong) NSString * noDeal;  //未处理
@property (nonatomic, strong) NSString * inDeal;  //处理中
@property (nonatomic, strong) NSString * byEval;  //待评价
@property (nonatomic, strong) void(^block)(CustServiceCount *);

- (void)parseData:(NSData *)data complete:(void (^)(CustServiceCount *))block;

@end
