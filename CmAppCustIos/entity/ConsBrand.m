//
//  ConsBrand.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsBrand.h"

@interface ConsBrand () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void(^block)(NSArray * array);

@end


@implementation ConsBrand

- (id)init
{
    if (self = [super init]) {
        self.tTotalScore = @"0";
        self.tPackageScore = @"0";
        self.tPriceScore = @"0";
        self.tTasteScore = @"0";
        self.surverNum = @"0";
    }
    return self;
}

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        ConsBrand * item = [[ConsBrand alloc] init];
        item.typeCode = [attributeDict objectForKey:@"typeCode"];
        item.typeName = [attributeDict objectForKey:@"typeName"];
        item.ePrice = [attributeDict objectForKey:@"ePrice"];
        item.sPrice = [attributeDict objectForKey:@"sPrice"];
        item.priceName = [attributeDict objectForKey:@"priceName"];
        item.orderName = [attributeDict objectForKey:@"orderName"];
        item.orderNo = [attributeDict objectForKey:@"orderNo"];
        item.qtyTar = [attributeDict objectForKey:@"qtyTar"];
        item.retailPrice = [attributeDict objectForKey:@"retailPrice"];
        item.picUrl = [attributeDict objectForKey:@"picUrl"];
        item.brandName = [attributeDict objectForKey:@"brandName"];
        item.brandId = [attributeDict objectForKey:@"brandId"];
        item.tTotalScore = [attributeDict objectForKey:@"tTotalScore"];
        item.surverNum = [attributeDict objectForKey:@"surverNum"];
        item.supplierName = [attributeDict objectForKey:@"supplierName"];
        item.totalFeature = [attributeDict objectForKey:@"totalFeature"];
        item.brandPackstyleName = [attributeDict objectForKey:@"brandPackstyleName"];
        item.mainPackColor = [attributeDict objectForKey:@"mainPackColor"];
        item.imgUrl = [attributeDict objectForKey:@"imgUrl"];
        item.reInfo = [attributeDict objectForKey:@"reInfo"];
        if (item.retailPrice == nil || item.retailPrice.length == 0) {
            item.retailPrice = [attributeDict objectForKey:@"brandPrice"];
        }
        item.tPriceScore = [attributeDict objectForKey:@"tPriceScore"];
        item.tPackageScore = [attributeDict objectForKey:@"tPackageScore"];
        item.tTasteScore = [attributeDict objectForKey:@"tTasteScore"];
        item.userName = [attributeDict objectForKey:@"userName"];
        item.surverTime = [attributeDict objectForKey:@"surverTime"];
        
        if ([attributeDict objectForKey:@"imgUrl"] != nil) {
            item.picUrl = [attributeDict objectForKey:@"imgUrl"];
        }
        if ([item.brandId isEqualToString:@"null"]) {
            item.brandId = @"0";
        }
        
        [self.array addObject:item];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
