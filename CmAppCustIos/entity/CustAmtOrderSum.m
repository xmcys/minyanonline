//
//  CustAmtOrderSum.m
//  CmAppCustIos
//
//  Created by hsit on 14-7-31.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustAmtOrderSum.h"

@implementation CustAmtOrderSum

- (void)parseData:(NSData *)data
{
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    self.tmp = string;
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"amtOrderSum"]) {
        self.amtOrderSum = self.tmp;
    } else if ([elementName isEqualToString:@"balStatus"]){
        self.balStatus = self.tmp;
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
}

@end
