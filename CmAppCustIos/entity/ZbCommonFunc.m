//
//  ZbCommonFunc.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-10-23.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "ZbCommonFunc.h"

@implementation ZbCommonFunc

+ (UIImage *)getImage:(CGSize)size withColor:(UIColor *)color
{
    UIGraphicsBeginImageContextWithOptions(size, 0, [UIScreen mainScreen].scale);
    [color set];
    UIRectFill(CGRectMake(0, 0, size.width, size.height));
    UIImage *pressedColorImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return pressedColorImg;
}

@end
