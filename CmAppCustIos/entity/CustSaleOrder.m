//
//  RYKSaleOrder.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-7-3.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CustSaleOrder.h"

@interface CustSaleOrder () <NSXMLParserDelegate>

@property (nonatomic, strong) void(^block)(NSArray *);
@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation CustSaleOrder

- (id)init
{
    self = [super init];
    if (self) {
        self.brandCnt = @"0";
        self.brandName = @"";
        self.brandUnit = @"";
        self.price = @"0";
        self.leftBrandCnt = @"0";
    }
    return self;
}

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        CustSaleOrder * curOrder = [[CustSaleOrder alloc] init];
        curOrder.orderId = [attributeDict objectForKey:@"orderId"];
        curOrder.orderDate = [attributeDict objectForKey:@"orderDate"];
        curOrder.orderDate = [curOrder.orderDate substringToIndex:curOrder.orderDate.length - 2];
        curOrder.qtyOrderSum = [attributeDict objectForKey:@"qtyOrderSum"];
        curOrder.amtOrderSum = [attributeDict objectForKey:@"amtOderSum"];
        curOrder.contactName = [attributeDict objectForKey:@"contactName"];
        curOrder.brandName = [attributeDict objectForKey:@"brandName"];
        curOrder.brandUnit = [attributeDict objectForKey:@"brandUnit"];
        curOrder.brandCnt = [attributeDict objectForKey:@"qtyOrder"];
        curOrder.price = [attributeDict objectForKey:@"price"];
        
        [self.array addObject:curOrder];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
