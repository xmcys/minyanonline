//
//  CustActivity.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-25.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustActivity.h"

@interface CustActivity () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray * array;

- (NSString *)formatImgPath:(NSString *)path;

@end

@implementation CustActivity

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

- (NSString *)formatImgPath:(NSString *)path
{
    if ([path hasSuffix:@".png"] || [path hasSuffix:@".PNG"]
        || [path hasSuffix:@".jpg"] || [path hasSuffix:@".JPG"]
        || [path hasSuffix:@".jpeg"] || [path hasSuffix:@".JPEG"]) {
        return path;
    } else {
        return @"";
    }
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        CustActivity * act = [[CustActivity alloc] init];
        act.actId = [attributeDict objectForKey:@"actId"];
        act.actTitle = [attributeDict objectForKey:@"actTitle"];
        act.actCont = [attributeDict objectForKey:@"actCont"];
        act.beginTime = [[attributeDict objectForKey:@"beginTime"] stringByReplacingOccurrencesOfString:@"-" withString:@"/"];
        act.endTime = [[attributeDict objectForKey:@"endTime"] stringByReplacingOccurrencesOfString:@"-" withString:@"/"];
        act.actUrl = [attributeDict objectForKey:@"actUrl"];
        act.actPicUrl = [self formatImgPath:[attributeDict objectForKey:@"actPicUrl"]];
        act.actImgUrl = [self formatImgPath:[attributeDict objectForKey:@"actImgUrl"]];
        act.actNum = [attributeDict objectForKey:@"actNum"];
        act.proImg = [self formatImgPath:[attributeDict objectForKey:@"proImg"]];
        act.infoId = [attributeDict objectForKey:@"infoId"];
        act.proType = [attributeDict objectForKey:@"proType"];
        act.appType = [attributeDict objectForKey:@"appType"];
        act.versionNo = [attributeDict objectForKey:@"versionNo"];
        act.url = [attributeDict objectForKey:@"url"];
        if ([act.actNum isEqualToString:@"null"]) {
            act.actNum = @"0";
        }
        if (act.actId == nil) {
            act.actId = @"-1";
        }
        
        // 未获取到版本信息的游戏活动，不显示
        if ([act.appType isEqualToString:@"null"] && [act.actUrl isEqualToString:@"ALL_ACT_LIST"]) {
            return;
        }
        [self.array addObject:act];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
