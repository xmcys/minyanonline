//
//  CustInfo.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-24.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustInfo : NSObject

@property (nonatomic, strong) NSString * userId;           // 用户ID
@property (nonatomic, strong) NSString * userCode;         // 登陆账号（许可证号）
@property (nonatomic, strong) NSString * realName;         // 真实姓名
@property (nonatomic, strong) NSString * sex;              // 性别 1男 2女
@property (nonatomic, strong) NSString * idcardNumber;     // 身份证号
@property (nonatomic, strong) NSString * email;            // 邮箱
@property (nonatomic, strong) NSString * userType;         // 用户类别 1零售户、2vip、3消费者
@property (nonatomic, strong) NSString * userAddr;         // 地址
@property (nonatomic, strong) NSString * custLicenceCode;  // 许可证号
@property (nonatomic, strong) NSString * custName;         // 客户名称
@property (nonatomic, strong) NSString * custLevle;        // 星级
@property (nonatomic, strong) NSString * custLevelName;    // 星级
@property (nonatomic, strong) NSString * mobilphone;       // 电话
@property (nonatomic, strong) NSString * balMode;          // 结算户
@property (nonatomic, strong) NSString * custOrderDate;    // 订货日
@property (nonatomic, strong) NSString * orderStatus;      // 订单状态

@property (nonatomic, strong) NSString * custMgrCode;      // 客户经理代码
@property (nonatomic, strong) NSString * custMgrName;      // 客户经理名称
@property (nonatomic, strong) NSString * saleDeptCode;     // 客户服务部代码
@property (nonatomic, strong) NSString * saleDeptName;     // 客户服务部名称
@property (nonatomic, strong) NSString * mgrMb;            // 客户经理电话
@property (nonatomic, strong) NSString * regiePersonName;  // 专管员姓名
@property (nonatomic, strong) NSString * regieMb;          // 专管员电话
@property (nonatomic, strong) NSString * distPersonName;   // 送货员姓名
@property (nonatomic, strong) NSString * distMb;           // 送货员电话

- (void)parseData:(NSData *)data;
- (BOOL)isLogin;

@end
