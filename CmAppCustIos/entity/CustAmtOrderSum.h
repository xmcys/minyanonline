//
//  CustAmtOrderSum.h
//  CmAppCustIos
//
//  Created by hsit on 14-7-31.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustAmtOrderSum : NSObject <NSXMLParserDelegate>

@property (nonatomic, strong) NSString * amtOrderSum;  //订单金额
@property (nonatomic, strong) NSString * balStatus;    //支付标志， 0未支付 1已支付
@property (nonatomic, strong) NSString * tmp;


- (void)parseData:(NSData *)data;


@end
