//
//  CustSumer.h
//  CmAppCustIos
//
//  Created by hsit on 15-4-2.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustSumer : NSObject

@property (nonatomic, strong) NSString *vipName;
@property (nonatomic, strong) NSString *tel;
@property (nonatomic, strong) NSString *birthday;
@property (nonatomic, strong) NSString *applyindate;
@property (nonatomic, strong) NSString *vipId;
@property (nonatomic, strong) NSString *vipCardNo;
@property (nonatomic, strong) NSString *sex;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) void (^block)(NSArray *);

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;

@end
