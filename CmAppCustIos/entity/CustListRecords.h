//
//  RYKListRecords.h
//  RYKForCustIos
//
//  Created by hsit on 14-7-2.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustListRecords : NSObject<NSXMLParserDelegate>

@property (nonatomic, strong) NSString *brandName;
@property (nonatomic, strong) NSString *brandUnit;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *qtyOrder;
@property (nonatomic, strong) NSString *amt;
@property (nonatomic, strong) NSString *contactName;
@property (nonatomic, strong) NSString *receiveAddr;
@property (nonatomic, strong) NSString *mobilePhone;
@property (nonatomic, strong) NSString *orderDesc;
@property (nonatomic, strong) NSString *orderDate;

@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic ,strong) void(^block)(NSArray *array);

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;





@end
