//
//  ConsSupplier.m
//  CmAppConsumerIos
//
//  Created by 宏超 陈 on 14-5-20.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsSupplier.h"

@interface ConsSupplier () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void(^block)(NSArray * array);

@end

@implementation ConsSupplier

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        ConsSupplier * item = [[ConsSupplier alloc] init];
        item.supplierCode = [attributeDict objectForKey:@"supplierCode"];
        item.supplierName = [attributeDict objectForKey:@"supplierName"];
        item.totalBrand = [attributeDict objectForKey:@"totalBrand"];
        item.userIcon = [attributeDict objectForKey:@"userIcon"];
        item.totalBrandPic = [attributeDict objectForKey:@"totalBrandPic"];
        
        if ([attributeDict objectForKey:@"id"] != nil) {
            item.supplierCode = [attributeDict objectForKey:@"id"];
        }
        
        if ([attributeDict objectForKey:@"brandName"] != nil) {
            item.supplierName = [attributeDict objectForKey:@"brandName"];
        }
        
        if ([attributeDict objectForKey:@"brandDesc"] != nil) {
            item.totalBrand = [attributeDict objectForKey:@"brandDesc"];
        }
        
        if ([attributeDict objectForKey:@"imgUrl"] != nil) {
            item.totalBrandPic = [attributeDict objectForKey:@"imgUrl"];
        }
        
        if ([attributeDict objectForKey:@"supplikerCode"] != nil) {
            item.supplierCode = [attributeDict objectForKey:@"supplikerCode"];
        }
        
        if ([attributeDict objectForKey:@"supplikerName"] != nil) {
            item.supplierName = [attributeDict objectForKey:@"supplikerName"];
        }
        
        if (item.totalBrand != nil) {
            item.totalBrand = [[item.totalBrand stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"] stringByReplacingOccurrencesOfString:@"&lt;br>" withString:@"\n"];
        }
        
        if (item.totalBrandPic != nil && [item.totalBrandPic isEqualToString:@"/"]) {
            item.totalBrandPic = nil;
        }
        
        [self.array addObject:item];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
