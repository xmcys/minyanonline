//
//  CustTatics.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-26.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustTatics.h"

@interface CustTatics () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray * array;

@end

@implementation CustTatics

- (void)parseData:(NSData *)data
{
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        CustTatics * ct = [[CustTatics alloc] init];
        ct.brandName = [attributeDict objectForKey:@"cell_0"];
        ct.level51 = [attributeDict objectForKey:@"cell_1"];
        ct.level52 = [attributeDict objectForKey:@"cell_2"];
        ct.level41 = [attributeDict objectForKey:@"cell_3"];
        ct.level42 = [attributeDict objectForKey:@"cell_4"];
        ct.level3 = [attributeDict objectForKey:@"cell_5"];
        ct.level2 = [attributeDict objectForKey:@"cell_6"];
        ct.level1 = [attributeDict objectForKey:@"cell_7"];
        [self.array addObject:ct];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    if ([self.delegate respondsToSelector:@selector(custTatics:didFinishParsing:)]) {
        [self.delegate custTatics:self didFinishParsing:self.array];
    }
}


@end
