//
//  CustCountFlag.m
//  CmAppCustIos
//
//  Created by hsit on 14-7-30.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustCountFlag.h"

@implementation CustCountFlag

- (void)parseData:(NSData *)data complete:(void (^)(CustCountFlag *))block
{
    self.block = block;
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        self.value = [attributeDict objectForKey:@"value"];
    }
}

-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self);
}



@end
