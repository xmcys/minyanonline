//
//  CustSystemMsg.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CustSystemMsg;

@protocol CustSystemMsgDelegate <NSObject>

@optional
- (void)custSystemMsgDidFinishingParse:(CustSystemMsg *)custSystemMsg;

@end

@interface CustSystemMsg : NSObject

@property (nonatomic, strong) NSString * code;
@property (nonatomic, strong) NSString * msg;
@property (nonatomic, strong) NSString * value;
@property (nonatomic, weak) id<CustSystemMsgDelegate> delegate;

- (void)parseData:(NSData *)data;
- (void)parseData:(NSData *)data complete:(void(^)(CustSystemMsg *msg))block;

@end
