//
//  CustBiDeView.m
//  CmAppCustIos
//
//  Created by hsit on 15-4-13.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustBiDeView.h"

@interface CustBiDeView ()<NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) void(^block) (NSArray *);

@end

@implementation CustBiDeView

- (void)parseData:(NSData *)date complete:(void (^)(NSArray *))block {
    self.array = [[NSMutableArray alloc] init];
    self.block = block;
    NSXMLParser *xml = [[NSXMLParser alloc] init];
    xml.delegate = self;
    [xml parse];
}

#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"item"]) {
        CustBiDeView *bi = [[CustBiDeView alloc] init];
        bi.amtSale = [attributeDict objectForKey:@"amtSale"];
        bi.productName = [attributeDict objectForKey:@"productName"];
        bi.zb = [attributeDict objectForKey:@"zb"];
        [self.array addObject:bi];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
