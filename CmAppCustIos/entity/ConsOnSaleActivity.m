//
//  ConsOnSaleActivity.m
//  CmAppConsumerIos
//
//  Created by 张斌 on 14-6-4.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsOnSaleActivity.h"

@interface ConsOnSaleActivity () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void(^block)(NSArray *array);

- (NSString *)formatImgPath:(NSString *)path;

@end

@implementation ConsOnSaleActivity

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

- (NSString *)formatImgPath:(NSString *)path
{
    if ([path hasSuffix:@".png"] || [path hasSuffix:@".PNG"]
        || [path hasSuffix:@".jpg"] || [path hasSuffix:@".JPG"]
        || [path hasSuffix:@".jpeg"] || [path hasSuffix:@".JPEG"]) {
        return path;
    } else {
        return @"";
    }
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        ConsOnSaleActivity * act = [[ConsOnSaleActivity alloc] init];
        act.actId = [attributeDict objectForKey:@"msgId"];
        act.actTitle = [attributeDict objectForKey:@"msgTitle"];
        act.actCont = [attributeDict objectForKey:@"msgCont"];
        act.publishTime = [[attributeDict objectForKey:@"publishTime"] stringByReplacingOccurrencesOfString:@"-" withString:@"/"];
        act.msgImg01 = [self formatImgPath:[attributeDict objectForKey:@"msgImg01"]];
        act.msgImg02 = [self formatImgPath:[attributeDict objectForKey:@"msgImg02"]];
        act.msgImg03 = [self formatImgPath:[attributeDict objectForKey:@"msgImg03"]];
        act.msgImg04 = [self formatImgPath:[attributeDict objectForKey:@"msgImg04"]];
        act.msgImg05 = [self formatImgPath:[attributeDict objectForKey:@"msgImg05"]];
        
        [self.array addObject:act];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
