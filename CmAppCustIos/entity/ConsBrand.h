//
//  ConsBrand.h
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConsBrand : NSObject

@property (nonatomic, strong) NSString * typeName;             // 类型名称
@property (nonatomic, strong) NSString * typeCode;             // 类型值
@property (nonatomic, strong) NSString * ePrice;               // 最高价
@property (nonatomic, strong) NSString * sPrice;               // 最低价
@property (nonatomic, strong) NSString * priceName;            // 价格区间名称
@property (nonatomic, strong) NSString * orderName;            // 排序方式
@property (nonatomic, strong) NSString * orderNo;              // 排序方式代码
@property (nonatomic, strong) NSString * qtyTar;               // 焦油含量
@property (nonatomic, strong) NSString * retailPrice;          // 零售价
@property (nonatomic, strong) NSString * picUrl;               // 图片
@property (nonatomic, strong) NSString * brandName;            // 卷烟名称
@property (nonatomic, strong) NSString * brandId;              // 卷烟ID
@property (nonatomic, strong) NSString * tTotalScore;          // 评分
@property (nonatomic, strong) NSString * surverNum;            // 评论数
@property (nonatomic, strong) NSString * supplierName;         // 产地
@property (nonatomic, strong) NSString * totalFeature;         // 简介
@property (nonatomic, strong) NSString * brandPackstyleName;   // 包装形式
@property (nonatomic, strong) NSString * mainPackColor;        // 主色调
@property (nonatomic, strong) NSString * imgUrl;               // 图片
@property (nonatomic, strong) NSString * reInfo;               // 推荐活动信息
@property (nonatomic, strong) NSString * tPriceScore;          // 性价比
@property (nonatomic, strong) NSString * tPackageScore;        // 外观
@property (nonatomic, strong) NSString * tTasteScore;          // 口感
@property (nonatomic, strong) NSString * surverTime;           // 口感
@property (nonatomic, strong) NSString * userName;             // 评论姓名


- (void)parseData:(NSData *)data complete:(void(^)(NSArray * array))block;

@end
