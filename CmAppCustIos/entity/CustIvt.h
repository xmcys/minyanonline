//
//  CustIvt.h
//  CmAppCustIos
//
//  Created by hsit on 15-4-13.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustIvt : NSObject

@property (nonatomic, strong) NSString *productName;  //商品名称
@property (nonatomic, strong) NSString *stock;  //库存
@property (nonatomic, strong) NSString *tradePrice;  //批发价
@property (nonatomic, strong) NSString *retailPrice;  //零售价

- (void)parseData:(NSData *)date complete:(void(^)(NSArray *))block;


@end
