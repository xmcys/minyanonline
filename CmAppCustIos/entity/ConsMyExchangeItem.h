//
//  ConsMyExchangeItem.h
//  CmAppConsumerIos
//
//  Created by 张斌 on 14-6-18.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConsMyExchangeItem : NSObject

@property (nonatomic, strong) NSString * changeDate;       // 兑换日期
@property (nonatomic, strong) NSString * giftName;         // 礼品名称
@property (nonatomic, strong) NSString * changePoint;      // 积分
@property (nonatomic, strong) NSString * changeQty;        // 数量
@property (nonatomic, strong) NSString * giftUnit;         // 单位
@property (nonatomic, strong) NSString * giftCode;         // 礼品代码
@property (nonatomic, strong) NSString * supplierCode;     // 提供公司代码
@property (nonatomic, strong) NSString * supplierName;     // 提供公司名

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;

@end
