//
//  CustSystemMsg.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-23.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustSystemMsg.h"

@interface CustSystemMsg () <NSXMLParserDelegate>

@property (nonatomic, strong) NSString * tmp;
@property (nonatomic, strong) void(^block)(CustSystemMsg *msg);

@end

@implementation CustSystemMsg

- (void)parseData:(NSData *)data
{
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

- (void)parseData:(NSData *)data complete:(void(^)(CustSystemMsg *msg))block
{
    self.block = block;
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
    return;
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    self.tmp = string;
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"code"]) {
        self.code = self.tmp;
    } else if ([elementName isEqualToString:@"msg"]){
        self.msg = self.tmp;
    } else if ([elementName isEqualToString:@"value"]){
        self.value = self.tmp;
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    if (self.block != nil) {
        self.block(self);
    }
    if ([self.delegate respondsToSelector:@selector(custSystemMsgDidFinishingParse:)]) {
        [self.delegate custSystemMsgDidFinishingParse:self];
    }
}

@end
