//
//  CustBalance.m
//  CmAppCustIos
//
//  Created by Chenhc on 15/3/19.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustBalance.h"

@interface CustBalance ()<NSXMLParserDelegate>

@property (nonatomic, strong) NSString * tempStr;
@property (nonatomic, strong) CustBalance * tempItem;
@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void (^block)(NSArray *);

@end

@implementation CustBalance

- (void)parseData:(NSData *)xmlData complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:xmlData];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXmlParserDelegate

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"balance"]) {
        self.tempItem.balance = self.tempStr;
    } else if ([elementName isEqualToString:@"bankName"]) {
        self.tempItem.bankName = self.tempStr;
    } else if ([elementName isEqualToString:@"cardNo"]) {
        self.tempItem.sCardNo = self.tempStr;
    } else if ([elementName isEqualToString:@"isDefaultName"]) {
        self.tempItem.isDefaultName = self.tempStr;
    } else if ([elementName isEqualToString:@"lastModifyTime"]) {
        self.tempItem.lastModifyTime = self.tempStr;
    } else if ([elementName isEqualToString:@"BankBalance"]){
        [self.array addObject:self.tempItem];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    self.tempStr = string;
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    if ([elementName isEqualToString:@"item"]) {
        CustBalance * item = [[CustBalance alloc] init];
        item.picUrl = [attributeDict objectForKey:@"picUrl"];
        item.bankName = [attributeDict objectForKey:@"bankName"];
        item.cardNo = [attributeDict objectForKey:@"cardNo"];
        item.sCardNo = [attributeDict objectForKey:@"sCardNo"];
        item.isDefaultName = [attributeDict objectForKey:@"isDefaultName"];
        item.balance = [attributeDict objectForKey:@"balance"];
        item.lastModifyTime = [attributeDict objectForKey:@"lastModifyTime"];
        
        [self.array addObject:item];
    } else if ([elementName isEqualToString:@"BankBalance"]) {
        self.tempItem = [[CustBalance alloc] init];
    }
}

-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}
@end
