//
//  ConsSetting.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-3.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "ConsSetting.h"

@implementation ConsSetting

+ (void)setUserName:(NSString *)userName
{
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    [user setObject:userName forKey:@"USER_NAME"];
    [user synchronize];
}

+ (NSString *)userName
{
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    return [user objectForKey:@"USER_NAME"];
}

+ (void)setPassword:(NSString *)password
{
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    [user setObject:password forKey:@"PASSWORD"];
    [user synchronize];
}

+ (NSString *)password
{
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    return [user objectForKey:@"PASSWORD"];
}

+ (void)setMusicEnable:(BOOL)enable
{
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    if (enable) {
        [user setObject:@"1" forKey:@"MUSIC_ENABLE"];
    } else {
        [user setObject:@"0" forKey:@"MUSIC_ENABLE"];
    }
    [user synchronize];
}

+ (BOOL)musicEnable
{
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    if ([user objectForKey:@"MUSIC_ENABLE"] == nil) {
        return YES;
    } else {
        return [[user objectForKey:@"MUSIC_ENABLE"] boolValue];
    }
}

+ (void)setLastestGuideVersion:(NSString *)version
{
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    [user setObject:version forKey:@"LASTEST_GUIDE_VERSION"];
    [user synchronize];
}

+ (NSString *)lastestGuideVersion
{
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    return [user objectForKey:@"LASTEST_GUIDE_VERSION"];
}

@end
