//
//  CustSumPoints.h
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/24.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustSumPoints : NSObject

@property (nonatomic, strong) NSString * sumPoints; // 总积分

- (void)parseData:(NSData *)data complete:(void(^)(CustSumPoints *))block;

@end
