//
//  CustVersion.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-2-10.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustVersion.h"

@implementation CustVersion

- (void)parseData:(NSData *)data
{
    NSError *error;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
    self.appName = [dict objectForKey:@"appName"];
    self.appType = [dict objectForKey:@"appType"];
    self.versionNo = [dict objectForKey:@"versionNo"];
    self.buildCode = [dict objectForKey:@"buildCode"];
    self.publishDate = [dict objectForKey:@"publishDate"];
    self.content = [dict objectForKey:@"content"];
    self.url = [dict objectForKey:@"url"];
    
    if (![self.versionNo hasPrefix:@"v"] && ![self.versionNo hasPrefix:@"V"]) {
        self.versionNo = [NSString stringWithFormat:@"v%@", self.versionNo];
    }
}

@end
