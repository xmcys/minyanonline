//
//  CustCompanyCode.h
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/24.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustCompanyCode : NSObject

@property (nonatomic, strong) NSString * companyName;    // 公司名
@property (nonatomic, strong) NSString * companyCode;    // 公司代码

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *))block;

@end
