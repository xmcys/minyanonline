//
//  CustPerTatics.h
//  CmAppCustIos
//
//  Created by hsit on 15-3-17.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustPerTatics : NSObject<NSXMLParserDelegate>

@property (nonatomic, strong) NSString *brandName;
@property (nonatomic, strong) NSString *startDate;
@property (nonatomic, strong) NSString *endDate;
@property (nonatomic, strong) NSString *qtyUpperLimit;
@property (nonatomic, strong) void (^block)(NSArray *array);


- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;

@end
