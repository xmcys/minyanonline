//
//  CustInfo.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-24.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustInfo.h"

@interface CustInfo () <NSXMLParserDelegate>

@end

@implementation CustInfo

- (void)parseData:(NSData *)data
{
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

- (BOOL)isLogin
{
    if (self.userId != nil && ![self.userId isEqualToString:@"-1"]) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        self.custLicenceCode = [attributeDict objectForKey:@"custLicenceCode"];
        self.custName = [attributeDict objectForKey:@"custName"];
        self.custLevle = [attributeDict objectForKey:@"custLevle"];
        self.mobilphone = [attributeDict objectForKey:@"mobilphone"];
        self.balMode = [attributeDict objectForKey:@"balMode"];
        self.custOrderDate = [attributeDict objectForKey:@"custOrderDate"];
        self.orderStatus = [attributeDict objectForKey:@"msg"];
        self.userId = [attributeDict objectForKey:@"userId"];
        self.userCode = [attributeDict objectForKey:@"userCode"];
        self.realName = [attributeDict objectForKey:@"realName"];
        self.sex = [attributeDict objectForKey:@"sex"];
        self.idcardNumber = [attributeDict objectForKey:@"idcardNumber"];
        self.email = [attributeDict objectForKey:@"email"];
        self.userType = [attributeDict objectForKey:@"userType"];
        self.userAddr = [attributeDict objectForKey:@"userAddr"];
        self.custMgrCode = [attributeDict objectForKey:@"custMgrCode"];
        self.custMgrName = [attributeDict objectForKey:@"custMgrName"];
        self.saleDeptCode = [attributeDict objectForKey:@"saleDeptCode"];
        self.saleDeptName = [attributeDict objectForKey:@"saleDeptName"];
        if (self.custLevle == nil) {
            self.custLevle = [attributeDict objectForKey:@"custLevel"];
        }
        self.mgrMb = [attributeDict objectForKey:@"mgrMb"];
        self.regiePersonName = [attributeDict objectForKey:@"regiePersonName"];
        self.regieMb = [attributeDict objectForKey:@"regieMb"];
        self.distPersonName = [attributeDict objectForKey:@"distPersonName"];
        self.distMb = [attributeDict objectForKey:@"distMb"];
        
        
        if (self.custLevle == nil || self.custLevle.length == 0) {
            self.custLevle = @"0";
            self.custLevelName = @"暂无";
        } else if ([self.custLevle rangeOfString:@"一"].location != NSNotFound || [self.custLevle isEqualToString:@"1"]){
            self.custLevle = @"1";
            self.custLevelName = @"一星";
        } else if ([self.custLevle rangeOfString:@"二"].location != NSNotFound || [self.custLevle isEqualToString:@"2"]){
            self.custLevle = @"2";
            self.custLevelName = @"二星";
        } else if ([self.custLevle rangeOfString:@"三"].location != NSNotFound || [self.custLevle isEqualToString:@"3"]){
            self.custLevle = @"3";
            self.custLevelName = @"三星";
        } else if ([self.custLevle rangeOfString:@"四"].location != NSNotFound || [self.custLevle isEqualToString:@"4"]){
            self.custLevle = @"4";
            self.custLevelName = @"四星";
        } else if ([self.custLevle rangeOfString:@"五"].location != NSNotFound || [self.custLevle isEqualToString:@"5"]){
            self.custLevle = @"5";
            self.custLevelName = @"五星";
        } else if ([self.custLevle isEqualToString:@"41"]){
            self.custLevle = @"4";
            self.custLevelName = @"四星I";
        } else if ([self.custLevle isEqualToString:@"42"]){
            self.custLevle = @"4";
            self.custLevelName = @"四星II";
        } else if ([self.custLevle isEqualToString:@"51"]){
            self.custLevle = @"5";
            self.custLevelName = @"五星I";
        } else if ([self.custLevle isEqualToString:@"52"]){
            self.custLevle = @"5";
            self.custLevelName = @"五星II";
        }
    }
}

@end
