//
//  RYKCustName.m
//  RYKForCustIos
//
//  Created by hsit on 14-7-2.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CustCustName.h"

@implementation CustCustName

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        CustCustName *item = [[CustCustName alloc] init];
        item.orderCode = [attributeDict objectForKey:@"orderCode"];
        item.operateDate = [attributeDict objectForKey:@"operateDate"];
        item.productName = [attributeDict objectForKey:@"productName"];
        item.currentPrice = [attributeDict objectForKey:@"currentPrice"];
        item.quantity = [attributeDict objectForKey:@"quantity"];
        item.sumMoney = [attributeDict objectForKey:@"sumMoney"];
        item.oNum = [attributeDict objectForKey:@"oNum"];
        item.hj = [attributeDict objectForKey:@"hj"];
        item.num = [attributeDict objectForKey:@"num"];
        [self.array addObject:item];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}




@end
