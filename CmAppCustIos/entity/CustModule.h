//
//  CustModule.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-2-7.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustModule : NSObject

@property (nonatomic, strong) NSString * code;
@property (nonatomic, strong) NSString * name;

-(void)parseData:(NSData *)data complete:(void (^)(NSArray *))block;

+ (void)recordWithModCode:(NSString *)modCode operateId:(NSString *)operateId;

@end
