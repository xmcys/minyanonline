//
//  CustMyPointRecordsDetail.m
//  CmAppCustIos
//
//  Created by 张斌 on 15/3/24.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustMyPointRecordsDetail.h"

@interface CustMyPointRecordsDetail () <NSXMLParserDelegate>

@property (nonatomic, strong) void(^block)(NSArray *);
@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation CustMyPointRecordsDetail

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        CustMyPointRecordsDetail * curRecord = [[CustMyPointRecordsDetail alloc] init];
        curRecord.pointDate = [attributeDict objectForKey:@"pointDate"];
        curRecord.addPoint = [attributeDict objectForKey:@"addPoint"];
        curRecord.pointDesc = [attributeDict objectForKey:@"pointDesc"];
        if (!curRecord.pointDesc || [curRecord.pointDesc isEqualToString:@"null"]) {
            curRecord.pointDesc = @"无";
        }
        
        [self.array addObject:curRecord];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
