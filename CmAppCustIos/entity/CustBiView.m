//
//  CustLiOrder.m
//  CmAppCustIos
//
//  Created by hsit on 15-4-7.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustBiView.h"

@interface CustBiView ()<NSXMLParserDelegate>

@property (nonatomic, strong) void (^block)(CustBiView *);

@end

@implementation CustBiView

- (void)parseData:(NSData *)data complete:(void(^)(CustBiView *biView))block {
    self.block = block;
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"item"]) {
        CustBiView *bi = [[CustBiView alloc] init];
        bi.amtProfit = [attributeDict objectForKey:@"amtProfit"];
        bi.amtSale = [attributeDict objectForKey:@"amtSale"];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self);
}





@end
