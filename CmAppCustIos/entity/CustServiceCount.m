//
//  CustServiceCount.m
//  CmAppCustIos
//
//  Created by hsit on 14-9-3.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustServiceCount.h"

@implementation CustServiceCount

- (void)parseData:(NSData *)data complete:(void (^)(CustServiceCount *))block
{
    self.block = block;
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"]) {
        self.noDeal = [attributeDict objectForKey:@"noDeal"];
        self.inDeal = [attributeDict objectForKey:@"inDeal"];
        self.byEval = [attributeDict objectForKey:@"byEval"];
    }
}

-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self);
}


@end
