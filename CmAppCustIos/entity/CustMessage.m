//
//  CustMessage.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-24.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustMessage.h"

@interface CustMessage () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray * array;

- (NSString *)formatImgPath:(NSString *)path;

@end

@implementation CustMessage

- (void)parseData:(NSData *)data
{
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

- (NSString *)formatImgPath:(NSString *)path
{
    if ([path hasSuffix:@".png"] || [path hasSuffix:@".PNG"]
        || [path hasSuffix:@".jpg"] || [path hasSuffix:@".JPG"]
        || [path hasSuffix:@".jpeg"] || [path hasSuffix:@".JPEG"]) {
        return path;
    } else {
        return @"";
    }
}
#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"list"]) {
        CustMessage * msg = [[CustMessage alloc] init];
        msg.msgId = [attributeDict objectForKey:@"msgId"];
        msg.msgType = [attributeDict objectForKey:@"msgType"];
        msg.msgTitle = [attributeDict objectForKey:@"msgTitle"];
        msg.msgCont = [attributeDict objectForKey:@"msgCont"];
        msg.msgUrl = [attributeDict objectForKey:@"msgUrl"];
        msg.publishTime = [attributeDict objectForKey:@"publishTime"];
        msg.readFlag = [self formatImgPath:[attributeDict objectForKey:@"readFlag"]];
        msg.msgImg01 = [self formatImgPath:[attributeDict objectForKey:@"msgImg01"]];
        msg.msgImg02 = [self formatImgPath:[attributeDict objectForKey:@"msgImg02"]];
        msg.msgImg03 = [self formatImgPath:[attributeDict objectForKey:@"msgImg03"]];
        msg.msgImg04 = [self formatImgPath:[attributeDict objectForKey:@"msgImg04"]];
        msg.msgImg05 = [self formatImgPath:[attributeDict objectForKey:@"msgImg05"]];
        [self.array addObject:msg];
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    if ([self.delegate respondsToSelector:@selector(custMessage:didFinishParsing:)]) {
        [self.delegate custMessage:self didFinishParsing:self.array];
    }
}

@end
