//
//  ConsClassTradeMarkView.h
//  CmAppConsumerIos
//
//  Created by 宏超 陈 on 14-5-20.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ConsClassTradeMarkView;
@class ConsSupplier;

@protocol ConsClassTradeMarkViewDelegate <NSObject>

@optional
- (void)consClassTradeMarkView:(ConsClassTradeMarkView *)tradeMarkView didSelectedWithItem:(ConsSupplier *)supplier;

@end

@interface ConsClassTradeMarkView : UIView

@property (nonatomic, weak) id<ConsClassTradeMarkViewDelegate> delegate;

- (void)loadData;

@end
