//
//  CustSumer.m
//  CmAppCustIos
//
//  Created by hsit on 15-4-2.
//  Copyright (c) 2015年 ICSSHS. All rights reserved.
//

#import "CustSumer.h"

@interface CustSumer ()<NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation CustSumer

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block {
    self.array = [[NSMutableArray alloc] init];
    self.block = block;
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"item"]) {
        CustSumer *sum = [[CustSumer alloc] init];
        sum.vipName = [attributeDict objectForKey:@"vipName"];
        sum.tel = [attributeDict objectForKey:@"tel"];
        sum.applyindate = [attributeDict objectForKey:@"applyindate"];
        sum.vipId = [attributeDict objectForKey:@"vipId"];
        sum.vipCardNo = [attributeDict objectForKey:@"vipCardNo"];
        sum.birthday = [attributeDict objectForKey:@"birthday"];
        sum.sex = [attributeDict objectForKey:@"sex"];
        sum.email = [attributeDict objectForKey:@"email"];
        [self.array addObject:sum];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
