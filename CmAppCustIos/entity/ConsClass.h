//
//  ConsClassFun.h
//  CmAppConsumerIos
//
//  Created by 宏超 陈 on 14-5-20.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, ConsClassShowType)
{
    ClassFun = 0,  // 烟趣
    ClassDistinguish,  // 真伪鉴别
    ClassCustTeach, // 零售户讲堂
    ClassOperateGuide // 经营指导
};

@interface ConsClass : NSObject

@property (nonatomic, strong) NSString * schoolId;
@property (nonatomic, strong) NSString * schoolTitle;
@property (nonatomic, strong) NSString * schoolCont;
@property (nonatomic, strong) NSString * publishTime;
@property (nonatomic, strong) NSString * schoolImg01;
@property (nonatomic, strong) NSString * schoolImg02;
@property (nonatomic, strong) NSString * schoolImg03;
@property (nonatomic, strong) NSString * schoolImg04;
@property (nonatomic, strong) NSString * schoolImg05;
@property (nonatomic, strong) NSString * likeNums;
@property (nonatomic) NSInteger showType;


- (void)parseData:(NSData *)data complete:(void(^)(NSArray *))block;


@end
