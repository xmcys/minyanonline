//
//  ConsSetting.h
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-3.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConsSetting : NSObject

// 登陆名
+ (void)setUserName:(NSString *)userName;
+ (NSString *)userName;
// 密码
+ (void)setPassword:(NSString *)password;
+ (NSString *)password;
// 音效
+ (void)setMusicEnable:(BOOL)enable;
+ (BOOL)musicEnable;
// 设置引导页最后显示版本
+ (void)setLastestGuideVersion:(NSString *)version;
+ (NSString *)lastestGuideVersion;

@end
