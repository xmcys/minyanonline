//
//  CustOrder.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-21.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CustOrder;

@protocol CustOrderDelegate <NSObject>

@optional
- (void)custOrder:(CustOrder *)custOrder didFinishingParse:(NSArray *)array;

@end

@interface CustOrder : NSObject

@property (nonatomic, strong) NSString * orderId;      // 订单ID
@property (nonatomic, strong) NSString * shoppingId;   // 购物车ID
@property (nonatomic, strong) NSString * brandId;      // 卷烟ID
@property (nonatomic, strong) NSString * brandName;    // 卷烟名称
@property (nonatomic, strong) NSString * demand;       // 需求量
@property (nonatomic, strong) NSString * order;        // 订单量
@property (nonatomic, strong) NSString * cash;         // 金额
@property (nonatomic, strong) NSString * orderDate;    // 日期
@property (nonatomic, strong) NSString * tradePrice;   // 批发价
@property (nonatomic, strong) NSString * retailPrice;  // 零售价
@property (nonatomic, strong) NSString * qtyTar;       // 焦油含量
@property (nonatomic, strong) NSString * totalScore;   // 综合评分
@property (nonatomic, strong) NSString * qtyOrderBrands; // 品牌数
@property (nonatomic) int norFlag;                     // 异型烟标记 0否 1是
@property (nonatomic) int newFlag;                     // 新烟标记 0否 1是
@property (nonatomic) int selectedFlag;                // 已选标记 0否 1是
@property (nonatomic, strong) NSString * brandLimit;   // 单品限量
@property (nonatomic, strong) NSString * dayLimit;     // 日限量
@property (nonatomic, strong) NSString * qtyMonthLimit;         // 月限量
@property (nonatomic, strong) NSString * qtyRemainMonthLimit;   // 剩余月限量
@property (nonatomic) float demandSum;   // 需求总量
@property (nonatomic) float orderSum;    // 非异型烟订单总量
@property (nonatomic) float norOrderSum; // 异型烟订单总量
@property (nonatomic) float cashSum;     // 订单总金额
@property (nonatomic) BOOL added;        // 标识是否选中
@property (nonatomic, strong) NSString * code;  // 订货标记
@property (nonatomic, strong) NSString * msg;   // 订货消息
@property (nonatomic, weak) id<CustOrderDelegate> delegate;

- (void)parseData:(NSData *)data;

@end
