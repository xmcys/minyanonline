//
//  CustVersion.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-2-10.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustVersion : NSObject

@property (nonatomic, strong) NSString * appName;
@property (nonatomic, strong) NSString * appType;
@property (nonatomic, strong) NSString * versionNo;
@property (nonatomic, strong) NSString * buildCode;
@property (nonatomic, strong) NSString * publishDate;
@property (nonatomic, strong) NSString * content;
@property (nonatomic, strong) NSString * url;

- (void)parseData:(NSData *)data;


@end
