//
//  CTTabBar.h
//  ChyoTools
//
//  Created by 陈 宏超 on 14-1-20.
//  Copyright (c) 2014年 Chyo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTTabBarItem : UIControl

@property (nonatomic, strong) UIImage * image;
@property (nonatomic, strong) UIImage * selectedImage;
@property (nonatomic) BOOL isSelected;

@end

@class CTTabBar;

@protocol CTTabBarDelegate <NSObject>

@optional
- (void)ctTabBar:(CTTabBar *)ctTabBar didSelectedAtIndex:(NSUInteger)index;

@end

@interface CTTabBar : UIView

@property (nonatomic, weak) id<CTTabBarDelegate> delegate;

- (id)initWithFrame:(CGRect)frame andRootViewController:(UIViewController *)rootViewController;
- (NSArray *)controllers;
- (NSArray *)tabBarItems;
- (UIViewController *)rootViewController;
- (void)addTabBarItem:(CTTabBarItem *)tabBarItem withController:(UIViewController *)controller;

@end

