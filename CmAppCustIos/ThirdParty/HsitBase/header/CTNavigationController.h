//
//  CTNavigationController.h
//  ChyoTools
//
//  XCode5新建项目中，如果将导航控制器的View加入到UIViewController，在IOS6下就会出现位移偏差的问题，最好不要这样加
//
//
//
//
//  Created by 陈 宏超 on 13-12-31.
//  Copyright (c) 2013年 Chyo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTNavigationController;

@protocol CTNavigationDelegate <NSObject>

- (void)ctNavigationDidChangeCurrentController:(CTNavigationController *)navController animateFinished:(BOOL)finised;

@end

@interface CTNavigationController : UINavigationController

@property (nonatomic) BOOL gestureBackEnabled;  // 标识是否启用手势返回
@property (nonatomic, weak) id<CTNavigationDelegate> ctNavDelegate;

// 设置导航栏背景图片 IOS7以下导航栏占用44像素，IOS7以上占用64像素
- (void)setNavigationBarBackgroundImage:(NSString *)imgName44 imgName64:(NSString *)imgName64;

@end
