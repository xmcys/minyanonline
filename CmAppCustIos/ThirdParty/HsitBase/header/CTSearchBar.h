//
//  CTSearchBar.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-21.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTSearchBar;

@protocol CTSearchBarDelegate <NSObject>

@optional
- (void)ctSearchBar:(CTSearchBar *)ctSearchBar didSearchWithContent:(NSString *)content;

@end

@interface CTSearchBar : UIView

@property (nonatomic, strong) UIImageView * inputBoxBg;
@property (nonatomic, strong) UITextField * textField;
@property (nonatomic, weak) id<CTSearchBarDelegate> delegate;

@end
