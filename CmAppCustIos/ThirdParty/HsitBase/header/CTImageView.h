//
//  CTImageView.h   异步加载图片
//  ChyoTools
//
//
//  如果需要停止或开始加载动画请使用方法 startAniamtion 及 stopAnimating
//
//  Created by 陈 宏超 on 14-1-7.
//  Copyright (c) 2014年 Chyo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, CTImageViewState)
{
    CTImageViewStateFailure,
    CTImageViewStateLoadFromUrl,
    CTImageViewStateLoadFromFileCache
};

@class CTImageView;

@protocol CTImageViewDelegate <NSObject>

// 图片加载完毕后，会调用此方法
- (void)ctImageViewLoadDone:(CTImageView *)ctImageView withResult:(CTImageViewState)state;
// 图片点击事件，需启用userInteractionEnabled并且实现本方法才可触发
- (void)ctImageViewDidClicked:(CTImageView *)ctImageView;

@end

@interface CTImageView : UIImageView

@property (nonatomic) BOOL loadingViewEnabled;   // 标识是否启用加载视图（默认启用）
@property (nonatomic) BOOL fileCacheEnabled;     // 设置是否启用文件缓存（默认启用）
@property (nonatomic, readonly) BOOL isLoading;  // 标识是否正在下载
@property (nonatomic, strong) NSString * url;    // 图片下载地址
@property (nonatomic, strong) NSString * specailName;  // 图片唯一名称，如果没设置，默认使用URL当名称
@property (nonatomic, strong) NSString * imgId;  // 图片唯一标识（预留给点击事件使用，作区别）
@property (nonatomic, weak) id<CTImageViewDelegate> delegate;

- (void)loadImage;

@end
