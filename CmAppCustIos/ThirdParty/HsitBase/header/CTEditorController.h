//
//  CTPickerController.h
//  HsitBase
//
//  Created by 宏超 陈 on 14-6-18.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTViewController.h"

typedef  NS_ENUM(NSInteger, CTEditorType)
{
    CTEditorTypeTextField,  // 单行编辑框
    CTEditorTypeTextView,   // 多行编辑框
    CTEditorTypePicker      // 单选器
};

@class CTEditorController;

@protocol CTEditorControllerDelegate <NSObject>

@optional

- (void)ctEditorController:(CTEditorController *)controller editDoneWithText:(NSString *)text;
- (void)ctEditorController:(CTEditorController *)controller didPickedAtIndex:(NSInteger) index withKey:(NSString *)selectedKey andValue:(NSArray *)value;

@end
@interface CTEditorController : CTViewController

@property (nonatomic, strong, readonly) NSArray * keys;    // 键数组（NSString）
@property (nonatomic, strong) NSArray * values;  // 值数组（NSString）
@property (nonatomic, readonly) NSInteger selectedIndex;   // 单选器选中的位置
@property (nonatomic) CTEditorType editorType; // 编辑器类型
@property (nonatomic, strong, readonly) NSString * editText;
@property (nonatomic, weak) id<CTEditorControllerDelegate> delegate; // 委托
@property (nonatomic) BOOL enableSearch;    //是否启用搜索视图 默认不启用   仅单CTEditorTypePicker才能启用

- (void)setKeys:(NSArray *)keys withValues:(NSArray *)values; // 设置单选器键值列表
- (void)setDefaultValue:(NSString *)value; // 如果是单选器，则设置成key，如果是文本则直接填写文本
- (void)setKeyboardType:(UIKeyboardType)keyboardType;


@end
