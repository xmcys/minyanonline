//
//  CTSoapConnection.h
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-22.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTURLConnection.h"

@class CTSoapConnection;

@protocol CTSoapConnectionDelegate <NSObject>

@optional

// 请求结束执行的方法
- (void)soapConnection:(CTSoapConnection *)connection didFinishLoading:(NSData *)data;
// 请求失败时执行的方法
- (void)soapConnection:(CTSoapConnection *)connection didFailWithError:(NSError *)error;

@end

@interface CTSoapConnection : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>

@property (nonatomic, assign) id<CTSoapConnectionDelegate> delegate;
@property (nonatomic, retain) NSString * funcName;
@property (nonatomic, retain) NSString * params;
@property (nonatomic, strong) NSString * soapUrl;
@property (nonatomic, strong) NSString * soapNameSpace;
@property (nonatomic) int tag;

- (id)initWithFunctionName:(NSString *)functionName params:(NSString *)params delegate:(id<CTSoapConnectionDelegate>)delegate;
- (void)start;
- (void)cancel;

@end
