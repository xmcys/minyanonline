//
//  CTCheckBox.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-21.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTCheckBox;

@protocol CTCheckBoxDelegate <NSObject>

- (void)ctCheckBox:(CTCheckBox *)checkBox didValueChanged:(BOOL)checked;

@end

@interface CTCheckBox : UIControl

@property (nonatomic, strong) NSString * text;
@property (nonatomic, strong) UIImage * image;
@property (nonatomic, strong) UIImage * checkedImage;
@property (nonatomic, weak) id<CTCheckBoxDelegate> delegate;
@property (nonatomic, strong) NSString * specialId;
@property (nonatomic, strong) NSString * specialName;

- (void)setChecked:(BOOL)checked;
- (BOOL)checked;
- (void)setTextHidden:(BOOL)hidden;
- (BOOL)textHidden;
- (void)setTextColor:(UIColor *)color;

@end
