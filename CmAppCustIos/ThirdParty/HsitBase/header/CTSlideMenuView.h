//
//  CTSlideMenuView.h
//  HsitBase
//
//  Created by Chenhc on 15/3/19.
//  Copyright (c) 2015年 hsit. All rights reserved.
//

#import <UIKit/UIKit.h>

static const CGFloat kLeftViewInitalOffsetX = -80.0f;
static const CGFloat kOpenStartMinPositionX = 20;

typedef NS_ENUM(NSUInteger, SlideMenuState){
    SlideMenuStateOpen,
    SlideMenuStateClosed,
    SlideMenuStateMove
};

@interface CTSlideMenuView : UIView

@property (nonatomic, strong) UIViewController * leftViewController;
@property (nonatomic, strong) UIViewController * centerViewController;
@property (nonatomic) SlideMenuState slideMenuState;
@property (nonatomic) BOOL gestureMustBeganFromMinPlace;  // 用于打开左侧视图的拖动手势是否需要从最小位置开始（20px处）才生效


- (id)initWithFrame:(CGRect)frame leftViewController:(UIViewController *)leftViewController centerViewController:(UIViewController *)centerViewController;

- (void)setLeftViewOpened:(BOOL)opened;

// 设置点击关闭左侧视图的手势是否可用，默认可用，如果centerviewcontroller是个导航控制器的话，push新的控制器后，tap手势会导致返回按钮无法点击，因此要取消点击手势；
- (void)setTapGestureEnabled:(BOOL)enabled;

// 设置拖动显示隐藏左侧视图的手势是否可用，默认可用，如果centerviewcontroller是个导航控制器的话，push新的控制器后，此手势仍然生效，如果不需要生效，可以设置为NO，并且在返回第一级view的时候，重新设置成YES
- (void)setPanGestureEnabled:(BOOL)enabled;

@end
