//
//  TransitController.h
//  WhatAStation
//
//  Created by chendongliang on 14-5-7.
//  Copyright (c) 2014年 chendongliang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransitController : UIViewController
-(void) didEnb;
-(void) didEnf;
+ (void)setUserId:(NSString *)userId userCode:(NSString *)code userName:(NSString *)name userType:(NSString *)type activityId:(NSString*)aid_ cdnUrl:(NSString*)curl activityName:(NSString*)an versionNo:(NSString *)versionNo;
+(void) hMessageBox:(NSString*)pszMsg title:(NSString*)pszTitle;
+(void) backToApp;
+(void) registeScriptHandler:(NSDictionary *)dict;
+(void)callLuaWithFunctionId:(NSString*)function paramKey:(NSString*)pk andParams:(NSString*)val;
@end

