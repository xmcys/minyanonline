//
//  TransitController.m
//  WhatAStation
//
//  Created by chendongliang on 14-5-7.
//  Copyright (c) 2014年 chendongliang. All rights reserved.
//

#import "TransitController.h"
#include "cocos2d.h"
#include "cocostudio/cocostudio.h"
#import "platform/ios/CCEAGLView-ios.h"
#import "CustWebService.h"
//#import "CocosController.h"

#import "AppDelegateC.h"
#import "CCScriptSupport.h"
#include "HttpClient.h"
#import "scripting/lua-bindings/manual/platform/ios/CCLuaObjcBridge.h"
using namespace cocos2d;

static AppDelegateC* s_pSharedApplication = nullptr;
static NSMutableDictionary* function_handlers;
static int activity_id = 10000001;
static bool is_landscape = false;
static NSString* uid;
static NSString* ucode;
static NSString* utype;
static NSString* aid;
static NSString* cdnurl;
static NSString* aname;
@interface TransitController ()

@end

static TransitController* transitMe;

@implementation TransitController

#pragma mark 初始化用户信息
-(void)initUserInfo{
    
}

+ (void)setUserId:(NSString *)userId userCode:(NSString *)code userName:(NSString *)name userType:(NSString *)type activityId:(NSString*)aid_ cdnUrl:(NSString*)curl activityName:(NSString*)an versionNo:(NSString *)versionNo{
    uid = userId;
    ucode = code;
    utype = type;
    aid = aid_;
    cdnurl = curl;
    aname = an;
    
    // 1横 2竖
    if ([versionNo isEqualToString:@"1"]) {
        is_landscape = true;
    } else {
        is_landscape = false;
    }
}

+(void) registeScriptHandler:(NSDictionary *)dict{
    //    [function_handlers setValue: [[dict objectForKey:@"setUid"] integerValue] forKey:@"setUid"];
    [function_handlers setValue:[dict objectForKey:@"setUid"] forKey:@"setUid"];
    [function_handlers setValue:[dict objectForKey:@"setUcode"] forKey:@"setUcode"];
    [function_handlers setValue:[dict objectForKey:@"setAid"] forKey:@"setAid"];
    [function_handlers setValue:[dict objectForKey:@"setTermType"] forKey:@"setTermType"];
    [function_handlers setValue:[dict objectForKey:@"setServerURL"] forKey:@"setServerURL"];
    [function_handlers setValue:[dict objectForKey:@"setCdnURL"] forKey:@"setCdnURL"];
    [function_handlers setValue:[dict objectForKey:@"setActivityName"] forKey:@"setActivityName"];
    [function_handlers setValue:[dict objectForKey:@"setUsertype"] forKey:@"setUsertype"];
    
    // this should called by you
    //[TransitController setUserId:uid userCode:ucode userName:@"" userType:utype activityId:aid cdnUrl:cdnurl activityName:aname];
    [TransitController callLuaWithFunctionId:@"setUid" paramKey:@"uid" andParams:uid];
    [TransitController callLuaWithFunctionId:@"setAid" paramKey:@"aid" andParams:aid];
    [TransitController callLuaWithFunctionId:@"setUcode" paramKey:@"ucode" andParams:ucode];
    [TransitController callLuaWithFunctionId:@"setTermType" paramKey:@"tt" andParams:@"2"];
    NSString *activityHost = [CustWebService WebServiceActivityHost];
    [TransitController callLuaWithFunctionId:@"setServerURL" paramKey:@"surl" andParams:activityHost];
    [TransitController callLuaWithFunctionId:@"setCdnURL" paramKey:@"curl" andParams:cdnurl];
    [TransitController callLuaWithFunctionId:@"setActivityName" paramKey:@"an" andParams:aname];
    [TransitController callLuaWithFunctionId:@"setUsertype" paramKey:@"utype" andParams:utype];
}

// use string for param for now
+(void)callLuaWithFunctionId:(NSString*)function paramKey:(NSString*)pk andParams:(NSString*)val{
    // 1. 将引用 ID 对应的 Lua function 放入 Lua stack
    int func = [[function_handlers objectForKey:function] intValue];
    cocos2d::LuaObjcBridge::pushLuaFunctionById(func);
    // 2. 将需要传递给 Lua function 的参数放入 Lua stack
    cocos2d::LuaValueDict item;
    item[[pk UTF8String]] = cocos2d::LuaValue::stringValue([val UTF8String]);
    cocos2d::LuaObjcBridge::getStack()->pushLuaValueDict(item);
    // 3. 执行 Lua function
    cocos2d::LuaObjcBridge::getStack()->executeFunction(1);
    // 4. 释放引用 ID
    //    cocos2d::LuaObjcBridge::releaseLuaFunctionById(func);
}

-(void) didEnb{
    if(s_pSharedApplication != nullptr){
        if(cocos2d::Application::getInstance()){
            
            cocos2d::Application::getInstance()->applicationDidEnterBackground();
        }
    }
}

-(void) didEnf{
    if(s_pSharedApplication != nullptr){
        if(cocos2d::Application::getInstance()){
            
            cocos2d::Application::getInstance()->applicationWillEnterForeground();
        }
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    function_handlers = [[NSMutableDictionary alloc] init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnb) name:@"ENB" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnf) name:@"ENF" object:nil];
        // Custom initialization
        // comment it when real enviroment
        [self initUserInfo];
        //NSLog(@"沙盒路径：%@",NSHomeDirectory());
        s_pSharedApplication = new AppDelegateC();
        
        // this should modify by configuration
        s_pSharedApplication->is_landscape = is_landscape;
        
        transitMe = self;
        UIWindow *window =[[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
        
        // game is landscape or portait
        CGRect rect;
        if (s_pSharedApplication->is_landscape) {
            rect = CGRectMake(0, 0, [window bounds].size.height, [window bounds].size.width);
        }else{
            rect = CGRectMake(0, 0, [window bounds].size.width, [window bounds].size.height);
        }
        CCEAGLView *eaglview = [CCEAGLView viewWithFrame:rect
                                             pixelFormat:kEAGLColorFormatRGB565
                                             depthFormat:GL_DEPTH24_STENCIL8_OES
                                      preserveBackbuffer:NO
                                              sharegroup:Nil
                                           multiSampling:NO
                                         numberOfSamples:0];
        [eaglview setMultipleTouchEnabled:YES];
        
        self.view = eaglview;
//        self.wantsFullScreenLayout = YES;
        if ([[UIDevice currentDevice].systemVersion floatValue] <6.0) {
            [window addSubview:self.view];
        }else
        {
            [window setRootViewController:self];
        }
//        [window makeKeyAndVisible];
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        //tscontroller.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        cocos2d::GLView *glview = cocos2d::GLViewImpl::createWithEAGLView(eaglview);
        cocos2d::Director::getInstance()->setOpenGLView(glview);
        cocos2d::Application::getInstance()->run();

    }
    return self;
}

-(void)dealloc{
    // release function id
    NSEnumerator * enumerator = [function_handlers keyEnumerator];
    //定义一个不确定类型的对象
    id object;
    //遍历输出
    while(object = [enumerator nextObject])
    {
        //        NSLog(@"键值为：%@",object);
        id objectValue = [function_handlers objectForKey:object];
        if(objectValue != nil)
        {
            //            NSLog(@"%@所对应的value是 %@",object,objectValue);
            int func = [objectValue intValue];
            cocos2d::LuaObjcBridge::releaseLuaFunctionById(func);
        }
    }
    function_handlers = nil;
    [super dealloc];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
}

- (IBAction)button:(id)sender {

}

-(BOOL)shouldAutorotate{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations{
    if(is_landscape){
        return UIInterfaceOrientationMaskLandscape;
    }else{
        return UIInterfaceOrientationMaskPortrait;
    }
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    BOOL left = NO;
    BOOL right = NO;
    BOOL up = NO;
    BOOL down = NO;
    if (is_landscape) {
        left = YES;
        right = YES;
    }else{
        up = YES;
    }
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft) {
        //zuo
        return left;
    }
    if (interfaceOrientation==UIInterfaceOrientationLandscapeRight) {
        //you
        return right;
    }
    if (interfaceOrientation==UIInterfaceOrientationPortrait) {
        //shang
        return up;
    }
    if (interfaceOrientation==UIInterfaceOrientationPortraitUpsideDown) {
        //xia
        return down;
    }
    return YES;
}

+(void) hMessageBox:(NSString*)pszMsg title:(NSString*)pszTitle{
//    Director::getInstance()->end();
//    CC_SAFE_DELETE(s_pSharedApplication);
    UIAlertView * messageBox = [[UIAlertView alloc] initWithTitle: pszTitle
                                                          message: pszMsg
                                                         delegate: nil
                                                cancelButtonTitle: @"OK"
                                                otherButtonTitles: nil];
    [messageBox show];
}


+(void) backToApp{
    if(transitMe != nil){
        
//        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        cocos2d::network::HttpClient::destroyInstance();
        cocostudio::ActionManagerEx::destroyInstance();
        Director::getInstance()->end();
        
        // delete the script engine can not do here,because thread will still use next scriptengine staff current loop
        // if do not delete the scriptmanager staff,you need re init it next new start
        //    ScriptEngineManager::destroyInstance();

        CC_SAFE_DELETE(s_pSharedApplication);
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ENB" object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ENF" object:nil];
        [transitMe dismissViewControllerAnimated:YES completion:^{
//            transitMe = nil;
        }];
//        exit(0);
    }
}

@end
