#include "AppDelegateC.h"
//#include "HelloWorldScene.h"
//#include "GameLayer.h"
#include "lua_assetsmanager_test_sample.h"
#include "CCLuaEngine.h"
#include "lua_module_register.h"
#include "cocostudio/cocostudio.h"
#include "HttpClient.h"
#include "lua_assetsmanager_test_sample.h"
USING_NS_CC;

bool AppDelegateC::is_landscape = false;

AppDelegateC::AppDelegateC() {
//    is_landscape = true;
}

AppDelegateC::~AppDelegateC(){
    
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegateC::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};
    
    GLView::setGLContextAttrs(glContextAttrs);
}

static bool first_time = true;
bool AppDelegateC::applicationDidFinishLaunching() {
    
    FileUtils::getInstance()->addSearchPath("MyResources");
    restartAppDelegate();
    return true;
}

void AppDelegateC::restartAppDelegate(){
    // initialize director
//    cocos2d::network::HttpClient::destroyInstance();
//    cocostudio::ActionManagerEx::destroyInstance();
    
    auto director = Director::getInstance();
    Director::getInstance()->getTextureCache()->removeAllTextures();
    
    auto glview = director->getOpenGLView();
    
    if(is_landscape){
        glview->setDesignResolutionSize(960,640, ResolutionPolicy::FIXED_HEIGHT);
    }else{
        glview->setDesignResolutionSize(640,960, ResolutionPolicy::FIXED_WIDTH);
    }
    //    glview->setDesignResolutionSize(960,640, ResolutionPolicy::EXACT_FIT);
    // turn on display FPS
    director->setDisplayStats(false);
    
    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);
    
    //#ifdef COCOS2D_DEBUG
    //    if (startRuntime())
    //        return true;
    //#endif
    
    auto engine = LuaEngine::getInstance();
    ScriptEngineManager::getInstance()->setScriptEngine(engine);
    if (!first_time) {
//        engine->reinit();
        ScriptEngineManager::destroyInstance();
        auto engine2 = LuaEngine::getInstance();
        ScriptEngineManager::getInstance()->setScriptEngine(engine2);
        engine = engine2;
    }else{
        first_time = false;
    }
    lua_State* L = engine->getLuaStack()->getLuaState();
    lua_module_register(L);
    //ScriptEngineManager::getInstance()->setScriptEngine(engine);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 || CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID ||CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    LuaStack* stack = engine->getLuaStack();
    register_assetsmanager_test_sample(stack->getLuaState());
#endif
    
    // load loading script and use it to asset manager for version zip
    // if need increase patch,need change assetmanager
    //    engine->executeScriptFile("src/loading.lua");
    engine->executeScriptFile("src/loading.lua");
    //    engine->executeScriptFile("src/helper/loadKaelEx.lua");
}


// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegateC::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegateC::applicationWillEnterForeground() {
    //SoundManager::getInstance()->replayBackgroundMusic();
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}
