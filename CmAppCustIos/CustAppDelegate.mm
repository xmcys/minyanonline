//
//  CustAppDelegate.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-20.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CustAppDelegate.h"
#import "CTURLConnection.h"
#import <ShareSDK/ShareSDK.h>
#import "WXApi.h"
#import "WeiboApi.h"
//#import "WeiboSDK.h"

@implementation CustAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [CTURLConnection setNeedsLog:YES];
    
    if (!VERSION_LESS_THAN_IOS7) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
    [ShareSDK registerApp:@"49023892524c"];//字符串api20为您的ShareSDK的AppKey
    
    //添加新浪微博应用 注册网址 http://open.weibo.com
    [ShareSDK connectSinaWeiboWithAppKey:@"4252933620"
                               appSecret:@"d266d3e7ef59b4d842fe5912ab1c019a"
                             redirectUri:@"https://api.weibo.com/oauth2/default.html"];
//    //当使用新浪微博客户端分享的时候需要按照下面的方法来初始化新浪的平台
//    [ShareSDK  connectSinaWeiboWithAppKey:@"4252933620"
//                                appSecret:@"d266d3e7ef59b4d842fe5912ab1c019a"
//                              redirectUri:@"http://118.244.194.37:8080/app/myzx/home.html"
//                              weiboSDKCls:[WeiboSDK class]];

    //添加腾讯微博应用 注册网址 http://dev.t.qq.com
    [ShareSDK connectTencentWeiboWithAppKey:@"801561753"
                                  appSecret:@"110078596df48e534817e4b177e402f0"
                                redirectUri:@"http://118.244.194.37:8080/app/myzx/home.html"
                                   wbApiCls:[WeiboApi class]];
    //添加微信应用
    [ShareSDK connectWeChatWithAppId:@"wxe6110f3785c4b006"
                           wechatCls:[WXApi class]];
    
    
    [ShareSDK connectSMS];
    [ShareSDK ssoEnabled:NO];
    
//    self.menuController = [[CustMenuController alloc] initWithNibName:@"CustMenuController" bundle:nil];
//    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//    self.window.backgroundColor = [UIColor whiteColor];
//    self.window.rootViewController = self.menuController;
//    [self.window makeKeyAndVisible];
    
    self.signInController = [[CustSignInController alloc] initWithNibName:nil bundle:nil];
    self.signInController.delegate = self;
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = self.signInController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ENB" object:nil];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ENF" object:nil];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
      handleOpenURL:(NSURL *)url
{
    return [ShareSDK handleOpenURL:url
                        wxDelegate:self];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [ShareSDK handleOpenURL:url
                 sourceApplication:sourceApplication
                        annotation:annotation
                        wxDelegate:self];
}

#pragma mark -
#pragma mark CustSignInController Delegate
- (void)custSignInControllerDidSignIn:(CustSignInController *)signInController
{
    CustSlideMenuController * controller = [[CustSlideMenuController alloc] init];
    [self.signInController presentViewController:controller animated:YES completion:nil];
}

@end
