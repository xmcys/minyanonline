// CustomClass.cpp
#include "CustomClass.h"

NS_CC_EXTRA_BEGIN
    CustomClass::CustomClass(){
        
    }
    
    CustomClass::~CustomClass(){
        
    }
    
    bool CustomClass::init(){
        return true;
    }
    
    std::string CustomClass::helloMsg() {
        return "Hello from CustomClass::sayHello";
    }

NS_CC_EXTRA_END