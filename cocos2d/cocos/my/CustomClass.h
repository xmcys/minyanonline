// CustomClass.h

#ifndef __CUSTOM__CLASS

#define __CUSTOM__CLASS

#include "cocos2dx_extra.h"

NS_CC_EXTRA_BEGIN
    class CustomClass : public cocos2d::Ref
    {
    public:
        
        CustomClass();
        
        ~CustomClass();
        
        bool init();
        
        std::string helloMsg();
        
        CREATE_FUNC(CustomClass);
    };
NS_CC_EXTRA_END
#endif // __CUSTOM__CLASS