#include "lua_cocos2dx_custom_auto.hpp"
#include "CustomClass.h"
#include "CCNetwork.h"
#include "tolua_fix.h"
#include "LuaBasicConversions.h"



int lua_cocos2dx_custom_CustomClass_init(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::extra::CustomClass* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccextra.CustomClass",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::extra::CustomClass*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_cocos2dx_custom_CustomClass_init'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_cocos2dx_custom_CustomClass_init'", nullptr);
            return 0;
        }
        bool ret = cobj->init();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccextra.CustomClass:init",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_cocos2dx_custom_CustomClass_init'.",&tolua_err);
#endif

    return 0;
}
int lua_cocos2dx_custom_CustomClass_helloMsg(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::extra::CustomClass* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"ccextra.CustomClass",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (cocos2d::extra::CustomClass*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_cocos2dx_custom_CustomClass_helloMsg'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_cocos2dx_custom_CustomClass_helloMsg'", nullptr);
            return 0;
        }
        std::string ret = cobj->helloMsg();
        tolua_pushcppstring(tolua_S,ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccextra.CustomClass:helloMsg",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_cocos2dx_custom_CustomClass_helloMsg'.",&tolua_err);
#endif

    return 0;
}
int lua_cocos2dx_custom_CustomClass_create(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"ccextra.CustomClass",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_cocos2dx_custom_CustomClass_create'", nullptr);
            return 0;
        }
        cocos2d::extra::CustomClass* ret = cocos2d::extra::CustomClass::create();
        object_to_luaval<cocos2d::extra::CustomClass>(tolua_S, "ccextra.CustomClass",(cocos2d::extra::CustomClass*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "ccextra.CustomClass:create",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_cocos2dx_custom_CustomClass_create'.",&tolua_err);
#endif
    return 0;
}
int lua_cocos2dx_custom_CustomClass_constructor(lua_State* tolua_S)
{
    int argc = 0;
    cocos2d::extra::CustomClass* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_cocos2dx_custom_CustomClass_constructor'", nullptr);
            return 0;
        }
        cobj = new cocos2d::extra::CustomClass();
        cobj->autorelease();
        int ID =  (int)cobj->_ID ;
        int* luaID =  &cobj->_luaID ;
        toluafix_pushusertype_ccobject(tolua_S, ID, luaID, (void*)cobj,"ccextra.CustomClass");
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "ccextra.CustomClass:CustomClass",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_cocos2dx_custom_CustomClass_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_cocos2dx_custom_CustomClass_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (CustomClass)");
    return 0;
}

int lua_register_cocos2dx_custom_CustomClass(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"ccextra.CustomClass");
    tolua_cclass(tolua_S,"CustomClass","ccextra.CustomClass","cc.Ref",nullptr);

    tolua_beginmodule(tolua_S,"CustomClass");
        tolua_function(tolua_S,"new",lua_cocos2dx_custom_CustomClass_constructor);
        tolua_function(tolua_S,"init",lua_cocos2dx_custom_CustomClass_init);
        tolua_function(tolua_S,"helloMsg",lua_cocos2dx_custom_CustomClass_helloMsg);
        tolua_function(tolua_S,"create", lua_cocos2dx_custom_CustomClass_create);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(cocos2d::extra::CustomClass).name();
    g_luaType[typeName] = "ccextra.CustomClass";
    g_typeCast["CustomClass"] = "ccextra.CustomClass";
    return 1;
}

int lua_cocos2dx_custom_CCNetwork_isInternetConnectionAvailable(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"ccextra.CCNetwork",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_cocos2dx_custom_CCNetwork_isInternetConnectionAvailable'", nullptr);
            return 0;
        }
        bool ret = cocos2d::extra::CCNetwork::isInternetConnectionAvailable();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "ccextra.CCNetwork:isInternetConnectionAvailable",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_cocos2dx_custom_CCNetwork_isInternetConnectionAvailable'.",&tolua_err);
#endif
    return 0;
}
int lua_cocos2dx_custom_CCNetwork_isLocalWiFiAvailable(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"ccextra.CCNetwork",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_cocos2dx_custom_CCNetwork_isLocalWiFiAvailable'", nullptr);
            return 0;
        }
        bool ret = cocos2d::extra::CCNetwork::isLocalWiFiAvailable();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "ccextra.CCNetwork:isLocalWiFiAvailable",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_cocos2dx_custom_CCNetwork_isLocalWiFiAvailable'.",&tolua_err);
#endif
    return 0;
}
int lua_cocos2dx_custom_CCNetwork_getInternetConnectionStatus(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"ccextra.CCNetwork",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_cocos2dx_custom_CCNetwork_getInternetConnectionStatus'", nullptr);
            return 0;
        }
        int ret = cocos2d::extra::CCNetwork::getInternetConnectionStatus();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "ccextra.CCNetwork:getInternetConnectionStatus",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_cocos2dx_custom_CCNetwork_getInternetConnectionStatus'.",&tolua_err);
#endif
    return 0;
}
int lua_cocos2dx_custom_CCNetwork_isHostNameReachable(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"ccextra.CCNetwork",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 1)
    {
        const char* arg0;
        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "ccextra.CCNetwork:isHostNameReachable"); arg0 = arg0_tmp.c_str();
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_cocos2dx_custom_CCNetwork_isHostNameReachable'", nullptr);
            return 0;
        }
        bool ret = cocos2d::extra::CCNetwork::isHostNameReachable(arg0);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "ccextra.CCNetwork:isHostNameReachable",argc, 1);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_cocos2dx_custom_CCNetwork_isHostNameReachable'.",&tolua_err);
#endif
    return 0;
}
static int lua_cocos2dx_custom_CCNetwork_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (CCNetwork)");
    return 0;
}

int lua_register_cocos2dx_custom_CCNetwork(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"ccextra.CCNetwork");
    tolua_cclass(tolua_S,"CCNetwork","ccextra.CCNetwork","",nullptr);

    tolua_beginmodule(tolua_S,"CCNetwork");
        tolua_function(tolua_S,"isInternetConnectionAvailable", lua_cocos2dx_custom_CCNetwork_isInternetConnectionAvailable);
        tolua_function(tolua_S,"isLocalWiFiAvailable", lua_cocos2dx_custom_CCNetwork_isLocalWiFiAvailable);
        tolua_function(tolua_S,"getInternetConnectionStatus", lua_cocos2dx_custom_CCNetwork_getInternetConnectionStatus);
        tolua_function(tolua_S,"isHostNameReachable", lua_cocos2dx_custom_CCNetwork_isHostNameReachable);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(cocos2d::extra::CCNetwork).name();
    g_luaType[typeName] = "ccextra.CCNetwork";
    g_typeCast["CCNetwork"] = "ccextra.CCNetwork";
    return 1;
}
TOLUA_API int register_all_cocos2dx_custom(lua_State* tolua_S)
{
	tolua_open(tolua_S);
	
	tolua_module(tolua_S,"ccextra",0);
	tolua_beginmodule(tolua_S,"ccextra");

	lua_register_cocos2dx_custom_CustomClass(tolua_S);
	lua_register_cocos2dx_custom_CCNetwork(tolua_S);

	tolua_endmodule(tolua_S);
	return 1;
}

