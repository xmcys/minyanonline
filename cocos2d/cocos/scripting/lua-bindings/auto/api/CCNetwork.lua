
--------------------------------
-- @module CCNetwork
-- @parent_module ccextra

--------------------------------
--  @brief Checks whether the default route is available 
-- @function [parent=#CCNetwork] isInternetConnectionAvailable 
-- @param self
-- @return bool#bool ret (return value: bool)
        
--------------------------------
--  @brief Checks whether a local wifi connection is available 
-- @function [parent=#CCNetwork] isLocalWiFiAvailable 
-- @param self
-- @return bool#bool ret (return value: bool)
        
--------------------------------
--  @brief Checks Internet connection reachability status 
-- @function [parent=#CCNetwork] getInternetConnectionStatus 
-- @param self
-- @return int#int ret (return value: int)
        
--------------------------------
--  @brief Checks the reachability of a particular host name 
-- @function [parent=#CCNetwork] isHostNameReachable 
-- @param self
-- @param #char hostName
-- @return bool#bool ret (return value: bool)
        
return nil
